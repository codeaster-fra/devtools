# set environment to build the 'stable-updates' version

# It defines some environment variables for installation on official servers
# (ASTER_INSTALLROOT, ASTER_ROOT)
# + PATH for as_run
# All other settings are defined by env.d/<platform-file>_std.sh

export ASTER_BRANCH_ID=v15

# devtools environment
DEVTOOLS_ROOT=$(dirname $(dirname ${BASH_SOURCE}))
. ${DEVTOOLS_ROOT}/etc/env.sh $*
