# DO NOT SOURCE THIS SCRIPT DIRECTLY!
# USE 'env_unstable.sh', 'env_stable-updates.sh' OR OTHER VARIANTS.

# set environment
DEVTOOLS_ROOT=$(dirname $(dirname ${BASH_SOURCE}))
. ${DEVTOOLS_ROOT}/etc/env.sh $*

# Variables set here for further scripts:
#   machine identifier: DEVTOOLS_COMPUTER_ID
#   asrun installation root: ASTER_ROOT
#   installation root: ASTER_INSTALLROOT


# to use if different variants are supported
variant=
if [ $# -ne 0 ]; then
    variant=$1
fi

# cronos
fident=/software/restricted/simumeca/aster/cronos
if [ -z "$DEVTOOLS_COMPUTER_ID" ] && [ -f $fident ]; then
    DEVTOOLS_COMPUTER_ID=cronos
    ASTER_INSTALLROOT=/software/restricted/simumeca/aster
    ASTER_ROOT=${ASTER_INSTALLROOT}
    ASTER_ETC=${ASTER_INSTALLROOT}/etc
    . ${ASTER_ETC}/codeaster/profile.sh
    # To be able to submit a job with Slurm
    export SBATCH_WCKEY=p11yb:aster
    export SLURM_WCKEY=p11yb:aster
fi

# eole
fident=/projets/simumeca/etc/codeaster/eole
if [ -z "$DEVTOOLS_COMPUTER_ID" ] && [ -f $fident ]; then
    DEVTOOLS_COMPUTER_ID=eole
    ASTER_INSTALLROOT=/projets/simumeca
    ASTER_ROOT=${ASTER_INSTALLROOT}
    ASTER_ETC=$ASTER_ROOT/etc
    . $ASTER_ROOT/etc/codeaster/profile.sh
    # To be able to submit a job with Slurm
    export SBATCH_WCKEY=p11yb:aster
    export SLURM_WCKEY=p11yb:aster
fi

# gaia
fident=/projets/simumeca/etc/codeaster/gaia
if [ -z "$DEVTOOLS_COMPUTER_ID" ] && [ -f $fident ]; then
    DEVTOOLS_COMPUTER_ID=gaia
    if [ "$variant" = "gcc" ]; then
        DEVTOOLS_COMPUTER_ID=gaia_gcc
    fi
    ASTER_INSTALLROOT=/projets/simumeca
    ASTER_ROOT=${ASTER_INSTALLROOT}
    ASTER_ETC=$ASTER_ROOT/etc
    . $ASTER_ROOT/etc/codeaster/profile.sh
fi

# scibian9
if [ -z "$DEVTOOLS_COMPUTER_ID" ] && [ `egrep "Scibian 9.0" /etc/issue | wc -l` -eq 1 ]; then
    # use prerequisites installed by user
    fident=$HOME/dev/codeaster-prerequisites/${ASTER_BRANCH_ID}/salome_prerequisites.sh
    if [ "$variant" = "user" ] || [ -z "$DEVTOOLS_COMPUTER_ID" ] && [ -f "$fident" ]; then
        DEVTOOLS_COMPUTER_ID=scibian9_user
        # for 'admin' account
        [ `whoami` = "aster" ] && ASTER_INSTALLROOT=$HOME
        #ASTER_ROOT=
        . $fident
    fi

    if [ -z "$DEVTOOLS_COMPUTER_ID" ]; then
        printf "\n--------------------------------------------------------------\n"
        printf "WARNING: Prerequisites not found for Scibian9!\n"
        printf "    No such directory:\n"
        printf "        $HOME/dev/codeaster-prerequisites/${ASTER_BRANCH_ID}\n\n"
        printf "    You probably need to install them before to continue.\n"
        printf -- "--------------------------------------------------------------\n"
    fi
fi

# --- other linux with prerequisites package installed
# use prerequisites installed by user
fident=$HOME/dev/codeaster-prerequisites/${ASTER_BRANCH_ID}/salome_prerequisites.sh
if [ "$variant" = "linux" ] || [ -z "$DEVTOOLS_COMPUTER_ID" ] && [ -f "$fident" ]; then
    DEVTOOLS_COMPUTER_ID=linux
    #ASTER_ROOT=
    . $fident
fi
# try to use prerequisites from a Salome-Meca installation
fident=${USE_SALOME}/salome_prerequisites.sh
if [ "$variant" = "salome" ] || [ -z "$DEVTOOLS_COMPUTER_ID" ] && [ -f "$fident" ]; then
    DEVTOOLS_COMPUTER_ID=linux
    #ASTER_ROOT=
    . $fident
fi

export DEVTOOLS_COMPUTER_ID
export ASTER_ROOT
export ASTER_INSTALLROOT
