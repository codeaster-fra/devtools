# set environment to build the 'unstable' version + MPI

# All settings are defined by env.d/<platform-file>_mpi.sh

DEVTOOLS_ROOT=$(dirname $(dirname ${BASH_SOURCE}))
. ${DEVTOOLS_ROOT}/etc/env_unstable.sh $*
