#!/bin/bash

# $HOME/dev/codeaster/devtools/share/examples/run_testcases_validation mpi main

set_prefix() {
    local this=$(readlink -n -f "$1")
    devtools=$(dirname $(dirname $(dirname "${this}")))
    DEV=$(dirname $(dirname "${devtools}"))
}

set_prefix "${0}"

if [ $# -eq 1 ] && [ "$1" = "-h" ]; then
    echo "usage: $0 [--now] [BRANCH [VARIANT]]"
    echo "  This script runs the validation testcases using the provided variant (std|mpi)."
    echo "  It is intended to be run by cron. By default, the script really does"
    echo "  the job the first friday of each month."
    echo "  Use the option --now to avoid this restriction."
    echo "  Create the file ${DEV}/run_validation_next_<branch>"
    echo "  to force run at the next call."
    echo "  "
    echo "  BRANCH: is 'main' by default or another branch name."
    exit 1
fi

now=0
if [ $# -ge 1 ] && [ "$1" = "--now" ]; then
    now=1
    shift 1
fi

BR=$1
NAME="unstable"
if [ -z "${BR}" ]; then
    BR=main
fi
if [ "${BR}" != "main" ]; then
    NAME="stable-updates"
fi

VARIANT=$2
if [ -z "${VARIANT}" ]; then
    VARIANT=std
fi
SUFFIX="_${VARIANT}"

ENV=${devtools}/etc/env.sh
. ${ENV}

INSTALLDIR=${ASTER_INSTALLROOT}/install/${NAME}

flog=${DEV}/log/run_validation_${BR}${SUFFIX}.log
printf "\n\n`date`\nstarting run_testcases_validation..." >> ${flog}
dow=$(date +%u)
dom=$(date +%d)

fforce=${DEV}/run_validation_next_${BR}
if [ -f ${fforce} ]; then
    printf " forced run\n" >> ${flog}
    [ "${VARIANT}" = "mpi" ] && rm -f ${fforce}
    now=1
fi

fforce=${DEV}/run_validation_every_friday_${BR}
if [ -f ${fforce} ]; then
    printf " forced every friday\n" >> ${flog}
    dom=0
fi

# printf " dow: ${dow}, dom: ${dom}..." >> ${flog}

# only run on the first friday of the month
if [ $now -eq 1 ] || ( [ $dow -eq 6 ] && [ $dom -lt 8 ] ); then
    rotatelog ${flog}
    printf "\n`date`\nstarting run_testcases_validation...\n" >> ${flog}
    export WAF_SUFFIX="${VARIANT}"
    ${devtools}/bin/run_testcases \
        --vers=${INSTALLDIR}/share/aster \
        --testlist=validation \
        --resutest=${DEV}/results/${BR}/validation \
        --add-sequential --timefactor=1.0 --notify-resutest --clean \
        >> ${flog} 2>&1
else
    printf "stop, this is not the first friday of the month!\n" >> ${flog}
fi
