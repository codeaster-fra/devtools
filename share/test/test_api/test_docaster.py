# coding=utf-8

"""
Tests for the Docaster API.
"""

import unittest

from hamcrest import *
from api_doc import changelog_doc


def test_changelog_from_docaster():
    # this document should not be modified (intranet)
    descr = changelog_doc("A0.03.28")
    assert_that(len(descr), greater_than_or_equal_to(1))
    assert_that("add documents from export" in descr[-1])

    descr = changelog_doc("r5.03.01")
    assert_that(len(descr), greater_than_or_equal_to(5))

    descr = changelog_doc("u5.55.55")
    assert_that(descr, has_length(0))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
