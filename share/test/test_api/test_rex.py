# coding=utf-8

"""
Tests for the REX API.
"""

import base64
import unittest
from urllib.parse import urlencode

from api_roundup import build_histor, get_connection, get_documents, get_expected_issues
from api_roundup.rex import _get_url
from hamcrest import *


def test_get_url():
    """check url function"""
    url0 = "https://readonly:@aster.retd.edf.fr/rex"
    # query = "?" + urlencode({'auth': base64.encodebytes(b"readonly:")})
    query = ""
    assert_that(_get_url(), equal_to(url0 + "/xmlrpc" + query))
    assert_that(_get_url(debug=True), equal_to(url0 + "-test/xmlrpc" + query))
    # query = "?" + urlencode({'auth': base64.encodebytes(b"admin:pass")})
    # assert_that(_get_url('admin', 'pass'), equal_to(url0 + "/xmlrpc" + query))


def test_connection_schema():
    """check the connection and the schema of the tracker"""
    with get_connection() as serv:
        schema = serv.schema()
    assert_that("status", is_in(list(schema.keys())))
    assert_that("issue", is_in(list(schema.keys())))
    assert_that("produit", is_in(list(schema.keys())))
    assert_that("projet", is_in(list(schema.keys())))


def test_basic_request():
    """check a basic request"""
    with get_connection() as serv:
        status = serv.list("status")
    assert_that(len(status), greater_than(3))
    assert_that(status, has_item("emis"))


def test_build_histor():
    """check the build of histor text"""
    issue_list = [10000, 20000]
    histor = build_histor(issue_list, "text")
    assert_that(histor, contains_string("RESTITUTION FICHE 20000"))
    assert_that(histor, contains_string("dans le catalogue phenomene_modelisation"))


def test_expected_issues():
    """check list of expected issues"""
    issues = get_expected_issues("default", _unittest=True)
    n_all = len(issues)
    if issues:
        ids = list(issues.keys())
        issue_test = issues[ids[0]]
        assert_that(issue_test["status"], equal_to("6"))

    issues = get_expected_issues("default", product=("code_aster", "salome_meca"), _unittest=True)
    n_ca = len(issues)
    assert_that(n_all, greater_than_or_equal_to(n_ca))
    if issues:
        ids = list(issues.keys())
        prods = [issues[i]["produit"] for i in ids]
        assert_that(prods, only_contains("code_aster", "salome_meca"))


def test_expected_documents():
    """check for expected documents"""
    with get_connection() as serv:
        docs = get_documents("27165", serv)
        assert_that(docs, contains_inanyorder("u4.44.01", "v3.04.306"))
        docs = get_documents("27239", serv)
        assert_that(docs, empty())


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
