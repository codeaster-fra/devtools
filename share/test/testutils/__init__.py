# coding=utf-8

"""Auxiliary utilities for testing purposes."""


import os
import os.path as osp
import sys
import types
import unittest
from functools import wraps

from aslint.config import ASCFG

CODEASTER_SRC = os.environ.get(
    "CODEASTER_SRC", osp.abspath(osp.join(osp.dirname(ASCFG.get("devtools_root")), "src"))
)

REPO = osp.dirname(osp.dirname(osp.dirname(osp.dirname(osp.abspath(__file__)))))
CODEASTER_SRC = os.environ.get("CODEASTER_SRC", osp.join(osp.dirname(REPO), "src"))


def get_test_suite(module_name):
    """Create test cases for all test functions in the given module."""
    module = sys.modules[module_name]
    functions = [
        obj
        for name, obj in module.__dict__.items()
        if isinstance(obj, types.FunctionType) and name.startswith("test")
    ]
    cases = [
        obj
        for name, obj in module.__dict__.items()
        if isinstance(obj, type) and issubclass(obj, unittest.TestCase)
    ]
    loader = unittest.loader.defaultTestLoader
    cases = [loader.loadTestsFromTestCase(c) for c in cases]
    suite = unittest.TestSuite([unittest.FunctionTestCase(func) for func in functions] + cases)
    return suite


def silent_logger(func):
    """Execute the function with a silent logger."""
    from aslint.logger import ERROR, logger

    @wraps(func)
    def wrapper(*args, **kwds):
        """wrapper"""
        retcode = None
        prev = logger.getEffectiveLevel()
        logger.setLevel(ERROR + 1)
        try:
            retcode = func(*args, **kwds)
        finally:
            logger.setLevel(prev)
        return retcode

    return wrapper


def on_edf_network():
    """Tell if run on EDF network.

    Returns:
        bool: *True* if EDF network is detected, *False* otherwise.
    """
    from subprocess import call

    retcode = call(["wget", "--timeout=2", "-q", "-O", "/dev/null", "https://aster.retd.edf.fr"])
    return retcode == 0


def only_on_repository(repotype):
    """Skip a unittest if not in a repository of the expected type."""
    from repository_api import is_repository

    def _skip_if_not(func):
        """Skip a unittest if not in a Hg repository."""

        @wraps(func)
        def wrapper(*args, **kwds):
            """wrapper"""
            if is_repository(REPO, repotype):
                func(*args, **kwds)

        return wrapper

    return _skip_if_not
