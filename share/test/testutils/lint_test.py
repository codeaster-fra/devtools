# coding=utf-8

"""Convenient utilities for testcases.
"""

import os
import tempfile
from contextlib import contextmanager

from aslint.base_checkers import (CheckList, MsgList, Report, check_disabled,
                                  check_file_content, check_filename)


def check_one_file(fname, checklist=None, checker=None):
    """Check one file.

    Arguments:
        fname (str): Name of the file to be checked.
        checklist (*CheckList*, optional): List of checkers.
        checker (*Msg*, optional): Checker to be used.

    Returns:
        *Report*: Object that reports the errors.
    """
    if not checklist:
        checklist = CheckList([checker])
    lmsg = MsgList()
    lmsg.extend(check_filename(fname, checklist.on_filename()))
    lmsg.extend(check_file_content(fname, checklist.on_content()))
    check_disabled(fname, checklist, lmsg)
    report = Report()
    report.set(fname, lmsg)
    return report


@contextmanager
def check_snippet(text, suffix):
    """Context manager creating a temporary file to check a snippet."""
    tmpf = tempfile.NamedTemporaryFile(suffix=suffix, delete=False)
    tmpf.write(text.encode('utf-8'))
    tmpf.close()
    try:
        yield tmpf.name
    finally:
        os.remove(tmpf.name)
