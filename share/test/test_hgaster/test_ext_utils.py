# coding=utf-8

"""
Tests for ext_utils module.
"""


import os
import os.path as osp
import re
import unittest

from aslint.config import ASCFG
from aslint.logger import logger
from hamcrest import *
from repository_api import (
    get_description,
    get_parent_desc,
    get_reference_head,
    get_repo_by_rev,
    get_repo_name,
    parent_is_last,
    repository_type,
)
from testutils import CODEASTER_SRC, only_on_repository, silent_logger

from hgaster.ext_utils import (
    decode_author,
    get_issues_from_descr,
    get_solved_issues,
    sendmail,
    split_strip,
)


@silent_logger
def test1_sendmail(dry=True):
    from aslint.config import ADMIN_ADDR

    user = split_strip(ASCFG.get("notify.user"), ",")
    addrs = user
    dest = sendmail("user", "unittest sendmail user", text="email test to user", dry_run=dry)
    assert addrs == dest, (addrs, dest)
    dest = sendmail(
        "invalid", "unittest sendmail invalid", text="email test to invalid", dry_run=dry
    )
    assert not dest, dest
    addrs = split_strip(ADMIN_ADDR, ",")
    dest = sendmail("admins", "unittest sendmail admins", text="email test to admins", dry_run=dry)
    assert ADMIN_ADDR in dest, dest
    addrs = user
    dest = sendmail(
        ["user", "admins"],
        "unittest sendmail user and admins",
        text="email test to user and admins",
        dry_run=dry,
    )
    dest.remove(ADMIN_ADDR)
    assert addrs == dest, (addrs, dest)


@unittest.skip("do not spam developers!")
def test2_effective_send():
    """send mails effectively"""
    test1_sendmail(dry=False)


def test3_get_solved_from_text():
    """test message checking"""
    output = os.linesep.join(
        [
            "[#54321] valid message",
            "[#21066,#20963,#20982] adjustment of TOLE_MACHINE",
            "[#20963] other message",
            "[bb0014] bitbucket issue solved",
            "[#20963] fix error introduced by #20962",
        ]
    )
    issues = get_issues_from_descr(output)
    assert_that(issues, contains(54321, 21066, 20963, 20982, 14))
    issues = get_issues_from_descr(output, filter="rex")
    assert_that(issues, contains(54321, 21066, 20963, 20982))
    issues = get_issues_from_descr(output, filter="bitbucket")
    assert_that(issues, contains(14))
    issues = get_issues_from_descr(output, bracket=False)
    assert_that(issues, contains(54321, 21066, 20963, 20982, 14, 20962))


def test3b_get_solved_from_text():
    """real message"""
    text = os.linesep.join(
        [
            "Revision 1364:d1e5b39ff8f1cda83706311e651b5b8dc78651f0",
            "[#21066,#20963,#20982] adjustment of TOLE_MACHINE",
            "[#21039,#20964,#20961,#21066]",
        ]
    )
    issues = get_issues_from_descr(text)
    assert_that(issues, contains(21066, 20963, 20982, 21039, 20964, 20961))


def test3c_check_error():
    """check error in commit message"""
    text = os.linesep.join(["[#21066,20963,#20982] adjustment of TOLE_MACHINE"])
    assert_that(
        calling(get_issues_from_descr).with_args(text, only=True),
        raises(ValueError, "unexpected issue number"),
    )


def test3d_from_doc():
    descr = (
        "Issue27209: on change NOM_ENCASTRE et NOM_INTERFACE "
        "Issue26547: mot-clefs DOMAINE_MAXI, DOMAINE_MINI et "
        "CORR_COMPLET Issue26024: mot-clefs NB_COUCHE_SUPPL et "
        "DOMAINE_INCLUS Modification mineure Presence d'espace "
        "dans des mots Nouvelle commande DEFI_DOMAINE_REDUIT "
        "(issue24771)"
        "Issue 1426 is solved!"
    )
    done = get_issues_from_descr(descr, bracket=False)
    assert_that(done, contains(27209, 26547, 26024, 24771, 1426))


def test4_solved_issues():
    """search for solved issues"""
    try:
        src = get_repo_name(CODEASTER_SRC)
        if not src:
            raise KeyError
    except KeyError:
        print("only works if run from the src repository")
        return

    if repository_type(CODEASTER_SRC) == "git":
        rev1 = "1cb723f14e4f965"
        rev2 = "807c80ea47b141d7"
    else:
        rev1 = "c5b6f4045f0f"
        rev2 = "db5bf487d704"
    ret = get_solved_issues(CODEASTER_SRC, rev1, rev2)
    liss, lrex, lbb = ret
    assert_that(liss, contains_inanyorder(20890, 20893))
    assert_that(liss, contains_inanyorder(*lrex))
    assert not lbb, lbb


@silent_logger
def test5_last():
    """check get_reference_head function"""
    for reponame in ("data", "src", "validation"):
        path = osp.join(CODEASTER_SRC, "..", reponame)
        if not osp.exists(path):
            continue
        for br in ASCFG.get("branches"):
            path = osp.join(CODEASTER_SRC, "..", reponame)
            if not osp.isdir(path):
                continue
            src = [None, "local"]
            node = None
            while src:
                source = src.pop(0)
                try:
                    node = get_reference_head(path, branch=br, source=source)
                    src = None
                except IOError:
                    pass
            assert_that(node, instance_of(str))
            assert_that(node, is_not(""), path + " @" + br + " does probably not exist")
            assert_that(node, has_length(40), path + " @" + br + " %" + str(source))
            logger.info("%-10s %-7s %s", reponame, br, node)

            name = get_repo_by_rev(path)
            assert_that(name, equal_to(reponame))


@only_on_repository("hg")
def test6_parent_desc_hg():
    """print description of parent"""
    desc = get_parent_desc(CODEASTER_SRC)
    assert re.search("^[0-9a-f]{12}:", desc), desc


@only_on_repository("git")
def test6_parent_desc_git():
    """print description of parent"""
    desc = get_parent_desc(CODEASTER_SRC)
    assert re.search("^[0-9a-f]{10} ", desc), desc


def test6b_parent_is_last():
    """check if parent is last"""
    test = parent_is_last(CODEASTER_SRC)
    assert_that(test, instance_of(bool))


def test7_description():
    """print full description of a list of revisions"""
    # revision between 12.1.23 and 12.2.0
    if repository_type(CODEASTER_SRC) == "git":
        rev1 = "62fd0b901528"
        rev2 = "56c8ce4157c6"
    else:
        rev1 = "40344b84f7bb"
        rev2 = "17430917661a"
    desc = get_description(CODEASTER_SRC, rev1, rev2)
    assert len(desc) == 2, desc


@silent_logger
def test8_author():
    """check author decoding"""
    name, email = decode_author("Name <address>")
    assert name == "Name", (name, email)
    assert email == "address", (name, email)
    name, email = decode_author("unexpected field  ")
    assert name == "unexpected field  ", (name, email)
    assert email == "", (name, email)


if __name__ == "__main__":
    import sys

    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
