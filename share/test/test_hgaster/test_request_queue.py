# coding=utf-8

"""
Tests for hooks.
"""

import os
import os.path as osp
import shutil
import tempfile
import unittest

from aslint.config import ASCFG
from aslint.logger import DEBUG, ERROR, WARN, logger
from hamcrest import *
from hgaster.ext_utils import get_rev_log
from hgaster.request_queue import Request, RequestQueue


class TestQueue(unittest.TestCase):

    """Test queue of pull-requests object"""

    def setUp(self):
        """setup a repository for the test"""
        self.rev1 = 'c7d2a29f8fcb'
        self.rev2 = '987654fedcba'
        self.rev3 = '39e80175499b'
        uri = self.uri = tempfile.mkdtemp(prefix='RequestQueue_unittest_')
        self.wrkdir = tempfile.mkdtemp(prefix='RequestQueueClone_')
        self.repodir = osp.join(self.wrkdir, 'requestqueue')
        prev = os.getcwd()
        os.chdir(uri)
        os.system(' && '.join([
            "hg init .",
            "echo 'src default username PEND %s mailA' > queue" % self.rev1,
            "echo 'src default develop1 RUN  %s mail1' >> queue" % self.rev2,
            "hg add queue",
            "hg ci -m 'init test repo'"
        ]))
        os.chdir(prev)
        # for test purpose only
        ASCFG.admin_queue_repoid = get_rev_log(self.uri)
        # logger.setLevel(DEBUG)
        logger.setLevel(WARN)
        logger.info(" ")

    def tearDown(self):
        """cleanup"""
        if logger.getEffectiveLevel() <= DEBUG:
            os.system('echo && cd %s && hg glog && '
                      'hg cat -r tip queue' % self.uri)
            logger.debug("uri: %s", self.uri)
        shutil.rmtree(self.uri)
        shutil.rmtree(self.wrkdir)

    def test02_base_funcs(self):
        """test base functions"""
        queue = RequestQueue(self.uri, repodir=self.repodir).init()
        assert get_rev_log(queue.repodir) == ASCFG.get('admin.queue_repoid')
        queue._update()
        idx = queue.index(('src', 'default', 'username'))
        assert idx == 0, queue.queue
        req = queue.get(('src', 'default', 'username'))
        assert req is not None and req.node.startswith('c7d2a29f8fcb'), req
        assert req is not None and req.status == 'PEND', req
        assert len(queue) == 2, queue.queue
        fake_node = 'abcdef123456'
        req = Request('src', 'v10', 'username', 'PEND', fake_node, '_', 'addr')
        queue.set(len(queue), req)
        assert len(queue) == 3, queue.queue
        idx = queue.index(('src', 'v10', 'username'))
        assert idx == 2, queue.queue
        req = queue.get(('src', 'v10', 'username'))
        assert req is not None and req.node == fake_node, req
        assert req is not None and req.status == 'PEND', req
        prev = logger.getEffectiveLevel()
        logger.setLevel(ERROR)
        iret = queue.remove(('src', 'default', 'develop1'), status='PEND')
        logger.setLevel(prev)
        assert iret == 1, iret
        assert len(queue) == 3, queue.queue
        iret = queue.remove(('src', 'default', 'username'), status='PEND')
        assert iret == 0, iret
        assert len(queue) == 2, queue.queue
        idx = queue.index(('src', 'default', 'username'))
        assert idx == -1, queue.queue
        key = ('src', 'default', 'develop1')
        req = queue.get(key)
        assert req is not None and req.node == self.rev2, req
        assert req.status == 'RUN', req
        iret = queue.modify(key, 'status', 'PEND', 'XXX')
        assert iret == 1, iret
        iret = queue.modify(key, 'status', 'PEND', 'RUN')
        assert iret == 0, iret
        key = ('src', 'default', 'develop1')
        req = queue.get(key)
        assert req is not None and req.node == self.rev2, req
        assert req.status == 'PEND', req

    def test03_usage(self, notify=False):
        """test requests queue usage"""
        queue = RequestQueue(self.uri, notify=notify, repodir=self.repodir)
        queue.init()
        key = ('src', 'default', 'newdev')
        # check there is no other request for this developer
        req = queue.get(key)
        assert req is None, queue.queue
        # submit a revision
        fake_node = 'abcdef123456'
        req = Request('src', 'default', 'newdev', None, fake_node,
                      '_', 'addrdev')
        iret = queue.submit(req)
        assert iret == 0, iret
        req = queue.get(key)
        assert req is not None and req.node == fake_node, req.node
        assert req.status == 'PEND', req.status
        # cancel this revision
        iret = queue.cancel(key)
        assert iret == 0, iret
        # submit a new revision
        req = Request('src', 'default', 'newdev', None, self.rev3,
                      '_', 'addrdev')
        iret = queue.submit(req)
        assert iret == 0, iret
        req = queue.get(key)
        assert req.status == 'PEND', req
        # start it
        iret = queue.start(key)
        assert iret == 0, iret
        # mark as to be tested
        finalrev = 'a1b2c3d4e5f6'
        iret = queue.test(key, finalrev)
        assert iret == 0, iret
        req = queue.get(key)
        assert req is not None, queue.queue
        assert req.node == self.rev3, req
        assert req.finalrev == finalrev, req
        assert req.status == 'TEST', req
        # close as definitively integrated
        iret = queue.close(key)
        assert iret == 0, iret
        req = queue.get(key)
        assert req is None, queue.queue
        # stop revision of develop1
        key = ('src', 'default', 'develop1')
        iret = queue.stop(key)
        assert iret == 0, iret
        # restart it
        iret = queue.start(key)
        assert iret == 0, iret

    @unittest.skip("do not spam developers!")
    def test03_usage_notify(self):
        """test requests queue usage +sendmail"""
        self.test03_usage(notify=True)

    def test04_conflict(self):
        """test conflict during operations on the queue"""
        qu1 = RequestQueue(self.uri, repodir=self.repodir).init()
        qu2 = RequestQueue(self.uri, delay=0.1, repodir=self.repodir).init()
        # submit a revision in queue #1
        req = Request('src', 'default', 'newdev1', None, 'fake_node#1',
                      '_', 'addr1')
        iret = qu1.submit(req)
        assert iret == 0, iret
        # submit a revision in queue #2: "try #2..." should be printed in debug
        req = Request('src', 'default', 'newdev2', None, 'fake_node#2',
                      '_', 'addr2')
        iret = qu2.submit(req)
        assert iret == 0, iret

    def test05_sort(self):
        # test sort of Request, used by iter
        qu1 = RequestQueue(self.uri, repodir=self.repodir).init()
        req1 = Request('src', 'default', 'newdev1', None, 'fake_node#1',
                       '_', 'addr1')
        # add a Request with the same idx to check that 'sorted' does not fail
        qu1.set(0, req1)
        qu1.sorted()
        assert_that(qu1, has_length(3))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
