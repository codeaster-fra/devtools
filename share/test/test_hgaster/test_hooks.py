# coding=utf-8

"""
Tests for hooks.
"""

import os
import os.path as osp
import unittest

from aslint.logger import Abort
from hamcrest import *
from repository_api import has_uncommitted_changes
from testutils import CODEASTER_SRC, only_on_repository, silent_logger


@silent_logger
def test01_msg():
    """test message checking"""
    from hgaster.hooks.generic import OK, check_commit_msg, on_error

    res = check_commit_msg("[#12345] valid message")
    assert_that(res, equal_to(OK))
    res = check_commit_msg("[#12345,#2222] valid message")
    assert_that(res, equal_to(OK))
    res = check_commit_msg("[#12345,2222] valid message")
    assert_that(res, equal_to(on_error()))
    res = check_commit_msg("#12345 invalid closure")
    assert_that(res, equal_to(on_error()))
    res = check_commit_msg("[#12] invalid message")
    assert_that(res, equal_to(on_error()))
    res = check_commit_msg("[#0000] tooshort")
    assert_that(res, equal_to(on_error()))
    res = check_commit_msg("[issue0000] nocomment")
    assert_that(res, equal_to(on_error()))
    res = check_commit_msg("[#9999] to NEW_COMMAND")
    assert_that(res, equal_to(on_error()))
    long_lines = os.linesep.join(["[#9999] to NEW_COMMAND", "", "bla " * 25])
    res = check_commit_msg(long_lines)
    assert_that(res, equal_to(on_error()))


def test02_branch_codeaster():
    from hgaster.hooks.codeaster import repo_branch_checker as checker
    from hgaster.hooks.generic import NOOK, OK

    assert_that(checker("default"), equal_to(OK))
    assert_that(checker("edf/mc"), equal_to(OK))
    assert_that(checker("asterxx/mc"), equal_to(OK))
    assert_that(checker("asterxxmc"), equal_to(NOOK))
    assert_that(checker("v13"), equal_to(OK))
    assert_that(checker("v18"), equal_to(OK))
    assert_that(checker("default_test"), equal_to(NOOK))
    assert_that(checker("issue8888"), equal_to(NOOK))


def test02_branch_check():
    from hgaster.hooks.generic import NOOK, OK, branch_checker

    checker = branch_checker(authorized="^edf/", unauthorized=("^edf/default$", "^edf/build$"))
    assert_that(checker("edf/xxx"), equal_to(OK))
    assert_that(checker("edf/default"), equal_to(NOOK))
    assert_that(checker("edf/build"), equal_to(NOOK))
    assert_that(checker("edf/mybuild"), equal_to(OK))
    assert_that(checker("edf/build_test"), equal_to(OK))
    assert_that(checker("default"), equal_to(NOOK))


def test03_asterstudy():
    from hgaster.hooks.asterstudy import admin_or_contributors

    assert_that(admin_or_contributors("code_aster <code-aster@edf.fr>"), equal_to(True))
    assert_that(admin_or_contributors("vsr@opencascade.com"), equal_to(True))
    assert_that(admin_or_contributors("user@edf.fr"), equal_to(False))


class FakeCset(object):
    """Fake hg changeset containing a minimal set of data for testing."""

    def __init__(self, rev, user="user@edf.fr", branch="default", head=False):
        self.rev = lambda: rev
        self.hex = lambda: str(rev) * 10
        self.user = lambda: user
        self.branch = lambda: branch
        self.phase = lambda: 0
        self.description = lambda: "[#0123] changeset description"
        self._head = head
        self.extra = lambda: dict(close=not head)


class FakeRepo(object):
    """Fake hg repository containing a minimal set of data for testing."""

    def __init__(self, csets):
        self.dcsets = dict([i.rev(), i] for i in csets)
        self.dcsets[-1] = csets[-1]

    def __getitem__(self, key):
        return self.dcsets.get(key, self.dcsets[-1])

    def branchmap(self):
        dbr = dict()
        for rev, cset in list(self.dcsets.items()):
            if rev < 0:
                continue
            dbr.setdefault(cset.branch(), [])
            if cset._head:
                dbr[cset.branch()].append(rev)
        return dbr

    def __len__(self):
        return len(self.dcsets)


@silent_logger
def test04_commit_hook():
    from hgaster.ext_utils import is_admin
    from hgaster.hooks.generic import NOOK, OK, branch_user_commit_hook, branch_user_push_hook

    def brch_chk(branch):
        if branch == "default":
            return OK
        return NOOK

    def priv_chk(user):
        return is_admin(user)

    pretxncommit = branch_user_commit_hook(brch_chk, priv_chk, unittest=True)
    pretxnchangegroup = branch_user_push_hook(brch_chk, priv_chk, unittest=True)

    repo = FakeRepo([FakeCset(0), FakeCset(1)])
    assert_that(pretxncommit(repo), equal_to(OK))
    assert_that(pretxnchangegroup(repo, 0), equal_to(OK))

    repo = FakeRepo(
        [
            FakeCset(0, branch="test"),
        ]
    )
    assert_that(calling(pretxncommit).with_args(repo), raises(Abort))
    assert_that(calling(pretxnchangegroup).with_args(repo, 0), raises(Abort))

    repo = FakeRepo([FakeCset(0, user="mergerobot")])
    assert_that(pretxncommit(repo), equal_to(OK))
    assert_that(pretxnchangegroup(repo, 0), equal_to(OK))


@silent_logger
def test05_devtools_hook():
    from hgaster.hooks.devtools import pretxnchangegroup, pretxncommit
    from hgaster.hooks.generic import OK

    repo = FakeRepo([FakeCset(0, branch="edf/user")])
    assert_that(pretxncommit(repo), equal_to(OK))
    assert_that(pretxnchangegroup(repo, 0), equal_to(OK))

    repo = FakeRepo([FakeCset(0)])
    # branch not checked anymore
    assert_that(pretxncommit(repo), equal_to(OK))
    assert_that(pretxnchangegroup(repo, 0), equal_to(OK))

    repo = FakeRepo([FakeCset(0, user="mergerobot")])
    assert_that(pretxncommit(repo), equal_to(OK))
    assert_that(pretxnchangegroup(repo, 0), equal_to(OK))


@silent_logger
def test05_asterstudy_hook():
    from hgaster.hooks.asterstudy import pretxnchangegroup, pretxncommit
    from hgaster.hooks.generic import OK

    repo = FakeRepo([FakeCset(0, branch="edf/user")])
    assert_that(pretxncommit(repo), equal_to(OK))
    assert_that(pretxnchangegroup(repo, 0), equal_to(OK))

    repo = FakeRepo([FakeCset(0)])
    assert_that(calling(pretxncommit).with_args(repo, unittest=True), raises(Abort))
    assert_that(calling(pretxnchangegroup).with_args(repo, 0, unittest=True), raises(Abort))

    repo = FakeRepo([FakeCset(0, user="mergerobot")])
    assert_that(pretxncommit(repo), equal_to(OK))
    assert_that(pretxnchangegroup(repo, 0), equal_to(OK))


@only_on_repository("hg")
@silent_logger
def test_single_head():
    from repository_api import hg

    from hgaster.hooks import single_head_per_branch

    # In 'devtools' repository, this branch should not be updated :-(
    devtools = osp.dirname(osp.dirname(osp.dirname(osp.dirname(osp.abspath(__file__)))))
    repo = hg.Repository(devtools)
    assert_that(single_head_per_branch(repo, repo["edf/tds"]), equal_to(0))


@silent_logger
def test_precommit():
    from hgaster.hooks.codeaster import aslint
    from hgaster.hooks.generic import OK

    # this test fails if there are changes
    if has_uncommitted_changes(CODEASTER_SRC):
        return

    # call aslint hook on the devtools repository...
    iret = aslint(CODEASTER_SRC)
    assert_that(iret, equal_to(OK))


if __name__ == "__main__":
    import sys

    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
