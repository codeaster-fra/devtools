# coding=utf-8

"""
Tests for mercurial API.
"""

import unittest

from hamcrest import *
from repository_api import hg, hgcmd, is_repository
from repository_api import UI
from repository_api.utils import shell_cmd
from testutils import REPO, only_on_repository


@only_on_repository("hg")
def test_hg():
    repo = hg.Repository(REPO)
    assert_that(repo.root, equal_to(REPO))

    current = repo["tip"]
    assert_that(current.rev(), instance_of(int))
    assert_that(current.hex(), instance_of(str))
    assert_that(str(current), equal_to(current.hex()))
    assert_that(int(current.hex(), 16), instance_of(int))
    assert_that(current.phase(), is_in(["secret", "draft", "public"]))
    assert_that(current.user(), instance_of(str))
    assert_that(current.description(), instance_of(str))
    assert_that(current.branch(), instance_of(str))

    same = repo["tip"]
    assert_that(current == same, equal_to(True))
    assert_that(current != same, equal_to(False))

    parent = current.ancestor(same)
    assert_that(current == parent, equal_to(True))

    assert_that(current.children(), empty())

    assert_that(calling(hg.Repository).with_args("xx"), raises(IOError))

    repo = hg.repository("fake ui", REPO)
    assert_that(repo.root, equal_to(REPO))

    ui = UI.ui()
    # hope 'username' is defined and contains an email address!
    assert_that("@", is_in(ui.config("ui", "username")))
    assert_that(ui.configbool("ui", "unknown_option"), equal_to(False))
    assert_that(calling(ui.configbool).with_args("ui", "username"), raises(ValueError))


if __name__ == "__main__":
    import sys

    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
