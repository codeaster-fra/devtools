# devtools unittests

## Running the testcases

Just run `ctest` in this directory.

For example:

```bash
. env_test.sh
ctest -j 8
```

The prerequisites for the unittests are Python >= 3.6 and the Python Six module.

You may load the code_aster environment that should be required for some unittests.
For example, in code_aster containers, source `/opt/public/xxxx_mpi.sh` before
running `ctest`.

Some unittests check some source files.
This may require an up-to-date ``src`` repository.

## Running isolated

A good test is to execute the unittests in an isolated environment
(isolated from existing repositories, user configuration...).

Example:

```bash
rm -rf /tmp/devtools-tests
singularity shell --home /tmp/devtools-tests \
    --bind $HOME/dev/codeaster/devtools:/scratch/devtools \
    --bind $HOME/dev/codeaster/src:/scratch/src \
    $HOME/containers/salome_meca-edf-2022.1.0-1-20221225-scibian-9.sif

. /opt/public/scibian9_mpi.sh
cd /scratch/devtools/share/test
. env_test.sh
ctest -j 8
```
