
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2023 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------
# person_in_charge: mathieu.courtois@edf.fr

set(COMPONENT_NAME DEVTOOLS)

file(GLOB_RECURSE testlist "test_*.py")

set(runner python3)
if(DEFINED ENV{DEVTOOLS_TEST_RUNNER})
    set(runner "$ENV{DEVTOOLS_TEST_RUNNER}")
endif()

# test_hgaster checks ~/.gitconfig - run first
foreach(tfile ${testlist})
    get_filename_component(base ${tfile} NAME)
    set(TEST_NAME DEVTOOLS_${base})
    add_test(${TEST_NAME} ${runner} ${tfile})
    string(FIND "${TEST_NAME}" "test_hgaster" is_first)
    if(${is_first} GREATER -1)
        set_tests_properties(${TEST_NAME} PROPERTIES LABELS "${COMPONENT_NAME}")
    else()
        set_tests_properties(${TEST_NAME}
            PROPERTIES LABELS "${COMPONENT_NAME}" DEPENDS "DEVTOOLS_test_hgaster.py")
    endif()
endforeach()
