# coding=utf-8
"""
Unittests for the parser of Block and Statement objects
"""

import os
import unittest
from functools import partial

import aslint.fortran.free.fortran_code as FC
from aslint.utils import difftxt
from hamcrest import *

FC_source80 = partial(FC.source, opts={'maxlength': 80})


def test_comment():
    """comment"""
    txt = os.linesep.join([
        "!     BUT : AJOUTER L'OBJET .DIGS A UNE MATR_ASSE",
        "!           ET CALCULER UN EPSILON NUMERIQUE POUR LA FACTORISATION",
    ])
    src = FC_source80(txt)
    assert_that(src.code_text().strip(), equal_to(txt))


def test_end_do():
    """continue with label"""
    src = FC_source80("10 continue")
    cont = src[0]
    assert cont.level == 0, src.dbg()
    txt = cont.code_text().rstrip()
    assert txt == " 10 continue", repr(txt)
    cont.level = 2
    txt = cont.code_text().rstrip()
    assert txt == " 10     continue", repr(txt)


def test_decl():
    """decl"""
    txt = os.linesep.join([
        "    integer :: k, f, desc(*),desctm(*),conexk(*),nbpt,lstpt(*)",
        "    real(kind=8)            :: epsi, sgt(*), coordo(*)",
        "    logical          find, atrv, btrv, fink",
        "    real(kind=8)  :: z0(6,6),z1(6,(nr-ndt)),"
        " z2((nr-ndt),6),z3((nr-ndt),(nr-ndt))", "PetscInt :: count",
        "AsterAddr :: iaddr", "integer , intent(in) :: vari",
        "real (kind=8), intent(out),optional,parameter :: varo=123",
        "type (smumps_struc) , pointer :: smpsk",
        "character (len=16) :: nomcmd = 'Initial string'"
    ])
    src = FC_source80(txt)
    src.code_text()
    assert len(src) == 10, src.blocks
    for stmt in src:
        assert isinstance(stmt, FC.Decl), stmt.dbg()
    assert len(src[2].args) == 4, src[2].args
    assert len(src[3].args) == 4, src[3].args
    assert src[0].attr['type'] == 'integer', src[0].dbg()
    assert src[1].attr['type'] == 'real(kind=8)', src[1].dbg()
    assert src[2].attr['type'] == 'logical', src[2].dbg()
    assert src[3].attr['type'] == 'real(kind=8)', src[3].dbg()
    assert src[4].attr['type'] == 'PetscInt', src[4].dbg()
    assert src[5].attr['type'] == 'AsterAddr', src[5].dbg()
    assert src[6].attr['type'] == 'integer', src[6].dbg()
    assert src[7].attr['type'] == 'real(kind=8)', src[7].dbg()
    assert src[7].attr['kind'] == 8, src[7].dbg()
    assert 'optional' in src[7].attr['attr'], src[7].dbg()
    assert 'parameter' in src[7].attr['attr'], src[7].dbg()
    assert src[7].attr['init']['varo'] == '123', src[7].dbg()
    decl = "real(kind=8), intent(out), optional, parameter :: varo=123"
    assert src[7].code_text().strip() == decl, src[7].code_text()
    lines = src[3].code_text().splitlines()
    assert len(lines) == 2, lines
    assert not lines[0].strip().endswith(','), lines
    assert src[8].attr['type'] == 'type(smumps_struc)', src[8].dbg()
    assert src[8].code_text().strip().startswith('type('), src[8].code_text()
    assert 'pointer' in src[8].attr['attr'], src[8].dbg()
    assert src[9].attr['type'] == 'character(len=16)', src[9].dbg()
    assert src[9].attr['kind'] == 16, src[9].dbg()
    assert 'nomcmd' in src[9].attr['names'], src[9].dbg()


def test_nondecl():
    """non decl"""
    txt = "write(ifm,'(10(A))') 'HUJJID :: NON DEFINI DANS ',&\n" \
        "    'LA MAILLE ',nomail"
    src = FC_source80(txt)
    src.code_text()
    assert len(src.get_type(FC.Decl)) == 0
    assert len(src.get_type(FC.Untyped)) == 1


def test_long_line():
    """parse long call"""
    txt = os.linesep.join([
        "call assert(&",
        " & (option(1:9).eq.'RIGI_MECA' ) &",
        ".or.&",
        " & (option(1:9).eq.'RAPH_MECA' ) &",
        ".or.&",
        " & (option(1:9).eq.'FULL_MECA' ) &",
        ".or.&",
        " & (option(1:9).eq.'XXXX_MECA' )&",
        ")",
    ])

    def _check(code):
        """List of checks of this code"""
        src = FC_source80(code)
        src.code_text()
        calls = src.get_type(FC.Call)
        assert len(calls) == 1, len(calls)
        call = calls[0]
        assert len(call.args) == 1, call.dbg()
        return call.code_text()

    txt = _check(txt)
    assert len(txt.splitlines()) == 3, txt
    txt = _check(txt)
    assert len(txt.splitlines()) == 3, txt


def test_long_args():
    """parse long arguments"""
    txt = os.linesep.join([
        "   call with3(typext.eq.'MIN'.or.&",
        #"&             typext.eq.'MAX'.or.&",
        #"&             typext.eq.'MIN_ABS'.or.&",
        #"&             typext.eq.'MAX_ABS'.or.&",
        "&             typext.eq.'MIN_VAR',&",
        "              arg1, &",
        "              arg2, &",
        "              arg3, &",
        "              arg4, &",
        "&  'line containing 50 characters ******************* &",
        "   &line containing 50 characters ******************* &",
        "   &line containing 50 characters ******************* ',&",
        "              arg6, &",
        "              arg7, &",
        "    )",
    ])

    def _check(code):
        """List of checks of this code"""
        src = FC_source80(code, init_level=0)
        # src.code_text()
        assert len(src) == 1, src.dbg()
        call = src[0]
        assert len(call.args) == 8, call.dbg()
        assert call.args[1] == 'arg1', call.dbg()
        return call.code_text()

    txt = _check(txt)
    assert len(txt.splitlines()) == 5, txt
    txt = _check(txt)
    assert len(txt.splitlines()) == 5, txt


def test_long_space():
    """long line with spaces"""
    txt = os.linesep.join([
        "      call jeveuo(jexnum(modgen//'      .MODG.LIMA',     "
        "         zi(llipr+(i1-1)*9+8)), &",
        "       'L',                                         ibid)",
    ])

    def _check(code):
        """List of checks of this code"""
        src = FC_source80(code, init_level=0)
        src.code_text()
        assert len(src) == 1, src.dbg()
        call = src[0]
        assert len(call.args) == 3, call.dbg()
        assert call.args[1] == "'L'", call.dbg()
        return call.code_text()

    txt = _check(txt)
    assert len(txt.splitlines()) == 2, txt


def test_format():
    """format with multiple ' in string"""
    # from op0178 - known as badly splitted
    src = FC_source80(
        os.linesep.join([
            """form1 = "('_F(NOM=''', A24, ''', VALE_CALC=', " //formr//&""",
            """    ", ',TOLE_MACHINE="//preci(1:lxlgut(preci)) //"),')" """,
            #"              form1 = '(''_F(NOM='''''',A24,'''''',VALE_CALC='','"
            #"                   //FORMR// ','',TOLE_MACHINE='"
            #"                   //PRECI(1:LXLGUT(PRECI))//'),'')'"
        ]),
        init_level=6)
    txt = src.code_text()
    nblines = len(txt.splitlines())
    # print src.dbg()
    # assert nblines <= 2, txt


def test_neutral():
    """neutral statements"""
    txt = os.linesep.join([
        "#if defined(ASTER_HAVE_MPI) && !defined(ASTER_DISABLE_MPI_CHECK)",
        "!",
        "! this is comment with dangerous strings ! :: here",
        "",
        "#endif",
    ])
    src = FC_source80(txt)
    src.code_text()
    assert len(src) == 5, src.dbg()
    assert isinstance(src[0], FC.PreprocIfDef), src[0].dbg()
    assert isinstance(src[1], FC.EmptyLine), src[1].dbg()
    assert isinstance(src[2], FC.Comment), src[2].dbg()
    assert isinstance(src[3], FC.EmptyLine), src[3].dbg()
    assert isinstance(src[4], FC.PreprocEndIf), src[4].dbg()


def test_call_noargs():
    """call without arg"""
    src = FC_source80("call jemarq  ")
    src.code_text()
    assert len(src) == 1, src.blocks
    call = src[0]
    assert isinstance(call, FC.Call), call.dbg()
    assert call.name == "jemarq", call.name
    assert len(call.args) == 0, call.args


def test_call():
    """call with args"""
    src = FC_source80("    call mysub((nr-ndt), (nr-ndt), 0.d0, a, b)")
    src.code_text()
    assert len(src) == 1, src.blocks
    call = src[0]
    assert isinstance(call, FC.Call), call.dbg()
    assert call.name == "mysub", call.name
    assert len(call.args) == 5, call.args


def test_call2():
    """call member"""
    src = FC_source80("    call myobj%mysub((nr-ndt), (nr-ndt), 0.d0, a, b)")
    src.code_text()
    assert len(src) == 1, src.blocks
    call = src[0]
    assert isinstance(call, FC.Call), call.dbg()
    assert call.name == "myobj%mysub", call.name
    assert len(call.args) == 5, call.args


def test_assert():
    """assert"""
    src = FC_source80("    ASSERT ((nr-ndt) .eq. (nr-ndt) .neqv. .true.)")
    src.code_text()
    assert len(src) == 1, src.blocks
    ass = src[0]
    assert isinstance(ass, FC.Assert), ass.dbg()
    assert ".neqv." in ass.cond, ass.dbg()


def test_sub_args():
    """subroutine with args"""
    src = FC_source80(
        os.linesep.join([
            "subroutine my55sub(a, b, c, d, e,   ffff&    ",
            "    &f, g)",
            "end",
        ]))
    src.code_text()
    assert len(src) == 1, src.blocks
    sub = src[0]
    assert len(sub) == 1, sub.blocks
    assert isinstance(sub, FC.Subroutine), sub.dbg()
    assert isinstance(sub[0], FC.EndSub), sub.dbg()
    assert sub.name == "my55sub", sub.dbg()
    assert len(sub.args) == 7, sub.dbg()
    assert sub.args[5] == 'fffff', sub.dbg()


def test_sub_noargs():
    """subroutine without arg"""
    src = FC_source80(
        os.linesep.join([
            "subroutine my55sub",
            "end subroutine my55sub",
        ]))
    src.code_text()
    assert len(src) == 1, src.blocks
    sub = src[0]
    assert len(sub) == 1, sub.blocks
    assert isinstance(sub, FC.Subroutine), sub.dbg()
    assert isinstance(sub[0], FC.EndSub), sub.dbg()
    assert sub.name == "my55sub", sub.dbg()
    assert len(sub.args) == 0, sub.dbg()


def test_sub_wrap():
    """subroutine warp arg"""
    src = FC_source80(
        os.linesep.join([
            "subroutine my55sub&",
            "       (arg1, zmod)",
            "end",
        ]))
    src.code_text()
    assert len(src) == 1, src.blocks
    sub = src[0]
    assert len(sub) == 1, sub.dbg()
    assert isinstance(sub, FC.Subroutine), sub.dbg()
    assert isinstance(sub[0], FC.EndSub), sub.dbg()
    assert sub.name == "my55sub", sub.dbg()
    assert len(sub.args) == 2, sub.dbg()


def test_func_args():
    """subroutine with args"""
    src = FC_source80(
        os.linesep.join([
            "integer function th3fun(a, b, c, d, e,   ffff&    ",
            "    &f, g)",
            "      th3fun = 2",
            "end",
        ]))
    src.code_text()
    assert len(src) == 1, src.blocks
    func = src[0]
    assert len(func) == 2, func.dbg()
    assert isinstance(func, FC.Function), func.dbg()
    assert func.attr['type'] == 'integer', func.dbg()
    assert isinstance(func[0], FC.Assign), func.dbg()
    assert isinstance(func[1], FC.EndSub), func.dbg()
    assert func.name == "th3fun", func.dbg()
    assert len(func.args) == 7, func.dbg()
    assert func.args[5] == 'fffff', func.dbg()


def test_func_noargs():
    """subroutine with args"""
    src = FC_source80(
        os.linesep.join([
            " function th3fun&",
            "       &()",
            " AsterInt :: th3fun",
            "      th3fun = 44",
            "end",
        ]))
    src.code_text()
    assert len(src) == 1, src.blocks
    func = src[0]
    assert len(func) == 3, func.dbg()
    assert isinstance(func, FC.Function), func.dbg()
    assert isinstance(func[0], FC.Decl), func.dbg()
    assert func[0].attr['type'] == 'AsterInt', func.dbg()
    assert isinstance(func[1], FC.Assign), func.dbg()
    assert isinstance(func[2], FC.EndSub), func.dbg()
    assert func.name == "th3fun", func.dbg()
    assert len(func.args) == 0, func.dbg()


def test_recurs_sub():
    """recursive subroutine warp arg"""
    src = FC_source80(
        os.linesep.join([
            "recursive subroutine my55sub&",
            "       (arg1, zmod)",
            "end",
        ]))
    src.code_text()
    assert len(src) == 1, src.blocks
    sub = src[0]
    assert len(sub) == 1, sub.dbg()
    assert isinstance(sub, FC.Subroutine), sub.dbg()
    assert isinstance(sub[0], FC.EndSub), sub.dbg()
    assert sub.name == "my55sub", sub.dbg()
    assert len(sub.args) == 2, sub.dbg()
    assert 'recursive' in sub.attr['qual'], sub.dbg()


def test_module():
    """module"""
    txt = os.linesep.join([
        "module test",
        "    use name1",
        "    use name2, loc1 => orig1, loc2 => orig2",
        "    use name3, only: a, b",
        "    use name4, only: loc_a => a, b",
        "    implicit none",
        "    public",
        "    private :: localvar",
        "contains",
        "    subroutine foo()",
        "        int :: a",
        "    end subroutine",
        "end module",
    ])
    src = FC_source80(txt)
    assert src.code_text().strip() == txt, src.code_text()
    mod = src[0]
    assert isinstance(mod, FC.Module), mod.dbg()
    assert isinstance(mod[0], FC.Use), mod[0].dbg()
    assert mod[0].attr['mod'] == 'name1', mod[0].dbg()
    assert isinstance(mod[1], FC.Use), mod[1].dbg()
    assert mod[1].attr['mod'] == 'name2', mod[1].dbg()
    assert isinstance(mod[1].attr['rename'], dict), mod[1].dbg()
    assert mod[1].attr['rename']['orig1'] == 'loc1', mod[1].dbg()
    assert mod[1].attr['rename']['orig2'] == 'loc2', mod[1].dbg()
    assert isinstance(mod[2], FC.Use), mod[0].dbg()
    assert mod[2].attr['mod'] == 'name3', mod[2].dbg()
    assert isinstance(mod[2].attr['only'], dict), mod[2].dbg()
    assert mod[2].attr['only']['a'] == 'a', mod[2].dbg()
    assert mod[2].attr['only']['b'] == 'b', mod[2].dbg()
    assert isinstance(mod[3], FC.Use), mod[0].dbg()
    assert mod[3].attr['mod'] == 'name4', mod[3].dbg()
    assert isinstance(mod[3].attr['only'], dict), mod[3].dbg()
    assert mod[3].attr['only']['a'] == 'loc_a', mod[3].dbg()
    assert mod[3].attr['only']['b'] == 'b', mod[3].dbg()


def test_module_decl():
    """module"""
    txt = os.linesep.join([
        "module test",
        "    implicit none",
        "    integer :: a",
        "end module",
    ])
    src = FC_source80(txt)
    assert src.code_text().strip() == txt, src.code_text()
    mod = src[0]
    assert isinstance(mod, FC.Module), mod.dbg()
    assert mod.name == "test", mod.dbg()
    assert isinstance(mod[0], FC.Implicit), mod[0].dbg()
    assert isinstance(mod[1], FC.Decl), mod[1].dbg()


def test_interface():
    """interface basic"""
    txt = os.linesep.join([
        "interface",
        "    subroutine mysub()",
        "    end subroutine mysub",
        "end interface",
    ])
    src = FC_source80(txt)
    assert src.code_text().strip() == txt, src.code_text()
    mod = src[0]
    assert isinstance(mod, FC.Interface), mod.dbg()
    assert mod.attr['name'] == "", mod.dbg()


def test_interface_generic():
    """interface basic"""
    txt = os.linesep.join([
        "interface generic_name",
        "    module procedure variant1",
        "    module procedure variant2",
        "end interface",
    ])
    src = FC_source80(txt)
    assert src.code_text().strip() == txt, src.code_text()
    mod = src[0]
    assert isinstance(mod, FC.Interface), mod.dbg()
    assert mod.attr['name'] == "generic_name", mod.dbg()


def test_do_enddo():
    """loop do/end do"""
    src = FC_source80(
        os.linesep.join([
            "    do i=1, nno",
            "       print *, i",
            "    end do",
        ]))
    src.code_text()
    assert len(src) == 1, src.blocks
    loop = src[0]
    assert len(loop) == 2, loop.blocks
    assert isinstance(loop, FC.LoopDo), loop.dbg()
    assert isinstance(loop[0], FC.Untyped), loop.dbg()
    assert isinstance(loop[1], FC.EndDo), loop.dbg()
    assert len(loop.args) == 2, loop.dbg()


def test_do_continue():
    """loop do/continue"""
    src = FC_source80(
        os.linesep.join([
            "    do 10 i=1, nno",
            "                print *, i",
            "100 continue",
            " 10 continue",
            "110 continue",
        ]))
    src.code_text()
    assert len(src) == 2, src.blocks
    loop = src[0]
    assert len(loop) == 3, loop.blocks
    assert isinstance(loop, FC.LoopDo), loop.dbg()
    assert isinstance(src[1], FC.Continue), src.dbg()
    assert isinstance(loop[0], FC.Untyped), loop.dbg()
    assert isinstance(loop[1], FC.Continue), loop.dbg()
    assert isinstance(loop[2], FC.CloseContinue), loop.dbg()
    assert len(loop.args) == 2, loop.dbg()
    assert loop[1].code_text() == "100 continue", loop[1].code_text()


def test_do_while():
    """loop do while"""
    src = FC_source80(
        os.linesep.join([
            "    i = 0",
            "    do while ( i < 10)",
            "           i = i + 1",
            "       print *, i",
            "   end do",
        ]))
    src.code_text()
    assert len(src) == 2, src.dbg()
    assert isinstance(src[0], FC.Assign), src.dbg()
    loop = src[1]
    assert len(loop) == 3, loop.dbg()
    assert isinstance(loop, FC.LoopDoWhile), loop.dbg()
    assert isinstance(loop[0], FC.Assign), loop.dbg()
    assert isinstance(loop[1], FC.Untyped), loop.dbg()
    assert isinstance(loop[2], FC.EndDo), loop.dbg()
    assert loop.cond, loop.dbg()


def test_nested_loop1():
    """nested loops 1"""
    src = FC_source80(
        os.linesep.join([
            "    do 10 i=1, nno", "       do 10 j=i+1, nno",
            "           print *, i, j", " 10 continue",
            "    print *, 'should be at level 0'"
        ]))
    assert len(src) == 2, src.dbg()
    extloop = src[0]
    assert isinstance(extloop, FC.LoopDo), extloop.dbg()
    assert len(extloop.args) == 2, extloop.dbg()
    assert len(extloop) == 1, extloop.dbg()
    intloop = extloop[0]
    assert isinstance(intloop, FC.LoopDo), intloop.dbg()
    assert len(intloop.args) == 2, intloop.dbg()
    assert len(intloop) == 2, intloop.dbg()
    assert isinstance(intloop[0], FC.Untyped), intloop.dbg()
    assert isinstance(intloop[1], FC.CloseContinue), intloop.dbg()
    assert intloop[0].level == 2, intloop.dbg()
    assert intloop[1].level == 2, intloop.dbg()
    assert isinstance(src[1], FC.Untyped), src.dbg()
    assert src[1].level == extloop.level == 0, src.dbg()


def test_nested_loop2():
    """nested loops 2"""
    src = FC_source80(
        os.linesep.join([
            "subroutine zzzz()",
            "    do 10 i=1, nno",
            "       do 11 j=i+1, nno",
            "           print *, i, j",
            "    11 continue",
            " 10 continue",
            "    print *, 'should be at level 1'",
            "end",
        ]))
    refe = os.linesep.join([
        "subroutine zzzz()", "    do 10 i = 1, nno",
        "        do 11 j = i+1, nno", "            print *, i, j",
        " 11     continue", " 10 continue",
        "    print *, 'should be at level 1'", "end", ""
    ])
    assert len(src) == 1, src.dbg()
    sub = src[0]
    assert isinstance(sub, FC.Subroutine), sub.dbg()
    assert sub.level == 0, sub.dbg()
    assert len(sub) == 3, sub.dbg()
    extloop = sub[0]
    assert isinstance(extloop, FC.LoopDo), extloop.dbg()
    assert len(extloop.args) == 2, extloop.dbg()
    assert extloop.level == 1, extloop.dbg()
    assert len(extloop) == 2, extloop.dbg()
    intloop = extloop[0]
    assert isinstance(intloop, FC.LoopDo), intloop.dbg()
    assert len(intloop.args) == 2, intloop.dbg()
    assert intloop.level == 2, intloop.dbg()
    assert len(intloop) == 2, intloop.dbg()
    assert isinstance(intloop[0], FC.Untyped), intloop.dbg()
    assert intloop[0].level == 3, intloop[0].dbg()
    assert isinstance(intloop[1], FC.CloseContinue), intloop.dbg()
    assert intloop[0].level == 3, intloop[1].dbg()
    assert isinstance(extloop[1], FC.CloseContinue), extloop[1].dbg()
    assert extloop[1].level == 2, intloop[1].dbg()
    assert isinstance(sub[1], FC.Untyped), sub[1].dbg()
    assert sub[1].level == 1, sub[1].dbg()
    assert isinstance(sub[2], FC.EndSub), sub[2].dbg()
    assert sub[2].level == 1, sub[2].dbg()
    assert src.code_text() == refe, difftxt(refe, src.code_text())


def test_if():
    """if statements"""
    src = FC_source80(
        os.linesep.join([
            "    if (  a > 1 )goto 100",
            "    print *, i",
            "    if ( b .lt. 2 ) then",
            "       b = b + 1",
            "    endif",
            "    if (c .ne. 3) then",
            "       c = c - 1",
            "    else",
            "       a = b + c",
            "    endif",
            "    if (d .gt. 4) then",
            "       d = d / 4",
            "    else if (e == 5) then   ",
            "       a = b + c",
            "    endif",
        ]))
    src.code_text()
    assert len(src) == 5, src.blocks
    assert isinstance(src[0], FC.IfThen), src[0].dbg()
    assert src[0].cond == "a > 1", src[0].dbg()
    assert src[0].attr['instr'] == "goto 100", src[0].dbg()
    assert isinstance(src[1], FC.Untyped), src[1].dbg()
    ifthen = src[2]
    assert isinstance(ifthen, FC.IfThen), ifthen.dbg()
    assert ifthen.cond == "b .lt. 2", ifthen.dbg()
    assert len(ifthen) == 2, ifthen.blocks
    assert isinstance(ifthen[0], FC.Assign), ifthen[0].dbg()
    assert isinstance(ifthen[1], FC.EndIf), ifthen[1].dbg()
    ifelse = src[3]
    assert isinstance(ifelse, FC.IfThen), ifelse.dbg()
    assert ifelse.cond == "c .ne. 3", ifelse.dbg()
    assert isinstance(ifelse[0], FC.Assign), ifelse[0].dbg()
    assert isinstance(ifelse[1], FC.Else), ifelse[1].dbg()
    assert isinstance(ifelse[2], FC.Assign), ifelse[2].dbg()
    assert isinstance(ifelse[3], FC.EndIf), ifelse[3].dbg()
    ifelif = src[4]
    assert isinstance(ifelif, FC.IfThen), ifelif.dbg()
    assert ifelif.cond == "d .gt. 4", ifelif.dbg()
    assert isinstance(ifelif[0], FC.Assign), ifelif[0].dbg()
    assert isinstance(ifelif[1], FC.ElseIf), ifelif[1].dbg()
    assert isinstance(ifelif[2], FC.Assign), ifelif[2].dbg()
    assert isinstance(ifelif[3], FC.EndIf), ifelif[3].dbg()


def test_if2():
    """if statements"""
    src = FC_source80(os.linesep.join([
        "if (lacma)&",
        " &  call fetacc(2,rang,dimtet,imsmi,imsmk,nbreoa,itps,irg,irr,&",
        " &            ivlagi,nbi,ir1,ir2,ir3,nomggt,lrigid,dimgi,sdfeti,&",
        " &            ipiv,nbsd,zi(ifetf),zi(ifeth),matas,nomgi,lstogi,&",
        " &            infofe,irex,iprj,nbproc)",
        "end",
    ]),
                      init_level=3)
    lines = src.code_text().splitlines()
    assert len(lines) == 7, lines


def test_long_if1():
    """long if 1"""
    txt = "            if ( massi.ne.masse .or. amori.ne.amor .or.&\n" \
            "            raidi.ne.raide ) call u2mess('F','ALGELINE3_9')"
    src = FC_source80(txt, init_level=2)
    lines = src.code_text().splitlines()
    assert len(lines) == 2, lines
    assert lines[1].strip().startswith('call u2mess')


def test_long_if2():
    """long if 2"""
    txt = os.linesep.join([
        "                    if ( (dmin-d).gt.r8prem().or. (abs(dmin-d)&",
        "                    .le.r8prem().and.angle.lt.anglem) ) then",
        "                        dmin=d",
        "                        anglem = angle",
        "                        xlt=ddot(3,vn,1,pm,1)",
        "                    endif",
    ])
    src = FC_source80(txt, init_level=5)
    assert isinstance(src[0], FC.IfThen), src.dbg()
    lines = src.code_text().splitlines()
    assert len(lines) == 7, lines
    assert lines[2].strip().endswith('then'), lines[2]
    assert lines[-1].strip() == 'endif', lines[-1]


def test_long_if3():
    """long if 3"""
    txt = os.linesep.join([
        "    if(abs( coor((numno1-1)*3 + irot(1)) - coor((numno2-1)&",
        "    *3 + irot(1)) ).gt.epsit.or. abs( coor((numno1-1)*3 +&",
        "    irot(2)) - coor((numno2-1)*3 + irot(2)) ).gt.epsit)&",
        "    then",
        "          a = 0",
        "                endif",
    ])
    src = FC_source80(txt, init_level=4)
    assert isinstance(src[0], FC.IfThen), src.dbg()
    lines = src.code_text().splitlines()
    assert len(lines) == 16, lines
    assert lines[13].strip().endswith('then'), lines[13]
    assert lines[-1].strip() == 'endif', lines[-1]


def test_long_if4():
    """long if 4"""
    txt = os.linesep.join([
        "    if ((rang.eq.0).and.((infofe(9:9).eq.'T').or. (infofe(11:11)&",
        "    .eq.'T'))) write(ifm,1060)niter",
    ])
    src = FC_source80(txt, init_level=1)
    assert isinstance(src[0], FC.IfThen), src.dbg()
    lines = src.code_text().splitlines()
    assert len(lines) == 3, lines


def test_long_assign():
    """long assignment"""
    src = FC_source80(os.linesep.join([
        " cosvec = zr(jnorm+3*(ino-1) )*zr(jnorn+3*(numa-1) )&",
        " + zr(jnorm+3*(ino-1)+1)*zr(jnorn+3*(numa-1)+1) + zr(jnorm+&",
        " 3*(ino-1)+2)*zr(jnorn+3*(numa-1)+2)",
    ]),
                      init_level=3)
    lines = src.code_text().splitlines()
    assert len(lines) == 5, lines


def test_parser_error():
    """parser error"""
    src = FC_source80(os.linesep.join(
        ["if (cond.eq.1)", "   continue", "endif"]),
                      init_level=3)
    assert_that(calling(src.code_text), raises(FC.FortranParserError))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
