# coding=utf-8

"""
Tests for the TestResult object.
"""

import unittest

from aslint.i18n import _
from aslint.logger import logger
from aslint.test_result import Diag, TestResult
from hamcrest import *


def test01():
    """check the report"""
    dict_resu = {
        '__global__': {'astest_dir': [],
                       'err_all': 1,
                       'err_noresu': 0,
                       'err_vers': 0,
                       'nbtest': 7,
                       's_astest_dir': '',
                       'version': '12.2.8'},
        'petsc01a': {'diag': '<F>_INVALID_PARAMETER',
                     'tcpu': 0.02,
                     'test': 'petsc01a',
                     'tsys': 0.02,
                     'ttot': 0.04,
                     'vers': '12.2.8'},
        'ssnp15a': {'diag': '<A>_ALARM',
                    'tcpu': 5.84,
                    'test': 'ssnp15a',
                    'tsys': 0.56,
                    'ttot': 6.4,
                    'vers': '12.2.8'},
        'zzzz100f': {'diag': 'OK',
                     'tcpu': 0.71,
                     'test': 'zzzz100f',
                     'tsys': 0.15,
                     'ttot': 0.86,
                     'vers': '12.2.8'},
        'zzzz159a': {'diag': '_',
                     'tcpu': 0.71,
                     'test': 'zzzz159a',
                     'tsys': 0.15,
                     'ttot': 0.86,
                     'vers': '12.2.8'},
        'zzzz159c': {'diag': '<F>_NOT_RUN',
                     'tcpu': 0,
                     'test': 'zzzz159c',
                     'tsys': 0,
                     'ttot': 0,
                     'vers': '?'},
        'zzzz317a': {'diag': 'NOOK_TEST_RESU',
                     'tcpu': 0.71,
                     'test': 'zzzz317a',
                     'tsys': 0.15,
                     'ttot': 0.86,
                     'vers': '12.2.8'},
        'zzzz387a': {'diag': 'NO_TEST_RESU',
                     'tcpu': 0.71,
                     'test': 'zzzz387a',
                     'tsys': 0.15,
                     'ttot': 0.86,
                     'vers': '12.2.8'},
    }
    result = TestResult()
    result.read_diag(dict_resu)
    nbok = result.get_success()
    assert_that(nbok, has_length(2))

    nbok = result.get_success(detail=True)
    assert_that(nbok, has_length(2))

    err = result.get_error(detail=True)
    assert_that(err, has_length(5))

    err = result.get_error()
    assert_that(err, has_length(5))

    err = result.get_result(Diag.Nook)
    assert_that(err, has_length(1))

    err = result.get_result(Diag.NotTested)
    assert_that(err, has_length(1))

    err = result.get_result(Diag.Error | Diag.Nook | Diag.Unknown)
    assert_that(err, has_length(3))

    err = result.get_result(Diag.Success | Diag.Failure)
    assert_that(err, has_length(7))

    # disable printing
    from aslint.logger import ERROR
    previous = logger.getEffectiveLevel()
    logger.setLevel(ERROR + 1)
    nberr = result.report(_("results of the testcases run"))
    if nberr:
        logger.error(_("%6d testcases failed"), nberr)
    assert_that(nberr, equal_to(5))
    logger.setLevel(previous)

    # Consider that NotTested and NotRun is a success
    result.set_success(Diag.Ok | Diag.Warn |
                       Diag.NotTested | Diag.NotRun)
    nbok = result.get_success()
    assert_that(nbok, has_length(4))

    err = result.get_error()
    assert_that(err, has_length(3))


def test_from_text():
    """Test for conversion"""
    value = Diag.use(Diag.Success)
    assert_that(value, equal_to(Diag.Success))

    value = Diag.use("Ok")
    assert_that(value, equal_to(Diag.Ok))

    value = Diag.use("Ok | Warn")
    assert_that(value, equal_to(Diag.Ok | Diag.Warn))

    assert_that(calling(Diag.use).with_args("OK or NOOK"),
                raises(ValueError, "Can not evaluate"))

    assert_that(calling(Diag.use).with_args(0),
                raises(ValueError, "Invalid diagnostic"))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
