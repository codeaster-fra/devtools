# coding=utf-8

"""
Sample codes for unittests
"""

TRIVIAL = """
subroutine loop(a)
    implicit  none
    integer  :: i, a
    i = 0
    do i=1, 10
        i = i + 1
    end do
    call noarg()
end subroutine
"""

SAMPLE = """
subroutine subprg12 ( monoap, typcmo, nbsup, nsupp, neq, nbmode,&
     &                    repmo1, repmo2, amort, modal, id, temps,&
     &                    recmor, recmop, tabs, nomsy, vecmod, reasup,&
     &                    spectr, corfre, muapde, tcosup, nintra, nbdis,&
     &                    f1gup, f2gup)
    implicit  none
    include 'asterc/r8pi.h'
    integer  :: nbsup, nsupp(*), neq, nbmode, id, nintra, tcosup(nbsup,*)
    integer  :: nbdis(nbsup), n
    real(kind=8)             :: vecmod(neq,*), spectr(*), &
        repmo1(nbsup,neq,*), amort(*), &
        repmo2(nbsup,neq,*), &
        reasup(nbsup,nbmode,*)
    real(kind=8)             :: modal(nbmode,*), tabs(nbsup,*)
    real(kind=8)             :: recmor(nbsup,neq,*), recmop(nbsup,neq,*)
    real(kind=8)  :: z0(6,6),z1(6,(nr-ndt)),z2((nr-ndt),6),z3((nr-ndt),(nr-ndt))
    real(kind=8)         :: vecta(9,2,3), vectn(9,3), temps
    logical       locapr
    character(len=*)      :: typcmo
    character(len=16)       :: nomsy
    real(kind=8)  :: realro(deg),ccoef,coef(deg+1)
    logical          find, atrv, btrv, fink

    call jemarq()
    call r8inir(deg,0.0d0,realro,1)

    if (n .eq. 2) then
!     RECHERCHE DES RACINES DU POLYNOME DU SECOND DEGRE
!     Y = A(1) * X**2 + A(2) * X + A(3)
        call draac2(a(1),a(2),a(3), realro(nbroot+1),realro(nbroot+2),&
        nroot)
        nbroot = nbroot+nroot
    elseif (n .eq. 1) then
        nbroot = nbroot + 1
        realro(nbroot) = -a(2)/a(1)
    endif
    call assert(&
        (option(1:9).eq.'RIGI_MECA' ) .or.&
        (option(1:9).eq.'RAPH_MECA' ) .or.&
        (option(1:9).eq.'FULL_MECA' ))
    write(ifm,'(10(A))') 'HUJJID :: LOG(PK/PC) NON DEFINI DANS ',&
        'LA MAILLE ',nomail
    call dvout (logfil, 1, rnorm0, ndigit,&
        '_GETV0: RE-ORTHONALIZATION , RNORM0 IS')
    end subroutine
"""
