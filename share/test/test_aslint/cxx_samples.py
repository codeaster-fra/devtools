# coding=utf-8

INTERFACE_ERROR = """
void exportBucklingModeContainerToPython()
{
    namespace py = boost::python;

    py::class_< BucklingModeContainerInstance, BucklingModeContainerPtr,
            py::bases< FullResultsContainerInstance > > ( "BucklingModeContainer", py::no_init )
        .def( "__init__", py::make_constructor(
            &initFactoryPtr< BucklingModeContainerInstance > ) )
    ;
};
"""

INTERFACE_OK = """
void exportBucklingModeContainerToPython()
{
    namespace py = boost::python;

    py::class_< BucklingModeContainerInstance, BucklingModeContainerPtr,
            py::bases< FullResultsContainerInstance > > ( "BucklingModeContainer", py::no_init )
        .def( "__init__", py::make_constructor(
            &initFactoryPtr< BucklingModeContainerInstance > ) )
        .def( "__init__", py::make_constructor(
            &initFactoryPtr< BucklingModeContainerInstance,
                             std::string > ) )
    ;
};
"""
