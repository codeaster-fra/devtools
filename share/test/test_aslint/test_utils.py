# coding=utf-8

"""
Tests for aslint utilities.
"""

import unittest

from hamcrest import *
from testutils import silent_logger


def test01_get_prefix():
    """test prefix"""
    from aslint.utils import prefix_from_options
    prefix = prefix_from_options(['--prefix=PPP'])
    assert prefix == 'PPP', prefix
    prefix = prefix_from_options(['--prefixX=PPP'])
    assert prefix is None, prefix
    prefix = prefix_from_options(
        ['--prefix=${DIR}', '--install-tests', '--install-i18n '])
    assert prefix == '${DIR}', prefix

@silent_logger
def test02_convert_to_addr():
    """test convert_to_addr"""
    from aslint.utils import convert_to_addr
    addr = convert_to_addr('user at mail.net')
    assert len(addr) == 1 and addr[0] == 'user@mail.net'
    addr = convert_to_addr('user at mail.net, other@elsewhere.com  ')
    assert len(addr) == 2 \
        and addr == ['user@mail.net', 'other@elsewhere.com'], addr
    addr = convert_to_addr('')
    assert len(addr) == 0, addr


def test01_accent():
    """check remove_accent function"""
    from aslint.string_utils import remove_accent
    assert_that(remove_accent('é'), equal_to('e'))

def test02_levenshtein():
    """check levenshtein"""
    from aslint.string_utils import levenshtein, relative_levenshtein
    assert_that(levenshtein("abc", "abxc"), equal_to(1))
    assert_that(levenshtein("é", "e"), equal_to(1))
    assert_that(relative_levenshtein("abc", "abxc"), equal_to(1./3.5))
    assert_that(relative_levenshtein("éé", "ee"), equal_to(0.))

def test03_search():
    """check search similar"""
    from aslint.string_utils import relative_levenshtein, search_similar
    searched = "hello world"
    values = ["bonjour", "hi", "hello the world", "hell"]
    assert_that(search_similar(searched, values, 0.), none())
    dist = relative_levenshtein(searched, "hello the world")
    assert_that(dist, equal_to(4./13.))
    assert_that(search_similar(searched, values), "hello the world")
    assert_that(search_similar("not found", values), "bonjour")

def test04_edit():
    from aslint.utils import open_editor
    text = "initial"
    res = open_editor(text, editor=["cat"])
    assert_that(res, contains_string("initial"))
    res = open_editor(text, editor=["rm"])
    assert_that(res, empty())
    res = open_editor(text, editor=["_invalid_command_"])
    assert_that(res, empty())


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
