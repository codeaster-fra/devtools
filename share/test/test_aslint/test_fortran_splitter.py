# coding=utf-8

"""
Unittests for Splitter objects
"""

import unittest
from functools import partial

import aslint.fortran.free.code_func as CF


def test00_base():
    """space separated"""
    spl = CF.SpaceSplitter("a b cdef 'g 5gg' h    jk l")
    assert spl.size() == 7, spl.values
    tst = [val[1] for val in spl]
    assert len(tst) == 7, tst
    assert spl.values == tst, spl.values

def test01_argsplit1():
    """arguments"""
    spl = CF.ArgSplitter(" z0(6,6),z1(6,(nr-ndt)), z2((nr-ndt),6),"
                         "z3((nr-ndt),  (nr-ndt))")
    assert spl.size() == 4, spl.values
    lines = spl.alloc(59, cont=False)
    assert len(lines) == 2, lines
    assert not lines[0].strip().endswith(','), lines

def test01_argsplit2():
    """more arguments"""
    sarg = "(option(1:9).eq.'RIGI_MECA' ) .or. " \
            "(option(1:9).eq.'RAPH_MECA' ) .or. " \
            "(option(1:9).eq.'FULL_MECA' ) .or. " \
            "(option(1:9).eq.'XXXX_MECA' )"
    args = CF.ArgSplitter(sarg).strings()
    assert len(args) == 1, args
    spl = CF.args_from_list(args)
    allocd = spl.alloc(65, maxi=5, cont=True)
    assert list(map(len, allocd)) == [65, 40, 29], allocd

def test02_logical():
    """logical operation"""
    txt = "(option(1:9).eq.'RIGI_MECA') .or. (option(1:9).eq.'RAPH_MECA')" \
            ".or.(  option(1:9).eq.'FULL_MECA'  )"
    spl = CF.LogicalSplitter(txt)
    assert spl.size() == 5, spl
    for i in (0, 2, 4):
        spl.set_value(i, CF.ParSplitter(spl[i]))
        spl[i].set_value(1, CF.LogicalSplitter(spl[i][1]))

def test03_parsplit1():
    """parenthesis"""
    spl = CF.ParSplitter("array((expr1) + (expr2))")
    assert spl.size() == 3, spl.values

def test03_parsplit2():
    """parenthesis nook"""
    spl = CF.ParSplitter("(expr1) + (expr2)")
    assert spl.size() == 1, spl.values

def test03_parsplit3():
    """parenthesis if"""
    # TODO ?
    CF.ParSplitter("(he.eq.0)he=pthea(nfiss*(cface(i,j)-1)+jfiss)")

def test04_string():
    """long string"""
    txt = "'line containing 50 characters azertyuiopmlkjhgfd'"
    spl = CF.StrSplitter(txt)
    assert len(spl) == 50, len(spl)
    assert spl.size() == 1, spl.values
    spl.sub_split(20)
    assert spl.size() == 3, spl.values
    assert spl.maxlength() == 20, spl.values
    # alloc
    spl = CF.StrSplitter(txt)
    allocd = spl.alloc(20)
    assert len(allocd) == 3, allocd

def test_assign():
    """assignment"""
    txt = "realro(nbroot) = -a(2) / a(1)"
    spl = CF.SpaceSplitter(txt)
    assert spl[1] == '='
    assert str(spl[0]) == 'realro(nbroot)'
    val = ' '.join(spl[2:])
    assert val == '-a(2) / a(1)', repr(val)

def test05_auto_string():
    """base to string"""
    txt = "'line containing 50 characters azertyuiopmlkjhgfd'"
    spl = CF.BaseSplitter(txt)
    spl.sub_split(20)
    assert spl.size() == 1, spl.values
    assert len([val for val in spl]) == 3, spl.values

def test10_manual_split():
    """manual split"""
    spl = CF.ArgSplitter("jexnum(modgen//'      .MODG.LIMA',     "
                         "        fzi(j,llipr+(i1-1)*9+8)),        'L',"
                         "                                         ibid")
    jexnum = CF.ParSplitter(spl[0])
    spl.set_value(0, jexnum)
    arg_jex = CF.ArgSplitter(jexnum[1])
    jexnum.set_value(1, arg_jex)
    fzi = CF.ParSplitter(arg_jex[1])
    arg_jex.set_value(1, fzi)
    arg_fzi = CF.ArgSplitter(fzi[1])
    fzi.set_value(1, arg_fzi)
    assert spl.size() == 3, spl.values
    assert jexnum.size() == 3, jexnum
    assert arg_jex.size() == 2, arg_jex.values
    assert fzi.size() == 3, fzi.values
    assert arg_fzi.size() == 2, arg_fzi.values

def test21_auto():
    """auto split"""
    txt = "(option(1:9).eq.'RIGI_MECA') .or. (option(1:9).eq.'RAPH_MECA')" \
            ".or. flog(iad-1+k)"
    spl = CF.LogicalSplitter(txt)
    spl.sub_split(70)
    assert spl.size() == 5, spl.size()
    assert len(spl) == 81, len(spl)
    spl.sub_split(30)
    assert spl.size() == 5, spl.size()
    assert len(spl) == 81, (len(spl), str(spl))

def test22_alloc():
    """allocate"""
    txt = "   z0(6,6),z1(6,(nr-ndt))," \
            " z2((nr-ndt),6),z3((nr-ndt),(nr-ndt))"
    args = CF.ArgSplitter(txt)
    lines = args.alloc(59, cont=False)
    assert len(lines) == 2, lines


def test00_continuation():
    """continuation"""
    # similar to Block.lines2text
    lines = ["if (dble(scale) .ne. 0.0d0 .or. dimag(scale) .ne.&",
             "         0.0d0) callzmult (n, 1.0d0/scale, evec(1,j), 1)"]
    lines = CF.apply_on_lines(CF.remove_continuation, lines)
    assert lines[0].endswith('.ne.'), repr(lines[0])
    assert lines[1].startswith(' 0.0d0'), repr(lines[1])
    frepl = partial(CF.replace_extra, char=' ')
    lines = CF.apply_on_lines(frepl, lines)

def test0_first_enclosed():
    """search first enclosed"""
    txt = "((rang.eq.0).and.((infofe(9:9).eq.'T').or. (infofe(11:11)" \
          " .eq.'T'))) write(ifm,*) niter"
    cond = CF.first_enclosed(txt)
    assert '.and.' in cond, cond
    assert 'write' not in cond, cond
    assert len(cond) == 68, (cond, len(cond))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
