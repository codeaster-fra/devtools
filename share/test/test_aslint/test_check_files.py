# coding=utf-8

"""
Tests for the Docaster API.
"""

import os
import os.path as osp
import tempfile
import unittest

from hamcrest import *
from testutils import CODEASTER_SRC

# the parser fails in eval_parameters if 'if' statement is incomplete
SNIPPET_PARSER_ERROR = """
subroutine rc32t
    implicit none
    character(len=8) :: option, poum
    if (option .eq. 'RIGI_MECA_TANG')
        poum = '-'
    else
        poum = '+'
    endif
end subroutine
"""


def test_get_file_type():
    """test manifest"""
    from subprocess import PIPE, Popen

    from aslint.filetype import get_file_type
    from aslint.string_utils import convert

    out = Popen(
        ["git", "-C", CODEASTER_SRC, "ls-tree", "--full-tree", "-r", "--name-only", "HEAD"],
        stdout=PIPE,
    ).communicate()[0]
    out = convert(out)
    filenames = out.split()
    top = (
        ".clang-format",
        ".fprettify.rc",
        ".readthedocs.yaml",
        "CHANGELOG",
        "configure",
        "CONTRIBUTORS",
        "datg",
        "LICENSE",
        "makefile",
        "materiau",
        "pyproject.toml",
        "README.md",
        "README.rst",
    )
    for path in filenames:
        base = path.split("/")[0]
        typ = get_file_type(path)
        assert (
            (
                typ == "build"
                and (
                    "waf" in path
                    or "wscript" in path
                    or path.startswith("check_")
                    or path.startswith(".jenkinsedf/")
                    or path.startswith(".hg")
                    or path.startswith(".git")
                    or path.startswith("env.d/")
                    or path.startswith("extern/")
                    or path.startswith("gcovr")
                    or path.endswith(".md")
                )
            )
            or path in top
            or (typ is None and base in ("fermetur", "histor"))
            or (typ == "c" and base in ("bibc", "mfront", "libs"))
            or (typ == "h" and base == "bibc" and path.endswith(".h"))
            or (typ == "cxx" and base in ("bibcxx", "libs"))
            or (typ == "hxx" and base == "bibcxx" and path.endswith(".h"))
            or (typ == "for" and base in ("bibfor", "libs"))
            or (typ == "hf" and base == "bibfor" and path.endswith(".h"))
            or (typ == "py" and base in ("bibpyt", "code_aster", "run_aster"))
            or (typ == "cython" and base == "code_aster")
            or (typ == "cata" and base == "catalo")
            or (typ == "catalo" and base == "catalo")
            or (typ == "capy" and base == "catapy")
            or (typ == "test" and base == "astest")
            or (typ == "i18n" and base == "i18n")
            or (typ == "mfront" and base == "mfront" or path.endswith(".mfront"))
            or (typ == "data" and base == "data")
            or (typ == "doc" and base == "doc")
            or path.endswith(".placeholder")
        ), (base, typ, path)


def test_filter_names():
    """test filter by status"""
    from aslint.filetype import filter_names

    flist = [("fadd1", "A"), ("fch1", "M"), ("fadd2", "A")]
    assert len(filter_names(flist, "A")) == 2
    assert len(filter_names(flist, "M")) == 1
    assert len(filter_names(flist)) == 3


def test_check_files():
    """check source files"""
    from aslint.base_checkers import CompilMsg, Report
    from aslint.check_files import check_files, read_waf_parameters
    from aslint.filetype import filter_arguments
    from aslint.logger import logger

    fname = tempfile.NamedTemporaryFile(suffix=".F90").name
    with open(fname, "w") as fort:
        fort.write(SNIPPET_PARSER_ERROR)

    args = [
        "bibc/supervis/indik8.c",
        "bibcxx/MFront/MFrontBehaviour.cxx",
        "bibfor/utilifor/ibmain.F90",
        "bibpyt/Utilitai/Utmess.py",
        "code_aster/Cata/Commands/lire_maillage.py",
        "catalo/cataelem/Elements/meca_c_plan.py",
        "wscript",
        fname,
    ]
    args = [osp.join(CODEASTER_SRC, i) for i in args]
    filtered_args = filter_arguments(args, status="M")
    assert_that(filtered_args, has_key("c"))
    assert_that(filtered_args["c"], has_length(1))
    assert_that(filtered_args, has_key("for"))
    assert_that(filtered_args["for"], has_length(2))

    wafc4che = osp.join(CODEASTER_SRC, "build", "mpi", "c4che", "release_cache.py")
    if not osp.exists(wafc4che):
        wafc4che = osp.join(CODEASTER_SRC, "build", "mpi", "c4che", "debug_cache.py")
    if not osp.exists(wafc4che):
        wafc4che = osp.join(CODEASTER_SRC, "build", "mpidebug", "c4che", "debug_cache.py")
    if not osp.exists(wafc4che):
        raise IOError("requires waf c4che directory")

    report = Report()
    try:
        prev = os.getcwd()
        os.chdir(CODEASTER_SRC)
        c4che = read_waf_parameters(wafc4che)
        check_files(report, filtered_args, c4che)
    finally:
        os.chdir(prev)
    logger.info(report.to_text())

    assert_that(report, has_key(fname))
    parser_error = [i for i in report[fname] if isinstance(i, CompilMsg)]
    assert_that(parser_error, has_length(3))


def test_check_messages():
    # useful to increase coverage without an installation directory
    from aslint.check_global import check_messages

    check_messages(CODEASTER_SRC)


if __name__ == "__main__":
    import sys

    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
