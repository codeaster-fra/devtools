# coding=utf-8

"""
Unittests for Block objects (fortran_code and change_code modules)
"""


import os.path as osp
import unittest
from glob import glob

import aslint.fortran.free.fortran_code as FC
import fortran_samples as CODE
from aslint.fortran.free.change_code import (remove_args, remove_call_arg,
                                             remove_vars)
from aslint.fortran.free.code_func import replace_enclosed
from aslint.logger import logger
from testutils import CODEASTER_SRC


def test00_elem():
    """replace_enclosed"""
    txt = "    real(kind=8)  :: z0(6,6),z1(6,(nr-ndt))," \
            "z2((nr-ndt),6),z3((nr-ndt),(nr-ndt)), val('h,w')"
    new = replace_enclosed(txt, ',', '%COMMA%', '(\'"', ')\'"')
    ref = "    real(kind=8)  :: z0(6%COMMA%6),z1(6%COMMA%(nr-ndt))," \
            "z2((nr-ndt)%COMMA%6),z3((nr-ndt)%COMMA%(nr-ndt)), " \
            "val('h%COMMA%w')"
    assert new == ref, new
    txt = """call jevete ( '&INEL.'//nomte(1:8)//'.DESI' , ' ' , lzi )"""
    src = FC.source(text=txt)
    src.code_text()
    assert isinstance(src[0], FC.Call)
    assert len(src[0].args) == 3, src[0].args

def test01_elem():
    """block creation, copy"""
    src = FC.source(text=CODE.TRIVIAL)
    src.code_text()
    src.dbg()
    # test copy
    wrk = src.copy()
    wrk.code_text()
    wrk.dbg()
    for st1, st2 in zip(src, wrk):
        assert st1 != st2, (st1, st2)
    assert len(wrk) == 1, len(wrk)
    # test copy of sub-statements
    src = FC.get_subprogram(src)
    wrk = FC.get_subprogram(wrk)
    assert src != wrk, (src, wrk)
    assert len(src) == len(wrk) == 6, (len(src), len(wrk))
    for st1, st2 in zip(src, wrk):
        assert st1 != st2, (st1, st2)
    #
    assert wrk.args == src.args, (wrk.args, src.args)
    wrk.remove_arg('a')
    assert wrk.args != src.args, (wrk.args, src.args)
    # test get_type on itself
    src = FC.get_subprogram(src)
    src = FC.get_subprogram(src)
    src.dbg()
    assert len(src) == 6, len(src)
    assert isinstance(src[3], FC.LoopDo), src[3].dbg()
    loop = src[3]
    assert len(loop) == 2, loop.dbg()
    assert isinstance(loop[1], FC.EndDo), loop[1].dbg()
    assert isinstance(src[4], FC.Call), src[4].dbg()
    assert isinstance(src[5], FC.EndSub), src[5].dbg()

def test02_remove_arg():
    """remove_arg"""
    src = FC.source(text=CODE.TRIVIAL)
    wrk = src.copy()
    sub = FC.get_subprogram(src)
    wrk = FC.get_subprogram(wrk)
    wrk.remove_arg('a')
    assert len(sub.args) == 1, sub.args
    assert len(wrk.args) == 0, wrk.args

def test10_parse_sample():
    """parse source"""
    src = FC.source(CODE.SAMPLE)
    sub = src.get_type(FC.Subroutine)
    src.code_text()
    assert len(sub) == 1, len(sub)
    sub = FC.get_subprogram(src)
    assert sub.name == 'subprg12'
    assert len(sub.args) == 26, sub.args
    assert len(sub) == 23, len(sub)
    decls = sub.get_type(FC.Decl)
    assert len(decls) == 12, len(decls)
    call = sub.get_type(FC.Call)
    assert len(call) == 5, call.dbg()
    jemarq = call[0]
    assert jemarq.args == [], jemarq.dbg()
    empty = sub.get_type(FC.EmptyLine)
    assert len(empty) == 2, src.dbg()

def test20_remove():
    """remove_vars, remove_args"""
    src = FC.source(CODE.SAMPLE)
    src = FC.get_subprogram(src)
    src.code_text()
    src.dbg()
    src = remove_vars(src, 'subprg12', ('btrv', 'coef'))
    src.code_text()
    # print src.dbg()
    idx = {}
    chg = remove_args(src, 'subprg12', ('repmo2', 'temps'), idx)
    chg.code_text()
    assert idx['total'] == 26, idx
    assert len(chg.args) == idx['total'] - 2, (idx, chg.args)
    assert idx['repmo2'] == 7, idx
    assert idx['temps'] == 11, idx
    src = remove_call_arg(src, 'draac2', 4, 6)
    src = remove_call_arg(src, 'draac2', 2, 5)
    src.code_text()
    call = src.get_type(FC.Call)
    assert len(call) == 5, len(call)
    assert call[2].name == 'draac2'
    assert len(call[2].args) == 4, call[2].dbg()
    # print src.code_text()

def test30_debug():
    """debug"""
    fname = osp.join(CODEASTER_SRC, 'bibfor/parallel/asmpi_status.F90')
    src = FC.source(filename=fname)
    src.code_text()
    # src.write_code(fname)

@unittest.skip("for a global migration")
def test99_parse_all():
    """parse all"""
    from aslint.decorators import interrupt_decorator
    from aslint.utils import apply_in_parallel

    @interrupt_decorator
    def _beautify(fname):
        """Beautify a file"""
        logger.info(fname)
        try:
            src = FC.source(filename=fname)
            src.code_text()
            src.write_code(fname)
        except (AssertionError, Exception):
            print('error reading', fname)
            raise

    lfor = glob(osp.join('bibfor', '*', '*.F90'))
    # not efficient on several threads (about 130 s)
    apply_in_parallel(_beautify, lfor, 4)


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
