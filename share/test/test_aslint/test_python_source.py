# coding=utf-8

"""
Unittests for Python checkers
"""

import unittest

import py_samples as CODE
from aslint.python import python_tools
from hamcrest import *


def test_masked_imports():
    masked = python_tools.get_masked_imports(CODE.MASKED_IMPORTS)
    assert_that(masked, has_length(2))
    imp0 = masked[0]
    mod0 = [i.name for i in imp0.names]
    assert_that(mod0, has_length(1))
    imp1 = masked[1]
    mod1 = [i.name for i in imp1.names]
    assert_that(mod1, has_length(1))
    assert_that(mod0 + mod1, contains_inanyorder("sys", "shutil"))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
