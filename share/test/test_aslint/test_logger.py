# coding=utf-8

"""
Tests for logger module.
"""

import logging
import unittest

from aslint.logger import (ERROR, OK, RETURNCODE, VERBOSE, WARN, ErrorCode,
                           logger)
from hamcrest import *
from testutils import silent_logger


def test_error_code():
    """test ErrorCode object"""
    errcode = ErrorCode()
    assert errcode.is_ok(), errcode.value
    assert errcode.numeric() == RETURNCODE[OK] == 0
    errcode.add(VERBOSE)
    assert errcode.is_ok(), errcode.value
    errcode.add(WARN)
    assert errcode.is_warn(), errcode.value
    assert errcode.numeric() == RETURNCODE[WARN] == 2
    errcode.add(OK)
    assert errcode.is_warn(), errcode.value
    errcode.add(ERROR)
    assert errcode.is_error(), errcode.value
    assert errcode.is_value(ERROR), errcode.value
    assert errcode.numeric() == RETURNCODE[ERROR] == 4

def test_from_status():
    """test ErrorCode.from_status object"""
    errcode = ErrorCode()
    errcode.from_status(0)
    assert errcode.is_ok(), errcode.value
    errcode.from_status(2)
    assert not errcode.is_ok(), errcode.value
    assert not errcode.is_warn(), errcode.value
    assert errcode.is_error(), errcode.value
    assert errcode.is_error(), errcode.value
    errcode.from_status(-15)
    assert not errcode.is_ok(), errcode.value
    assert not errcode.is_warn(), errcode.value
    assert errcode.is_error(), errcode.value

def test_gettext():
    """test get_text"""
    logger.setLevel(logging.INFO)
    logger.info("text line 1")
    logger.info("text line 2")
    logger.info("text line 3")
    lines = logger.get_text().strip().splitlines()
    nb_lines = len(lines)
    assert_that(nb_lines, greater_than_or_equal_to(3))

    logger.info("text line 4")
    logger.info("text line 5")
    lines = logger.get_text().strip().splitlines()
    assert_that(len(lines), equal_to(nb_lines + 1 + 2))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
