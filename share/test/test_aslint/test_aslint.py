# coding=utf-8

"""
Tests for aslint generic functions.
"""

import os
import unittest

from hamcrest import *


def test_basic():
    """test initialization"""
    from aslint.config import ASCFG, UNCACHED

    assert_that(ASCFG.get("fortran.form"), equal_to("free"))
    assert_that(ASCFG.get("username"), is_not(empty()))
    if os.environ.get("CI"):
        return
    assert_that(ASCFG.get("notify.user"), any_of(contains_string("@"), contains_string(" at ")))
    assert_that(ASCFG.get("notify.admins"), contains_string("@"))
    assert_that(len(ASCFG.get("notify.admins").split(",")), greater_than(0))
    try:
        assert_that(ASCFG.get("asrun.lib"), is_not(same_instance(UNCACHED)))
    except OSError:
        raise OSError("as_run probably not in PATH")


def test_never_failed_dict():
    """test NeverFailedDict"""
    from aslint.baseutils import NeverFailedDict

    data = {"content": "value"}
    nfd = NeverFailedDict(data)
    assert_that(nfd["content"], equal_to("value"))
    assert_that(nfd["unknown"], empty())
    assert_that(nfd["unknown"]["more less known"], empty())


def test_depends():
    """check dependency resolution"""
    from aslint.dependency import Node, resolve_dependency

    a = Node("a")
    b = Node("b")
    c = Node("c")
    d = Node("d")
    e = Node("e")
    f = Node("f")

    a.add_dependency(b)  # a depends on b
    a.add_dependency(d)  # a depends on d
    b.add_dependency(c)  # b depends on c
    b.add_dependency(e)  # b depends on e
    c.add_dependency(d)  # c depends on d
    c.add_dependency(e)  # c depends on e
    e.add_dependency(f)  # e depends on f

    resolved, unresolved = resolve_dependency(a, return_unresolved=True)
    assert_that(a, is_in(resolved))
    assert_that(b, is_in(resolved))
    assert_that(c, is_in(resolved))
    assert_that(resolved, has_length(6))
    assert_that(unresolved, has_length(0))

    # print resolve_dependency(a)
    # print resolve_dependency(b)
    # print resolve_dependency(c)
    # print resolve_dependency(d)
    # print resolve_dependency(e)
    # print resolve_dependency(f)

    d.add_dependency(b)
    assert_that(calling(resolve_dependency).with_args(a), raises(ValueError))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
