# coding=utf-8

"""
Tests for the Docaster API.
"""

import unittest

from aslint.fortran.free.fortran_utilities import args_as_dict
from hamcrest import *


def test_kwargs():
    """args_as_dict"""
    kwargs = args_as_dict((1, '2', 3, 'd=4', 'e=5'),
                            ['a', 'b', 'c', 'd', 'e', 'f'])
    assert_that(kwargs['a'], equal_to(1))
    assert_that(kwargs['b'], equal_to(2))
    assert_that(kwargs['c'], equal_to(3))
    assert_that(kwargs['d'], equal_to(4))
    assert_that(kwargs['e'], equal_to(5))
    assert_that(kwargs['f'], none())

    assert_that(calling(args_as_dict).with_args((1, 'b=2', 3, 'd=4', 'e=5'),
                                                ['a', 'b', 'c', 'd', 'e', 'f']),
                raises(AssertionError))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
