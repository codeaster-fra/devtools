#!/bin/bash

usage()
{
    echo "usage: ${0} [arguments] FILENAME"
    echo
    echo "  This script sends the content of a file to the DevOps Teams channel."
    echo
    echo "arguments:"
    echo "  --from NAME             name of the sender."
    echo "  --subject STRING        subject of the message."
    echo "  --html                  expect html content in file."
    echo "  --notifications         send to 'notifications' channel (default)."
    echo "  --admin                 send to 'admin' channel."
    exit 1
}

_error()
{
    # usage: _error messsage
    echo "ERROR: ${1}"
    echo
    usage
}

NOTIF="ZWFhZTQzZTguZWRmb25saW5lLm9ubWljcm9zb2Z0LmNvbUBlbWVhLnRlYW1zLm1zCg=="
ADMIN="MmI3NmFmN2IuZWRmb25saW5lLm9ubWljcm9zb2Z0LmNvbUBlbWVhLnRlYW1zLm1zCg=="
TEST="ODQ4MDRkZjcuZWRmb25saW5lLm9ubWljcm9zb2Z0LmNvbUBlbWVhLnRlYW1zLm1zCg=="


send_msteams_main()
{
    local from="no-reply@edf.fr"
    local subject
    local dest="${NOTIF}"
    local admin=0
    local html=0

    OPTS=$(getopt -o h --long help,from:,subject:,html,admin,notifications \
        -n $(basename $0) -- "$@")
    if [ $? != 0 ] ; then
        _error "invalid arguments." >&2
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            -h | --help ) usage ;;
            --from ) from="$2"; shift ;;
            --subject ) subject="$2"; shift ;;
            --html ) html=1 ;;
            --notifications ) dest="${NOTIF}"; shift ;;
            --admin ) admin=1 ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
        shift
    done
    [ -z "${from}" ] && _error "'--from' option is required"
    [ -z "${subject}" ] && _error "'--subject' option is required"
    [ ${admin} -eq 1 ] && dest="${ADMIN}"

    [ $# -ne 1 ] && _error "exactly one argument is expected (${#} passed)"

    ping -c 1 -W 3 mailhost.der.edf.fr > /dev/null 2>&1
    ok=$?
    if [ ${ok} -ne 0 ] && [ -z "${SINGULARITY_NAME}" ]; then
        if [ ${admin} -eq 0 ]; then
            echo "WARNING: can not send notifications to the Teams Channel."
            echo "Please notify the 'DevOps Notifications' channel by"\
                 "yourself for: ${subject}"
            return 1
        fi
        address=$(base64 -d <<< "cmV0ZC1jb2RlYXN0ZXItaW50ZWdyQGdyb3Vwcy5lZGYuZnIK")
        mail -s "${subject}" "${address}" < "${1}"
        iret=$?
        return ${iret}
    fi

    if [ ${html} -eq 0 ]; then
        text="$(sed -e 's/</\&lt;/g' -e 's/>/\&gt;/g' "${1}")"
        text="<pre>${text}\n</pre>"
    else
        text=$(cat "${1}")
    fi
    address=$(base64 -d <<< "${dest}")
    cat << EOF | curl --url smtp://mailhost.der.edf.fr \
        --mail-from "no-reply@edf.fr" \
        --mail-rcpt "${address}" \
    --upload-file -
From: "${from}" <no-reply@edf.fr>
To: "${address}"
Content-Type: text/html; charset=utf-8
Mime-version: 1.0
Subject: ${subject}

<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head><body>
${text}
</body></html>
EOF
    iret=$?
    return ${iret}
}

send_msteams_main "${@}"
exit ${?}
