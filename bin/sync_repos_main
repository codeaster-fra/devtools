#!/usr/bin/env python3
# coding: utf-8

"""
    %(prog)s [options]

Synchronize the 'devtools', 'data' and 'validation' repositories with the
current revision of the 'src' repository.

This script reads the date attribute of the current revision and
update the other repositories at the first revision preceding this date.

Returns 0 on success, != 0 if an error occurred.
"""

import argparse
import os
import os.path as osp
import sys

from aslint.i18n import _
from aslint.logger import SetLevelAction, logger
from repository_api import hgcmd, get_repo_by_rev

if sys.version_info < (3, 5):
    raise ValueError("This script requires Python >=3.5")


def main(root, repos, dry_run=False):
    """Main function."""
    if get_repo_by_rev(".") != "src":
        logger.error(_("this script expects to be executed in the "
                       "'src' repository"))
        return 1
    branch = hgcmd("branch", ".")[1]
    date = hgcmd("log", ".", rev=".", template="{date|isodate}")[1] #.split()[0]
    logger.info(_("update other repositories at date '{0}' in branch '{1}'")
                .format(date, branch))
    if dry_run:
        return 0
    for sub in repos:
        bri = branch if sub != "devtools" else "default"
        logger.info(_("\n--- '{0}':").format(sub))
        path = osp.join(root, sub)
        if not osp.isdir(path):
            continue
        iret, out = hgcmd("update", path,
                          rev='branch({1}) and date("<{0}")'.format(date, bri))
        logger.info(out)
        if iret != 0:
            logger.error(_("can not update '{0}', check if there are "
                           "uncommitted changes").format(path))
            continue
        out = hgcmd("log", path, rev=".")[1]
        logger.info(_("updated at revision:\n{0}").format(out))
    return 0


if __name__ == '__main__':
    # command arguments parser
    parser = argparse.ArgumentParser(
        usage=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-g', '--debug', action=SetLevelAction)

    parser.add_argument('--root', action='store',
        default=osp.dirname(os.getcwd()),
        help=_("root of the repositories"))
    parser.add_argument('-t', '--devtools', action='store_true', default=False,
        help=_("update devtools repository"))
    parser.add_argument('-d', '--data', action='store_true', default=False,
        help=_("update data repository"))
    parser.add_argument('-v', '--validation', action='store_true', default=False,
        help=_("update validation repository"))
    parser.add_argument('-a', '--all', action='store_true', default=False,
        help=_("update all repositories"))
    parser.add_argument('-n', '--dry-run', action='store_true', default=False,
        help=_("does nothing, just show what it would be done"))

    args = parser.parse_args()
    repos = set()
    if args.all:
        repos.update(["devtools", "data", "validation"])
    if args.devtools:
        repos.add("devtools")
    if args.data:
        repos.add("data")
    if args.validation:
        repos.add("validation")
    if not repos:
        parser.error("enable at least one option to select repositories")

    main(args.root, repos, args.dry_run)
    logger.exit()
