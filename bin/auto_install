#!/bin/bash

# This script is simply a shortcut to make download and configuration of
# repositories fully automatic.

if [ $# -ge 1 ] && ( [ "$1" = "-h" ] || [ "$1" = "--help" ] ); then
    echo "usage: auto_install [-h/--help] [rootdir [branch]]"
    echo "  by default:"
    echo "      rootdir=$HOME/dev/codeaster"
    echo "      branch=main"
    exit 1
fi

# defaults to $HOME/dev/codeaster
base=$HOME/dev/codeaster
# change default destination on Cronos
if [ -d /scratch/users/$(basename $HOME) ]; then
    base=/scratch/users/$(basename $HOME)/dev/codeaster
fi
if [ $# -ge 1 ]; then
    base=$1
    shift
fi

branch=main
if [ $# -ge 1 ]; then
    branch=$1
    shift
fi

printf "\n+ starting auto-installation for branch ${branch} into ${base}...\n"

printf "\n+ checking existing developer files... "
if [ ! -e ${base} ]; then
    printf "none\n"
else
    printf "\nERROR: ${base} already exists. If you want to ignore existing files remove this directory. DO IT CAREFULLY!\n"
    exit 1
fi


printf "\n+ searching for git... "
mkdir -p ${base}
cd ${base}
base=$(pwd)
if [ -e devtools ]; then
    printf "\n+ renaming devtools to devtools.orig...\n"
    mv devtools devtools.orig
fi

if which git; then
    printf "\n+ downloading devtools repository...\n"
    git clone --single-branch --branch main https://gitlab.pleiade.edf.fr/codeaster/devtools
    printf "done\n"
else
    printf "\nERROR: command 'git' not available! please install git!\n"
    exit 1
fi

printf "\n+ downloading and configuring source repositories... \n"
${base}/devtools/bin/install_env --clone --reset --branch=${branch} $@ ${base}
