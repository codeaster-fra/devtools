#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [--tag=TAG] [--edsm] [--rex=FILE] [options] [issues list]

Generate the changelog (histor) of the list of the solved issues provided in
files given with '--rex' option + issues in 'issues list'.
The tag is used as the version number.

If '--tag' is used the tag must exist in REPO (to extract the node id).
The changelog is written onto stdout.

With '--edsm' the tag is not checked and just added in a short header.
"""


import os
import re
import time
from optparse import OptionParser
from subprocess import check_output

from api_roundup import build_histor
from aslint.i18n import _
from aslint.logger import logger, setlevel
from aslint.string_utils import convert


def read_issue_list(fname):
    """Read the issues numbers from 'fname'"""
    ignore = re.compile("#.*$", re.M)
    expr = re.compile("([0-9]+)")
    with open(fname, "r") as fobj:
        lines = fobj.read().splitlines()
    # remove twins by keeping the order
    issues_list = []
    for line in lines:
        line = ignore.sub("", line)
        numbers = expr.findall(line)
        for i in numbers:
            if i not in issues_list:
                issues_list.append(i)
            else:
                logger.warn(_("already seen: %s"), i)
    issues = " ".join(issues_list).split()
    try:
        issues = [int(i) for i in issues]
    except ValueError:
        logger.error(_("only issue numbers expected, strings must be " "commented using '#'"))
        raise
    return issues


def build_header(tag, repopath, edsm=False):
    """return a header with the version number"""
    # do not check tag for edsm
    if edsm:
        header = os.linesep.join(
            [
                "==================================================================",
                "Version %(tag)s, intégration du %(date)s",
                "==================================================================",
            ]
        )
        return header % {"tag": tag, "date": time.strftime("%d/%m/%Y")}
    header = os.linesep.join(
        [
            "==================================================================",
            "Version %(tag)s (révision %(node)s) du %(date)s",
            "==================================================================",
        ]
    )
    try:
        cmd = ["git", "-C", repopath, "log", "-1", "--format=%h", tag]
        node = check_output(cmd, text=True).strip()
        cmd = ["git", "-C", repopath, "tag", "-l", "--format=%(taggerdate:iso)", tag]
        date = check_output(cmd, text=True).strip()
    except ValueError:
        logger.error(
            _(f"unexpected output, perhaps you are not in the repository: tag '{tag}' not found")
        )
        raise RuntimeError
    return header % {"tag": tag, "node": node, "date": date}


def major_text(branch, tag, repopath):
    """return the text for a major version"""
    content = os.linesep.join(
        [
            "",
            "Stabilisation de la version %(tag)s de Code_Aster",
            "",
            "- Étiquette %(tag)s posée sur la révision %(node)s.",
            "- Étiquette %(name)s posée sur la révision %(node)s.",
            "",
            "Les mises à jour %(short)s (dans la branche %(branch)s) seront des ",
            "versions de %(maint)s.",
            "",
        ]
    )
    # git log --format=%H --max-count=1 16.6.0
    cmd = ["git", "-C", repopath, "log", "-1", "--format=%h", tag]
    node = check_output(cmd, text=True).strip()
    cmd = ["git", "-C", repopath, "tag", "-l", f"--contains={node}"]
    tags = check_output(cmd, text=True).strip()
    name = [i for i in tags if not i[0].isdigit()][0]
    infos = {"tag": tag, "node": node, "name": name, "branch": branch}
    infos["short"] = ".".join(tag.split(".")[:2]) + ".*"
    # XXX voir texte pour oldstable, et stable sur main ?
    if branch == "main":
        infos["maint"] = "maintenance corrective et évolutive"
    else:
        infos["maint"] = "maintenance corrective uniquement"
    return content % infos


def check_issue_list(items):
    """Check that all items are valid issue number"""
    for idx in items:
        try:
            int(idx)
        except (TypeError, ValueError):
            logger.error(_("invalid issue number: {0}").format(idx))
            return False
    return True


def create_histor(rex, tag, repo, major, edsm=False, lmsg="last", format="text", req_status=None):
    """Main function"""
    text = []
    if tag:
        text.append(convert(build_header(tag, repo, edsm)))
    text.append("")
    if major and tag:
        text.append(convert(major_text(major, tag, repo)))
    else:
        if rex and check_issue_list(rex):
            logger.debug(_("getting REX changelog"))
            text.append(convert(build_histor(rex, format, lmsg, req_status=req_status)))
    return os.linesep.join(text)


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    parser.add_option(
        "--rex", action="store", metavar="FILE", help="file of the issues solved in REX"
    )
    parser.add_option(
        "--status",
        action="store",
        default="valide_EDA",
        help="required status of REX issues (default: "
        "valide_EDA). Use None to disable the checking.",
    )
    parser.add_option("--tag", action="store", help="tag of the version")
    parser.add_option(
        "--repo",
        action="store",
        default=".",
        help="path to the src repository (default: current directory)",
    )
    parser.add_option("--major", action="store", help="branch to create a file for a major version")
    parser.add_option("--edsm", action="store_true", help="create an histor for Salome-Meca")
    parser.add_option("--all", action="store_true", help="insert all the messages for each issues")
    parser.add_option(
        "--format",
        action="store",
        default="text",
        help="output format of the changelog: " "text (default), html, fsq",
    )
    opts, args = parser.parse_args()
    if opts.tag is None:
        logger.warn("no header will be inserted")
    rex = opts.rex and read_issue_list(opts.rex) or []
    if len(args) != 0:
        logger.info(_("add issues: {0}").format(args))
        rex.extend(args)
    lmsg = "all" if opts.all else "last"
    bitb = []
    if "None" in opts.status:
        opts.status = None
    text = create_histor(
        rex,
        opts.tag,
        opts.repo,
        opts.major,
        edsm=opts.edsm,
        lmsg=lmsg,
        format=opts.format,
        req_status=opts.status,
    )
    print(text)
