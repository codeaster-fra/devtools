#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options] filename_of_list_of_issues

Validate the issues listed in the given filename.
"""

from optparse import OptionParser

from aslint.logger import setlevel

from api_roundup.utils import read_issue_list
from api_roundup import validate_issue


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    ltyp = []
    for typ in ("anomalie", "evolution"):
        print(f"Valid issues of type {typ!r} ([y]/n)?", end=" ")
        ret = input()
        if ret.lower() not in ("n", "no"):
            ltyp.append(typ)

    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error("exactly one argument is required")
    validate_issue(read_issue_list(args[0]), types=ltyp)
