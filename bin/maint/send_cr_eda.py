#!/usr/bin/env python3
# coding: utf-8

"""
    send_cr_eda.py [options] REPORT

Send the EDA meeting report file to the EDA mailing list.

"""

import argparse
import os.path as osp
import re

from aslint.config import ASCFG
from aslint.i18n import _
from aslint.logger import logger, setlevel
from hgaster.ext_utils import sendmail, split_strip

TEXT = """
Bonjour,

{text}

Cordialement,
"""

MSG = """
<p>
Publication du compte-rendu de l'EDA du {date}.
</p>
<p>
Lien :
<a href="https://gitlab.pleiade.edf.fr/codeaster/lab/cr_eda/-/blob/main/{year}/{pdf}">
https://gitlab.pleiade.edf.fr/codeaster/lab/cr_eda/-/blob/main/{year}/{pdf}
</a>
</p>
"""


def get_subject_from(fname):
    """Return the subject from the report filename"""
    reg = re.compile("e(?P<year>[0-9]{4})(?P<month>[0-9]{2})(?P<day>[0-9]{2})" ".pdf$")
    mat = reg.search(fname)
    subject = "Compte-rendu de l'EDA"
    text = "Veuillez trouver ci-joint le dernier compte-rendu de l'EDA."
    msg = ""
    if mat:
        date = "{day}/{month}/{year}".format(**mat.groupdict())
        subject = "Compte-rendu de l'EDA du " + date
        text = "Veuillez trouver ci-joint le compte-rendu de l'EDA du {0}.".format(date)
        msg = MSG.format(date=date, pdf=osp.basename(fname), year=mat.group("year"))
    return subject, TEXT.format(text=text), msg


if __name__ == "__main__":
    # command arguments parser
    parser = argparse.ArgumentParser(
        usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-g", "--debug", action="store_true", help="add debug informations")
    parser.add_argument(
        "--to", action="store", help="destination address " "(default: aster-eda mailing list)"
    )
    parser.add_argument("-n", "--dry-run", action="store_true", help="do not send the email")
    parser.add_argument("report", metavar="REPORT", nargs=1, help="Filename(s) of the report")
    args = parser.parse_args()
    if args.debug:
        setlevel()
    subject, text, msg = get_subject_from(args.report[0])

    dest = split_strip(args.to or ASCFG.get("notify.eda"), ",")
    if args.dry_run:
        logger.info(_("Send email to {0}").format(dest))
        logger.info(_("Subject: {0}").format(subject))
        logger.info(_("Message:\n{0}").format(text))
        logger.info(_("Attached files: {0}").format(args.report))
        logger.info(_("Teams message: {0}").format(msg))
    else:
        sendmail(
            dest,
            subject,
            text,
            fromaddr=ASCFG.get("notify.codeaster"),
            bcc=True,
            add_html=True,
            attach=args.report,
            footer=False,
        )
