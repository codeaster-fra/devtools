#!/usr/bin/python3

"""Authentication agent for the web proxy at EDF
"""


PRG = "proxy_auth"

import os
import sys
import errno
import signal
import select
import getopt
import getpass

import http.client
from optparse import OptionParser

try:
    from mcutils import pidfile_start, pidfile_end, pidfile_kill, pidfile_write
except ImportError:
    def null_func(*args, **kwargs):
        pass
    pidfile_start = pidfile_end = pidfile_write = null_func

# Re-auth period: 29 minutes (proxy timer: 30 minutes)
AUTH_PERIOD = 29 * 60 * 1000

proxy_url = 'proxypac.edf.fr'
proxy_port = 3128

victim_uri = 'http://www.google.fr/'

cfru_uri = '/?cfru=' + victim_uri.encode("base64").rstrip("\r\n")

auth_hosts = ['authnoe.edf.fr', 'authpcy.edf.fr']
auth_realms = ['Auth_NOE', 'Auth PCY']

reset_url = 'redirauth2.edfgdf.fr'


def do_cfru_auth(creds, host):
    try:
        ha = http.client.HTTPConnection(host, proxy_port)
        ha.request("GET", "http://%s/" % reset_url)
        resp = ha.getresponse()
        ha.close()
    except:
        print(">O< Auth reset error: ", sys.exc_info()[0])
        return False
    # 403 (web1) or 404 (web2) on deauth, 307 if not auth
    if resp.status != 403 and resp.status != 404 and resp.status != 307:
        print(">O< Auth reset result: %d %s" % (resp.status, resp.reason))
        return False
    # Authenticate for real
    try:
        ha = http.client.HTTPSConnection(host)
        ha.request("GET", cfru_uri, '', {
                   'Authorization': 'Basic ' + creds.get_basic_auth()})
        resp = ha.getresponse()
        ha.close()
    except:
        print(">O< Auth request error: ", sys.exc_info()[0])
        return False
    # 401 => bad credentials
    if (resp.status == 302 or resp.status == 307) and resp.getheader('location', None) == victim_uri:
        # print ">O< Final status (%s): Success" % host
        return True
    else:
        print(">O< Final status (%s): %d %s" % (host, resp.status, resp.reason))
        return False
    return False


def do_auth(creds):
    nbok = 0
    for host in auth_hosts:
        ret = do_cfru_auth(creds, host)
        if ret:
            nbok = nbok + 1
    print(">O< Auth result: %d OK" % nbok)


class ProxyCreds:

    def __init__(self):
        self.basic_creds = None

    def valid(self):
        return self.basic_creds is not None

    def invalidate(self):
        self.basic_creds = None

    def read_from_console(self):
        login = eval(input('Login Sesame: '))
        password = getpass.getpass('Mot de passe Sesame: ')

        return (login, password)

    def load_from_console(self):
        login, password = self.read_from_console()
        # login, password = 'LOGIN', 'PASS'
        while login == '' or password == '':
            print("\nVeuillez fournir un login et un mot de passe Sesame !")

            login, password = self.read_from_console()

        self.set_creds(login, password)

    def set_creds(self, login, password):
        self.set_basic_auth(login, password)

    def set_basic_auth(self, login, password):
        creds = login + ':' + password
        self.basic_creds = creds.encode("base64").rstrip("\r\n")

    def get_basic_auth(self):
        return self.basic_creds


def hdlr_sighup(signum, frame):
    """signal handler"""
    sys.exit(0)


def main(daemon_mode):
    """main"""
    creds = ProxyCreds()
    creds.load_from_console()
    do_auth(creds)
    print("*** Proxy parameters:")
    print("export http_proxy=%s:%d" % (proxy_url, proxy_port))
    print("export https_proxy=%s:%d" % (proxy_url, proxy_port))
    if daemon_mode:
        pid = os.fork()
        if pid > 0:
            pidfile_write(PRG, pid)
            os._exit(0)
        elif pid < 0:
            print("Could not fork")
            sys.exit(1)
        signal.signal(signal.SIGHUP, hdlr_sighup)
        sys.stdout = open(os.devnull, 'w')
        sys.stderr = open(os.devnull, 'w')
        while True:
            # SIGHUP doesn't get delivered for some reason when the terminal
            # goes away, so resort to checking that stdin is a tty (since we
            # used it to interactively prompt for login & password at startup)
            if not os.isatty(sys.stdin.fileno()):
                sys.exit(0)
            select.select([], [], [], AUTH_PERIOD)
            do_auth(creds)


if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-d', '--daemon', action='store_true',
                      help="daemon mode")
    parser.add_option('-k', '--kill', action='store_true',
                      help="kill the running process")

    opts, args = parser.parse_args()

    if opts.kill:
        pidfile_kill(PRG)
        pidfile_end(PRG)
        sys.exit(0)
    run = pidfile_start(PRG, exit_if_run=False)
    if run is True:
        pidfile_kill(PRG)
        pidfile_start(PRG)
    try:
        main(opts.daemon)
    finally:
        if not opts.daemon:
            pidfile_end(PRG)
