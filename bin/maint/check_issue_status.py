#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options] filename_of_list_of_issues

Check that the status the issues listed in the given filename is the
expected one.
"""

import sys
from optparse import OptionParser

from api_roundup import check_status, get_connection
from api_roundup.utils import read_issue_list
from aslint.i18n import _
from aslint.logger import setlevel


def check_issue_status(expected, ids):
    """Check the status of some issues.

    Arguments:
        expected (str): expected status.
        ids (list[int]): Identifiers of the issues.

    Returns:
        bool: *True* if the status is the expected one, *False* otherwise.
    """
    ok = True
    with get_connection() as rex:
        for num in set(ids):
            issue_id = "issue" + str(num)
            issue = rex.display(issue_id)
            ok = ok and check_status(
                issue_id,
                issue,
                expected,
                rex,
                verbose=True,
                force=opts.expected is None,
            )
    return ok


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    parser.add_option("--expected", action="store", help="the expected status")
    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error("exactly one argument is required")

    ok = check_issue_status(opts.expected, read_issue_list(args[0]))
    sys.exit(0 if ok else 1)
