#!/usr/bin/env python3
# coding: utf8

"""
Helper script to write the EDA report

usage: cr_eda.py --friday [13.0.0 13.0.1]
       cr_eda.py --monday

Friday: list of the integrated issues, testcases statistics, summary of aom and
        the issues in the maintenance branch.

Monday: prepare the list of the issues ready to be solved.
"""

import csv
import os
import os.path as osp
import re
import sys
import time
from datetime import datetime
from string import Template
from subprocess import PIPE, Popen

from api_roundup import get_connection
from api_roundup.utils import (format_date, get_productname, get_realname,
                               read_issue_list)
from aslint.i18n import _
from aslint.logger import logger, setlevel
from aslint.string_utils import convert
from hgaster.ext_utils import decode_author, get_issues_from_descr


class SolvedIssue(object):
    """Store data about a solved issues"""

    __slots__ = ("idx", "testcases", "valid_date")

    def __init__(self, idx):
        """Initialisation"""
        self.idx = idx
        self.testcases = []
        self.valid_date = None

    def add_testcases(self, names):
        """Add testcases added by this issues"""
        self.testcases.extend(names)

    def set_validation_date(self):
        """Request the REX for the validation date"""
        today = time.strftime("%d/%m/%Y")
        # TODO request the REX for the validation date
        # (probably only the date of the last change is available)
        self.valid_date = today

    def merge(self, other):
        """Merge with another issue object"""
        assert self.idx == other.idx
        self.testcases.extend(other.testcases)


class DeveloperJob(object):
    """Store data about the issues solved by a developer"""

    __slots__ = ("name", "email", "issues")

    def __init__(self, name, email):
        """Initialisation"""
        self.name = name
        self.email = email
        self.issues = []

    def add_issue(self, issue):
        """Add a solved issue"""
        indexes = [i.idx for i in self.issues]
        try:
            i = indexes.index(issue.idx)
            self.issues[i].merge(issue)
        except ValueError:
            self.issues.append(issue)


def list_integrated_issues(root, rev0, rev1):
    """List the integrated issues between two revisions/tags
    Return a list of DeveloperJob objects"""
    data = {}
    for repo in ("src", "validation", "data"):
        logger.info(_("checking {0} repository...").format(repo))
        repodir = osp.join(root, repo)
        if not osp.exists(repodir):
            logger.warn(_("no such repository: {0}").format(repodir))
            continue
        if repo == "src":
            cmd = [
                "hg",
                "log",
                "-R",
                repodir,
                "--rev",
                rev1,
                "--template",
                "{node|short}\n",
            ]
            out = Popen(cmd, stdout=PIPE).communicate()[0]
            out = convert(out)
            logger.info(_("last revision: {0}").format(out.strip()))
        cmd = [
            "hg",
            "log",
            "-R",
            repodir,
            "--rev",
            "{0}::{1} and not {0}".format(rev0, rev1),
            "--template",
            "{author}@SEP@{desc|firstline}@SEP@{file_adds}\n",
        ]
        out = Popen(cmd, stdout=PIPE).communicate()[0]
        out = convert(out)
        lines = out.splitlines()
        logger.info(_("{0:>6} changesets found").format(len(lines)))
        for line in out.splitlines():
            author, desc, file_adds = line.split("@SEP@")
            author, email = decode_author(author)
            # author = unicode(author, "utf-8", errors="replace")
            adds = [
                name.replace("astest/", "")
                for name in file_adds.split()
                if name.startswith("astest/") and name.endswith(".export")
            ]
            adds = list(set([osp.splitext(name)[0] for name in adds]))
            adds.sort()
            issues = get_issues_from_descr(desc)
            # print author, issues, adds
            if issues:
                data[email] = data.get(email, DeveloperJob(author, email))
                for idx in issues:
                    issue = SolvedIssue(idx)
                    issue.add_testcases(adds)
                    issue.set_validation_date()
                    data[email].add_issue(issue)
    return list(data.values())


def export_csv(data):
    """Export a dict created by `list_integrated_issues` as CSV file"""
    filename = tempfile.NamedTemporaryFile(prefix="cr_integr_", suffix=".csv").name
    fieldnames = ["name", "issues", "testcases", "valid"]
    with open(filename, "w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow(
            {
                "name": "Nom",
                "issues": "Fiches traitées",
                "testcases": "Nouveaux tests",
                "valid": "Validée le",
            }
        )
        for job in data:
            row = {"name": job.name}
            first = True
            for issue in job.issues:
                if not first:
                    row["name"] = ""
                first = False
                row["issues"] = issue.idx
                row["testcases"] = os.linesep.join(issue.testcases)
                row["valid"] = issue.valid_date
                writer.writerow(row)
    logger.info(_("CSV file created: {0}").format(filename))
    logger.info(_("Formatting:"))
    logger.info(_(" LibreOffice Calc:"))
    logger.info(_(" - Open CSV file, utf-8 charset, comma separated"))
    logger.info(_(" - Format Cells: alignment: center, top, wrap text automatically"))
    logger.info(_(" - Columns width: 4cm, 3cm, 5cm, 3cm"))
    logger.info(_(" - Title line in bold"))
    logger.info(_(" - Select cells, Copy"))
    logger.info(_(" LibreOffice Writer:"))
    logger.info(_(" - Paste special, Formatted text RTF"))
    logger.info(
        _(" - Table: Center, 15cm width, add borders, repeat heading (first row)")
    )
    return filename


def test_summary(root, tag):
    """Display a summary of the testcases at the given tag"""
    logger.warn(_("Ensure that the repositories is at the revision {0}").format(tag))
    fmt = "{number:6} {mpi:<3}"
    branch = "*"
    for verif in ("verification", "validation"):
        for repo in ("src", "validation"):
            logger.info(
                _("counting {0} testcases in repository {1}...").format(verif, repo)
            )
            repodir = osp.join(root, repo)
            if not osp.exists(repodir):
                logger.warn(_("no such repository: {0}").format(repodir))
                continue
            for mpi in ("sequential", "parallel"):
                cmd = "cd {0} && grep -l {1} $( grep -l {2} astest/*.export ) | wc".format(
                    repodir, verif, mpi
                )
                # print cmd
                out = Popen(cmd, stdout=PIPE, shell=True).communicate()[0]
                out = convert(out)
                nbtest = out.split()[0]
                logger.info(fmt.format(number=int(nbtest), mpi=mpi))
            # read the current branch for the next step
            out = Popen(["hg", "branch"], stdout=PIPE).communicate()[0]
            out = convert(out)
            branch = out.strip()

    # executed testcases
    logger.info(_("Executed testcases:"))
    path = Template(
        "/home/$admin/dev/codeaster/resu_{test,validation}/"
        "$branch{,_mpi}/{src,validation}/RESULTAT"
    )
    extract = re.compile(
        ".*/dev/codeaster/resu_(?P<test>\w+)/"
        "(?P<branch>[a-z0-9]+)_?(?P<mpi>[mpi]*)/(?P<repo>\w+)/RESULTAT"
    )
    fmt = "{number:6} {branch:<10} {mpi:<12} {test:^12} {repo:<12}"
    logger.info(
        fmt.format(
            number="",
            branch="branch",
            mpi="seq/par",
            test="verif/valid",
            repo="repository",
        )
    )
    for host, admin in [
        ("athosdev.hpc.edf.fr", "rd-ap-simumeca"),
        ("aster5.hpc.edf.fr", "rd-ap-simumeca"),
        ("clau5cic.der.edf.fr", "rd-ap-simumeca"),
        ("clap0f0q.der.edf.fr", "aster"),
    ]:
        cmd = [
            "ssh",
            host,
            "wc",
            path.substitute(admin=admin, branch=branch),
            "2>",
            "/dev/null",
        ]
        out = Popen(cmd, stdout=PIPE).communicate()[0]
        out = convert(out)
        logger.info(_("host: {0}").format(host))
        for result in out.splitlines():
            fields = result.split()
            number, directory = fields[0], fields[-1]
            mat = extract.search(directory.strip())
            if not mat:
                # total
                continue
            info = mat.groupdict()
            info["test"] = "verif" if info["test"] == "test" else "valid"
            info["mpi"] = "parallel" if info["mpi"] == "mpi" else "sequential"
            info["number"] = int(number)
            # print info
            logger.info(fmt.format(**info))


def aom_summary():
    """Display the list of issues of type 'aom'"""
    with get_connection() as rex:
        filename = tempfile.NamedTemporaryFile(prefix="cr_aom_", suffix=".csv").name
        aom = rex.lookup("type", "aide utilisation")
        enreg = rex.lookup("status", "enregistre")
        issues = rex.filter("issue", None, {"type": aom, "status": enreg})
        logger.info(_("{0} issues of type 'aom'").format(len(issues)))
        fieldnames = ["id", "creator", "date", "assignedto", "title"]
        with open(filename, "w") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writerow(
                {
                    "id": "Fiche",
                    "creator": "Émetteur",
                    "date": "Émission - Modification",
                    "assignedto": "Responsable",
                    "title": "Titre",
                }
            )
            for issue_id in issues:
                dval = rex.display(
                    "issue" + issue_id,
                    "id",
                    "creator",
                    "creation",
                    "activity",
                    "assignedto",
                    "title",
                )
                row = {"id": dval["id"]}
                row["creator"] = convert(get_realname(rex, dval["creator"]))
                row["assignedto"] = convert(get_realname(rex, dval["assignedto"]))
                row["date"] = (
                    format_date(dval["creation"]) + " " + format_date(dval["activity"])
                )
                row["title"] = convert(dval["title"])
                writer.writerow(row)
            logger.info(_("CSV file created: {0}").format(filename))
    return filename


def vexpl_summary():
    """Display a summary of issues expected in Vexpl"""
    with get_connection() as rex:
        enreg = rex.lookup("status", "enregistre")
        codeaster = rex.lookup("produit", "code_aster")
        issues = rex.filter("issue", None, {"status": enreg})
        # logger.info(_("Issues expected in Vexpl"))
        logger.info(_("{0} {1}").format("registered issues", len(issues)))
        al = rex.lookup("type", "anomalie")
        filt = rex.filter("issue", issues, {"type": al})
        logger.info(_("{0} {1}").format("anomalie", len(filt)))
        filt = rex.filter("issue", issues, {"type": al, "produit": int(codeaster)})
        logger.info(_("{0} {1}").format("anomalie code_aster", len(filt)))
        filt = rex.filter("issue", issues, {"type": al, "produit": "3,8"})
        logger.info(_("{0} {1}").format("anomalie code_aster", len(filt)))


def prepare_eda(issue_list=None):
    """Initialise the table of solved issues to discuss in EDA"""
    with get_connection() as rex:
        if issue_list is None:
            from pre_eda import get_solved_issues_eda

            dIssue = get_solved_issues_eda()
            issue_list = []
            for issues in list(dIssue.values()):
                issue_list.extend(issues)

        filename = tempfile.NamedTemporaryFile(prefix="cr_preeda_", suffix=".csv").name
        logger.info(_("{0} solved issues").format(len(issue_list)))
        fieldnames = ["assignedto", "id", "v12", "default", "comment"]
        with open(filename, "w") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writerow(
                {
                    "assignedto": "Nom",
                    "id": "Fiche",
                    "v12": "v12",
                    "default": "default",
                    "comment": "Modifications demandées et commentaires",
                }
            )
            prev = None
            for issue_id in issue_list:
                dval = rex.display(
                    "issue" + str(issue_id), "id", "assignedto", "corrVexpl", "corrVdev"
                )
                row = {"id": dval["id"]}
                resp = get_realname(rex, dval["assignedto"])
                if resp != prev:
                    row["assignedto"] = resp
                prev = resp
                if dval["corrVexpl"]:
                    row["v12"] = "x"
                if dval["corrVdev"]:
                    row["default"] = "x"
                writer.writerow(row)
            logger.info(_("CSV file created: {0}").format(filename))
    return filename


if __name__ == "__main__":
    import tempfile
    from optparse import OptionParser

    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g",
        "--debug",
        action="callback",
        callback=setlevel,
        help="add debug informations",
    )
    parser.add_option(
        "--root",
        action="store",
        default=".",
        help="path to the root of the repositories (parent of " "src, validation...)",
    )
    parser.add_option(
        "--friday", action="store_true", help="run actions relevant on friday"
    )
    parser.add_option(
        "--monday", action="store_true", help="run actions relevant on monday"
    )
    opts, args = parser.parse_args()

    if opts.friday:
        if len(args) != 2:
            parser.error("exactly two arguments are required")
        data = list_integrated_issues(opts.root, *args)
        export_csv(data)
        test_summary(opts.root, args[1])

    if opts.monday:
        if len(args) != 0:
            logger.warn("no argument will be used")
        issue_list = None
        prepare_eda(issue_list)
        aom_summary()
        vexpl_summary()

    issue_list = read_issue_list("liste_fiches.eda")

    report = {}
    list_el = []
    with get_connection() as rex:
        for issue_id in issue_list:
            sys.stderr.write(f"reading issue{issue_id}...\n")
            dval = rex.display(
                "issue" + str(issue_id),
                "assignedto",
                "id",
                "corrVexpl",
                "corrVdev",
                "title",
                "type",
                "produit",
            )
            resp = get_realname(rex, dval["assignedto"])
            prod = get_productname(rex, dval["produit"])
            if prod != "code_aster":
                prod = "salome_meca"
            report.setdefault(prod, [])
            line = [
                f'"{resp}"',
                dval["id"],
                '"x"' if dval["corrVexpl"] else "",
                '"x"' if dval["corrVdev"] else "",
                f'"{dval["title"]}"',
            ]
            if prod == "salome_meca":
                del line[2]
            report[prod].append(line)
            if dval["type"] == "2":
                list_el.append(f"{issue_id} : {dval['title']}")
    for prod in ("code_aster", "salome_meca"):
        lines = report.get(prod)
        print(f'"Produit {prod}"')
        for line in sorted(lines):
            print(";".join(line))

    for line in list_el:
        sys.stderr.write(line + "\n")

    sys.stderr.write("Colonnes 4cm, 1,8cm, 1,6cm, 1,6cm, 8,47cm\n")
