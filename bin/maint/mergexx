#!/bin/bash

dest="asterxx"
upstream="asterxx/upstream"
rev="default"

usage()
{
    echo "usage: mergexx [-h] [--dest=BR] [--upstream=BR] [--rev=REV] TAG"
    echo
    echo "    Update to 'dest', open 'upstream' branch, merge with 'rev' and commit"
    echo "    using the given 'tag'."
    echo
    echo "    By default, the head of '${rev}' is merged with '${dest}' using"
    echo "    the working branch '${upstream}'."
    echo
    exit 1
}

_error()
{
    echo "ERROR: ${@}"
    echo
    usage
}

run_main()
{
    local tag

    OPTS=$(getopt -o hr:d: --long help,dest:,rev: -n $(basename $0) -- "$@")
    if [ $? != 0 ] ; then
        _error "invalid arguments." >&2
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            -h | --help ) usage ;;
            -d | --dest ) dest="$2"; shift ;;
            -u | --upstream ) upstream="$2"; shift ;;
            -r | --rev ) rev="$2"; shift ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
        shift
    done

    [ $# -ne 1 ] && _error "exactly one argument is expected."
    tag="$1"

    if [ $(hg status -ardm | wc -l) -ne 0 ]; then
        _error "uncommitted changes! Please discard or commit first."
    fi

    printf "\nupdating on '${dest}' branch...\n"
    hg update ${dest} || _error "failed"

    printf "\nopening '${upstream}' branch...\n"
    hg branch -f ${upstream} || _error "failed"

    msg="[#23037] merge '${rev}' @ ${tag}."

    printf "\nmerging '${rev}'..."
    hg merge --tool=internal:merge ${rev}
    iret=${?}
    if [ ${iret} -ne 0 ]; then
        echo "${msg}" > .hg/last-message.txt
        _error "failed (commit message saved in '.hg/last-message.txt')"
    fi

    printf "\ncommitting with message '${msg}'...\n"
    hg ci -m "${msg}" || _error "failed"

    return ${?}
}


run_main "${@}"
exit ${?}
