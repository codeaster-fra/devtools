#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options] HIST

Close the issues marked as solved in HIST
"""

import re
from optparse import OptionParser
from subprocess import PIPE, Popen

from api_roundup import mark_as_closed
from api_roundup.utils import read_issue_changelog
from aslint.config import ASCFG
from aslint.i18n import _
from aslint.logger import logger, setlevel
from aslint.string_utils import convert


def close_issue(issues, tag, repo, branch, checktag):
    """Main function"""
    if len(issues) == 0:
        logger.warn(_("no issue to close"))
    if checktag:
        cmd = ["git", "-C", repo, "rev-parse", "--abbrev-ref", branch]
        out = Popen(cmd, stdout=PIPE).communicate()[0]
        branch = convert(out).strip()
    else:
        branch = "main"
    assert branch in ASCFG.get("branches"), branch
    mark_as_closed(issues, tag, re.search("^v[0-9]+$", branch) is not None)


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    parser.add_option("--branch", action="store", help="working branch")
    parser.add_option("--tag", action="store", help="tag to insert in issues")
    parser.add_option("--issue", action="store", help="close a issue giving its id")
    parser.add_option(
        "--no-check",
        action="store_false",
        dest="checktag",
        default=True,
        help="do not check that the tag exists in the branch",
    )
    parser.add_option("--repo", action="store", default=".", help="path to the src repository")
    opts, args = parser.parse_args()
    if len(args) != 1 and not opts.issue:
        parser.error("exactly one argument is required")
    if opts.tag is None:
        parser.error("'--tag' is required")
    if not opts.issue:
        issues = read_issue_changelog(args[0])
    else:
        issues = [opts.issue]
    close_issue(issues, opts.tag, opts.repo, opts.branch, opts.checktag)
