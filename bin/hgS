#!/usr/bin/env python3
# encoding: utf-8

"""hgS [-v/--verbose] [-e CMD/--command=CMD] [cmdargs & cmdoptions]

Execute a mercurial command in several repositories.
It does not work as '--subrepos'. It just loops on a list of directories.

Examples:
    hgS status
    hgS --repo=src log -r -3:
"""

import os
import os.path as osp
from subprocess import Popen, PIPE
from optparse import OptionParser
from glob import glob

VERB = False
def set_verbosity(option, opt, value, parser):
    """Callback for verbose option."""
    global VERB
    VERB = True

def hgcmd(args, cmd, repos):
    """Execute hg `cmd` in the given repositories"""
    larg = [cmd, ]
    for a_i in args:
        if ' ' in a_i:
            a_i = repr(a_i)
        larg.append(a_i)
    if VERB:
        print('Execute : ', ' '.join(larg))
    cwd = os.getcwd()
    for repo in repos:
        repodir = osp.join(cwd, repo)
        if not osp.isdir(repodir):
            print('no such directory:', repodir)
            continue
        os.chdir(repodir)
        cmdi = [cmd, ]
        if cmd == 'mq':
            cmdi = ['hg', '-R', osp.join('.hg', 'patches')]
        cmdi.extend(args)
        if VERB:
            print(osp.basename(os.getcwd()), ':')
        p = Popen(cmdi, close_fds=True)
        p.wait()
    os.chdir(cwd)

if __name__ == '__main__':
    parser = OptionParser(usage=__doc__)
    parser.add_option('-v', '--verbose', action='callback', callback=set_verbosity,
        help="enable additional output")
    parser.add_option('-r', '--repo', action='store', dest='repos',
        default='devtools,data,src,validation',
        help="list of repositories (comma separated)")
    parser.add_option('-e', '--command', action='store', dest='command',
        default='hg',
        help="command name (default: hg)")
    parser.add_option('--mq', action='store_true', help="same as --command=mq")
    parser.disable_interspersed_args()
    opts, args = parser.parse_args()

    cmd = opts.command
    if opts.mq:
        cmd = 'mq'
    if cmd == 'hg' and len(args) < 1:
        parser.error("no argument provided")
    hgcmd(args, cmd, opts.repos.split(','))
