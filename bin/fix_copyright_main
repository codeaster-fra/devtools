#!/usr/bin/env python3
# coding=utf-8

"""
    %prog [options] [filenames]

This script updates the date of the copyright of the last changes.

Example:

- Update the copyright date in all the files you changed since the last
  reference revision::

    fix_copyright $( hg status -n -am --rev refe )
"""

import os.path as osp
import re
import time
from optparse import OptionParser

from aslint.utils import apply_in_sandbox, apply_func_text
from aslint.logger import logger, setlevel
from aslint.baseutils import is_binary

re_copy = re.compile('^(.*COPYRIGHT.*[0-9]{4}.*)[0-9]{4}(.*EDF .*)$', re.M | re.I)


def change_text(txt):
    """Call subfunctions to change 'txt'"""
    minimal = False
    year = time.strftime('%Y')
    txt = re_copy.sub('\g<1>{0}\g<2>'.format(year), txt)
    return txt

def check_ascii_binary(fname):
    """print ascii/binary detection"""
    if not osp.isfile(fname):
        logger.error('not a file: %s', fname)
        return
    typ = is_binary(fname) and 'binary' or 'ascii'
    logger.info('%s: %s', typ, fname)
    if typ == 'ascii':
        apply_func_text(change_text, fname)

def loop_on_files(*args):
    """Apply a function"""
    apply_in_sandbox(check_ascii_binary, args)


if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('files ?')
    loop_on_files(*args)
