#!/usr/bin/env python3
# coding: utf-8

"""usage: definition.py [options] arguments

    This module creates the header file `definition.h` to ease
    the declarations and the arguments passing between C and Fortran
    without regarding the platform::

        gen_definition bibc/* bibcxx/* bibc/include/aster_fort*.h --output=bibc/include/definition.h
"""

import os
import os.path as osp
import re
import time
from optparse import OptionParser
from glob import glob


def prod_def(chaine):
    """Return the such two lines for a string 'DEFPPSP' ::

        #define DEFPPSP(UN,LN,a,b,c,lc,d)   STDCALL(UN,LN)(a,b,c,d,lc)
        #define CALLPPSP(UN,LN,a,b,c,d)     F_FUNC(UN,LN)(a,b,c,d,strlen(c))

    for ASTER_PLATFORM_POSIX and ASTER_PLATFORM_MINGW.
    """
    if "_" in chaine:
        print("IGNORED:", chaine)
        return None

    def add(ch, plus):
        return ",".join(list(ch) + list(plus))

    fmt_def = "#define DEF%(vars)s(UN,LN,%(arg_ini)s)               STDCALL(UN,LN)(%(arg_def)s)"
    fmt_call = "#define CALL%(vars)s(UN,LN,%(arg_simp)s)              %(decalage)sF_FUNC(UN,LN)(%(arg_call)s)"
    fmt_calo = "#define CALL%(varo)s(UN,LN,%(arg_simp)s)              %(decalage)sF_FUNC(UN,LN)(%(arg_calo)s)"
    svar = re.search("([OPSV]+)", chaine).group(0)

    res = []
    for plt in ("ASTER_PLATFORM_POSIX", "ASTER_PLATFORM_MINGW"):
        l_var = list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        d = {
            "vars": svar,
            "varo": svar.replace("S", "O"),
            "arg_ini": [],
            "arg_simp": [],
            "arg_def": [],
            "arg_call": [],
            "arg_calo": [],
        }
        dif_def = []
        dif_call = []
        dif_calo = []
        for ns in svar:
            var = l_var.pop(0)
            s = "%s" % var
            ls = "l%s" % var
            strls = "strlen(%s)" % var
            objls = "(%s).size()" % var
            cptrs = "(%s).c_str()" % var
            for k in ["arg_ini", "arg_simp", "arg_def", "arg_call"]:
                d[k].append(s)
            if ns in ("V", "S"):
                d["arg_ini"].append(ls)
                d["arg_calo"].append(cptrs)
                differed = plt == "ASTER_PLATFORM_POSIX"
                if ns == "V":
                    differed = False
                if differed:
                    dif_def.append(ls)
                    dif_call.append(strls)
                    dif_calo.append(objls)
                else:
                    d["arg_def"].append(ls)
                    d["arg_call"].append(strls)
                    d["arg_calo"].append(objls)
            else:
                d["arg_calo"].append(s)
        d["arg_def"].extend(dif_def)
        d["arg_call"].extend(dif_call)
        d["arg_calo"].extend(dif_calo)
        for k in ["arg_ini", "arg_simp", "arg_def", "arg_call", "arg_calo"]:
            d[k] = ",".join(d[k])
        d["decalage"] = "   " * svar.count("S")
        res.append([fmt_def % d, fmt_call % d, fmt_calo % d])
    return res


def prod_include(fich, variants=None):
    """Build the part of the header file containing the DEF..."""
    variants = set(variants)
    variants.update(
        [
            # if needed
        ]
    )
    withoutStr = [i for i in variants if "S" not in i]
    variants.difference_update(withoutStr)
    variants = sortByLength(variants)
    withoutStr = sortByLength(withoutStr)
    template = """/* -------------------------------------------------------------------- */
/* Copyright (C) 1991 - %(year)s - EDF R&D - www.code-aster.org             */
/* This file is part of code_aster.                                     */
/*                                                                      */
/* code_aster is free software: you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* code_aster is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with code_aster.  If not, see <http://www.gnu.org/licenses/>.  */
/* -------------------------------------------------------------------- */

/* This file should be auto-generated by `gen_definition` devtools script */
// aslint: disable=C3001

#ifndef DEFINITION_H_
#define DEFINITION_H_

#include "aster_depend.h"

/* Pour définir les appels et signatures de fonctions appelables en Fortran
 * On utilise l'operateur de concatenation ## du préprocesseur C (cpp) pour ajouter l'underscore
 * au nom en majuscule ou minuscule de la fonction à définir ou à appeler.
 * Pour les anciens compilateurs non ANSI, utiliser un commentaire vide à la place.
 * Pour appeler une subroutine Fortran de nom SUB avec un argument string et 2 arguments autres,
 * faire: #define CALL_SUB(a,b,c) CALLSPP(SUB,sub,a,b,c) puis : CALL_SUB(a,b,c) Pour définir une
 * fonction C de nom SUB avec un argument string et 2 arguments autres, appelable depuis le fortran,
 * faire: void DEFSPP(SUB, sub, char * nomobj, STRING_SIZE lnom, ASTERDOUBLE *d, ASTERINTEGER *i)
 * {
 * }
 * ici, lnom est l'entier qui indique la longueur de la chaine Fortran nomobj
 * Les macros définies ici ne servent qu'à former le nom de la fonction et à
 * mettre les arguments dans le bon ordre. On utilise l'ordre _WIN32 comme
 * base (pointeur char suivi de la longueur) et on reordonne pour les autres compilateurs.
 * STRING_SIZE est le type retourné par strlen.
 */

/*
 * V is used for a value <type string> returned by a function.
 * The first two arguments are for the return of the function.
 */

/*
 * O is used for a C++ <std::string> object (only for CALLxx).
 * One uses '.char_ptr()' for the pointeur and '.size()' instead of 'strlen'.
 */

/* Appels : avec/sans underscore */
#ifndef ASTER_NO_UNDERSCORE
#define F_FUNC( UN, LN ) LN##_
#else
#define F_FUNC( UN, LN ) LN
#endif

/* http://gcc.gnu.org/onlinedocs/cpp/Stringification.html */
#define xstr( s ) str( s )
/* not with c++ */
#ifndef __cplusplus
#define str( s ) #s
#endif

#define S_FUNC( UN, LN ) xstr( F_FUNC( UN, LN ) )

/* STDCALL for "old" windows version */
#ifdef ASTER_ADD_STDCALL
#define STDCALL( UN, LN ) __stdcall F_FUNC( UN, LN )
#else
#define STDCALL( UN, LN ) F_FUNC( UN, LN )
#endif

// clang-format off
/* Appels et signatures avec strlen en fin de liste */
#ifdef ASTER_STRLEN_AT_END

%(posix)s

/* Appels et signatures avec strlen juste après le pointeur de chaine */
#else

%(win32)s

#endif

/* Appels et signatures sans chaine de caractères */
#define DEF0( UN, LN ) STDCALL( UN, LN )( void )
#define CALL0( UN, LN ) F_FUNC( UN, LN )()
%(commun)s

// clang-format on
#endif
"""
    l_posix = []
    l_win32 = []
    l_comm = []
    for flag in variants:
        lines = prod_def(flag)
        if not lines:
            continue
        lx, lw = lines
        l_posix.extend(lx)
        l_win32.extend(lw)

    for flag in withoutStr:
        if "O" in flag:
            continue
        lines = prod_def(flag)
        if not lines:
            continue
        lx, lw = lines
        l_comm.extend(lx)

    txt = template % {
        "year": time.strftime("%Y"),
        "posix": os.linesep.join(l_posix),
        "win32": os.linesep.join(l_win32),
        "commun": os.linesep.join(l_comm),
    }
    print("writing definitions into:", fich)
    with open(fich, "w") as fobj:
        fobj.write(txt)


def get_def(*l_rep):
    """Search the DEFxxx/CALLxxx usages in the files/directories in argument"""
    txt = []
    for rep in l_rep:
        lfiles = []
        if osp.isdir(rep):
            lfiles.extend(glob(osp.join(rep, "*.h")))
            lfiles.extend(glob(osp.join(rep, "*.c")))
            lfiles.extend(glob(osp.join(rep, "*.cxx")))
        elif osp.isfile(rep):
            lfiles.append(rep)
        for f in lfiles:
            if osp.basename(f) == "definition.h":
                continue
            with open(f, "r") as fobj:
                txt.extend(fobj.read().splitlines())
    txt = os.linesep.join(txt)
    expr = re.compile("(?:CALL|DEF)([OPSV]+) *\(", re.MULTILINE)
    s_def = set(expr.findall(txt))
    return s_def


def sortByLength(sequence):
    """Sort a list/set of strings, starting with the shorter ones"""
    lv = list(sequence)
    llong = map(len, lv)
    l_tmp = sorted(zip(llong, lv))
    lv = [j for i, j in l_tmp]
    return lv


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-v",
        "--verbose",
        dest="verbose",
        action="store_true",
        default=None,
        help="show more details.",
    )
    parser.add_option(
        "-o",
        "--output",
        dest="output",
        action="store",
        default="bibc/include/definition.h",
        help="the output file for '--definition'.",
    )

    opts, l_args = parser.parse_args()
    if len(l_args) == 0:
        print(
            "no argument provided, use default directories:",
            "bibc/* bibcxx/* bibc/include/aster_fort*.h...",
        )
        l_args.extend(glob("bibc/*"))
        l_args.extend(glob("bibcxx/*"))
        l_args.extend(glob("bibc/include/aster_fort*.h"))
    if not opts.output:
        parser.error("--output is required to write the definition file")
    variants = get_def(*l_args)
    if opts.verbose:
        variants = sortByLength(variants)
        for v in variants:
            print("    '%s'," % v)
    prod_include(opts.output, variants)
