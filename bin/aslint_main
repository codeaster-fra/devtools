#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options] [filenames]
    %prog [options] --repo [REV]

aslint is a code checker for Code_Aster source files.

For fortran source files, most of the job is done by analyzing the errors and
warnings by gfortran.
It adds some checkings of some coding conventions for Code_Aster.
"""
# because it's a main program
# pylint: disable=C0103

import os
import os.path as osp
from optparse import SUPPRESS_HELP, OptionParser
from subprocess import call

from aslint.base_checkers import Report, checkcontext
from aslint.check_files import check_files, read_waf_parameters
from aslint.config import ASCFG
from aslint.filetype import filter_arguments
from aslint.logger import logger, setlevel
from repository_api import (
    get_list_of_changed_files,
    get_main_branch,
    get_repo_name,
    is_repository,
)

if __name__ == "__main__":
    default_builddir = None
    default_target = "release"
    if os.environ.get("BUILD") == "debug":
        default_builddir = "build/mpidebug" if osp.isdir("build/mpidebug") else "build/mpi"
        default_target = "debug"
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    parser.add_option("--hg", action="store_true", dest="repo", help=SUPPRESS_HELP)
    parser.add_option("--git", action="store_true", dest="repo", help=SUPPRESS_HELP)
    parser.add_option(
        "--repo",
        action="store_true",
        dest="repo",
        help="check files changed in the repository since REV (since HEAD by default)",
    )
    parser.add_option(
        "--build",
        action="store",
        default=default_builddir,
        metavar="DIR",
        help="waf build directory containing configuration parameters",
    )
    parser.add_option(
        "--target",
        action="store",
        default=default_target,
        help="configuration: release or debug",
    )
    parser.add_option(
        "--skip-global",
        action="store_true",
        default=False,
        help="skip global checkings (like error messages)",
    )
    parser.add_option("-s", "--skip_global", action="store_true", help=SUPPRESS_HELP)
    parser.add_option(
        "--skip-format",
        dest="format",
        action="store_false",
        default=True,
        help="skip source formating checkings",
    )
    parser.add_option(
        "-f",
        "--force-build-module",
        dest="use_cached_module",
        action="store_false",
        default=True,
        help="force rebuild of fortran modules",
    )
    parser.add_option(
        "-j",
        "--jobs",
        action="store",
        default=None,
        metavar="N",
        type=int,
        help="number of jobs to run simultaneously",
    )
    parser.add_option(
        "--fix",
        action="store_true",
        default=False,
        help="execute the fixers if any",
    )
    parser.add_option(
        "--reponame",
        action="store",
        help='set the repository name (one of "src", "data" or "validation"), '
        "to be used when the repository has not been fully cloned.",
    )
    opts, args = parser.parse_args()
    if opts.repo:
        rev = "HEAD"
        if len(args) == 1:
            rev = args[0]
        elif len(args) > 1:
            parser.error("at most one argument expected with --repo")
        output = get_list_of_changed_files(".", (rev,))
        args = [i.split()[1] for i in output.splitlines()]

    if not is_repository("."):
        logger.error("please restart from the repository root directory.", exit=True)

    ctg = checkcontext
    ctg.reponame = opts.reponame or get_repo_name(".")
    ctg.branch = get_main_branch(".")
    if not opts.format:
        ctg.disable(ctg.FormatOpt)
    if not opts.use_cached_module:
        ctg.disable(ctg.UseCachedModule)

    if len(args) == 0:
        parser.error("which files ?")
    # flags = opts.flag or []
    # flags.extend(['-Wimplicit-interface', '-Wintrinsic-shadow'])
    builddir = opts.build or ASCFG.get("waf.builddir")
    filtered_args = filter_arguments(args, status="M")
    wafc4che = osp.join(builddir, "c4che", opts.target + "_cache.py")
    c4che = read_waf_parameters(wafc4che)
    ASCFG.set("waf.builddir", builddir)
    ASCFG.set("waf.c4che", c4che)
    report = Report()
    logger.update_errcode(
        check_files(report, filtered_args, c4che, skip_global=opts.skip_global, numthread=opts.jobs)
    )
    logger.add_cr()
    logger.info(report.to_text())
    report.report_summary()
    if opts.fix and report.hasError():
        # run the fixers
        dcmd = report.getFixerCommands()
        for eid, cmd in list(dcmd.items()):
            logger.warn("fixing error {0}...".format(eid))
            call(cmd, shell=True)
    logger.exit()
