#!/bin/bash

usage()
{
    echo "usage: waf_build_children [--debug] [--configure] [--std|--mpi] [-j/--jobs N] [--fast]"
    echo "  Build all revisions that follow the current one"
    echo "  (always takes the first children if there are branches)."
    exit 1
}

_error()
{
    echo "ERROR: ${@}"
    echo
    usage
}

run_main()
{
    local variant=""
    local debug=0
    local safe=1
    local action="install"
    declare -a opts
    opts=( "-p" )

    OPTS=$(getopt -o hcgj: --long help,configure,debug,std,mpi,jobs:,fast \
                  -n $(basename $0) -- "$@")
    if [ $? != 0 ] ; then
        _error "invalid arguments." >&2
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            -h | --help ) usage ;;
            -c | --configure ) action="configure ${action}" ;;
            -g | --debug ) debug=1 ;;
            -j | --jobs ) opts=( "${opts[@]}" "-j" "$2" ); shift ;;
            --fast ) safe=0 ;;
            --std ) variant="_std" ;;
            --mpi ) variant="_mpi" ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
        shift
    done

    [ ${debug} -eq 1 ] && action=$(sed 's/install/install_debug/' $(echo ${action}))

    local from=$(hg log -r . --template '{rev}:{node|short}')
    local to=$(hg log -r "last(descendants(.))" --template '{rev}:{node|short}')
    echo "building from ${from} to ${to}..."
    echo "using: waf${variant} ${action}..."

    echo "starting from:"
    hg parent

    if [ $(hg branch) != "v14" ]; then
        if [ ${safe} -eq 1 ]; then
            opts+=( "--safe" )
        else
            opts+=( "--fast" )
        fi
    fi
    iret=0
    while [ $iret -eq 0 ]
    do
        #  and .::tip
        ./waf${variant} ${action} "${opts[@]}" && hg up 'children(.)' && hg parent
        iret=$?
    done
}

run_main "${@}"
exit ${?}
