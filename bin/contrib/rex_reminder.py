#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options]

This script sends reminder emails to the developers for issues without activity
since more than 30 days.
It also sends a global email to the managers (by team).
"""

import os

from aslint.logger import logger, setlevel
from aslint.i18n import _
from aslint.config import ASCFG
from aslint.string_utils import convert
from hgaster.ext_utils import sendmail
import api_roundup.stats as ST

DEBUG = False
DEBUG_ADDRESS = "mathieu.courtois@edf.fr"

FILENAME = "rex_all_issues.pick"
DELAY_AL = 1 * 30

SUBJECT = "[Rex] Relance des fiches sans activité"

HEADER = """
Bonjour,

Veuillez trouver ci-dessous la liste des fiches d'anomalie dont vous êtes
responsable et qui n'ont pas été mises à jour depuis plus de {DELAY_AL} jours.
"""

FOOTER = """
Les fiches d'anomalies, qu'elles soient la conséquence de développements récents
ou bien présentes depuis longtemps dans le code, sont un indicateur essentiel de
la qualité du code.

Il est donc important que ces fiches soient tenues à jour par leur responsable,
ne serait-ce que pour établir un premier diagnostic et notamment identifier
rapidement le risque de résultats faux, qui fera alors l'objet d'une
communication dans la fiche de suivi qualité livrée avec chaque version de
code_aster, y compris si l'anomalie n'a pas pu être corrigée.

Nous comptons donc sur vous pour la correction des anomalies identifiées le plus
rapidement possible afin de stabiliser la prochaine version de code_aster dans
les meilleures conditions.

Cordialement,
Équipe de développement de code_aster & salome_meca

"""

HEADER_MANAGER = """
Bonjour,

Veuillez trouver ci-dessous la liste des fiches d'anomalie qui n'ont pas été
mises à jour depuis plus de {DELAY_AL} jours et dont le responsable est
rattaché à votre groupe.
"""

SEPAR = "----------"

MANAGERS = [
    ('T61', 'lasoroski'),
    ('T62', 'junker'),
    ('T63', 'foucault'),
    ('T64', 'sefta'),
    ('T65', 'levy'),
    ('T66', 'autrusson')
]


def getDeveloperIssues(issues, developer):
    """Return the text of the issues assigned to a developer"""
    todo = issues.filter_equal('assignedto', developer)
    ids = sorted(todo.keys())
    text = []
    for id in ids:
        text.append('')
        text.append(issues[id].summary_reminder())
    return os.linesep.join(text)

def getTeamIssues(issues, list_dvp):
    """Return the text of issues for a list of developers"""
    text = []
    for dvp in list_dvp:
        text.append(SEPAR)
        text.append("Fiches affectées à {realname} ({username}):".format(**users[dvp]))
        text_dvp = getDeveloperIssues(issues, dvp)
        if not text_dvp:
            continue
        text.append(text_dvp)
        text.append('')
    return os.linesep.join(text)

def userList(issues):
    """Read the infos from the REX for users that are responsible of issues"""
    list_dvp = issues.getValues('assignedto')
    users = ST.EntityList()
    for dvp in list_dvp:
        logger.debug(_("reading user {0}...").format(dvp))
        users[dvp] = ST.download_user(dvp)
    return users

def remind(issues, users, do_send=True):
    """Remind the developers for the unsolved issues"""
    logger.info("{0:6} anomalies sans activité depuis {1} jours" \
                .format(len(issues), DELAY_AL))
    logger.info("{0:6} développeurs concernés".format(len(users)))

    dict_info = { 'DELAY_AL': DELAY_AL }
    header = HEADER.format(**dict_info)
    for dvp, user in list(users.items()):
        logger.info(_("reminding user {0}...").format(dvp))
        mail_content = [header + FOOTER, SEPAR]
        mail_content.append("Liste des fiches :")
        text = getDeveloperIssues(issues, dvp)
        if not text:
            continue
        mail_content.append(text)
        mail_content.append('')
        msg = os.linesep.join([convert(i) for i in mail_content])
        if not do_send:
            logger.info(msg)
        recipients = [user['address'], ]
        if DEBUG:
            recipients = DEBUG_ADDRESS
        sendmail(recipients, SUBJECT, msg,
                 fromaddr=ASCFG.get('notify.codeaster'),
                 footer=False,
                 dry_run=not opts.sendmail)
        if DEBUG:
            break

def remind_manager(issues, users, do_send=True):
    """Remind the group managers for the unsolved issues in their team"""
    dict_info = { 'DELAY_AL': DELAY_AL }
    header = HEADER_MANAGER.format(**dict_info)
    for team, resp in MANAGERS:
        manager = ST.download_user(resp)
        logger.info(_("Groupe {0} ({1} <{2}>)").format(team,
                    manager['realname'].encode('utf-8'), manager['address']))
        list_dvp = users.filter_regexp('organisation', team)
        logger.info("{0:6} développeurs concernés".format(len(list_dvp)))
        if not list_dvp:
            continue
        mail_content = [header]
        s = "s" if len(list_dvp) > 1 else ""
        mail_content.append("Il y a {0} développeur{s} concerné{s} :" \
                            .format(len(list_dvp), s=s))
        for dvp in list_dvp:
            mail_content.append("    - {realname}".format(**users[dvp]))
        mail_content.append(FOOTER)
        text = getTeamIssues(issues, list_dvp)
        logger.info(text)
        mail_content.append(text)
        recipients = [manager['address'], ]
        if DEBUG:
            recipients = DEBUG_ADDRESS
        msg = os.linesep.join([convert(i) for i in mail_content])
        sendmail(recipients, SUBJECT, msg,
                 fromaddr=ASCFG.get('notify.codeaster'),
                 footer=False,
                 dry_run=not opts.sendmail)


if __name__ == '__main__':
    from optparse import OptionParser
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    parser.add_option('--developer', action='store_true',
                      help="send reminder email to the developpers")
    parser.add_option('--manager', action='store_true',
                      help="send reminder email to the managers")
    parser.add_option('--sendmail', action='store_true',
                      help="really send emails (by default, only print addresses)")
    opts, args = parser.parse_args()

    issues = ST.read_dict(FILENAME)
    issues.add_delay_reminder()
    issues = issues.filter_not_equal('projet', 'Bonnes idées') & \
             (issues.filter_equal('produit', 'code_aster') | \
              issues.filter_equal('produit', 'Documentation')) & \
             issues.filter_equal('status', 'enregistre')
    all_al = issues.filter_equal('type', 'anomalie')
    remind_al = all_al.filter_greater('_delay', DELAY_AL)
    users = userList(remind_al)

    if opts.developer:
        remind(remind_al, users, opts.sendmail)
    if opts.manager:
        remind_manager(remind_al, users, opts.sendmail)
