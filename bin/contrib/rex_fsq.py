#!/usr/bin/env python3
# coding: utf-8

"""
This script helps to write the AQ documents (FQ/FSQ):
- by extracting the list of issues with possible wrong results,
- by preparing tables of published and newly created documents.
"""


import os
import os.path as osp
import pickle
from optparse import OptionParser
from functools import partial

from aslint.logger import logger, setlevel
from aslint.i18n import _
import api_roundup.stats as ST


# labels des versions "précédentes" (histors dans lesquels chercher)
INFOS = {"vexpl": "16.6", "vdev": "17.1"}

FILENAME = "rex_all_issues.pick"
CHANGELOG_PATTERN = osp.join(
    os.environ["HOME"], "dev", "codeaster", "changelog", "histor", "v*", "[0-9].*"
)


def is_fixed(vers, issue):
    """Tell if the issue is fixed in V + 'vers'."""
    return (issue["verCorrV" + vers] or "").strip().startswith(INFOS["v" + vers])


fixed_dev = partial(is_fixed, "dev")
fixed_expl = partial(is_fixed, "expl")


def is_wrong(vers, issue):
    """Tell if the issue is wrong in V + 'vers'."""
    return (issue["fauxV" + vers] or "").strip() != ""


wrong_dev = partial(is_wrong, "dev")
wrong_expl = partial(is_wrong, "expl")


def get_wrong_results(fname):
    """Build the list of issues with possible wrong results"""
    assert osp.exists(FILENAME), "can be extracted by bin/contrib/stats.py"
    issues = ST.read_dict(FILENAME)
    closure = ST.extract_closure(CHANGELOG_PATTERN)
    issues.add_delay(closure)
    closed = (
        issues.filter_equal("status", "ferme")
        | issues.filter_equal("status", "attente_doc")
        | issues.filter_equal("status", "valide_EDA")
        | issues.filter_equal("status", "sans_suite")
    )
    wrong = []
    for id, issue in list(closed.items()):
        if (fixed_dev(issue) or fixed_expl(issue)) and (wrong_dev(issue) or wrong_expl(issue)):
            wrong.append(id)
            logger.debug("issue with wrong results: {0}".format(id))
    with open(fname, "w") as fobj:
        fobj.write(os.linesep.join(wrong))


def build_csv(fname):
    """Build a CSV file from the dict of published documents"""

    def fdate(datet):
        return "%02d/%02d/%d" % (datet.day, datet.month, datet.year)

    def upper_name(name):
        if len(name) < 2:
            return name
        return name[0].upper() + name[1:]

    assert osp.exists(fname), "can be copied from docaster@aster-doc:public_html/"
    with open(fname, "rb") as fo:
        dicpub = pickle.load(fo)

    lines = [(key, val["title"], val["modification"]) for key, val in list(dicpub.items())]
    fmt = '"%s";"%s";"%s"'
    csv1 = [fmt % l for l in lines]
    csv1.sort()
    content1 = os.linesep.join(csv1)

    lines = [
        (key, val["title"], val["creation"], upper_name(val["responsible"]))
        for key, val in list(dicpub.items())
    ]
    fmt = '"%s";"%s";%s;"%s"'
    csv2 = [fmt % l for l in lines]
    csv2.sort()
    content2 = os.linesep.join(csv2)
    return content1, content2, dicpub


def responsible_csv(fname):
    """Build a file containing the responsible of the documents"""
    with open(fname, "rb") as fo:
        dicpub = pickle.load(fo)

    lines = [(key, val["responsible"], val["title"]) for key, val in list(dicpub.items())]
    fmt = '"%s";"%s";"%s"'
    csv = [fmt % l for l in lines]
    csv.sort()
    content = os.linesep.join(csv)
    return content


# inutile: cf. created_docs_XXX.csv
def listed_doc(pickname, docname):
    """Build a csv file containing the documents listed in `docname`."""
    with open(pickname, "rb") as fo:
        dicpub = pickle.load(fo)

    with open(docname, "r") as fobj:
        dockeys = fobj.read().split()
    lines = []
    for key in dockeys:
        dval = dicpub.get(key)
        if not dval:
            print("unknown document: %s" % key)
        else:
            lines.append((key, dval["responsible"], dval["title"]))
    fmt = '"%s";"%s";"%s"'
    csv = [fmt % l for l in lines]
    csv.sort()
    content = os.linesep.join(csv)
    return content


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    parser.add_option(
        "--branch", dest="branch", action="store", help="branch of the qualified version"
    )
    parser.add_option(
        "--histor",
        dest="histor",
        action="store_true",
        help="generate list of issues with wrong results",
    )
    parser.add_option(
        "--doc", dest="doc", action="store_true", help="generate list of published documents"
    )

    opts, l_args = parser.parse_args()

    if opts.histor:
        get_wrong_results("list_fiches")

    if opts.doc:
        assert opts.branch, "use --branch=... !"
        csv1, csv2, dicpub = build_csv("published_docs_%s.pick" % opts.branch)
        with open("published_docs_%s.csv" % opts.branch, "w") as fobj:
            fobj.write(csv1)
        with open("created_docs_%s.csv" % opts.branch, "w") as fobj:
            fobj.write(csv2)
