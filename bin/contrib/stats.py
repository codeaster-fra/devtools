#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options] action [args...]

Create a report of statistics extracted from REX.

Available actions are:

    %prog download [filename]

        This will extract the data from REX and write a pickled file containing
        the list of the issue dicts.
        Default filename: ./rex_all_issues.pick

    %prog show [filename]

        Print the report on stdout.

    python -i ./bin/contrib/%prog read [filename]
    >>> type(issues)
    <type 'dict'>
    >>> issues['23456']
    ...
    >>> issues['23456']['status']
    'ferme'

        Read a pickled file containing the REX data.
        Objects available: issues
"""

import os
import os.path as osp

from aslint.logger import logger, setlevel
from aslint.i18n import _
import api_roundup.stats as ST


YEAR = "2016"
VDEV = 13
VEXPL = VDEV - 1
FILENAME = "rex_all_issues.pick"
CHANGELOG_PATTERN = osp.join(os.environ['HOME'], "dev", "codeaster",
                             "histor", "1[0-9].*")


if __name__ == '__main__':
    from optparse import OptionParser
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    parser.add_option('--changelog', action='store', dest='chlog',
                      default=CHANGELOG_PATTERN, metavar='PATH',
                      help=("pattern of the changelogs to read, "
                            "(default: {0})").format(CHANGELOG_PATTERN))
    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('at least argument is required')

    fname = FILENAME if len(args) < 2 else args[1]
    if args[0] == 'download':
        ST.download(fname, 'issue')
    elif args[0] in ('read', 'show'):
        issues = ST.read_dict(fname)
        closure = ST.extract_closure(opts.chlog)
        issues.add_delay(closure)
    else:
        parser.error('unknown action: {0}'.format(args[0]))
    # here is the "main" report creation...
    if args[0] == 'show':
        types = ('all', 'anomalie', 'anomalie-vdev', 'evolution', 'aide utilisation')
        logger.title("Fiches code_aster émises en {0}".format(YEAR))
        logger.add_cr()
        last = issues.filter_regexp('creation', '^' + YEAR) & \
               issues.filter_equal('produit', 'code_aster')
        for typ in types:
            ST.summary( last.filter_type(typ), "Type = {0}".format(typ) )

        logger.title("contenant FORUM dans le titre")
        forum = last.filter_regexp('title', 'FORUM')
        for typ in types:
            ST.summary( forum.filter_type(typ), "Type = {0}".format(typ) )

        # initial filter: only code_aster or all salome_meca components
        if True:
            for_stats = issues.filter_equal('produit', 'code_aster')
        else:
            for_stats = issues.filter_equal('produit', 'code_aster') | \
                        issues.filter_equal('produit', 'astk') | \
                        issues.filter_equal('produit', 'salome_meca') | \
                        issues.filter_equal('produit', 'devtools') | \
                        issues.filter_equal('produit', 'Rex') | \
                        issues.filter_equal('produit', 'Site web') | \
                        issues.filter_equal('produit', 'Appli doc') | \
                        issues.filter_equal('produit', 'Documentation')

        logger.add_cr()
        logger.title("Fiches code_aster soldées en {0}".format(YEAR))
        aster = for_stats.filter_regexp('_closure', '^' + YEAR)
        solved = aster.filter_equal('status', 'ferme') | \
                 aster.filter_equal('status', 'attente_doc') | \
                 aster.filter_equal('status', 'valide_EDA') | \
                 aster.filter_equal('status', 'sans_suite')

        for typ in types:
            by_type = solved.filter_type(typ)
            in_vexpl = by_type.not_empty('verCorrVexpl')
            only_vexpl = by_type.empty('verCorrVdev') & by_type.not_empty('verCorrVexpl')
            only_vdev = by_type.not_empty('verCorrVdev') & by_type.empty('verCorrVexpl')
            no_changelog = by_type.empty('verCorrVdev') & by_type.empty('verCorrVexpl')
            emit_vexpl = by_type.filter_regexp('version', '^{0}'.format(VEXPL))
            emit_vdev = by_type.filter_regexp('version', '^{0}'.format(VDEV))

            logger.add_cr()
            logger.title("Type = {0}".format(typ))
            ST.summary( by_type, "Toutes" )
            ST.summary( in_vexpl, "Restituées en version d'exploitation" )
            ST.summary( only_vexpl, "Restituées uniquement en version d'exploitation" )
            ST.summary( only_vdev, "Restituées uniquement en version de développement" )
            ST.summary( no_changelog, "Non restituées (ou soldées à la main)" )
            ST.summary( emit_vexpl, "Émises en version {0}".format(VEXPL) )
            ST.summary( emit_vdev, "Émises en version {0}".format(VDEV) )

        logger.add_cr()
        logger.title("Par développeur")
        for typ in types:
            by_type = solved.filter_type(typ)
            ST.developer_stats(by_type, "Type = {0}".format(typ))

        logger.add_cr()
        logger.title("Par projet contributeur")
        for typ in types:
            by_type = solved.filter_type(typ)
            ST.project_stats(by_type, "Type = {0}".format(typ))
        # apply a ratio: el / (el + al-vdev) on days of work
        evol = solved.filter_type("evolution").deepcopy()
        fixes = solved.filter_type("anomalie-vdev")
        nbday = ST.sum_days(evol)
        nbdayf = ST.sum_days(fixes)
        for i in list(evol.values()):
            i['nbJours'] = ST.tofloat(i['nbJours']) / nbday * (nbday + nbdayf)
        ST.project_stats(evol, "Type = evolution + repart. anomalie-vdev")

        logger.add_cr()
        logger.title("Par intervenant extérieur")
        ST.extern_stats(solved)

        logger.add_cr()
        logger.title("Fiches Documentation")
        doc = issues.filter_less('creation', str(int(YEAR) + 1)) & \
              issues.filter_regexp('_closure', '^' + YEAR) & \
              issues.filter_equal('produit', 'Documentation')
        ST.summary( doc, "" )

        logger.add_cr()
        logger.title("Fiches salome_meca")
        smeca = issues.filter_less('creation', str(int(YEAR) + 1)) & \
                issues.filter_regexp('_closure', '^' + YEAR) & \
                issues.filter_equal('produit', 'salome_meca')
        for typ in types:
            by_type = smeca.filter_type(typ)
            ST.summary(by_type, "Type = {0}".format(typ))
        ST.project_stats(smeca.filter_type("evolution"), "Type = evolution salome_meca")

        logger.add_cr()
        logger.title("Fiches Europlexus")
        epx = issues.filter_less('creation', str(int(YEAR) + 1)) & \
              issues.filter_regexp('_closure', '^' + YEAR) & \
              issues.filter_equal('produit', 'Europlexus')
        for typ in types:
            by_type = epx.filter_type(typ)
            ST.summary(by_type, "Type = {0}".format(typ))

        # issues with a lot of days of work
        logger.add_cr()
        logger.title("Fiches avec NbJours > 50.")
        big = ST.EntityList(dict([(id, issue) for id, issue in list(solved.items()) \
                                 if ST.tofloat(issue['nbJours']) > 50.]))
        ST.summary(big, "")
        for issue in list(big.values()):
            logger.info( issue.str() + os.linesep )
