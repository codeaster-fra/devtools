# coding=utf-8

"""
This package gives agnostic minimal support of Git/Mercurial repositories.

Only the needed feature have been added here.
"""

import re
from itertools import chain

from . import hg
from . import ui_config as UI
from .git_utils import REPOID as GITREPOID
from .git_utils import _diffstatus
from .hg_utils import REPOID as HGREPOID
from .hg_utils import hg_get_rev_log, hg_rev, hgcmd
from .utils import repocmd, repository_switch, repository_type


def is_repository(path, type=None):
    """Tell if `path` contains a repository.

    Arguments:
        path (str): Directory to be tested.
        type (str): Expected repository type: "git", "hg" or None for either of these.

    Returns:
        bool: *True* if the directory is a repository of expected type, *False* otherwise.
    """
    try:
        repotype = repository_type(path)
    except TypeError:
        return False
    return not type or type == repotype


@repository_switch
def get_rev_log(repotype, repopath, rev="INIT", fmt=None):
    """Return the hash of the revision 'rev'.

    Arguments:
        repopath (str): Path to repository.
        rev (str): Revision or revisions range (default: INIT, means first revision).
        fmt (str): Output format.
    """
    # we should adopt Git defaults/notations/syntax and translate for Hg when necessary
    if repotype == "hg":
        return hg_get_rev_log(repopath, rev, fmt)
    if repotype == "git":
        fmt = fmt or "%H"
        args = []
        if rev == "INIT":
            rev = "HEAD"
            args.append("--max-parents=0")
        if not _is_range(rev):
            args.append("--max-count=1")
        args.append(rev)
        try:
            out = repocmd(repopath, "log", format=fmt, *args)[1]
        except Exception:
            out = None
        return out


def _is_range(rev):
    """Tell if 'rev' argument represents a revisions range."""
    return ".." in rev


@repository_switch
def get_main_branch(repotype, repopath):
    """Return the branch : dev or expl"""
    if repotype == "hg":
        branch = repocmd(repopath, "branch")[1]
        if branch == "default":
            branch = "main"
        return branch
    if repotype == "git":
        current = get_branch(repopath)
        if current == "main" or re.search("^(v[0-9]+)$", current):
            return current
        out = repocmd(repopath, ("log", "--first-parent", "--format=%D"))[1]
        out = out.replace("HEAD -> ", "")
        out = list(chain.from_iterable([ref.split(",") for ref in out.split("\n") if ref.strip()]))
        parent_refs = [ref.strip() for ref in out]
        branch = "main"
        for ref in parent_refs:
            if ref == current:
                continue
            if ref == "main" or re.search("^(v[0-9]+)$", ref):
                branch = ref
                break
        return branch


@repository_switch
def get_repo_name(repotype, repopath):
    """Return the name of the repository (using its id).
    Returns 'None' if it is not known."""
    repoid = GITREPOID if repotype == "git" else HGREPOID
    return repoid.get(get_rev_log(repopath), None)


@repository_switch
def get_repo_by_rev(repotype, repopath):
    """Return the name of the repository (using its id).
    Returns 'None' if it is not known."""
    repoid = GITREPOID if repotype == "git" else HGREPOID
    for rev, name in list(repoid.items()):
        if get_rev_log(repopath, rev) == rev:
            return name
    return None


@repository_switch
def has_uncommitted_changes(repotype, repopath, unknown=True, ignored=False):
    """Tell if the repository has uncommitted changes"""
    if repotype == "hg":
        lst = repocmd(
            repopath,
            "status",
            "-n",
            modified=True,
            added=True,
            removed=True,
            deleted=True,
            unknown=unknown,
            ignored=ignored,
        )[1]
        return lst.strip().split()
    if repotype == "git":
        opts = []
        if not unknown:
            opts.append("-uno")
        if ignored:
            opts.append("--ignored")
        st = repocmd(repopath, "status", "--porcelain", *opts)[1]
        lst = [line[1] for line in st.strip().splitlines()]
        return lst


@repository_switch
def parent_is_last(repotype, repopath, branch="main"):
    """Check that the parent revision is the newest one in the given branch."""
    # not relevant in all repositories
    if repotype == "hg":
        if branch == "main":
            branch = "default"
        last = get_rev_log(repopath, rev="last({})".format(branch))
        par = get_rev_log(repopath, rev="parents()")
    if repotype == "git":
        last = repocmd(repopath, "rev-list", "--max-count=1", branch)[1]
        par = get_rev_log(repopath, rev="HEAD")
    return par == last


@repository_switch
def get_parent_desc(repotype, repopath):
    """Return the short description of the parent revision"""
    if repotype == "hg":
        return get_rev_log(repopath, rev="parents()", fmt="{node|short}: {desc|firstline}")
    if repotype == "git":
        return repocmd(repopath, "log", "--format=oneline", "--abbrev-commit", "-1", "HEAD")[1]


@repository_switch
def get_description(repotype, repopath, rev1, rev2):
    """Return the list of the description of each revision
    between `rev1` and `rev2`"""
    if repotype == "hg":
        fmt = "_DELIMITER_{desc}\n"
        rev1, rev2 = hg_rev(rev1), hg_rev(rev2)
    if repotype == "git":
        fmt = "_DELIMITER_%B"
    revrange = "{rev1}..{rev2}".format(rev1=rev1, rev2=rev2)
    output = get_rev_log(repopath, rev=revrange, fmt=fmt)
    descr = [txt for txt in output.split("_DELIMITER_") if txt]
    return descr


@repository_switch
def get_reference_head(repotype, repopath, branch=None, source=None):
    """Return the current integrated node in the branch"""
    if repotype == "hg":
        branch = branch or "."
        if branch == "main":
            branch = "default"
        source = source or "codeaster"
        rset = "last(branch({0}) and not outgoing('{1}') and not secret())".format(branch, source)
        return get_rev_log(repopath, rset)
    if repotype == "git":
        branch = branch or "$(git rev-parse --abbrev-ref HEAD)"
        source = source or "origin/main"
        out = repocmd(repopath, "merge-base", source, branch)
        return out[1]


@repository_switch
def get_branch(repotype, repopath):
    """Return the current branch"""
    if repotype == "hg":
        return get_rev_log(repopath, ".", fmt="{branch}")
    if repotype == "git":
        return repocmd(repopath, "rev-parse", "--abbrev-ref", "HEAD")[1]


@repository_switch
def get_list_of_changed_files(repotype, repopath, revs=None, files=(), cached=False):
    """Return the list of changed files between two commits.

    Git: unknown files not shown.

    Arguments:
        repopath (str): Path to repository.
        revs (list): Revisions range.
        files (list): Limit search to these paths.
        cached (bool): Only show cached files (only for git).
    """
    args = list(files)
    if repotype == "hg":
        if revs:
            revs = [hg_rev(i) for i in revs]
            args = ["--rev={}".format(hg_rev(i)) for i in revs] + args
        return repocmd(repopath, "status", *args)[1]
    if repotype == "git":
        if revs:
            args = list(revs) + args
        if cached:
            args.insert(0, "--cached")
        return _diffstatus(repocmd(repopath, "diff", "--name-status", *args)[1])
