# coding=utf-8

"""Utilities for Mercurial emulator."""

from collections import OrderedDict

from .utils import getcmd, repocmd, shell_cmd

# identifier of the repositories (hash of revision 0 or of the revision that
# opens the branch for derivated repository)
REPOID = OrderedDict(
    [
        ("08ee4973b18cc37bc89a3ed356b91397a352b9c1", "data"),
        ("becdb3e46d91de04ce69a240b714ec9df3a50826", "src"),
        ("8918a03f384fdd666260a381d5dad564ec0a177d", "validation"),
        ("d9ffe8b5560b5799cf9821b9c08d0f6f41497dee", "devtools"),
        ("ee137aec1fe656351ca17b42751f2a0de5f157f0", "salome-codeaster-study"),
        ("550c13b26fd780e034e54d79200f5ec7511456a4", "salome-meca-rtool"),
    ]
)


def hg_rev(rev):
    if rev == "INIT":
        rev = "0"
    if rev == "HEAD":
        rev = "."
    return rev


def hg_get_rev_log(repopath, rev="INIT", fmt=None):
    """Return the hash of the revision 'rev'.

    Arguments:
        repopath (str): Path to repository.
        rev (str): Revision or revisions range (default: INIT, means first revision).
        fmt (str): Output format.
    """
    rev = hg_rev(rev)
    if ".." in rev:
        rng = rev.split("..")
        rev = "::".join(rng) + " and not " + rng[0]
    opts = {"rev": rev, "template": fmt or "{node}\n"}
    try:
        out = repocmd(repopath, "log", **opts)[1]
    except Exception:
        out = None
    return out


def hgcmd(cmd, repopath, *args, **opts):
    """Return the output of `cmd`.

    Returns:
        (int, str): Exit code and output of the command.
    """
    if not repopath:
        assert cmd == "clone", cmd
        return shell_cmd(getcmd("hg", repopath, cmd, *args, **opts))
    return repocmd(repopath, cmd, *args, **opts)
