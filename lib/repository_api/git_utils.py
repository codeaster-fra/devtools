# coding=utf-8

"""Utilities for Git emulator."""

import os
from collections import OrderedDict

# identifier of the repositories (hash of first revision or of the revision that
# opens the branch for derivated repository)
REPOID = OrderedDict(
    [
        ("f65ef1d91e3d2c9df041455aedaff8863f44b925", "data"),
        ("a7edf000d1e71a9733b3200428dca51bc382992f", "src"),
        ("9a41c310ac687bbb86be73b3a19a787188bf26da", "validation"),
        ("9de6906b157d67f4a3cf23e14f51cfa5fc8d480d", "devtools"),
    ]
)


def _diffstatus(output):
    """Convert `git diff --name-status` output.

    Arguments:
        output (str): Multilines output.

    Returns:
        str: Converted output: lines with 2 columns "STATUS FILENAME"
    """
    conv = []
    for line in output.splitlines():
        spl = line.split()
        status = spl.pop(0)
        if "R" in status:
            # git returns R + perc of change
            status = "R"
        conv.append(" ".join([status, spl.pop(0)]))
    return os.linesep.join(conv)
