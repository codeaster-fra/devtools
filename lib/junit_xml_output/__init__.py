#!/usr/bin/env python

# Source: https://bitbucket.org/db_atlass/python-junit-xml-output-module

# The MIT License (MIT)

# Copyright (c) 2013 Atlassian Corporation Pty Ltd

# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import xml.etree.ElementTree as ET
import xml.dom.minidom

__version__ = "1.0.3"


class JunitXml(object):

    """ A class which is designed to create a junit test xml file.
        Note: currently this class is designed to return the junit xml file
        in a string format (through the dump method).
    """

    def __init__(self, testsuit_name, test_cases, total_tests=None,
                 total_failures=None):
        self.testsuit_name = testsuit_name
        self.test_cases = test_cases
        self.failing_test_cases = self._get_failing_test_cases()
        self.total_tests = total_tests
        self.total_failures = total_failures
        if total_tests is None:
            self.total_tests = len(self.test_cases)
        if total_failures is None:
            self.total_failures = len(self.failing_test_cases)
        self.root = ET.Element("testsuite",
                               {"name": str(self.testsuit_name),
                                "failures": str(self.total_failures),
                                "tests": str(self.total_tests)
                               })
        self.build_junit_xml()

    def _get_failing_test_cases(self):
        return set([case for case in self.test_cases if
                    case.is_failure()])

    def build_junit_xml(self):
        """ create the xml tree from the given testsuite name and
            testcase
        """
        for case in self.test_cases:
            test_case_element = ET.SubElement(self.root,
                                              "testcase",
                                              {"name": str(case.name)})
            if case.is_failure():
                failure_element = ET.Element("failure")
                failure_element.text = case.contents
                test_case_element.append(failure_element)

    def dump(self, pretty=True):
        """ returns a string representation of the junit xml tree. """
        out = ET.tostring(self.root)
        if pretty:
            dom = xml.dom.minidom.parseString(out)
            out = dom.toprettyxml()
        return out


class TestCase(object):

    """ A junit test case representation class.
            The JunitXml accepts a set of these and uses them to create
        the junit test xml tree
    """

    def __init__(self, name, contents, test_type=""):
        self.name = name
        self.contents = contents
        self.test_type = test_type

    def is_failure(self):
        """ returns True if this test case is a 'failure' type """
        return self.test_type == "failure"
