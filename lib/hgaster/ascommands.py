# coding=utf-8

"""commands of the Code_Aster extension"""

import os
import os.path as osp
import sys
import tempfile
from functools import partial
from subprocess import call

from api_doc import changelog_doc
from api_roundup import check_status, get_connection, get_documents
from aslint.api_asrun import check_asrun
from aslint.base_checkers import Context, Report, checkcontext
from aslint.check_files import (
    c4che_ismpi,
    check_diff,
    check_files,
    read_waf_parameters,
    run_waf_command,
)
from aslint.check_inst import check_install
from aslint.config import ASCFG, SMCFG, TOOLCFG
from aslint.decorators import (
    allow_cancel_error,
    allow_cancel_error_testcases,
    log_as_important,
    stop_on_failure,
)
from aslint.filetype import filter_dict, filter_names
from aslint.i18n import _
from aslint.logger import ErrorCode, logger
from aslint.string_utils import convert
from aslint.utils import convert_to_addr, open_editor
from repository_api import (
    get_description,
    get_reference_head,
    get_repo_name,
    has_uncommitted_changes,
    parent_is_last,
)

from .ext_utils import (
    check_for_updates,
    get_changed_files,
    get_diff,
    get_first_new_rev,
    get_hgpath,
    get_issues_from_descr,
    get_reference_ancestor,
    get_solved_issues,
    sendmail,
    shortrev,
    shortrev_s,
)
from .hooks.codeaster import repo_branch_checker as codeaster_branch_checker
from .hooks.generic import OK
from .request_queue import QueueError, Request, RequestQueue, requestkey

requestActions = [
    "start",
    "startall",
    "check",
    "test",
    "testall",
    "close",
    "closeall",
    "refuse",
    "differ",
    "cancel",
    "status",
]


MAX_MESSAGE_SIZE = int(1e6)

HEADER_DIFF = _(
    """
The developer (username=%(dvp)s, address=%(address)s) submitted a
request for integration of the revision %(wrkrev)s in the Code_Aster repository.

Related changests are:
%(related_issues)s

You are registered as the person in charge of the following files.

If you don't agree with these changes, you must warn the integrators and the
developer not to integrate them as soon as possible.

URL of the submitted changeset: %(url)s/rev/%(wrkrev)s

Note that its ancestors are also part of the changes.

If you want to rebuild this entire diff file in your local repository, you have
to pull this revision and type:

    hg pull --rev %(wrkrev)s %(url)s
    hg diff --rev %(refrev)s --rev %(wrkrev)s
"""
)

SUBMIT_TEMPLATE = _(
    """
Important messages of the submission:

%(msg)s

Full log content was written to: %(logf)s
"""
)

SEPAR = """
-----
"""


class CandidateRevisionCheck(object):
    """Offer several unit command to check a revision, to submit it,
    to integrate it

    Attributes:
        ui, repo: ui and repository objects passed from mercurial
        reponame: name of the repository
        wrkrev: revision to work on
        branch: branch name
        _queue: RequestQueue object
        refrev: reference revision
        dvp: username of the developer
        _report: Report object of the checkings
        _changes: dict of changes by type between refrev and wrkrev
        c4che: waf cache directory where to read parameters
    """

    _interrupt_message = _("command interrupted because of errors")
    _continue_message = _("do you want to continue")
    _cancel_message = _("command cancelled")

    def __init__(self, ui, repo, *args, **opts):
        """Initialization"""
        self._name = "not_set"
        self.ui = ui
        self._notify_on_error = False
        self.repo = repo
        self._dry = opts.get("dry_run")
        self._local = opts.get("local")
        self._refancestor = opts.get("rev")
        self.wrkrev = None
        self.reponame = None
        self.wrkbranch = None
        self.refbranch = None
        self._srcdir = None
        self._queue = None
        self.refrev = None
        self.dvp = self.get_cfg("username")
        self._report = None
        self._changes = None
        self.c4che = None
        self._issues_descr = "unknown"

    def get_cfg(self, key, default=None):
        """Get a value of a parameter from the configuration."""
        # exceptions for backward compatibility - force 'std' build
        if self.refbranch == "v15":
            if key == "waf.bin":
                return "waf_std"
            if key == "waf.builddir":
                return "build/std"
        return ASCFG.get(key, default)

    def _dump_log(self):
        """Dump the log content into a file"""
        return logger.write_log(self._name, path=osp.dirname(self.repo.root))

    def _error(self, message):
        """Log the message error and exit with a non-null status."""
        logger.error(message)
        self._dump_log()
        logger.error(self._interrupt_message, exit=True)

    def check_error(self):
        """Interrupt the process if an error occurred"""
        if not logger.is_error():
            return
        if self._notify_on_error and not self._local:
            sendmail("user", self._interrupt_message)
        logger.error(self._interrupt_message, exit=True)

    def confirm(self, question=None):
        """Ask confirmation"""
        if self._dry:
            logger.warn(_("skip confirmation"))
        else:
            question = (question or self._continue_message) + " (y/n)? "
            answ = self.ui.prompt(question, default="n")
            if answ.lower() not in ("y", "o"):
                self._error(self._cancel_message)

    def set_working_revision(self):
        """Set the working revision and branch"""
        self.wrkrev = self.repo["."]
        self.wrkbranch = self.wrkrev.branch()
        self.refbranch = self.refbranch or self.wrkbranch
        checkcontext.branch = self.refbranch

    def set_parameters(self, need_changes=True):
        """Set the parameters of the repository"""
        try:
            self.reponame = get_repo_name(self.repo.root)
            checkcontext.reponame = self.reponame
        except KeyError:
            if not self.get_cfg("admin._test"):
                self._error(_("not in a Code_Aster repository: {0}").format(self.repo.root))
        if self._refancestor:
            if self._name == "submit" and not self._local:
                self._error(_("'--rev' must only be used with '--local'"))
            self.refrev = self._refancestor
        else:
            if not self._local:
                self.refrev = self.get_reference_revision()
                self.get_from_revision(need_changes)
        if not self._local:
            if not self.valid_branch():
                self._error(
                    _("integration is not allowed in the branch '{0}'").format(self.wrkbranch)
                )
        if not self.refrev:
            self._error(
                _(
                    "without remote connection (--local enabled), "
                    "you must provide the last ancestor revision "
                    "already integrated using --rev"
                )
            )
        if need_changes and self.wrkrev.hex() == self.refrev:
            self._error(_("You are submitting the reference revision!"))
        if self.reponame == "src":
            srcdir = self.repo.root
        else:
            srcdir = osp.join(osp.dirname(self.repo.root), "src")
        if not osp.join(srcdir):
            self._error(_("source repository not found: '{0}'").format(srcdir))
        self._srcdir = srcdir

    def init_report(self):
        """Initialize the Report object"""
        context = Context()
        context.username = self.dvp
        context.repository = self.reponame
        context.submission = True
        self._report = Report(context)

    def init_queue(self, notify=None):
        """Create the RequestQueue object"""
        if self._queue is not None:
            return
        if notify is None:
            notify = not self._dry
        try:
            self._queue = RequestQueue(notify=notify)
            self._queue.init()
        except QueueError as exc:
            logger.error(exc.message)
        self.check_error()

    def check_children(self):
        """Check for children of the working revision"""
        if self.wrkrev.children():
            logger.warn(_("the submitted revision has children"))

    def check_uncommitted(self):
        """Check for uncommitted changes"""
        if has_uncommitted_changes(self.repo.root):
            if not self.get_cfg("admin._test"):
                self._error(_("there are uncommitted changes. Please commit or revert first."))
            else:
                logger.warn(_("uncommitted changes, skipped in TEST mode"))
        else:
            untracked = has_uncommitted_changes(self.repo.root, unknown=True)
            if untracked:
                logger.warn(_("there are untracked files:"))
                print(" " + "\n ".join(untracked))
                self.confirm()
                logger.cancel_warning()
            unknown = has_uncommitted_changes(self.repo.root, ignored=True, unknown=True)
            orig = [i for i in unknown if i.endswith(".orig") or i.endswith(".rej")]
            if orig:
                logger.warn(_("\nthere are '.orig/.rej' files:"))
                logger.info(" " + "\n ".join(orig))
                logger.info(_("the '.orig' files may cause errors during checkings"))
                self.confirm("please confirm the deletion of these files")
                for name in orig:
                    os.remove(name)
                logger.cancel_warning()

    def check_repositories_updates(self):
        """Check for updates of the required repositories"""
        if not self._local:
            opts = dict(source=self.get_cfg("admin_pull_uri"), branch=self.refbranch)
            _check_for_updates(self.reponame, self.repo.root, **opts)
            self.check_error()
        else:
            logger.warn(_("skipping check for updates"))

    @stop_on_failure
    @log_as_important
    def build_changes_dict(self):
        """changes with reference revision"""
        wrkrev = self.wrkrev
        fromrev = self.refrev
        revs = [fromrev, wrkrev.hex()]
        strfrom = shortrev_s(fromrev)
        strrev = str(wrkrev.rev()) + ":" + shortrev(wrkrev)
        if wrkrev != self.repo["."]:
            self._error(
                _(
                    "run 'hg update {0}' to update the working directory "
                    "to the revision to integrate"
                ).format(wrkrev.rev())
            )
        logger.title(_("getting changes between revisions '{0}' and '{1}'").format(strfrom, strrev))
        dchg = get_changed_files(self.repo.root, revs)
        if len(dchg["!"]):
            # should not happen after has_uncommitted_changes() checkings
            self._error(
                _(
                    "there are missing files (deleted by non-hg "
                    "command, but still tracked, "
                    "check with 'hg status')"
                )
            )
        if dchg["msg"]:
            logger.info(dchg["msg"])
        self._changes = filter_dict(dchg)
        return logger.errcode

    @stop_on_failure
    @allow_cancel_error
    @log_as_important
    def check_message_descr(self):
        """Check the commits description"""
        wrkrev = self.wrkrev
        fromrev = self.refrev
        strfrom = shortrev_s(fromrev)
        strrev = str(wrkrev.rev()) + ":" + shortrev(wrkrev)
        logger.title(
            _("checking descriptions between revisions '{0}' and '{1}'").format(strfrom, strrev)
        )
        descr = get_description(self.repo.root, strfrom, wrkrev.hex())
        if len(descr) == 0:
            logger.error(_("there is no revision to submit"))
        for msg in descr:
            _check_description(msg)
        return logger.errcode

    @log_as_important
    def report_on_new(self):
        """limit the report to the new files"""
        logger.add_cr()
        logger.title(_("report limited to the added files"))
        context = Context()
        context.username = self.dvp
        context.repository = self.reponame
        report = Report(context)
        new = []
        for flist in list(self._changes.values()):
            new.extend(filter_names(flist, "A"))
        for fname, msglist in self._report.with_msg_iter():
            if fname in new:
                report.set(fname, msglist)
        logger.info(report.to_text())
        report.report_summary()
        return report

    def accept_special_issue(self, issue_id):
        """Allow to accept issue even if the status is not as expected."""
        return False

    @stop_on_failure
    @allow_cancel_error
    @log_as_important
    def check_issues_status(self, expected="valide_EDA", logfunc=logger.error):
        """checking the status of the solved issues"""
        wrkrev = self.wrkrev
        fromrev = self.refrev
        revs = [fromrev, wrkrev.hex()]
        strfrom = shortrev_s(fromrev)
        strrev = str(wrkrev.rev()) + ":" + shortrev(wrkrev)
        logger.title(_("getting solved issues between '{0}' and '{1}'").format(strfrom, strrev))
        if self._local:
            logger.warn(_("status of issues can not be checked in --local mode"))
            return logger.errcode
        lrex, lbb, descr = get_solved_issues(self.repo.root, *revs, with_output=True)[1:]
        # keep issues short descr for notification
        self._issues_descr = descr
        with get_connection() as rex:
            for num in lrex:
                issue_id = "issue" + str(num)
                issue = rex.display(issue_id)
                check_status(
                    issue_id,
                    issue,
                    expected,
                    rex,
                    verbose=True,
                    force=self.accept_special_issue(num),
                    logfunc=logfunc,
                )
        for num in lbb:
            logger.warn(_("please ensure that the issue #%d is solved on bitbucket"), num)
        if not logger.is_error():
            self.check_expected_documents(lrex)
        return logger.errcode

    @stop_on_failure
    @allow_cancel_error
    @log_as_important
    def check_expected_documents(self, issues):
        """Check that expected documents have been committed.

        Arguments:
            list[int]: List of issues numbers.
        """
        logger.title(_("check that expected documents have been committed"))
        with get_connection() as rex:
            for num in issues:
                docs = get_documents(str(num), rex)
                if not docs:
                    logger.info(_("no changes needed for #{0}").format(num))
                for key in docs:
                    try:
                        descr = " ".join(changelog_doc(key))
                    except ValueError as exc:
                        descr = ""
                    done = get_issues_from_descr(descr, bracket=False)
                    msg = _("issue #{0} needs changes in {1}: {2}")
                    if num in done:
                        logger.info(msg.format(num, key, _("found")))
                    else:
                        logger.error(
                            msg.format(num, key, _("issue number not found in document history"))
                        )
        if logger.is_error():
            logger.error(
                _(
                    "\n"
                    "You have to mention the issue id with '[#nnnn]' "
                    "when you are committing a new revision of a "
                    "document. If you mispelled the issue number just "
                    "add a minor change (add a line feed for example). "
                    "Note that you have to wait at least 10 minutes "
                    "for the last changes are synced.\n"
                    "Please do not continue without contacting the "
                    "administrators."
                )
            )
            self.confirm()
            logger.cancel_error()
        return logger.errcode

    @stop_on_failure
    @log_as_important
    def push_revision(self, dest):
        """Push the working revision"""
        if not self._local:
            act = "push"
            if self._dry:
                act = "outgoing"
            logger.title(_("pushing the revision %s to the remote server"), self.wrkrev.hex())
            srev = str(self.wrkrev.rev())
            iret = call([self.get_cfg("hg_bin"), act, "--force", "--rev", srev, dest])
            if iret == 0:
                logger.info(_("push ended successfully"))
            else:
                logger.warn(_("hg push exits with code %s"), iret)
                logger.warn(_("no revision pushed, maybe already there"))
                logger.warn(
                    _("Please force the push by using:") + "\n" + "hg push --force --rev %s %s",
                    srev,
                    dest,
                )
        else:
            logger.warn(_("the revision is not push to the remote server"))
        return logger.errcode

    def check_init(self):
        """Execute the elementary tasks"""
        try:
            ASCFG.check_init()
        except AssertionError as exc:
            self._error(_("incorrect configuration:\n%s") % str(exc))
        if self.get_cfg("admin._test"):
            logger.warn(_("running is TEST mode"))
            self.confirm()

    def get_reference_revision(self):
        """Return the reference revision."""
        refrev = get_reference_ancestor(
            self.repo.root, self.wrkrev, self.refbranch, self.get_cfg("admin_pull_uri")
        )
        if not refrev:
            logger.error(_("can not find the last integrated revision"))
            logger.info(
                _(
                    "This often happens if the url of the 'codeaster' "
                    "repository is not defined.\nUsing 'hg paths' you should "
                    "see a line starting with 'codeaster = ...'."
                )
            )

            script = osp.join(ASCFG.get("devtools_root"), "bin", "install_env")
            logger.info(_("If not, run:\n %s --no-build\nand try again." % script))
            self._error(_("reference revision not found"))
        return refrev

    def get_from_revision(self, need_changes):
        """Return the revision of the first changes."""
        # check that 'wrkrev' is a descendant of 'headrev'
        headrev = get_reference_head(self.repo.root, self.refbranch, self.get_cfg("admin_pull_uri"))
        if not headrev:
            self._error(_("can not find the last reference head"))
        ref = self.repo[headrev]
        if self.wrkrev.ancestor(ref) != ref:
            logger.warn(_("The submitted revision is not a descendant of the reference head."))
            self.confirm()
        fromrev = get_first_new_rev(self.repo.root, self.wrkrev, self.get_cfg("admin_pull_uri"))
        if need_changes and not fromrev:
            self._error(_("can not find the revision of the first changes"))
        logger.info(_("first changes from {0}").format(fromrev))
        return fromrev

    def check_single_head(self):
        """Check for single head in branch."""
        from .hooks.generic import single_head_per_branch_hook

        checker = single_head_per_branch_hook()
        return checker(self.repo, self.wrkrev) == OK

    def check_branch(self, repo, branch):  # pragma pylint: disable=unused-argument
        """Check branch validity."""
        raise NotImplementedError("must be subclassed")

    def valid_branch(self):
        """Check branch validity."""
        return self.check_branch(self.repo, self.wrkbranch) and self.check_single_head()


class CandidateRevisionCheckAster(CandidateRevisionCheck):
    """Abstract class for code_aster repositories."""

    def check_repositories_updates(self):
        """Check for updates of the required repositories"""
        if not self._local:
            dev = "devtools"
            branch = "default"
            _check_for_updates(dev, update=True, branch=branch)
            if not parent_is_last(self.get_cfg("devtools_root")):
                logger.error(
                    (_("you can execute:") + " cd {0} && hg up -C {2} && cd {1}").format(
                        self.get_cfg("devtools_root"), os.getcwd(), branch
                    )
                )
                self._error(
                    _("'{0}' must be updated to the last revision of branch '{1}'").format(
                        dev, branch
                    )
                )
            opts = dict(source=self.get_cfg("admin_pull_uri"), branch=self.refbranch)
            _check_for_updates(self.reponame, self.repo.root, **opts)
            if self.reponame != "src":
                _check_for_updates("src", self._srcdir, **opts)
            self.check_error()
        else:
            logger.warn(_("skipping check for updates"))

    @stop_on_failure
    def check_source_files(self, askconfirm=True):
        """checking files"""
        prev = os.getcwd()
        os.chdir(self.repo.root)
        check_files(
            self._report, self._changes, self.c4che, srcdir=self.repo.root, dry_run=self._dry
        )
        logger.add_cr()
        self.check_diff_files()
        logger.add_cr()
        logger.info(self._report.to_text())
        self._report.report_summary()
        os.chdir(prev)
        if not askconfirm:
            logger.cancel_error()
            return logger.errcode
        if logger.is_warn():
            logger.warn(
                _(
                    "warnings have been emitted during check, try to fix "
                    "most of them before continuing"
                )
            )
            self.confirm()
        elif logger.is_error():
            logger.error(_("Errors have been emitted during check, you must fix them."))
            if self.get_cfg("check-submit", "error") == "warn":
                self.confirm()
                logger.cancel_error()
        return logger.errcode

    def check_diff_files(self):
        """checking the diff files"""
        logger.title(_("checking the diff of files"))
        prev = os.getcwd()
        os.chdir(self.repo.root)
        wrkrev = self.wrkrev
        refrev = self.refrev
        revs = [refrev, wrkrev.hex()]
        diffs = {}
        size = 0
        for couples in self._changes.values():
            size += len(couples)
        block = max(1, size / 100)
        if block > 1:
            logger.info(
                _("extracting the changes of {0} files ('.' represents {1} files)").format(
                    size, int(block)
                )
            )
        else:
            logger.info(_("extracting the changes of {0} files ").format(size))
        step = 1 if not logger.debug_enabled() else 100
        i = 0
        for typ, chg in list(self._changes.items()):
            lfiles = filter_names(chg, status="M")
            diffs[typ] = {}
            for fname in lfiles:
                # print(i, fname)
                i += 1
                if i > step * block:
                    step += 1
                    print(".", end="")
                    sys.stdout.flush()
                adiff = get_diff(self.repo.root, revs, [fname])
                diffs[typ][fname] = adiff
        print()
        check_diff(self._report, diffs)
        os.chdir(prev)

    @stop_on_failure
    def check_waf_build(self):
        """Check waf build"""
        logger.title(_("building the binaries using waf"))
        have_mpi = c4che_ismpi(self.c4che)
        if self.refbranch == "v15":
            if have_mpi:
                self._error(
                    _(
                        "a sequential version is necessary in 'v15' branch; "
                        "'waf.builddir' is forced to 'std'."
                    )
                )
        elif not have_mpi:
            self._error(
                _("a parallel version is necessary; use 'waf.builddir' to select another build.")
            )
        prefix = self.c4che["PREFIX"]
        ipref = osp.abspath(prefix).split(os.sep, 2)[1]
        isrc = osp.abspath(self._srcdir).split(os.sep, 2)[1]
        opts = []
        if ipref != isrc and not self.c4che["install_tests"]:
            logger.info(
                _("source and installation directories seem to be on different filesystems.")
            )
            opts.append("--install-tests")
        opts.append("--safe")
        return run_waf_command(
            self._srcdir,
            osp.join(self._srcdir, self.get_cfg("waf.bin")),
            "install",
            options=opts,
            env=self.get_cfg("waf.env"),
            dry_run=self._dry,
        )

    def init_waf_param(self):
        """Read and store the waf parameters"""
        wafc4che = osp.join(self._srcdir, self.get_cfg("waf.builddir"), "c4che", "release_cache.py")
        logger.debug("waf c4che: %s", wafc4che)
        self.c4che = read_waf_parameters(wafc4che)
        if not self.c4che:
            self._error(
                _(
                    "no such file: '{0}', please run '{1} configure'. "
                    "If you are using a specific version you can "
                    "adjust the 'waf.builddir' parameter in the [aster] "
                    "section of your ~/.gitconfig file."
                ).format(wafc4che, self.get_cfg("waf.bin"))
            )

    @stop_on_failure
    def check_waf_install(self):
        """Check waf installation"""
        return check_install(self.c4che, dry_run=self._dry)


class AbstractSubmit(CandidateRevisionCheck):
    """Check code_aster source files at the parent revision
    and, in case of success, submit a request for integration.

    Attributes:
        command_automerge (list): Command (as list) for automerge checking.
        command_lint (list): Command (as list) for source files checking.
        command_tests (list[list]): List of commands (each one as list) for
            running unittests, the first item is the title.
    """

    _interrupt_message = _("submission interrupted because of errors")
    _cancel_message = _("submission cancelled")
    command_automerge = None
    command_lint = None
    command_tests = None

    def __init__(self, ui, repo, *args, **opts):
        """Initialization"""
        super().__init__(ui, repo, *args, **opts)
        self._name = "submit"
        self._notify_on_error = True
        if len(args) > 0:
            self._error(_("no argument expected"))

    @stop_on_failure
    def check_existing_request(self):
        """check if there is already a submitted request"""
        if self._local:
            logger.warn(
                _("no request for integration will be submitted, only check the source files")
            )
            return logger.errcode
        if not self.dvp:
            logger.error(
                _(
                    "you must fill the 'name' parameter in the "
                    "[user] section of your ~/.gitconfig file"
                )
            )
        self.init_queue(notify=False)
        self.check_error()
        reqkey = requestkey(self.reponame, self.wrkbranch, self.dvp)
        req = self._queue.get(reqkey)
        if req is not None:
            logger.warn(
                _("you have already submitted a request for integration of the revision %s"),
                req.shortnode,
            )
            if req.status != "PEND":
                logger.error(
                    _(
                        "the integration is started, you must contact "
                        "the administrators to cancel it"
                    )
                )
            else:
                logger.warn(
                    _("if you continue, the previously submitted revisions won't be integrated.")
                )
                self.confirm()
                logger.cancel_warning()
                if not self._dry:
                    logger.update_from_status(self._queue.cancel(reqkey))
                else:
                    logger.info(_("(DRY RUN) cancelling the submission"))
        req = self._queue.get_by_node(self.wrkrev.hex())
        if req is not None:
            logger.error(
                _("this revision has already been submitted by another developer (%s)"),
                req.username,
            )
        return logger.errcode

    @stop_on_failure
    def submit_revision(self):
        """Submit the integration request"""
        if self._queue is not None:
            addr = convert_to_addr(self.get_cfg("notify.user"))
            addr = addr and addr[0] or ""
            req = Request(
                self.reponame, self.wrkbranch, self.dvp, None, self.wrkrev.hex(), email=addr
            )
            if not self._dry:
                iret = self._queue.submit(req)
                if iret != 0:
                    logger.error(_("PLEASE FORWARD THIS MESSAGE TO AN ADMINISTRATOR"))
            else:
                logger.info(_("(DRY RUN) submit request for %s"), req.username)
        else:
            logger.warn(_("your request for integration has not been submitted"))
        return logger.errcode

    @stop_on_failure
    def notify_responsible(self):
        """Notify the responsible of the changed files"""
        """Notify the responsible of the changed files,
        *temporarly disabled*."""
        logger.warn(_("do not send notification to persons in charge"))
        return logger.errcode

    @allow_cancel_error
    def _check_command(self, title, command):
        """Execute a shell command"""
        if not command:
            return logger.errcode
        errcode = ErrorCode()
        logger.add_cr()
        logger.title(title)
        scmd = command
        if isinstance(command, (list, tuple)):
            scmd = " ".join(command)
        logger.debug(_("Executing: {0}").format(scmd))
        returncode = call(command)
        errcode.from_status(returncode)
        if errcode.is_ok():
            logger.info(_("'{0}' ended successfully").format(title))
        else:
            logger.error(_("'{0}' failed.\nCommand line: {1}").format(title, command))
        return logger.errcode

    @stop_on_failure
    @log_as_important
    def check_automerge(self):
        """Check merge without conflict."""
        return self._check_command(_("checking for automatic merge"), self.command_automerge)

    @stop_on_failure
    def check_source_files(self, askconfirm=True):
        """checking files"""
        return self._check_command(_("checking for source files"), self.command_lint)

    @stop_on_failure
    @allow_cancel_error_testcases
    @log_as_important
    def check_testcase_run(self):
        """running testcases"""
        cmds = self.command_tests
        if not cmds:
            return logger.errcode
        elif not isinstance(cmds[0], list):
            cmds = [cmds]
        for cmd in cmds:
            self._check_command(cmd[0], cmd[1:])
            if logger.is_error():
                break
        return logger.errcode

    def end_message(self):
        """The end"""
        if self._local:
            logger.warn(_("only check files has been done"))
        elif not self._dry:
            if not logger.is_ok():
                logger.warn(_("the integration may be rejected because of previous alarms"))
            else:
                logger.info(_("the integration should be accepted"))
        logf = self._dump_log()
        if not self._local:
            subj = _("[submit@{{host}}] request for integration submitted ({0}/{1}) {2}").format(
                self.reponame, self.wrkbranch, shortrev(self.wrkrev)
            )
            sendmail(
                ["user", "admins"],
                subj,
                SUBMIT_TEMPLATE % {"msg": logger.get_text(), "logf": logf},
                #  attach=logf,
                dry_run=self._dry,
            )

    def run(self):
        """Execute the elementary tasks"""
        super().check_init()
        self.set_working_revision()
        self.set_parameters()
        logger.title(
            _("asking integration in branch '{0}' of the repository '{1}'").format(
                self.wrkbranch, self.reponame
            )
        )
        logger.info(_("reference revision is %s"), self.refrev[:12])
        self.check_uncommitted()
        self.check_children()
        self.check_repositories_updates()
        self.check_automerge()
        self.check_message_descr()
        self.check_issues_status()
        self.init_report()
        self.check_source_files()
        self.check_testcase_run()
        self.push_revision(dest=self.get_cfg("admin_ci_uri"))
        self.notify_responsible()
        self.end_message()
        return 0


class SubmitAster(CandidateRevisionCheckAster, AbstractSubmit):
    """Check code_aster source files at the parent revision
    and, in case of success, submit changesets for continuous integration.
    """

    command_tests = None  # defined by constructor

    def __init__(self, ui, repo, *args, **opts):
        """Initialization"""
        super().__init__(ui, repo, *args, **opts)

        resudir = opts.get("resutest") or tempfile.mkdtemp(prefix="resutest_")
        self.command_tests = [
            [
                _("check build of html documentation"),
                "./check_docs.sh",
                "--waf",
                osp.abspath(self.get_cfg("waf.bin")),
            ],
            [
                _("running testcases"),
                "run_testcases",
                "--root=..",
                "--testlist=submit",
                "--add-sequential",
                "--builddir",
                self.get_cfg("waf.builddir"),
                "--resutest",
                resudir,
                "--clean",
            ],
        ]

    @staticmethod
    def check_single_head():
        """Check single head in branch."""
        # multiple heads allowed in code_aster legacy repositories
        return True

    def check_branch(self, repo, branch):
        """Check branch validity."""
        return codeaster_branch_checker(branch) == OK

    @stop_on_failure
    @log_as_important
    def check_testcase_run(self):
        """running testcases"""
        if self.reponame != "src":
            return logger.errcode
        return AbstractSubmit.check_testcase_run(self)

    def run(self):
        """Execute the elementary tasks"""
        super().check_init()
        self.set_working_revision()
        self.set_parameters()
        logger.title(
            _("asking integration in branch '{0}' of the repository '{1}'").format(
                self.wrkbranch, self.reponame
            )
        )
        logger.info(_("reference revision is %s"), self.refrev[:12])
        if self.reponame != "src":
            logger.warn(
                _(
                    "please check that the current working directory "
                    "of 'src' is consistent with this repository "
                    "because it will be used to run testcases"
                )
            )
            self.confirm()
            logger.cancel_warning()
        check_asrun()
        self.check_uncommitted()
        self.check_children()
        self.check_repositories_updates()
        self.check_existing_request()
        self.build_changes_dict()
        self.check_message_descr()
        self.check_issues_status()
        self.init_waf_param()
        self.check_waf_build()
        self.check_waf_install()
        self.init_report()
        self.check_source_files()
        self.check_testcase_run()
        self.push_revision(dest=self.get_cfg("admin_ci_uri"))
        self.submit_revision()
        self.notify_responsible()
        self.end_message()
        return 0


class SubmitDevtools(AbstractSubmit):
    """Check devtools source files at the parent revision
    and, in case of success, submit changesets for continuous integration.
    """

    command_automerge = ["./share/test/check_automerge.sh"]
    command_tests = [_("check coverage"), "./share/test/check_tests.sh", "--coverage"]

    def __init__(self, ui, repo, *args, **opts):
        """Initialization"""
        super().__init__(ui, repo, *args, **opts)
        self.refbranch = "default"

    @classmethod
    def get_cfg(self, key, default=None):
        """Get a value of a parameter from the configuration."""
        return TOOLCFG.get(key, default)

    def check_issues_status(self, expected="valide_EDA", logfunc=None):
        """checking the status of the solved issues"""
        return super().check_issues_status(expected, logfunc=logger.warn)

    def check_branch(self, dummy, branch):
        """Check branch validity."""
        from .hooks.devtools import repo_branch_checker

        return repo_branch_checker(branch) == OK


class AbstractSubmitSmeca(AbstractSubmit):
    """Minimal 'submit' command for some salome_meca tools (only push changes)."""

    def __init__(self, ui, repo, *args, **opts):
        """Initialization"""
        super().__init__(ui, repo, *args, **opts)
        self.refbranch = "edf/default"

    @classmethod
    def get_cfg(self, key, default=None):
        """Get a value of a parameter from the configuration."""
        return SMCFG.get(key, default)


class SubmitAsterStudy(AbstractSubmitSmeca):
    """Check AsterStudy source files at the parent revision
    and, in case of success, submit changesets for continuous integration.
    """

    command_automerge = ["./dev/check_automerge_edf.sh"]
    command_lint = ["./dev/check_lint.sh", "-p"]
    command_tests = [
        [_("check coverage"), "./dev/check_salome.sh", "-c"],
        [("check documentation"), "./dev/check_docs.sh"],
    ]

    @staticmethod
    def check_branch(dummy, branch):
        """Check branch validity."""
        from .hooks.asterstudy import repo_branch_checker

        return repo_branch_checker(branch) == OK


class SubmitRTool(AbstractSubmitSmeca):
    """Submit command for some salome_meca RTOOL plugin."""

    @staticmethod
    def check_branch(dummy, branch):
        """Check branch validity."""
        from .hooks.rtool import repo_branch_checker

        return repo_branch_checker(branch) == OK


class Integration(CandidateRevisionCheckAster):
    """Set of elementary commands to manage the requests for integration

    scenarii for integration:
    - for a single developer, merged revs to linear history
    - merge and linear two heads of submitted revisions
    """

    _interrupt_message = _("integration process interrupted because of errors")
    _cancel_message = _("integration cancelled")

    def __init__(self, ui, repo, *args, **opts):
        """Initialization"""
        super().__init__(ui, repo, *args, **opts)
        self._name = "request"
        self._notify_on_error = False
        act = None
        for i in requestActions:
            if opts.get(i):
                act = i
        if act in ("start", "differ", "cancel", "test", "close", "refuse"):
            if len(args) != 1:
                self._error(_("exactly one argument is expected"))
        elif act in ("check", "startall", "testall", "closeall"):
            if len(args) > 0:
                self._error(_("no argument expected"))
        elif act in ("status",):
            pass
        else:
            self._error(
                _("unsupported action: '%s' (one of --status, --check... is required)") % act
            )
        self._main = getattr(self, act)
        self.args = list(args)
        self._details = opts.get("message") or None
        if not self._details and act not in ("start", "status", "check"):
            self._details = open_editor()

    def check_branch(self, repo, branch):
        """Check branch validity."""
        return codeaster_branch_checker(branch) == OK

    @staticmethod
    def check_single_head():
        """Check single head in branch."""
        # multiple heads allowed in code_aster legacy repositories
        return True

    def _call_queue_func(self, func):
        """call ``func()`` on the revision passed in arguments"""
        rev = self.args[0]
        req = self._queue.get_by_node(rev)
        if req is None:
            logger.error(_("no request for this revision '%s'"), rev)
        else:
            iret = func(req.key)
            logger.update_from_status(iret)
            if logger.is_error():
                fmsg = tempfile.NamedTemporaryFile(prefix="msg.").name
                with open(fmsg, "w") as fobj:
                    fobj.write(self._details)
                logger.warn(_("message written into: %s"), fmsg)
        self.check_error()

    def _loop_on_requests(self, status, func, repo=None, title=None):
        """Loop on requests in the given status and apply 'func' on each one
        If repo is not None, keep only requests in this repository."""
        self.init_queue()
        for req in self._queue:
            if req.status == status and repo in (None, req.repo) and req.branch == self.wrkbranch:
                self.args.append(req.node)
        while len(self.args):
            rev = self.args[0][:12]
            if title:
                logger.info(title, rev)
            func()
            self.args.pop(0)

    def start(self):
        """Mark a revision as started, its status is changed to RUN"""
        self.init_queue()
        func = partial(self._queue.start, details=self._details)
        self._call_queue_func(func)
        rev = self.args[0][:12]
        src = self.get_cfg("admin_push_uri")
        logger.title(_("pulling the revision %s from the remote server"), rev)
        iret = call([self.get_cfg("hg_bin"), "pull", "-r", rev, src])
        if iret == 0:
            logger.info(_("pull ended successfully"))
        else:
            logger.warn(_("hg pull exits with code %s"), iret)
            logger.warn(_("You must pull the revision manually."))
        logger.info(_("next steps: hg rebase ..., hg up ..., pull_request --check"))

    def startall(self):
        """Start all requests in status PEND in the current repository"""
        self._loop_on_requests(
            "PEND", self.start, repo=self.reponame, title=_("starting request for %s...")
        )

    def check(self):
        """Check of the source files,
        alert integrators on some specific warnings"""
        self.check_uncommitted()
        self.check_children()
        self.check_repositories_updates()
        self.build_changes_dict()
        self.check_message_descr()
        self.check_issues_status()
        self.init_waf_param()
        if self.reponame == "src":
            self.check_waf_build()
            self.check_waf_install()
        self.init_report()
        self.check_source_files(askconfirm=False)
        self.report_on_new()
        # TODO filter report, highlight unrecommended warnings...
        logger.add_cr()
        self.status()
        logger.info(_("next step: pull_request --test or --testall ..."))

    def differ(self):
        """Differ the integration process, the status of the request is
        reset to PEND"""
        self.init_queue()
        func = partial(self._queue.stop, details=self._details)
        self._call_queue_func(func)

    def cancel(self):
        """Cancel a request for integration, remove it from the queue"""
        self.init_queue()
        func = partial(self._queue.cancel, status=("PEND", "RUN"), details=self._details)
        self._call_queue_func(func)

    def close(self):
        """Revision has been integrated, remove its request from the queue"""
        self.init_queue()
        func = partial(self._queue.close, details=self._details)
        self._call_queue_func(func)

    def closeall(self):
        """Close all requests in status TEST"""
        self._loop_on_requests(
            "TEST", self.close, repo=self.reponame, title=_("closing request for %s...")
        )

    def refuse(self):
        """Revision has been refused, remove its request from the queue"""
        self.init_queue()
        func = partial(self._queue.refuse, details=self._details)
        self._call_queue_func(func)

    def test(self):
        """Mark a revision to be tested"""
        self.init_queue()
        wrkrev = self.wrkrev
        strrev = str(wrkrev.rev()) + ":" + shortrev(self.wrkrev)
        logger.title(
            _("the revision {0} will become the new reference head of {1}/{2}").format(
                strrev, self.reponame, self.wrkbranch
            )
        )
        func = partial(self._queue.test, finalrev=wrkrev.hex(), details=self._details)
        self._call_queue_func(func)
        logger.info(_("next step: run testcases"))

    def testall(self):
        """Mark all requests in status RUN to be tested"""
        self._loop_on_requests("RUN", self.test, repo=self.reponame, title=_("mark %s as TEST..."))

    def status(self):
        """Return the status of one or more requests"""
        self.init_queue()
        logger.title(_("status of the requests for integration:"))
        if self.args:
            for rev in self.args:
                req = self._queue.get_by_node(rev)
                if req is None:
                    logger.error(_("no request for this revision '%s'"), rev)
                else:
                    logger.info(repr(req))
        else:
            for req in self._queue:
                logger.info(repr(req))
            if len(self._queue) == 0:
                logger.info(_("no request"))

    def run(self):
        """Execute the action"""
        super().check_init()
        self.set_working_revision()
        self.set_parameters(need_changes=False)
        self._main()
        self._dump_log()


def _check_for_updates(reponame, path=None, update=False, **opts):
    """Check for updates"""
    logger.title(_("checking for updates for the repository '%s'"), reponame)
    if not path:
        path = ASCFG.get("devtools_root")
    retcode = check_for_updates(path, **opts)
    if retcode == 0:
        logger.error(
            _(
                "there are available updates, please pull & update "
                "in this repository before asking for the integration"
            )
        )
        logger.error(
            _("you should run: %s"),
            "cd %s && hg pull %s && cd %s" % (path, "-u" * update, os.getcwd()),
        )
        if not update:
            logger.error(
                _(
                    "then you must update your development onto the "
                    "last 'reference' revision by using rebase or merge"
                )
            )
    elif retcode != 1:
        logger.error(
            _("can not check for updates for the repository '{0}' (options: {1})").format(
                reponame, opts
            )
        )


def _check_description(descr):
    """Check a commit description
    The commit messages have already been checked by a dedicated hook but
    additional checkings can be done here."""
    linestar = [line for line in descr.splitlines() if line.startswith("***")]
    if linestar:
        logger.error("Message:\n" + descr)
        logger.error(
            "A line starts with '***'. It seems to be a temporary "
            "message of histedit. Please write a message that describe the "
            "final revision.\n"
        )
