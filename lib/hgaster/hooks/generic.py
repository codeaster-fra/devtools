# coding=utf-8

"""
Define generic functions to define hooks.
"""

import re

from functools import partial

from repository_api import UI, get_rev_log

from aslint.config import ASCFG
from aslint.baseutils import force_list
from aslint.logger import logger

from ..ext_utils import RE_ISSUE, get_issues_from_descr

OK, NOOK = 0, 1


def on_error():
    """Tell if it must fail on error or pass silently"""
    if ASCFG.get("check-commit", "error") != "error":
        return OK
    return NOOK


def branch_checker(authorized=(), unauthorized=()):
    """Return a function to check branch validity.

    Arguments:
        authorized (list[regexp]): List of authorized branches as regular
            expressions.
        unauthorized (list[regexp]): List of unauthorized branches as regular
            expressions.

    Returns:
        function: Checker function.
    """

    def _check_branch(branch):  # pragma pylint: disable=unused-argument,invalid-name
        """Check if the branch name is authorized."""
        check1 = NOOK
        for expr in force_list(authorized) or []:
            if re.search(expr, branch) is not None:
                check1 = OK
                break
        if check1 != OK:
            return NOOK
        for expr in force_list(unauthorized) or []:
            if re.search(expr, branch) is not None:
                return NOOK
        return OK

    return _check_branch


def branch_user_commit_hook(brch_chk, priv_chk, check_msg=False, unittest=False):
    """Return a pretxncommit hook that checks the branch name and author of
    a just-committed changeset.

    If the author name passes the `priv_chk` function the branch name is
    not checked (automatically validated).

    Arguments:
        brch_chk (function): Function that checks the branch name.
        priv_chk (function): Function that checks author with privileges.
        check_msg (bool): If *True*, enable the commit message checking.
        unittest (bool): If *False*, exists with a non-null code in case of
            error. Otherwise, it raises an exception.

    Returns:
        hook: A pretxncommit hook.
    """

    def _pretxncommit(repo, unittest=unittest):
        """Checker"""
        if not unittest:
            error = partial(logger.error, exit=True)
        else:
            error = logger.abort

        node = repo["tip"]
        if priv_chk(node.user()):
            return OK
        if check_msg and check_commit_msg(node.description()) is NOOK:
            error("invalid commit message.")
        branch = node.branch()
        if brch_chk(branch) is NOOK:
            error("You are not allowed to commit in " "branch {0!r}.".format(branch))
        return OK

    return _pretxncommit


def branch_user_push_hook(brch_chk, priv_chk, check_msg=False, unittest=False):
    """Return a pretxnchangegroup hook that checks the branch name and the
    author of each incoming changeset.

    If the author name passes the `priv_chk` function the branch name is
    not checked (automatically validated).

    Arguments:
        brch_chk (function): Function that checks the branch name.
        priv_chk (function): Function that checks the author.
        check_msg (bool): If *True*, enable the commit message checking.
        unittest (bool): If *False*, exists with a non-null code in case of
            error. Otherwise, it raises an exception.

    Returns:
        hook: A pretxnchangegroup hook.
    """

    def _pretxnchangegroup(repo, node, unittest=unittest):
        """Checker"""
        if not unittest:
            error = partial(logger.error, exit=True)
        else:
            error = logger.abort

        for rev in range(repo[node].rev(), repo["tip"].rev() + 1):
            author = repo[rev].user()
            if priv_chk(author):
                continue
            descr = repo[rev].description()
            branch = repo[rev].branch()
            if check_msg and check_commit_msg(descr) is NOOK:
                error("invalid commit message.")
            if brch_chk(branch) is NOOK:
                error(
                    "unauthorized branch name: {0!r} "
                    "(author: {1}, rev {2})".format(branch, author, repo[rev].hex()[:8])
                )
        return OK

    return _pretxnchangegroup


def check_commit_msg(descr):
    """Check the commit message.

    Require at least an issue number and at least 2 words longer
    than 2 letters.
    """
    err = on_error()
    lines = descr.splitlines()
    header = lines[0] if descr else ""
    try:
        issues = get_issues_from_descr(header, only=True)
    except ValueError:
        issues = []
    if len(issues) < 1 and "temp-revision" not in descr:
        logger.warning(
            "The first line of the commit message must start with the "
            "bug id that is solved between '[]'.\n"
            "For example: [#NNNN] or [issueNNNN] or [bbNNNN]. "
            "If more than one issue is solved, use "
            "[#NNNN,#MMMM,#PPPP].\n"
            "You can mention other issues in the message, just using "
            "their id (ex. #PPPP).\n"
        )
        logger.warning("Your message is:\n%s\n" % descr)
        if err == NOOK:
            return NOOK
    regexp = re.compile(RE_ISSUE, re.MULTILINE)
    words = re.findall(r"\w+", regexp.sub("", descr))
    words = [wrd for wrd in words if len(wrd) > 2]
    if len(words) < 2:
        logger.warning("Please describe your changes in a few words\n")
        if err == NOOK:
            return NOOK
    line0 = max(list(map(len, lines)))
    if line0 > 90:
        logger.warning("max. 80 characters per lines for the readability of the log\n")
        if err == NOOK:
            return NOOK
    body = lines[1:]
    if body and body[0].strip():
        logger.warning(
            "A blank line is required between the subject (the first line) and "
            "the body (the following lines). Lines of the message:\n%s\n",
            lines,
        )
        if err == NOOK:
            return NOOK
    return OK


def single_head_per_branch_hook(in_branch=None):
    """Return a pretxnchangegroup hook that checks that there is only one
    head per branch.

    Arguments:
        in_branch (str): Regular expression for relevant branches.

    Returns:
        hook: A pretxnchangegroup hook.
    """

    def _pretxnchangegroup(
        repo, node, *args, **opts
    ):  # pragma pylint: disable=unused-argument,invalid-name
        """Called using in `.hg/hgrc`::

        pretxnchangegroup.check_heads = python:aster.hooks.single_head_per_branch
        """
        if not hasattr(node, "phase"):
            node = repo[node]

        if node.phase() == "secret":
            logger.warning("you are submitting a secret changeset!")
            return NOOK
        branch = node.branch()

        query = 'head() and not secret() and branch("{0}")'.format(branch)
        if in_branch:
            if not re.search(in_branch, branch):
                return OK
            query += ' and branch("re:{0}")'.format(in_branch)

        unclosed = get_rev_log(repo.root, query).split()
        if len(unclosed) > 1:
            logger.warning(
                "Only one head per branch is allowed "
                "(problem on branch '{0}')!\n"
                "TIP: Merge your changes with the existing head or "
                "create a new branch.\n"
                "You can list the heads using: hg heads {0}\n\n".format(branch)
            )
            return NOOK

        return OK

    return _pretxnchangegroup
