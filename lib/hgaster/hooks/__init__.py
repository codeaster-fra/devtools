# coding=utf-8

"""Define hooks for code_aster & salome_meca repositories."""

# shortcuts for code_aster repositories
from .codeaster import aslint, pretxncommit, pretxnchangegroup
from .generic import single_head_per_branch_hook

single_head_per_branch = single_head_per_branch_hook()
single_head_asterxx = single_head_per_branch_hook('^asterxx/')
