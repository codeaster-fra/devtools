# coding=utf-8

"""
Define hooks for rtool repository.
"""

# Currently use the same hooks as for asterstudy

# pylint: disable=unused-import
from .asterstudy import pretxncommit, pretxnchangegroup, repo_branch_checker
