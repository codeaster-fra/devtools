# coding=utf-8

"""Checkers for testcase files"""

import os
import os.path as osp
import stat
import re

from aslint.api_asrun import check_asrun
from aslint.utils import read_testlist, checksum
from aslint.base_checkers import (
    TextMsg,
    TextMsgNotFound,
    GenericMsg,
    checkers_from_context,
    search_msg,
    DirnameCat,
    FilenameCat,
    FileContentCat,
    DiffCat,
    CheckContext,
)
import aslint.common_checkers as COMM
from aslint.config import ASCFG

COMM_EXT = [".com" + i for i in "m0123456789"] + [".py"]
REG_EXT = (
    "com[m0-9]",
    "py",
    "data",
    "datg",
    "export",
    "[0-9]{1,2}",
    "mail",
    "mgib",
    "med",
    "mmed",
    "msh",
    "msup",
    "mfront",
    "f",
    "json",
)


class UnsupportedExtension(FilenameCat, GenericMsg):
    """Unsupported extension
    Only these extensions are allowed for testcases: .comm, .com[0-9], .py,
    .data, .datg, .export, .mail, .mgib, .mmed, .msh, .msup, .[0-9]+"""

    id = "C9002"
    fmt = "%(id)s: %(label)s"
    _search = search_msg(r"(?P<ext>\.(" + "|".join(REG_EXT) + "))$")

    def search(self, fname):
        """Check for unsupported extension."""
        if not osp.isfile(fname) or [_ for _ in self._search(fname)]:
            return []
        return [fname]


class InvalidFilename(FilenameCat, TextMsgNotFound):
    """Invalid filename for a testcase
    Filenames matching 'aaaaNNNm.\*', 'aaaaNNNm\_\*.comm', 'aaaaNNNm\_\*.py',
    'subdir/\*.py' or 'NNN.med' are expected.
    See C9002 message for the allowed extensions.
    """

    id = "C2016"
    fmt = "%(id)s: %(label)s"

    def search(self, txt):
        """Check for unauthorized filenames."""
        # most of names are: aaaaNNNm
        # - some have only 2 numbers,
        # - mac3c needs the '3'
        # accept aaaaNNNm + a suffix starting with '_'
        _expr1 = re.compile(
            r"/(?P<root>[a-z3]{3,16}[0-9]{2,3}[a-z]{1})(?P<ext>\.(" + "|".join(REG_EXT) + "))$"
        )
        _expr2 = re.compile(
            r"/(?P<root>[a-z3]{3,16}[0-9]{2,3}[a-z]{1}_\w{1,16})(?P<ext>\.(comm|py))$"
        )
        _expr3 = re.compile(r"/(?P<root>[a-z3]{3,16}[0-9]{2,3}[a-z]{1})/")
        _expr4 = re.compile(r"/(?P<root>[0-9]+)(?P<ext>\.med)$")
        found = (
            _expr1.findall(txt) or _expr2.findall(txt) or _expr3.findall(txt) or _expr4.findall(txt)
        )
        # accept .mfront files too
        if not found and osp.splitext(txt)[1] == ".mfront":
            found = [txt]
        return found


class FileNotUsed(DirnameCat, GenericMsg):
    """File not used by any testcase
    A file is in the directory of the testcases but it is not used by any
    testcase."""

    id = "C2004"
    re_expr = re.compile("^[FR] +[^ ]+ +([^ ]+) +D", re.M)

    def search(self, txt):
        """Check if files exist"""
        # creating an AsterProfil object for each export costs twice more
        astest = osp.join(txt, "astest")
        files = set(os.listdir(astest))
        files.discard("wscript")
        files.discard("__pycache__")
        lexp = [fname for fname in files if fname.endswith(".export")]
        files.difference_update(lexp)
        ltxt = []
        for path in lexp:
            with open(osp.join(astest, path), "r") as fobj:
                ltxt.append(fobj.read())
        text = os.linesep.join(ltxt)
        used = self.re_expr.findall(text)
        unused = [": %s" % path for path in files.difference(used)]
        return unused


class DatafileNotFound(DirnameCat, GenericMsg):
    """Data file not found
    The datafile referenced in the export file does not exist."""

    id = "E2001"
    re_expr = re.compile("^[FR] +[^ ]+ +([^ ]+) +D", re.M)
    re_need = re.compile("^P +testlist .*need_data", re.M)

    def search(self, txt):
        """Check if files exist"""
        # creating an AsterProfil object for each export costs twice more
        astest = osp.join(txt, "astest")
        c4che = ASCFG.get("waf.c4che")
        data_path = (c4che and c4che.get("data_path")) or osp.join(txt, "..", "data")
        tests_data = osp.join(data_path, "tests_data")
        files = set(os.listdir(astest))
        lexp = [fname for fname in files if fname.endswith(".export")]
        notfound = []
        for path in lexp:
            with open(osp.join(astest, path), "r") as fobj:
                text = fobj.read()
            used = self.re_expr.findall(text)
            for path in used:
                if not osp.exists(osp.join(astest, path)) and not osp.exists(
                    osp.join(tests_data, path)
                ):
                    if not osp.exists(tests_data) and self.re_need.search(text):
                        # may be in data
                        continue
                    notfound.append(": %s" % path)
        return notfound


class DatafileTooBig(FilenameCat, GenericMsg):
    """Size of datafiles too big"""

    apply_to = [".export"]
    id = "C2002"
    limit = 2.0 * 1048576
    apply_ctxt = CheckContext(reponame=["src"])
    re_expr = re.compile("^[FR] +[^ ]+ +([^ ]+) +D", re.M)

    def search(self, txt):
        """Check size of files"""
        if not osp.isfile(txt):
            return []
        with open(txt, "r") as fobj:
            content = fobj.read()
        used = self.re_expr.findall(content)
        dname = osp.dirname(txt)
        results = []
        size = 0
        for data in used:
            path = osp.join(dname, data)
            if osp.exists(path):
                size += os.stat(path)[stat.ST_SIZE]
        if size > self.limit:
            results.append(
                ": {0:.3f} MB (limit: {1:.3f} MB)".format(size / 1048576.0, self.limit / 1048576.0)
            )
        return results


class DatafileTooBigV(DatafileTooBig):
    """Size of datafiles too big in validation repository"""

    id = "C2018"
    limit = 65.0 * 1048576
    apply_ctxt = CheckContext(reponame=["validation"])


class CommonFilesSrcValidation(DirnameCat, GenericMsg):
    """Filename used in src and validation
    The same filename can not be used in src and validation because the both
    files are installed into the same destination directory."""

    id = "C2021"
    apply_ctxt = CheckContext(reponame=["src", "validation"])

    def search(self, txt):
        """Check if files exist in src and validation"""
        astest = osp.join(txt, "astest")
        root = osp.dirname(txt)
        other = osp.join(
            root, "src" if self.ctxt.reponame == "validation" else "validation", "astest"
        )
        if not osp.isdir(astest) or not osp.isdir(other):
            # can not raise an error if the other repository does not exist
            return []
        files = os.listdir(astest)
        others = os.listdir(other)
        common = set(files).intersection(others)
        names = sorted(list(common))
        return [": " + i for i in names]


class InvalidExport(GenericMsg):
    """Invalid export file
    Required parameters: memory_limit, time_limit, testlist.

    testlist must contain 'verification', 'verif_interact' or 'validation',
    and, 'sequential' or 'parallel' + 'need_data' for data with restricted access.
    """

    apply_to = [".export"]
    auth = (
        "memory_limit",
        "time_limit",
        "ncpus",
        "mpi_nbnoeud",
        "mpi_nbcpu",
        "max_base",
        "testlist",
        "expected_diag",
    )
    required = ("memory_limit", "time_limit", "testlist", "ncpus", "mpi_nbcpu", "mpi_nbnoeud")
    testlist_names = ()
    project_list = ()

    def search(self, txt):
        """Check a .export. Return True if it is ok."""
        if not check_asrun(raise_on_error=False):
            return []
        from asrun.profil import AsterProfil

        # check asrun compatibility
        import inspect

        sign = inspect.signature(AsterProfil.__init__)
        opts = {}
        if "auto_param" in sign.parameters:
            opts["auto_param"] = False
        pexp = AsterProfil(txt, **opts)
        results = []
        for key in list(pexp.param.keys()):
            if not key in self.auth:
                results.append(": unauthorized parameter: %s" % key)
        for key in self.required:
            if pexp.param.get(key) is None:
                results.append(": required parameter: %s" % key)
        testlist = pexp.param.get("testlist", "")
        if type(testlist) in (list, tuple):
            testlist = " ".join(testlist)
        verif = "verification" in testlist
        verif_inter = "verif_interact" in testlist
        valid = "validation" in testlist
        seq = "sequential" in testlist
        par = "parallel" in testlist
        if (not (verif or verif_inter or valid)) or (verif and valid):
            results.append(
                ": expecting exactly one of 'verification', "
                "'verif_interact' or 'validation' in testlist"
            )
        if (not (seq or par)) or (seq and par):
            results.append(": expecting exactly one of sequential or " "parallel in testlist")
        # check for unknown list
        for name in testlist.split():
            if name not in list(self.testlist_names) + list(self.project_list):
                results.append(": unexpected list name: %r" % name)
        if valid:
            # project is required
            project_found = False
            for name in self.project_list:
                if name in testlist.split():
                    project_found = True
                    break
            if not project_found:
                results.append(
                    ": a valid domain name is required "
                    "(none in 'src'), one of {0}".format(self.project_list)
                )
        if pexp.param.get("mpi_nbcpu") or pexp.param.get("mpi_nbnoeud"):
            try:
                nbcpu = int(pexp.param.get("mpi_nbcpu")[0])
            except (TypeError, ValueError):
                results.append(": integer value is expected for mpi_nbcpu")
                nbcpu = 1
            try:
                nbnode = int(pexp.param.get("mpi_nbnoeud", 1)[0])
            except (TypeError, ValueError):
                results.append(": integer value is expected for mpi_nbnoeud")
                nbnode = 1
            if not valid and nbcpu > 8:
                results.append(": mpi_nbcpu is limited to 8 for verification")
            if seq and (nbcpu * nbnode) > 1:
                results.append(": mpi_nbcpu and mpi_nbnoeud must be 1 for sequential testcases")
            if nbnode > nbcpu:
                results.append(": mpi_nbnoeud can not be greater than mpi_nbcpu")

        for entry in pexp.get_data():
            orig = entry.path.strip()
            basn = osp.basename(orig)
            if orig != basn:
                results.append(": must be a filename without path: %s" % entry.path)
            if entry.ul in (6, 8, 15):
                results.append(": reserved logical unit number: %s" % entry.ul)
            if entry.type not in self.datatypes:
                results.append(": invalid type: %s" % entry.type)
        result = pexp.get_result()
        if len(result) != 0:
            results.append(": must contain no result: %d found" % len(result))
        larg = len(pexp.args)
        if larg != 0:
            results.append(": must contain no argument: %d found" % larg)
        return results


class InvalidExportVerif(FilenameCat, InvalidExport):
    """Invalid export file
    Required parameters: memory_limit, time_limit, testlist.

    testlist must contain 'verification' or 'verif_interact',
    and, 'sequential' or 'parallel'.
    """

    apply_ctxt = CheckContext(reponame=["src"])
    id = "E2003"
    datatypes = ["comm", "libr", "nom", "mail", "mmed", "msh", "datg", "msup", "mgib"]
    testlist_names = (
        "verification",
        "verif_interact",
        "ci",
        "sequential",
        "parallel",
        "submit",
        "need_data",
    )


class InvalidExportValid(FilenameCat, InvalidExport):
    """Invalid export file
    Required parameters: memory_limit, time_limit, testlist.

    testlist must contain 'verification', 'verif_interact' or 'validation',
    and, 'sequential' or 'parallel'.

    Additional supported names:
        'benchmark', 'bm', 'concrete', 'dam', 'fatigue', 'fluid_structure',
        'fracture', 'fuel_assembly', 'performance', 'prototype', 'rotor',
        'seism', 'ukc', 'welding'.
    """

    apply_ctxt = CheckContext(reponame=["validation"])
    id = "E2103"
    datatypes = ["comm", "libr", "nom", "mail", "mmed", "msh", "datg", "msup", "mgib", "repe"]
    testlist_names = ("validation", "verif_interact", "sequential", "parallel", "submit")
    project_list = (
        "benchmark",
        "bm",
        "concrete",
        "dam",
        "fatigue",
        "fluid_structure",
        "fracture",
        "fuel_assembly",
        "performance",
        "prototype",
        "rotor",
        "seism",
        "ukc",
        "welding",
    )


class ResourcesTooBig(FilenameCat, GenericMsg):
    """Testcase resources are too big
    Time limit must be less than 300 s, memory limit less than 1024 MB in 'src'.
    This error can be ignored in the 'validation' repository"."""

    apply_to = [".export"]
    id = "C2009"
    apply_ctxt = CheckContext(reponame=["src"])

    def search(self, txt):
        """Check a .export"""
        if not osp.isfile(txt):
            return []
        if not check_asrun(raise_on_error=False):
            return []
        from asrun.profil import AsterProfil

        pexp = AsterProfil(txt)
        results = []
        try:
            time_limit = int(pexp["time_limit"][0])
            assert time_limit <= 300
        except (AssertionError, ValueError, TypeError):
            val = pexp["time_limit"][0]
            results.append(
                ": invalid parameter, time_limit must be an " "integer less than 300: %s" % val
            )
        try:
            time_limit = int(pexp["memory_limit"][0])
            assert time_limit <= 1024
        except (AssertionError, ValueError, TypeError):
            val = pexp["memory_limit"][0]
            results.append(
                ": invalid parameter, memory_limit must be an " "integer less than 1024: %s" % val
            )
        return results


class DuplicatedDataFile(DirnameCat, GenericMsg):
    """Duplicated data files
    Datafiles can be shared between testcases through the .export definition.
    Do not add the same file several times."""

    id = "C2013"

    def search(self, txt):
        """Check for duplicate datafiles"""
        err = []
        astest = osp.join(txt, "astest")
        files = set(os.listdir(astest))
        dsize = {}
        for fname in files:
            path = osp.join(astest, fname)
            if not osp.isfile(path):
                continue
            size = os.stat(path)[stat.ST_SIZE]
            # skip very small files
            if size < 10:
                continue
            dsize[size] = dsize.get(size, []) + [path]
        for size, files in list(dsize.items()):
            if len(files) < 2:
                continue
            dble = {}
            for fname in files:
                fsum = checksum(fname)
                dble[fsum] = dble.get(fsum, []) + [fname]
            for files in list(dble.values()):
                if len(files) > 1:
                    files.sort()
                    err.append(": %s" % ", ".join(files))
        return err


class BaseUnrecommendedKeyword(TextMsg):
    """Unrecommended keyword in a command file
    This keyword should not be used in a testcase: TOLE_MACHINE."""

    # id must be defined in subclasses
    kwd = ["TOLE_MACHINE"]
    search = search_msg(r"^ *[^\#].*(?P<main>(%s))" % "|".join(kwd))


class UnrecommendedKeyword(FileContentCat, BaseUnrecommendedKeyword):
    """Unrecommended keyword in a command file
    This keyword should not be used in a testcase: TOLE_MACHINE."""

    id = "W2005"


class NewUnrecommendedKeyword(DiffCat, BaseUnrecommendedKeyword):
    """Unrecommended keyword in a command file
    This keyword should not be used in a testcase: TOLE_MACHINE."""

    id = "C2055"


class ObsoleteKeyword(FileContentCat, TextMsg):
    """Obsolete keyword in a command file
    This keyword should not be used in a testcase: PAR_LOT."""

    id = "C2020"
    kwd = ["PAR_LOT"]
    search = search_msg(r"^ *[^\#].*(?P<main>(%s))" % "|".join(kwd))


class InvalidKeywordValue(FileContentCat, GenericMsg):
    """Unrecommended value assigned to a keyword in a command file
    This value should be assigned with care in this repository."""

    id = "W2012"
    _invalid = [("NIV_PUB_WEB", "['\"]INTRANET['\"]")]
    _expr = [re.compile(f"(?P<kwd>{kwd}) *= *(?P<value>{val})", re.M) for kwd, val in _invalid]
    _recom = re.compile("^ *#.*$", re.M)
    apply_ctxt = CheckContext(reponame=["src"])

    def search(self, txt):
        """Check a command file for invalid values"""
        err = []
        txt = self._recom.sub("", txt)
        for expr in self._expr:
            mat = expr.search(txt)
            if mat:
                err.append(": %s=%s" % (mat.group("kwd"), mat.group("value")))
        return err


class InvalidKeywordValueError(FileContentCat, GenericMsg):
    """Invalid value assigned to a keyword in a command file"""

    id = "C2014"
    _invalid = [("UNITE", "15")]
    _expr = [re.compile(f"(?P<kwd>{kwd}) *= *(?P<value>{val})", re.M) for kwd, val in _invalid]
    _recom = re.compile("^ *#.*$", re.M)

    def search(self, txt):
        """Check a command file for invalid values"""
        err = []
        txt = self._recom.sub("", txt)
        for expr in self._expr:
            mat = expr.search(txt)
            if mat:
                err.append(": %s=%s" % (mat.group("kwd"), mat.group("value")))
        return err


class InvalidKeywordError(FileContentCat, GenericMsg):
    """Invalid keyword in a command file"""

    id = "C2015"
    _invalid = ["identifier"]
    _expr = [re.compile(f"(?P<kwd>{kwd}) *=", re.M) for kwd in _invalid]
    _recom = re.compile("^ *#.*$", re.M)

    def search(self, txt):
        """Check a command file for invalid values"""
        err = []
        txt = self._recom.sub("", txt)
        for expr in self._expr:
            mat = expr.search(txt)
            if mat:
                err.append(": %s" % (mat.group("kwd"),))
        return err


class DeprecatedComment(FileContentCat, TextMsg):
    """Deprecated comment in a command file
    This comment (TITRE) should not be used in a testcase: the title is stored
    in the documentation."""

    id = "C2017"
    apply_to = COMM_EXT
    kwd = ["TITRE"]
    search = search_msg("^ *# *(?P<main>(%s) .*)" % "|".join(kwd))


class LicenseNotFound(FileContentCat, COMM.LicenseNotFound):
    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org"""

    apply_to = COMM_EXT
    id = "C2006"


class EDFCopyright(FileContentCat, COMM.EDFCopyright):
    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and/or version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2024 EDF R&D www.code-aster.org
    """

    apply_to = COMM_EXT
    id = "C2007"


class InvalidCharacter(FileContentCat, COMM.InvalidCharacter):
    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""

    apply_to = COMM_EXT
    id = "C2008"


class RequiredKeyword(FileContentCat, GenericMsg):
    """Required keyword in a command file
    A testcase must use one of operators DEBUT or POURSUITE and the keyword
    CODE."""

    apply_to = COMM_EXT
    apply_ctxt = CheckContext(reponame=["src"], branch="main")
    id = "C2011"
    _debut = re.compile("^DEBUT", re.M)
    _debcod = re.compile(r"^DEBUT *\([^\#\)]*CODE *= *_F\(", re.M)
    _deberr = r"""'CODE=_F(...)'"""
    _pours = re.compile("^POURSUITE", re.M)
    _poucod = re.compile(r"^POURSUITE *\([^\#]*CODE *= *_F\(", re.M)
    _poucodalt = re.compile(r"^POURSUITE *\([^\#]*CODE *= *[\'\"]OUI", re.M)
    _init = re.compile(r"^CA\.init", re.M)
    _inittst = re.compile(r"^CA\.init *\([^\#\)]*\-\-test", re.M)
    _initerr = r"""'--test'"""

    def search(self, txt):
        """Check a command file for required keywords"""
        err = []
        if self._init.search(txt):
            if not self._inittst.search(txt):
                err.append(": %s not found in 'CA.init'" % self._initerr)
        elif self._debut.search(txt):
            if not self._debcod.search(txt):
                err.append(": %s not found in DEBUT" % self._deberr)
        elif self._pours.search(txt):
            if not (self._poucod.search(txt) or self._poucodalt.search(txt)):
                err.append(": %s not found in POURSUITE" % self._deberr)
        else:
            # pure python file
            pass
        return err


class ImportAndAutoStart(FileContentCat, GenericMsg):
    """AutoStart should not be used in testcases"""

    apply_ctxt = CheckContext(reponame=["src"], branch="main")
    id = "C2401"
    fixme = (
        r"sed -i -e 's/from \+code_aster import CA *$//g'"
        r" -e 's/from \+code_aster\.Commands.*/from code_aster.Commands import *\nfrom code_aster import CA/g'"
        " %(filenames)s"
    )

    def search(self, txt):
        """Check imports order"""
        recmd = re.compile(r"^from +code_aster\.Commands", re.M)
        reca = re.compile(r"^from +code_aster import CA", re.M)
        matcmd = recmd.search(txt)
        matca = reca.search(txt)
        err = []
        if matcmd and matca and matca.start() < matcmd.start():
            err.append(": CA must not be imported before Commands")
        return err


class ReformatSource(FilenameCat, COMM.ReformatPy):
    """Reformat Python source"""

    id = "C2522"
    _search = search_msg(r"(?P<ext>\.(com[m0-9]|py))$")

    def search(self, fname):
        """Check file"""
        if [_ for _ in self._search(fname)]:
            if osp.isfile(fname) and os.stat(fname)[stat.ST_SIZE] < 1e6:
                return COMM.ReformatPy.search(self, fname)
        return []

class ObsoleteSdj(FileContentCat, TextMsg):
    """Obsolete use of '.sdj.XXX'"""

    apply_to = COMM_EXT
    id = "C2023"
    search = search_msg(r"(?P<main>\w+\.sdj(?:\.\w+)?)", ignore_case=False)

class ObsoleteGetvectjev(FileContentCat, TextMsg):
    """Obsolete use of getvectjev or getcolljev"""

    apply_to = COMM_EXT
    id = "C2024"
    search = search_msg(r"(?P<main>(getvectjev|getcolljev))", ignore_case=False)

CHECK_LIST = checkers_from_context(globals(), TextMsg)
