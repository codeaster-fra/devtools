# coding=utf-8

"""Checkers for python source files"""

import os
import os.path as osp
import re
from glob import glob

from .. import common_checkers as COMM
from ..base_checkers import (
    CheckContext,
    DirnameCat,
    FileContentCat,
    FilenameCat,
    GenericMsg,
    TextMsg,
    checkers_from_context,
    search_msg,
)
from ..config import ASCFG
from ..contrib.directed_graph import Graph
from .diagr_import import get_all_imports
from .python_tools import get_masked_imports


class LineTooLong(FileContentCat, COMM.LineTooLong):
    """Line too long
    Lines must not be too long for readability and convention.
    Maximum line length is 80 columns in python
    (see http://www.python.org/dev/peps/pep-0008)."""

    id = "C4001"
    _maxlength = 300


class LicenseNotFound(FileContentCat, COMM.LicenseNotFound):
    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org
    See `fix_license` tool (use --help option for details)."""

    id = "C4002"
    fixme = ASCFG.get("devtools_root") + "/bin/fix_license %(filenames)s"


class EDFCopyright(FileContentCat, COMM.EDFCopyright):
    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and/or version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2025 EDF R&D www.code-aster.org
    """

    id = "C4003"


class ExternalCopyright(FileContentCat, COMM.ExternalCopyright):
    """Another Copyright than EDF"""

    id = "W4004"


class EncodingDeclNotFound(FileContentCat, COMM.EncodingDeclNotFound):
    """Encoding definition is required in the first or second line
    Conforming to PEP0263, the encoding of the source file must be defined
    by placing a magic comment as first or second line of the file
    (see http://www.python.org/dev/peps/pep-0263/)."""

    id = "C4005"


class InvalidCharacter(FileContentCat, COMM.InvalidCharacter):
    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""

    id = "C4006"


class ExecUsed(FileContentCat, TextMsg):
    """Using 'exec()' is discouraged"""

    id = "C4007"
    search = search_msg(r"\b(?P<main>exec *\(.*)", ignore_case=False)


class TopLevelImport(FileContentCat, GenericMsg):
    """Imports should always be written at the top of the file, after any
    module comments and docstrings."""

    id = "C4008"

    def search(self, txt):
        """Check for masked imports"""
        # exception for template files
        if "@VERSION_INFO@" in txt:
            return []
        # exception for catalo/cataelem/elem.py
        if "from cataelem.Elements.elements import EL" in txt:
            return []
        masked = get_masked_imports(txt)
        err = []
        for imp in masked:
            err.extend([": import {0} at line {1}".format(i.name, imp.lineno) for i in imp.names])
        return err


class RelativeImport(FileContentCat, TextMsg):
    """Use explicit relative imports inside 'code_aster' package.
    Use 'from ..Utilities import logger' instead of
    'from code_aster.Utilities import logger'."""

    id = "C4009"
    search = search_msg("^ *(?P<main>(import|from) +code_aster)", ignore_case=False)


class CyclicImport(DirnameCat, GenericMsg):
    """Cyclic import detected"""

    id = "C4010"

    def search(self, txt):
        """Check for cyclic imports"""
        files = []
        for base, _, __ in os.walk("code_aster"):
            files.extend(glob(osp.join(base, "*.py")))

        try:
            imports = get_all_imports(files, pkg=True)
        except Exception as exc:
            return [": ERROR " + str(exc)]
        graph = Graph(imports)
        pbs = graph.find_recursive()
        cyclic = []
        for v1, v2 in pbs:
            path = graph.find_path(v1, v2) + graph.find_path(v2, v1)[1:]
            cyclic.append(": " + " > ".join(path))
        return cyclic


class DisabledMsg(DirnameCat, GenericMsg):
    """Too much disabled messages for type C4008"""

    id = "E4011"
    _re = re.compile("aslint *: *disable *=.*C4008", re.M)

    def search(self, txt):
        """Check for disabled messages"""
        expected = 6
        err = []
        count = 0
        for base, _, __ in os.walk("code_aster"):
            for fname in glob(osp.join(base, "*.py")):
                with open(fname) as fobj:
                    if self._re.search(fobj.read()):
                        count += 1
        if count > expected:
            err = [": {0} (expected {1})".format(count, expected)]
        return err


class MissingImportTr(FileContentCat, GenericMsg):
    """Import of translation function is required, 'import _'
    Insert 'from ..Utilities import _'."""

    id = "C4012"
    _remsg = re.compile("^cata_msg *=", re.M)
    _reimp = re.compile("^from .* import _", re.M)

    def search(self, txt):
        """Check for import of '_' in messages."""
        err = []
        if self._remsg.search(txt) and not self._reimp.search(txt):
            err.append("not found")
        return err


class ObsoletePath(FilenameCat, GenericMsg):
    """Obsolete directory"""

    id = "C4013"
    _re = re.compile("^bibpyt", re.M)

    def search(self, fname):
        """Check for obsolete paths"""
        err = []
        if self._re.search(fname):
            err = [": {0}".format(fname)]
        return err


class ObsoleteSdj(FileContentCat, TextMsg):
    """Obsolete use of '.sdj.XXX'"""

    id = "C4014"
    apply_ctxt = CheckContext(reponame=["src"], branch="main")
    search = search_msg(r"(?P<main>\w+\.sdj(?:\.\w+)?)", ignore_case=False)


class ObsoleteGetvectjev(FileContentCat, TextMsg):
    """Obsolete use of getvectjev or getcolljev"""

    id = "C4015"
    search = search_msg(r"(?P<main>(getvectjev|getcolljev))", ignore_case=False)


class MultiLinesUtmess(FileContentCat, GenericMsg):
    """Message id not found on the same line (use a dict for args)"""

    id = "C4501"

    def search(self, txt):
        """Check for UTMESS without message id"""
        if "def format_exception" in txt:
            return []
        if "class MESSAGE_LOGGER" in txt:
            return []
        re_utm = re.compile(r"(?P<line>(?:UTMESS|GetText|message_exception)\s*\(.*)", re.M)
        re_id = re.compile(r"(UTMESS|GetText|message_exception)\s*\(.*_", re.I)
        err = []
        for mat in re_utm.finditer(txt):
            if not re_id.search(mat.group("line")):
                err.append(f": {mat.group('line')}")
        return err


class ReformatSource(FilenameCat, COMM.ReformatPy):
    """Reformat Python source"""

    id = "C4522"


CHECK_LIST = checkers_from_context(globals(), TextMsg)
