#!/usr/bin/env python3
# coding=utf-8

"""
Make a graphic of imported modules
"""

import argparse
import ast
import os
import os.path as osp
import re
import tempfile
from subprocess import Popen


BIBPYT = """
Applications Cata Coupling LinearAlgebra Objects SD Utilities
Behaviours CodeCommands Helpers MacroCommands ObjectsExt Solvers
CA Commands Messages Supervis
""".split()

ASTERPKG = """
aster aster_fonctions aster_core med_aster libaster
""".split()
NOSTD = ["code_aster"] + ASTERPKG + BIBPYT


def file2module(filename):
    """Convert a file name as a module name.

    Arguments:
        filename (str): File name.

    Returns:
        str: Module name.
    """
    name = osp.abspath(filename).replace(".py", "")
    name = re.sub("^%s" % os.getcwd(), "", name)
    name = name.replace(os.sep, ".")
    # name = re.sub("^%s" % re.escape(prefix + "."), "", name)
    return name.strip().strip(".")


def is_generated(path):
    """Tell if a filename will be generated during build phase and not
    yet exists.

    Arguments:
        path (str): File name.

    Returns:
        bool: *False* if the given file actually exists in the source tree.
            *True* otherwise.
    """
    return osp.isfile(path + ".pytmpl") or "c_mfront_official" in path


class Module:
    """Check imports of a module."""

    def __init__(self, filename, **opts):
        assert osp.exists(filename), filename
        self.name = file2module(filename)
        self.ignore_std = not opts.get("std", False)
        self.verbose = opts.get("verb", False)
        self.pkg_only = opts.get("pkg", False)
        self.grouped = opts.get("grp", False)
        self.stds = []
        self.mods = []
        self.labs = {}
        self._checked = False
        with open(filename, "r") as fobj:
            self.content = fobj.read()

    def abs_path(self, module):
        """Return the absolute path."""
        mod = module.split(".")
        mod = mod[:-1] if mod[-1] == "*" else mod
        if mod[0] in BIBPYT:
            mod.insert(0, "code_aster")
            raise ValueError(f"{self.name}: {module}")
        # is pkg1.pkg2.object?
        path = osp.join(*mod)
        if not (osp.isdir(path) or osp.isfile(path + ".py")) and len(mod) > 1:
            par = osp.join(*mod[:-1])
            if osp.isfile(osp.join(par, "__init__.py")):
                mod = mod[:-1]
        return ".".join(mod)

    def parent_pkg(self, module):
        """Returns the parent package of a module."""
        if self.isfile(module) or is_generated(module):
            module = ".".join(module.split(".")[:-1])
        return module

    def isfile(self, module):
        """Tell if it's a pure module (a file)."""
        path = osp.join(*module.split("."))
        fname = path + ".py"
        if not (osp.isdir(path) or osp.isfile(fname) or is_generated(path) or path in ASTERPKG):
            raise IOError("no such file or directory: {0} (from {1})".format(path, self.name))
        return osp.isfile(fname)

    def group(self, module):
        """Return a higher level package if results are grouped."""
        if module in ASTERPKG:
            module = "libaster"
        if self.grouped:
            for pkg in ("Cata", "MacroCommands"):
                pkg = "code_aster." + pkg
                if module.startswith(pkg + "."):
                    module = pkg
        return module

    def process(self):
        """Process file"""
        try:
            imp = self.extract_import()
        except SyntaxError as exc:
            exc.filename = self.name
            raise SyntaxError(exc)
        for mod in imp:
            mod0 = mod.split(".")[0]
            if mod0 in NOSTD:
                mod = self.abs_path(mod)
                if self.pkg_only:
                    parent = self.parent_pkg(mod)
                    if parent == "code_aster":
                        print("Warning:", self.name, "::", mod, "-->", parent)
                    mod = parent
                mod = self.group(mod)
                self.mods.append(mod)
            else:
                self.stds.append(mod)
        self.mods = sorted(set(self.mods))
        self.stds = sorted(set(self.stds))
        self._checked = True

    def extract_import(self):
        """Return the list of imported modules."""
        if self.verbose:
            print("module name:", self.name)
        mods = []
        asttree = ast.parse(self.content)
        for node in ast.walk(asttree):
            if isinstance(node, ast.Import):
                mods.extend([i.name for i in node.names])
            if isinstance(node, ast.ImportFrom):
                # print(node.level, node.module, [i.name for i in node.names])
                if node.module:
                    mods.append("." * node.level + node.module)
                else:
                    mods.extend(["." * node.level + i.name for i in node.names])
        path0 = self.name.split(".")
        mabs = []
        for mod in mods:
            if mod.startswith("."):
                mod = mod.split(".")
                path = path0[:]
                while not mod[0]:
                    path.pop()
                    mod.pop(0)
                mabs.append(".".join(path + mod))
            else:
                mabs.append(mod)
        if self.verbose:
            print(mabs)
        return mabs

    def get_name(self):
        """Return the module name or the parent package name.

        Returns:
            str: Module name or parent package name.
        """
        orig = self.abs_path(self.name)
        if self.pkg_only and self.isfile(self.name):
            orig = self.parent_pkg(self.name)
        orig = self.group(orig)
        return orig

    def get_imports(self):
        """Return the sorted list of the imported modules.

        Returns:
            list[str]: List of imported modules (absolute names).
        """
        if not self._checked:
            self.process()
        imp = []
        if not self.ignore_std:
            imp.extend(self.stds)
        imp.extend(self.mods)
        imp = [i for i in imp if i != self.get_name()]
        return sorted(imp)


def get_all_imports(files, **opts):
    """Create a dict of imports from a list of files.

    Arguments:
        files (list[str]): List of filenames.
        opts (dict): Options passed to *Module*.

    Returns:
        dict: Dict of imports for each module.
    """
    imports = {}
    for fname in files:
        mod = Module(fname, **opts)
        src = mod.get_name()
        imports.setdefault(src, [])
        imports[src].extend(mod.get_imports())
        imports[src] = sorted(set(imports[src]))
    return imports


def plot(imports, filename):
    """Represent imports as a graphviz plot.

    Arguments:
        imports (dict): Dict of imports.
        filename (str): Output filename.
    """

    def _label(mod):
        key = mod.replace(".", "_X_")
        if key[0].isdigit():
            key = "N_" + key
        return key

    edges = []
    labels = {}
    for src, tgt in imports.items():
        labels[src] = _label(src)
        for mod in tgt:
            labels[mod] = _label(mod)
            edges.append("    {0} -> {1};".format(labels[src], labels[mod]))

    lines = ["digraph G {"]
    lines.extend(["""    {1} [label="{0}"];""".format(i, j) for i, j in labels.items()])
    lines.extend(edges)
    lines.append("}")

    ext = osp.splitext(filename)[-1].replace(".", "")
    assert ext in ("png", "svg", "ps"), ext
    ftmp = tempfile.NamedTemporaryFile("w", suffix=".dot", delete=False)
    ftmp.write(os.linesep.join(lines))
    ftmp.close()
    try:
        proc = Popen(["dot", "-T%s" % ext, ftmp.name, "-o", filename])
        iret = proc.wait()
    finally:
        os.remove(ftmp.name)
    return iret


def main():
    # command arguments parser
    parser = argparse.ArgumentParser(
        usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-v", dest="verb", action="store_true", default=False, help="add comments")
    parser.add_argument(
        "-s",
        dest="std",
        action="store_true",
        default=False,
        help="show standard (and external) modules",
    )
    parser.add_argument(
        "-p", "--pkg", action="store_true", default=False, help="keep packages only"
    )
    parser.add_argument("--grp", action="store_true", default=False, help="group sub-packages")
    parser.add_argument("-g", "--graph", action="store", help="create a dot graph")
    parser.add_argument("files", nargs="*", help="file names")

    args = parser.parse_args()
    if len(args.files) == 0:
        parser.error("files ?")

    imports = get_all_imports(**(args.__dict__))
    if not args.graph:
        mods = sorted(imports.keys())
        for src in mods:
            print(src, ":", ", ".join(imports[src]))
    else:
        plot(imports, args.graph)


if __name__ == "__main__":
    # m = Module("code_aster/Behaviours/zirc.py")
    main()
