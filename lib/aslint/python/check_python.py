# coding=utf-8

"""This module gives convenient functions to check python source files"""

import os
import os.path as osp
from functools import partial

from .. import common_checkers as COMM
from ..base_checkers import (CheckList, MsgList, Report, check_disabled,
                             check_file_content, check_filename)
from ..i18n import _
from ..logger import logger
from ..python import python_checkers as PY
from ..utils import apply_in_sandbox


def check_python_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(PY.CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    report = Report()
    if args:
        # the global directory is given by the first argument to check
        dname = osp.dirname(osp.abspath(args[0])).split(os.sep)
        if "code_aster" in dname:
            idx = dname.index("code_aster")
            path = os.sep.join(dname[:idx])
            logger.info(_("add global checking of python source files"))
            lmsg = check_filename(path, checklist.on_dirname())
            report.set(path, lmsg)

    func_one = partial(check_python_file, checklist=checklist)
    lrep = apply_in_sandbox(func_one, args, numthread)
    report.merge(lrep)
    return report


def check_python_file(fname, checklist):
    """Check one file"""
    logger.info(fname)
    lmsg = MsgList()
    lmsg.extend(check_filename(fname, checklist.on_filename()))
    lmsg.extend(check_file_content(fname, checklist.on_content()))
    check_disabled(fname, checklist, lmsg)
    report = Report()
    report.set(fname, lmsg)
    return report
