# coding=utf-8

"""
Tools to analyze Python source code.
"""

import ast


def _is_import(node):
    return isinstance(node, (ast.Import, ast.ImportFrom))

def _is_auth_parent(node):
    return isinstance(node, (ast.Try, ast.ExceptHandler))

def get_main_imports(node):
    """Return the list of imports present at the top level."""
    imp = []
    for sub in node.body + getattr(node, "handlers", []):
        if _is_import(sub):
            imp.append(sub)
        if _is_auth_parent(sub):
            imp.extend(get_main_imports(sub))
    return imp

def get_all_imports(module):
    """Return all imports found (order is not specified)."""
    imp = []
    for node in ast.walk(module):
        if _is_import(node):
            imp.append(node)
    return imp

def get_masked_imports(text):
    """Return the list of `ast.Import` that are not at the top level of the
    module.

    Arguments:
        text (str): Module content.

    Returns:
        list[ast.Import|ast.ImportFrom]: List of imports objects.
    """
    module = ast.parse(text)
    all_imports = get_all_imports(module)
    main_imports = get_main_imports(module)
    masked = [i for i in all_imports if i not in main_imports]
    return masked
