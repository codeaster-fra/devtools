# coding=utf-8

# because of the 'search' method :
# pylint: disable=W0223

"""Check the source file using gfortran"""

import os.path as osp
import re

from aslint.base_checkers import CompilMsg, CompilOutputCat, checkers_from_context
from aslint.config import ASCFG
from aslint.logger import logger


def check_command_line(kwargs):
    """Return the command line to check 'fname'"""
    form = "-f%s-form" % ASCFG.get("fortran.form")
    length = "-f%s-line-length-none" % ASCFG.get("fortran.form")
    sizeopts = []
    if ASCFG.get("fortran.intsize") == 8:
        sizeopts.append("-fdefault-integer-8")
    if ASCFG.get("fortran.realsize") == 8:
        sizeopts.extend(["-fdefault-double-8", "-fdefault-real-8"])
    # do not use -M because of
    # http://gcc.gnu.org/bugzilla/show_bug.cgi?id=47486
    cmd = (
        [
            ASCFG.get("fortran.compiler"),
            "-O0",
            form,
            length,
            "-Wall",
            "-Wextra",
            "-Wimplicit-interface",  # '-Wintrinsic-shadow',
            "-pedantic",  # '-std=f2003',
            # non-Intel flags
            "-DASTER_STRINGIFY_USE_QUOTES=1",
            "-DASTER_HAVE_TRACEBACKQQ=0",
            "-DASTER_IGNORE_WITH_ASLINT=1",
        ]
        + sizeopts
        + kwargs["flags"]
        + ["-c", kwargs["source"], "-o", kwargs["object"]]
    )
    return cmd


def msg_splitter(fname, msgcompil, extern=False):
    """Split the compiler messages"""
    expr = re.compile("^(%s:|[^ ]+\.h:)" % re.escape(fname), flags=re.M)
    with_sep = expr.sub("WHERE=\g<1>", msgcompil)
    msg = re.split("WHERE=", with_sep)
    if not extern:
        msg = [occ for occ in msg if occ.startswith(fname)]
    logger.debug("number of messages: %d", len(msg))
    return msg


# variable used for automatic replacement
VARNAME = "VarName"
# expression for Error/Warning
ERRWARN = "(?P<errw>Error|Warning):\s+(?:|GNU Extension:\s*)"
# expression to get the localization of the error/warning
LOC = "(?P<fname>.*?):(?P<line>[0-9]+)(\.(?P<col>[0-9]+)|.*?):\s+"
# something else
STH = "(?P<sth>.*?)"


def search_msg(expr):
    """Convenient wrapper to search 'expr' with relevant re flags."""
    return re.compile(ERRWARN + expr, re.M | re.DOTALL).finditer


def search_loc_msg(expr):
    """Convenient wrapper to search the localization + 'expr'."""
    return re.compile(LOC + STH + ERRWARN + expr, re.M | re.DOTALL).finditer


class SentinelLocError(CompilOutputCat, CompilMsg):
    """Error"""

    id = "E9990"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(msg)s"
    search = re.compile(
        LOC + STH + "^(?P<errw>Error|Erreur):\s+(?P<msg>.*?$)", re.I | re.M | re.DOTALL
    ).finditer


class SentinelError(CompilOutputCat, CompilMsg):
    """Error"""

    id = "E9991"
    fmt = "%(id)s: %(label)s: %(msg)s"
    search = re.compile("(?P<errw>Error|Erreur):\s+(?P<msg>.*?$)", re.I | re.M | re.DOTALL).finditer


class SentinelLocWarning(CompilOutputCat, CompilMsg):
    """Unexpected warning"""

    id = "C9992"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(msg)s"
    search = re.compile(
        LOC + STH + "^(?P<errw>Warning):\s+(?P<msg>.*?$)", re.I | re.M | re.DOTALL
    ).finditer


class SentinelWarning(CompilOutputCat, CompilMsg):
    """Warning"""

    id = "W9993"
    fmt = "%(id)s: %(label)s: %(msg)s"
    search = re.compile("(?P<errw>Warning):\s+(?P<msg>.*?$)", re.I | re.M | re.DOTALL).finditer


# syntax error


class UnclassifiableStatement(CompilOutputCat, CompilMsg):
    """Unclassifiable statement"""

    id = "E0001"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s"
    search = search_loc_msg("Unclassifiable statement at .*?")


class InvalidCharacter(CompilOutputCat, CompilMsg):
    """Invalid character"""

    id = "E0002"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s : %(elt)r"
    search = search_loc_msg("Invalid character '(?P<elt>.*?)' at")


class InvalidUnit(CompilOutputCat, CompilMsg):
    """Invalid unit"""

    id = "E0003"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s : expecting %(elt)s"
    search = search_loc_msg("UNIT specification at .*? " "must be (?P<elt>.*?)$")


class LineTruncated(CompilOutputCat, CompilMsg):
    """Line truncated"""

    id = "C0004"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s"
    search = search_loc_msg("Line truncated at .*?")


class XDescriptor(CompilOutputCat, CompilMsg):
    """Invalid X descriptor"""

    id = "C0005"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)s"
    search = search_loc_msg("Extension: (?P<elt>X descriptor requires " "leading space count) at")


class InvalidStatement(CompilOutputCat, CompilMsg):
    """Invalid form of statement"""

    id = "E0006"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s : %(elt)r"
    search = search_loc_msg("Invalid form of (?P<elt>.*?) statement")


class ContinousMarkerMissing(CompilOutputCat, CompilMsg):
    """Missing continous character '&'"""

    id = "W0007"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s"
    search = search_loc_msg("Missing '&' in continued character")


# arguments errors


class TypeMismatch(CompilOutputCat, CompilMsg):
    """Type mismatch"""

    id = "E0101"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s for %(elt)r: %(types)r"
    search = search_loc_msg(
        "Type mismatch in argument '(?P<elt>.*?)' " "at \([0-9]+\); passed (?P<types>.*?)$"
    )


class RankMismatch(CompilOutputCat, CompilMsg):
    """Rank mismatch"""

    id = "E0103"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s for %(elt)r: %(repl)s"
    search = search_loc_msg(
        "Rank mismatch in argument '(?P<elt>.*?)' " "at \([0-9]+\) (?P<detail>\(.*?\))"
    )

    def analyze(self):
        """Compute possible fix"""
        detail = self.result["detail"]
        dim = -1
        matdim = re.search("rank\-([0-9]+)", detail)
        if matdim:
            dim = int(matdim.group(1))
        if "scalar and rank" in detail:
            # scalar expected, not array
            indexes = ",".join(["1"] * dim)
            self.result["repl"] = VARNAME + "(" + indexes + ")"
        else:
            # array expected, not scalar
            self.result["repl"] = "[" * dim + VARNAME + "]" * dim


class DummyArgument(CompilOutputCat, CompilMsg):
    """Dummy argument"""

    id = "W0104"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)r"
    _search = search_loc_msg("Unused dummy argument '(?P<elt>\w+)' at")

    def search(self, txt):
        """Search comment, no ['"] before '!'."""
        lmat = self._search(txt)
        result = []
        for mat in lmat:
            mdict = mat.groupdict()
            if mat.group("elt") in ("option", "nomte") and re.search(
                "/te[0-9]+\.F90", mat.group("fname")
            ):
                # hide the message
                mdict["id"] = "H" + self.id[1:]
            result.append(mdict)
        return result


class TooFewElements(CompilOutputCat, CompilMsg):
    """Too few elements in argument"""

    id = "E0105"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)r of size %(size)s, " "expecting %(expect)s"
    search = search_loc_msg(
        "Actual argument contains too few elements "
        "for dummy argument "
        "'(?P<elt>.*?)' \((?P<size>[0-9]+)/(?P<expect>[0-9]+)\).*?$"
    )


class TooManyArgs(CompilOutputCat, CompilMsg):
    """Too many arguments"""

    id = "E0106"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: call to %(elt)r"
    search = search_loc_msg("Too many arguments in call to '(?P<elt>.*?)' at")


class NotEnoughArgs(CompilOutputCat, CompilMsg):
    """Not enough arguments"""

    id = "E0107"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: call to %(elt)r, " "missing %(arg)r"
    search = search_loc_msg("Missing actual argument '(?P<arg>.*?)' in call to '(?P<elt>.*?)' at")


class UninitializedVariable(CompilOutputCat, CompilMsg):
    """Uninitialized variable"""

    id = "C0108"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(arg)r"
    search = search_loc_msg(".*?(?P<arg>\w+).*? is used uninitialized")


class MaybeUninitializedVariable(CompilOutputCat, CompilMsg):
    """Maybe uninitialized variable"""

    id = "C0109"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(arg)r"
    search = search_loc_msg(".*?(?P<arg>\w+).*? may be used uninitialized")


class TooLargeArraysOnStack(CompilOutputCat, CompilMsg):
    """Array is larger than limit on stack"""

    id = "C0110"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(arg)r"
    search = search_loc_msg(
        "Array '(?P<arg>.*?)' at .* is larger than limit set by .*\-fmax\-stack\-var\-size"
    )


# types errors


class PossibleChangeConversion(CompilOutputCat, CompilMsg):
    """Possible change of value in conversion"""

    id = "W0202"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)r"
    search = search_loc_msg("Possible change of value in conversion from " "(?P<elt>.*?) at.*?$")


class ImplicitType(CompilOutputCat, CompilMsg):
    """No implicit type"""

    id = "E0203"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(what)s %(elt)r"
    search = search_loc_msg("(?P<what>\w+) '(?P<elt>\w+)' at .*? has " "no IMPLICIT type")


class OperandsInComparison(CompilOutputCat, CompilMsg):
    """Operands of comparison operator mismatch"""

    id = "W0204"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: " "operator %(op)r, types %(elt)r"
    search = search_loc_msg(
        "Operands of comparison operator '(?P<op>.+?)' " "at .*? are (?P<elt>.*?)$"
    )


class SameArgInOut(CompilOutputCat, CompilMsg):
    """Same variable used for IN and OUT argument
    The same variable is used for an input and an output argument."""

    id = "C0205"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: " "arguments %(arg1)r and %(arg2)r"
    search = search_loc_msg(
        "Same actual argument associated with INTENT\(\w+\) "
        "argument '(?P<arg1>.+?)' and INTENT\(\w+\) argument '(?P<arg2>.+?)' at"
    )


class Misalignment(CompilOutputCat, CompilMsg):
    """Problem of alignment of variables"""

    id = "W0206"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(type)s %(where)s, before variable %(var)r"
    search = search_loc_msg(
        "Padding of (?P<byte>.*?) bytes required before "
        "'(?P<var>.+?)' in (?P<type>.+?) +'(?P<where>.+?)' at .*$"
    )


class ArrayOutOfBounds(CompilOutputCat, CompilMsg):
    """Array accessed out of its bounds"""

    id = "C0207"
    fmt = "%(id)s: %(label)s: in dimension %(dim)s: %(bounds)s"
    search = search_loc_msg(
        r"Array reference.*is out of bounds \((?P<bounds>.+?)\) in dimension (?P<dim>[0-9]+)"
    )


# declarations errors


class DuplicateExternal(CompilOutputCat, CompilMsg):
    """Duplicate external declaration"""

    id = "E0301"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)r"
    search = search_loc_msg(
        "EXTERNAL\s+(?P<elt>.*?)\s*$.*?^Error:\s+" "Duplicate EXTERNAL attribute specified at"
    )


class UnusedVar(CompilOutputCat, CompilMsg):
    """Unused variable"""

    id = "W0302"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)r"
    search = search_loc_msg("Unused variable '(?P<elt>\w+)' declared")


class UnusedParam(CompilOutputCat, CompilMsg):
    """Unused parameter"""

    id = "C0303"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)r"
    search = search_loc_msg("Unused parameter '(?P<elt>\w+)' declared")


class UnusedLabel(CompilOutputCat, CompilMsg):
    """Unused label"""

    id = "C0304"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)r"
    search = search_loc_msg("Label (?P<elt>[0-9]+) " "at .*? defined but not used")


class UsedBeforeTyped(CompilOutputCat, CompilMsg):
    """Symbol used before being typed"""

    id = "C0305"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)r"
    search = search_loc_msg("Extension:\s+" "Symbol '(?P<elt>\w+)' is used before it is typed at")


class ExpectedDimension(CompilOutputCat, CompilMsg):
    """Expected another dimension in array declaration"""

    id = "E0306"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s"
    search = search_loc_msg("Expected another dimension in array declaration " "at .*?")


class UnusedParameter(CompilOutputCat, CompilMsg):
    """Unused (function) parameter"""

    id = "W0307"
    fmt = "%(id)s: %(label)s: %(elt)r"
    search = re.compile(
        "(?P<errw>warning):\s+" "unused parameter\s+.*?(?P<elt>\w+)", flags=re.M
    ).finditer


class ImplicitInterface(CompilOutputCat, CompilMsg):
    """Call with an implicit interface
    An explicit interface must be defined to call an external subprogram.
    You must add the corresponding include statement for this subprogram."""

    id = "C0308"
    fmt = "%(id)s: %(label)s: %(elt)r"
    search = search_loc_msg(".*? '(?P<elt>\w+)' called with an implicit " "interface at .*?")


# standard conformance


class NoTabs(CompilOutputCat, CompilMsg):
    """Nonconforming tab character"""

    id = "C0401"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s (this position is often wrong)"
    search = search_msg(
        "Nonconforming tab character " "in column (?P<col>[0-9]+) of line (?P<line>[0-9]+)"
    )


class CharacterTruncated(CompilOutputCat, CompilMsg):
    """Character will be truncated"""

    id = "W0402"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(src)s chars but " "dest length is %(dest)s"
    search = search_loc_msg(
        "CHARACTER expression will be truncated "
        "in assignment \((?P<dest>[0-9]+)/(?P<src>[0-9]+)\).*?$"
    )

    def analyze(self):
        """Compute possible fix"""
        self.result["repl"] = VARNAME + "(1:%(dest)s)" % self.result


class CharacterTruncatedSure(CompilOutputCat, CompilMsg):
    """Character is truncated"""

    id = "C0403"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(src)s chars but " "dest length is %(dest)s"
    search = search_loc_msg(
        "CHARACTER expression at .*? is being truncated " "\((?P<src>[0-9]+)/(?P<dest>[0-9]+)\).*?$"
    )


class ObsolescentFeature(CompilOutputCat, CompilMsg):
    """Obsolescent feature
    Source code may not work unaltered with modern compilers.
    See http://fortranwiki.org/fortran/show/Modernizing+Old+Fortran

    ``DATA`` statements should be replaced by adding the ``PARAMETER`` attribute.

    Example::

        character(len=3) :: tchar(2)
        data tchar /'AAA', 'BBB'/

    replaced by::

        character(len=3), parameter :: tchar(2) = (/'AAA', 'BBB'/)
    """

    id = "C0404"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)s"
    search = search_loc_msg("Obsolescent feature: (?P<elt>.*?) at")


class EquivalenceMixingTypes(CompilOutputCat, CompilMsg):
    """Equivalence between different types"""

    id = "W0405"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)s non-%(type)s " "in %(equiv)s"
    search = search_loc_msg(
        "Non\-(?P<type>[a-zA-Z0-9_ ]+) object .*?" "(?P<elt>[a-zA-Z0-9_']+) in (?P<equiv>.*?) at"
    )


class MissingComma(CompilOutputCat, CompilMsg):
    """Missing comma"""

    id = "E0406"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s"
    search = search_loc_msg("Extension: Missing comma at")


class NonStdDeclaration(CompilOutputCat, CompilMsg):
    """Nonstandard type declaration"""

    id = "C0407"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)s"
    search = search_loc_msg("Nonstandard type declaration (?P<elt>.*?) at")


class HollerithConstant(CompilOutputCat, CompilMsg):
    """Deprecated Hollerith constant"""

    id = "C0408"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s"
    search = search_loc_msg("Extension: Hollerith constant")


class DeletedFeature(CompilOutputCat, CompilMsg):
    """Deleted feature"""

    id = "W0409"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(elt)s"
    search = search_loc_msg("Deleted feature: (?P<elt>.*?)$")


class ArrayIndex(CompilOutputCat, CompilMsg):
    """Unexpected type for array index"""

    id = "C0410"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: " "expecting %(req)r, found %(elt)r"
    search = search_loc_msg(
        "Array index at .*? must be of " "(?P<req>\w+) type, found (?P<elt>\w+)"
    )


class RealArrayIndex(CompilOutputCat, CompilMsg):
    """Real array index"""

    id = "C0411"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: must be an integer"
    search = search_loc_msg("Extension: (?P<main>REAL array index) at")


class DoubleComplex(CompilOutputCat, CompilMsg):
    """Double complex"""

    id = "C0412"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: use complex(kind=8)"
    search = search_loc_msg("Extension: DOUBLE COMPLEX at")


class ComparisonBetweenReal(CompilOutputCat, CompilMsg):
    """Equality/inequality comparison for floating-point numbers
    It is unsafe to rely on exact floating-point comparisons. Slight variations
    in rounding can change the outcome of such comparisons, leading to
    non-convergence or other unexpected behavior.
    It is relevant only if the variables have been assigned with the same value.
    Prefer use: abs(a-b) < abs((a+b)/2)*r8prem() to test equality with a small
    tolerance.
    We should only compare to r8vide() or other assigned numbers.
    """

    id = "W0413"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: only relevant after assignment"
    _search = search_loc_msg("(Ine|E)quality comparison for (?P<typ>.*) at")

    def search(self, txt):
        """Search comment, no ['"] before '!'."""
        lmat = self._search(txt)
        result = []
        for mat in lmat:
            mdict = mat.groupdict()
            if re.search("r8vide", mat.group("sth"), flags=re.I):
                # hide the message
                mdict["id"] = "H" + self.id[1:]
            result.append(mdict)
        return result


class UnaryOpAfterArithmeticOp(CompilOutputCat, CompilMsg):
    """Unary operator following arithmetic operator
    The same code may be compiled differently by different compilers.
    Example: ``d1=a**-2*b`` gets interpretted as ``d1=a**(-2*b)`` by ifort but
    as ``d1=(a**-2)*b`` by gfortran.
    """

    id = "C0414"
    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: must be an integer"
    search = search_loc_msg(
        "Extension: (?P<main>Unary operator following " "arithmetic operator).* at"
    )


CHECK_LIST = checkers_from_context(globals(), CompilMsg)
