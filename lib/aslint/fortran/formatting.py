# coding=utf-8

"""
Module to reformat fortran source code.
"""
# TODO: import regexp as RX...

import re

from aslint.logger import logger
from aslint.utils import join_lines

STEP_INDENT = 4
MAX_INDENT = 24

TRAILING_SPACE = re.compile(' +$', re.M)
CONT_SPACE = re.compile(' +&$', re.M)
FIRST_INDENT = re.compile('^      ', re.M)
SUBFUNC = re.compile('^ +(subroutine|function)', re.M | re.I)
END_SUBFUNC = re.compile(' +end +(subroutine|function) *$', re.I | re.DOTALL)
END_SUBFUNC2 = re.compile(' +end *$', re.I | re.DOTALL)
LABEL_INDENT = re.compile('^(?P<bef> *)(?P<label>[0-9]+)(?P<aft> +)', re.M)
EXPR_COMMENT = re.compile('^(?P<str>[!#].*?$)', re.M)
EXPR_STR = re.compile("'(?P<str>.+?)'", re.M)
EXPR_STR2 = re.compile('"(?P<str>.+?)"', re.M)
STR6P = re.compile('(?P<str>\w{6}\w+)')


def is_f90_txt(txt):
    """Return True if 'txt' is a f90 source code."""
    return re.search('^!', txt, flags=re.M) is not None


def reformat_source(txt, interface=False):
    """Reformat the given source code."""
    txt = TRAILING_SPACE.sub('', txt)
    txt = CONT_SPACE.sub('&', txt)
    txt = FIRST_INDENT.sub(' ' * STEP_INDENT, txt)
    txt = LABEL_INDENT.sub(label_repl, txt)
    is_sub = (not interface) and SUBFUNC.search(txt) is not None
    logger.debug("is_sub: %s", is_sub)
    if is_sub:
        subfunc = SUBFUNC.search(txt).group(1)
        logger.debug("subfunc: %s", subfunc)
        txt = SUBFUNC.sub('\g<1>', txt, count=1)
        txt = END_SUBFUNC.sub('end \g<1>', txt)
        txt = END_SUBFUNC2.sub('end ' + subfunc, txt)
    chg = lower_fortran(txt)
    txt = check_splitted_string(chg, txt)
    return txt


def lower_fortran(txt):
    """Change all 'txt' to lowercases but comments and strings."""
    low = txt.lower()
    mat_s6p = STR6P.finditer(txt)
    mat_cmt = EXPR_COMMENT.finditer(txt)
    mat_str = EXPR_STR.finditer(txt)
    petsc_typ = ('vecmpi', 'kspcr', 'kspcg', 'kspgcr',
                 'pcilu', 'pcsor', 'pcml', 'pcnone')
    expr_petsc = re.compile(r'\b(?P<str>' + '|'.join(petsc_typ) + r')\b', re.I)
    mat_pet = expr_petsc.finditer(txt)
    low = _loop_matchobj_iter_sub(low, mat_s6p, except_statement=True)
    low = _loop_matchobj_iter_sub(low, mat_pet)
    low = _loop_matchobj_iter_sub(low, mat_cmt)
    low = _loop_matchobj_iter_sub(low, mat_str)
    return low


def _loop_matchobj_iter_sub(txt, matchobj, except_statement=False):
    """Loop on 'matchobj' and substitute values in 'txt'.
    'matchobj' was built on 'txt' before changes [here before txt.lower()]."""
    _except_statement = (
        'subroutine', 'function', 'include', 'continue', 'interface',
        'equivalence', 'external', 'volatile',
        'character', 'integer', 'parameter', 'implicit', 'logical', 'complex',
        'inquire', 'backspace', 'sequence', 'default', 'pointer', 'adjustl',
        'allocate', 'deallocate', 'cpu_time', 'system_clock',
        'hpalloc', 'hpdeallc',
        'locname', 'request', 'esizemu',
    )
    for mat in matchobj:
        orig = mat.group('str')
        if except_statement and orig.lower() in _except_statement:
            orig = orig.lower()
        start, end = mat.start('str'), mat.end('str')
        txt = txt[:start] + orig + txt[end:]
    return txt


def label_repl(matchobj):
    """Replacement function for LABEL_INDENT"""
    lab = '%3d' % int(matchobj.group('label'))
    # - 6 (first indent)
    offset = len(''.join(matchobj.groups())) - 6 + STEP_INDENT - len(lab)
    offset = max(1, offset)
    return lab + ' ' * offset


def check_splitted_string(changed, original):
    """Fix splitted strings that contain a odd number of "'".
    'changed' is the text already changed to lower cases.
    The part of the line after the last or before the first "'" is
    reset to its original value."""
    # detection: egrep -n "^[^\!][^']+'[^']*$" bib*/**/*.f
    new = []
    lorig = original.splitlines()
    lchgd = changed.splitlines()
    for orig, line in zip(lorig, lchgd):
        spl = line.split("'")
        if len(line) > 0 and line[0] != '!' and len(spl) % 2 == 0:
            logger.debug('--- %s', line)
            splorig = orig.split("'")
            assert len(spl) == len(splorig), 'lines mismatch:\n%s\nvs\n%s' \
                % (orig, line)
            if re.search('^ *&', line) is None:
                line = "'".join(spl[:len(spl) - 1]) + "'" + splorig[-1]
            else:
                line = splorig[0] + "'" + "'".join(spl[1:])
            logger.debug("+++ %s", line)
        new.append(line)
    txt = join_lines(new)
    return txt
