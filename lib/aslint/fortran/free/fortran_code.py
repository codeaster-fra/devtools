# coding=utf-8

"""
Limited fortran parser.

Features and supported statements are limited. It is designed to allow
simple modification of the source code.
"""

# reference: http://www.ews.uiuc.edu/~mrgates2/docs/fortran.html

import os
import re
import copy
from functools import partial, wraps

from aslint.logger import logger
from aslint.utils import join_lines
from aslint.decorators import interrupt_decorator
import aslint.fortran.free.code_func as CF
import aslint.fortran.free.regexp as RX

_EMPTY_LINE = "_EMPTY_LINE_"
INDENT = " " * 4
MAXLENGTH = 98
MINLENGTH = 12


class FortranParserError(Exception):
    """Exception during parsing a fortran source code"""

    def __init__(self, msg, child_exc):
        """Initialization."""
        Exception.__init__(self)
        self.msg = msg
        self.child = child_exc

    def __str__(self):
        """To string"""
        return " ".join([self.msg, ":", str(self.child)])


def report_decorator(func):
    """Decorator to locate the error"""

    @wraps(func)
    def wrapper(inst, *args, **kwds):
        """Wrapper around a method of Block"""
        try:
            return func(inst, *args, **kwds)
        except Exception as exc:
            msg = "in '%s.%s()' at line %d" % (inst.__class__.__name__, func.__name__, inst.lnum)
            raise FortranParserError(msg, exc)

    return wrapper


class Block(object):
    """A block of fortran code.
    The method 'parse' is automatically called after the code is registered.
    Attributes:
    - lnum: the line number in the file of the first line of the block
    - lines: raw lines of code of the block (does not contain lines of
      its sub-blocks)
    - level: depth of the block. 0: the main toplevel block.
    - blocks: a list of sub-blocks or statements
    - code: formatted text of the code
    - attr: specific attributes of the statement.
    """

    regexp = None
    expr = None
    end_block = None
    _rx_eol = re.compile("& *$")
    _rm_end = partial(re.compile("& *$", re.M).sub, "")
    _rm_beg = partial(re.compile("^ *&?", re.M).sub, "")

    def __init__(self, lnum, level, opts=None):
        """Initialization"""
        self.lnum = lnum
        self.level = level
        self.lines = []
        self.code = ""
        self.blocks = []
        self.opts = None
        self.attr = {}
        self._closed = False
        self._parsed = False
        self.set_options(opts)

    def __iter__(self):
        """Iterator on the sub-blocks"""
        for blck in self.blocks:
            yield blck

    def find_block(self, searched):
        """Iterator on the sub-blocks recursively"""
        for i, blck in enumerate(self.blocks):
            if blck == searched:
                return i, self
            pos, parent = blck.find_block(searched)
            if pos > -1:
                return pos, parent
        return -1, None

    def loop_blocks(self):
        """Loop recursively on elementary blocks"""
        blocks = []
        for i, blck in enumerate(self.blocks):
            blocks.append(blck)
            if len(blck.blocks) > 0:
                blocks.extend(blck.loop_blocks())
        return blocks

    def __len__(self):
        """Return the number of sub-blocks of code"""
        return len(self.blocks)

    def __getitem__(self, i):
        """Return the i-th block"""
        return self.blocks[i]

    def set_options(self, opts):
        """Set and check the options"""
        self.opts = opts or {}
        self.opts["maxlength"] = self.opts.get("maxlength", MAXLENGTH)
        assert self.opts["maxlength"] > 8, "%s: maxlength=%d" % (self, self.opts["maxlength"])

    def copy(self):
        """Return a copy of the Block object."""
        new = self.__class__(self.lnum, self.level)
        new.lines = self.lines[:]
        new.code = self.code
        for blck in self:
            new.blocks.append(blck.copy())
        new.set_options(self.opts.copy())
        new.attr = copy.deepcopy(self.attr)
        new._closed = self._closed
        new._parsed = self._parsed
        return new

    def strip(self):
        """Remove leading and trailing EmptyLine of the Block."""
        while len(self.blocks) and isinstance(self.blocks[0], EmptyLine):
            del self.blocks[0]
        while len(self.blocks) and isinstance(self.blocks[-1], EmptyLine):
            del self.blocks[-1]

    def get_type(self, ltyp, only_this=False):
        """Return the statements (not copied) of the given types as
        a new Block object."""
        code = Block(1, 0, self.opts)
        if isinstance(self, ltyp):
            code.blocks.append(self)
        for blck in self:
            if only_this:
                if isinstance(self, ltyp):
                    code.blocks.append(self)
            else:
                code.blocks.extend(blck.get_type(ltyp))
        return code

    def lines2text(self):
        """Make a text from the read lines to make the parser work easier."""
        lines = CF.apply_on_lines(CF.remove_continuation, self.lines)
        frepl = partial(CF.replace_extra, char=" ")
        lines = CF.apply_on_lines(frepl, lines)
        self.code = "".join(lines)

    def loop_on_lines(self, lines):
        """Create the list of blocks.
        Return the current line number after read and a list of the
        MultipleCloseStatement statements seen in this block because they
        may close the parent too."""
        close_st = []
        # read the 'Begin of block' statement
        lnum = self.lnum
        if self.regexp:
            lnum += 1
            line = lines.pop(0)
            self.lines.append(line)
            init_closed = self._rx_eol.search(line) is None
            # print '#dbg blck init %d:%r : %s' % (lnum, line, init_closed)
            while not init_closed and len(lines) > 0:
                line = lines.pop(0)
                lnum += 1
                self.lines.append(line)
                init_closed = self._rx_eol.search(line) is None
                # print '#dbg blck  add %d:%r : %s' % (lnum, line, init_closed)
        # parse the 'main' statement of the block (it has no sub-block yet)
        self.parse()
        # fill inside up to next "End of block" instance
        next_level = self.level + 1
        while not self.is_closed() and len(lines) > 0:
            blck = statement_factory(lines[0], lnum, next_level, self.opts, self.attr)
            self.blocks.append(blck)
            lnum, sub_clst = blck.loop_on_lines(lines)
            if self.end_block:
                # does blck (or another sub-close stmt) ends this block
                may_close = [blck] + sub_clst
                for mcl in may_close:
                    close_this = (
                        isinstance(mcl, self.end_block)
                        and mcl.search(mcl.code, self.attr) is not None
                    )
                    self._closed = self._closed or close_this
                    if isinstance(mcl, MultipleCloseStatement):
                        close_st.append(mcl)
        return lnum, close_st

    def is_closed(self):
        """Return True if the end of the statement is reached.
        The top level block is only closed at EOF."""
        return self._closed

    @classmethod
    def search(cls, first_line, dummy=None):
        """Tell if the 'first_line' matches the block/statement.
        'dummy' (dict) allows more complicated search function."""
        return cls.regexp.search(first_line)

    def _parse(self):
        """Parse the code, store attributes..."""
        # should be derivated for statements other than comments

    def _code_text(self):
        """Reformatted source code of the header of the block"""
        return _EMPTY_LINE

    def _init_lines(self, start, indent):
        """init lines"""
        lines = []
        length = self.opts["maxlength"] - len(start)
        if length < MINLENGTH:
            lines.append(start + "&")
            start = indent = self.indent()
            length = self.opts["maxlength"] - len(start)
        return lines, start, indent, length

    def _dbg(self):
        """Debug print"""
        if not self.code:
            return _EMPTY_LINE
        klname = self.__class__.__name__
        return "%04d:%d:%-8s: %s\n     %s" % (self.lnum, self.level, klname, self.code, self.attr)

    def indent(self):
        """Return the indentation of the block/statement"""
        return INDENT * self.level

    @report_decorator
    def parse(self):
        """Ask each block to parse its code."""
        if not self._parsed:
            self.attr = {}
            self.lines2text()
            try:
                self._parse()
            except:
                logger.error("parse failed: %s", self._dbg())
                raise
            self._parsed = True
        for blck in self:
            blck.parse()

    @report_decorator
    def code_text(self):
        """Return the code, eventually reformatted."""
        lines = [self._code_text()]
        for blck in self:
            lines.extend(blck.code_text().splitlines())
        lines = [line for line in lines if line != _EMPTY_LINE]
        return join_lines(lines, end_cr=self.level <= 0)

    def dbg(self):
        """Ask each block to parse its code."""
        lines = [self._dbg()]
        lines.extend([blck.dbg() for blck in self])
        lines = [line for line in lines if line != _EMPTY_LINE]
        return join_lines(lines, end_cr=False)

    # file interface
    def set_code(self, txt):
        """Set the source code"""
        lines = txt.splitlines()
        self.loop_on_lines(lines)
        self.strip()

    def write_code(self, fname):
        """Write the code into 'fname'"""
        with open(fname, "w") as fobj:
            fobj.write(self.code_text())


class Statement(Block):
    """A statement: some code and the line number in the parent source code"""

    def loop_on_lines(self, lines):
        """Loop on the lines until the end of the statement."""
        lnum = self.lnum + 1
        line = lines.pop(0)
        self.lines.append(line)
        # print '#dbg stmt init %d:%r : %s' % (lnum, line, self.is_closed())
        while not self.is_closed() and len(lines) > 0:
            lnum += 1
            line = lines.pop(0)
            self.lines.append(line)
            # print '#dbg stmt  add %d:%r : %s' % (lnum, line,
            # self.is_closed())
        self.parse()
        return lnum, []

    def is_closed(self):
        """Return True if the end of the statement is reached."""
        self._closed = len(self.lines) > 0 and self._rx_eol.search(self.lines[-1]) is None
        return self._closed

    def _code_text(self):
        """Just indent the first line by default"""
        return self.indent() + self.code.lstrip()


class NoIndentStatement(Statement):
    """Not indented statement"""

    def _code_text(self):
        """no indent"""
        return self.code.lstrip()


class CloseStatement(Statement):
    """A statement that ends a block of code."""

    def indent(self):
        """Because it belongs to a block, suppress one level of indentation"""
        return INDENT * (self.level - 1)


class MultipleCloseStatement(CloseStatement):
    """Statement that must be signaled to the parent block because it may
    close it too."""


# attributes


class Attribute(object):
    """Base class for attributes"""

    def __init__(self):
        """Initialization"""
        self.attr = None
        super(Attribute, self).__init__()


class HasName(Attribute):
    """For a named statement."""

    def __get_attr(self):
        """Return the name"""
        return self.attr["name"]

    name = property(__get_attr)


class HasArgs(Attribute):
    """For a statement with arguments"""

    def _get_attr(self):
        """Return the name"""
        return self.attr["arg"]

    args = property(_get_attr)

    def get_arg(self, arg):
        """The 'arg' as found in code or None"""
        expr = re.compile("^%s($| *\()" % re.escape(arg))
        for tst in self.args:
            if expr.search(tst) is not None:
                return tst
        return None

    def remove_arg(self, arg):
        """Remove the given argument if found.
        Return the argument given by 'get_arg()'."""
        found = self.get_arg(arg)
        if found:
            self.args.remove(found)
        return found


class HasCondition(Attribute):
    """For a statement with condition"""

    def _get_attr(self):
        """Return the condition"""
        return self.attr["cond"]

    cond = property(_get_attr)


class HasLabel(Attribute):
    """For a statement with label"""

    def _get_attr(self):
        """Return the label"""
        return self.attr["label"]

    label = property(_get_attr)

    # TODO see the MRO to use _code_text
    def _label_in_indent(self, orig):
        """Insert the label into the first indentation"""
        indent = self.indent()
        noindent = re.sub("^ *%s *" % self.label, "", orig)
        indent = "%3s " % self.label.strip() + indent.replace(INDENT, "", 1)
        return indent + noindent

    def _clean_label(self):
        """Normalize the label"""
        self.attr["label"] = re.sub("^0*", "", self.attr["label"])


# statements and blocks


class EmptyLine(NoIndentStatement):
    """An empty line"""

    regexp = re.compile("^[!]? *$")

    def _code_text(self):
        """Return a single comment"""
        return "!"


class Comment(NoIndentStatement):
    """A comment"""

    regexp = re.compile("^ *![^$]")

    def is_closed(self):
        """Single line statement."""
        return True

    def lines2text(self):
        """Keep lines unchanged."""
        self.code = os.linesep.join(self.lines)


class Preproc(NoIndentStatement):
    """A preprocessor directive"""

    regexp = re.compile("^#\W*")


class PreprocDefine(NoIndentStatement, HasName):
    """A preprocessor directive"""

    regexp = re.compile("^# *define *(?P<name>.*?)[\( ]*")

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected #include: %r" % self.code
        self.attr = mat.groupdict()


class PreprocInclude(NoIndentStatement, HasName):
    """A preprocessor directive"""

    regexp = re.compile("^# *include *['\"](?P<name>.*?)['\"]")

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected #include: %r" % self.code
        self.attr = mat.groupdict()


# ifdef/#endif is not a block else it would need store the no-indentation level
# XXX instead of passing next_level to statement_factory, pass "next
# attrs" from parent


class PreprocEndIf(CloseStatement):
    """End of a IfThen block"""

    regexp = re.compile("^# *endif *$")


class PreprocElse(CloseStatement):
    """A else statement"""

    regexp = re.compile("^# *else *$")


class PreprocElseIf(CloseStatement):
    """A else if statement"""

    regexp = re.compile("^# *(elseif|else if)\W*")


class PreprocIfDef(NoIndentStatement, HasCondition):
    """A conditional block #if, #ifdef, #ifndef"""

    regexp = re.compile("^# *(?:if|ifdef|ifndef) *(?P<cond>.*?) *$")
    end_block = PreprocEndIf

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected #ifdef: %r" % self.code
        self.attr = mat.groupdict()


class Directive(Statement):
    """A fortran directive (OpenMP)"""

    regexp = re.compile("^ *!$")

    def is_closed(self):
        """Must not be concatenated"""
        return True


class Untyped(Statement):
    """Not yet supported statement"""

    regexp = re.compile(".*")

    def lines2text(self):
        """Keep lines unchanged."""
        self.code = os.linesep.join(self.lines)


class Implicit(Statement):
    """An empty line"""

    regexp = re.compile("^ *implicit ")


class Decl(Statement, HasArgs):
    """A declaration statement"""

    regexp = re.compile("^ *(" "(integer|logical|real|complex|character)\W" "|[^'\"]*?::)", re.I)
    _re1 = re.compile("^ *(?P<type>(%s)) *:: *(?P<arg>.*)$" % RX.DECLS)
    _ret = re.compile("^ *(?P<type>(%s)) *:: *(?P<arg>.*)$" % RX.DECLS_TYPE)
    _re2 = re.compile("^ *(?P<type>(%s)) +(?P<arg>.*?)$" % RX.DECLS)
    _re3 = re.compile("^ *(?P<type>\w.*?) *:: *(?P<arg>.*)$")
    _re4 = re.compile("^ *(?P<type>\w.*?) *(?P<arg>.*)$")

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = (
            self._re1.search(self.code)
            or self._ret.search(self.code)
            or self._re2.search(self.code)
            or self._re3.search(self.code)
            or self._re4.search(self.code)
        )
        assert mat, "unexpected decl: %r" % self.code
        self.attr = mat.groupdict()
        try:
            # use RX.DECLS
            decltype = re.sub("type *\(", "type(", self.attr["decltype"].strip())
            self.attr["type"] = decltype + self.attr["declsize"].strip()
            kind = (self.attr["declkind"] or "").strip()
            self.attr["kind"] = kind.isdigit() and int(kind) or kind
            self.attr["attr"] = self.attr["declattr"] or ""
            self.attr["attr"] = CF.ArgSplitter(self.attr["attr"]).strings()
        except KeyError:
            self.attr["type"] = self.attr["type"].strip()
            self.attr["kind"] = ""
            self.attr["attr"] = []
        self.attr["arg"] = CF.ArgSplitter(self.attr["arg"]).strings()
        vsiz = {}
        vini = {}
        for i in self.attr["arg"]:
            ini = None
            for assign in ("=>", "="):
                spl = i.split(assign)
                if len(spl) == 2:
                    i, ini = spl[0], spl[1].strip()
                    break
            spl = CF.ParSplitter(i).strings()
            if len(spl) == 3:
                nam = spl[0].split("(")[0].strip()
                sub = CF.ArgSplitter(spl[1]).strings()
            else:
                nam, sub = i.strip(), []
            vsiz[nam] = sub
            vini[nam] = ini
        self.attr["varsize"] = vsiz
        self.attr["init"] = vini
        self.attr["names"] = list(vsiz.keys())

    def _code_text(self):
        """Split long decls"""
        if len(self.args) == 0:
            return _EMPTY_LINE
        typ = [self.attr["type"].strip()]
        if self.attr["attr"]:
            typ.extend(self.attr["attr"])
        start = self.indent() + ", ".join(typ) + " :: "
        lines, start, dummy, length = self._init_lines(start, start)
        args = CF.args_from_list(self.args)
        allocd = args.alloc(length, cont=False)
        nosplit = [i for i in allocd if i.startswith("&")]
        if nosplit:
            lines.extend(self.lines)
        else:
            lines.extend([start + line for line in allocd])
        return join_lines(lines, end_cr=False)


class Call(Statement, HasName, HasArgs):
    """A call statement"""

    regexp = re.compile("^ *call +[a-zA-Z0-9_%]+", re.I)
    _re1 = re.compile("^ *call *(?P<name>[a-zA-Z0-9_%]+) *?\((?P<arg>.*)\)", re.I)
    _re2 = re.compile("^ *call *(?P<name>[a-zA-Z0-9_%]+) *$", re.I)

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self._re1.search(self.code)
        if not mat:
            mat = self._re2.search(self.code)
            self.attr = mat.groupdict()
            self.attr["arg"] = []
        else:
            assert mat, "unexpected call: %r" % self.code
            self.attr = mat.groupdict()
            self.attr["arg"] = CF.ArgSplitter(self.attr["arg"]).strings()

    def _code_text(self):
        """Split lines in groups of 5 arguments"""
        start = self.indent() + "call " + self.name + "("
        indent = " " * len(start)
        if len(self.args) == 0:
            return start + ")"
        lines, start, indent, length = self._init_lines(start, indent)
        args = CF.args_from_list(self.args)
        allocd = args.alloc(length, maxi=5, cont=True)
        lines.append(start + allocd.pop(0))
        lines.extend([indent + grp for grp in allocd])
        lines[-1] = lines[-1] + ")"
        return join_lines(lines, end_cr=False)


class EndSub(CloseStatement):
    """End of a Subroutine or Function block"""

    regexp = re.compile("^ *end(| +(subroutine|function)(| +\w+)) *$", re.I)


class Subroutine(Block, HasName, HasArgs):
    """A subroutine statement"""

    regexp = re.compile("^ *(?P<qual>(|recursive)) *subroutine +\w+", re.I)
    end_block = EndSub
    _re1 = re.compile(
        "^ *(?P<qual>(|recursive)) *subroutine *(?P<name>\w+) *?" "\((?P<arg>.*?)\)", re.I
    )
    _re2 = re.compile("^ *(?P<qual>(|recursive)) *subroutine *(?P<name>\w+) *$", re.I)

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self._re1.search(self.code)
        if not mat:
            mat = self._re2.search(self.code)
            self.attr = mat.groupdict()
            self.attr["arg"] = []
        else:
            assert mat, "unexpected subroutine: %r" % self.code
            self.attr = mat.groupdict()
            self.attr["arg"] = CF.ArgSplitter(self.attr["arg"]).strings()

    def _code_text(self):
        """Split lines in groups of 5 arguments"""
        qual = self.attr.get("qual", "")
        start = self.indent() + qual + "subroutine " + self.name + "("
        indent = " " * len(start)
        if len(self.args) == 0:
            return start + ")"
        lines, start, indent, length = self._init_lines(start, indent)
        args = CF.args_from_list(self.args)
        allocd = args.alloc(length, maxi=5, cont=True)
        lines.append(start + allocd.pop(0))
        lines.extend([indent + grp for grp in allocd])
        lines[-1] = lines[-1] + ")"
        return join_lines(lines, end_cr=False)


class Function(Block, HasName, HasArgs):
    """A function statement"""

    regexp = re.compile("^ *(%s)? *function +\w+" % RX.DECLS, re.I)
    end_block = EndSub
    _re1 = re.compile(
        "^ *(?P<type>(%s)?) *function *" "(?P<name>\w+) *?\((?P<arg>.*?)\)" % RX.DECLS, re.I
    )
    _re2 = re.compile("^ *(?P<type>(%s)?) *function *" "(?P<name>\w+) *$" % RX.DECLS, re.I)
    _re0 = re.compile(
        "^ *function *" "(?P<name>\w+) *?\((?P<arg>.*?)\) *" "result *\((?P<result>.*?)\)", re.I
    )

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self._re0.search(self.code)
        if not mat:
            mat = self._re1.search(self.code)
        if not mat:
            mat = self._re2.search(self.code)
            self.attr = mat.groupdict()
            self.attr["arg"] = []
        else:
            assert mat, "unexpected Function: %r" % self.code
            self.attr = mat.groupdict()
            self.attr["type"] = self.attr.get("type", "").strip()
            self.attr["arg"] = CF.ArgSplitter(self.attr["arg"]).strings()

    def _code_text(self):
        """Split lines in groups of 5 arguments"""
        typ = self.attr.get("type", "")
        res = ""
        if self.attr.get("result"):
            res = f' result ({self.attr["result"]})'
        start = self.indent() + typ + "function " + self.name + "("
        indent = " " * len(start)
        if len(self.args) == 0:
            return start + ")" + res
        lines, start, indent, length = self._init_lines(start, indent)
        args = CF.args_from_list(self.args)
        allocd = args.alloc(length, maxi=5, cont=True)
        lines.append(start + allocd.pop(0))
        lines.extend([indent + grp for grp in allocd])
        lines[-1] = lines[-1] + ")" + res
        return join_lines(lines, end_cr=False)


class EndDo(CloseStatement, HasLabel):
    """End of a LoopDo block"""

    expr = "^ *(|%(label)s +)(end *do)(|\W.*)$"
    _re1 = re.compile("^ *(?P<label>|[0-9]* +)end *do", re.I)

    @classmethod
    def search(cls, first_line, kwargs=None):
        """Use the label of the Loop with label"""
        if kwargs.get("label") is None:
            return False
        expr = re.compile(cls.expr % kwargs, flags=re.I)
        return expr.search(first_line)

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self._re1.search(self.code)
        assert mat, "unexpected EndDo: %r" % self.code
        self.attr = mat.groupdict()
        self._clean_label()

    def _code_text(self):
        """label"""
        orig = CloseStatement._code_text(self)
        return self._label_in_indent(orig)


class Continue(Statement, HasLabel):
    """A Continue statement not appaired to a loop"""

    regexp = re.compile("^ *(?P<label>|[0-9]* +)continue *$", re.I)

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected Continue: %r" % self.code
        self.attr = mat.groupdict()
        self._clean_label()

    def _code_text(self):
        """label"""
        orig = Statement._code_text(self)
        return self._label_in_indent(orig)


class CloseContinue(Continue, MultipleCloseStatement):
    """End of a Loop block using label"""

    expr = "^ *%(label)s +continue *$"

    @classmethod
    def search(cls, first_line, kwargs=None):
        """Use the label of the Loop with label"""
        if kwargs.get("label") is None:
            return False
        expr = re.compile(cls.expr % kwargs, flags=re.I)
        return expr.search(first_line)


class LoopDo(Block, HasArgs, HasLabel):
    """A block of type loop Do"""

    # should be UseLabel
    regexp = re.compile(
        "^ *do +(?P<label>[0-9]*)(|,) *(?P<var>\w+) *" "= *(?P<arg>.+? *, *.+(| *,.+))$", re.I
    )
    end_block = (EndDo, CloseContinue)

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected LoopDo: %r" % self.code
        self.attr = mat.groupdict()
        self._clean_label()
        self.attr["arg"] = CF.ArgSplitter(self.attr["arg"]).strings()

    def _code_text(self):
        """Command line"""
        attrs = self.attr.copy()
        attrs["args"] = str(CF.args_from_list(self.args))
        if attrs["label"]:
            attrs["label"] += " "
        text = "do %(label)s%(var)s = %(args)s" % attrs
        return self.indent() + text


class LoopDoWhile(Block, HasCondition):
    """A block of type loop Do while"""

    regexp = re.compile("^ *do +(?P<label>[0-9]*) *while *" "\( *(?P<cond>.*?) *\) *$", re.I)
    end_block = (EndDo, CloseContinue)

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected LoopDoWhile: %r" % self.code
        self.attr = mat.groupdict()

    def _code_text(self):
        """Command line"""
        text = "do while (%s)" % self.cond
        return self.indent() + text


class EndIf(CloseStatement):
    """End of a IfThen block"""

    regexp = re.compile("^ *end *if *$", re.I)


class Else(CloseStatement):
    """A else statement"""

    regexp = re.compile("^ *else *$", re.I)


class ElseIf(CloseStatement, HasCondition):
    """A else if statement"""

    regexp = re.compile("^ *else *if *\( *(?P<cond>.*?) *\) *then", re.I)

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected ElseIf: %r" % self.code
        self.attr = mat.groupdict()

    def _code_text(self):
        """Command line"""
        text = "else if (%s) then" % self.cond
        return self.indent() + text


class Assert(Statement, HasCondition):
    """A assert statement"""

    regexp = re.compile("^ *ASSERT *\((?P<cond>.*)\)")

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        self.attr = mat.groupdict()

    def _code_text(self):
        """Command line"""
        return self.indent() + "ASSERT(%s)" % self.cond


class IfThen(Block, HasCondition):
    """A conditional block if or if/then"""

    regexp = re.compile("^ *if((?: +|\().*)$", re.I)
    _re1 = re.compile("^ *if *\( *(?P<cond>.*) *\) *then", re.I)
    end_block = EndIf

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self._re1.search(self.code)
        if mat:
            assert mat, "unexpected IfThen: %r" % self.code
            self.attr = mat.groupdict()
            self.attr["cond"] = self.attr["cond"].strip()
            self.attr["then"] = "then"
        else:
            # single line if
            mat = self.regexp.search(self.code)
            assert mat, "unexpected IfThen: %r" % self.code
            txt = re.sub("^ +", "", mat.group(1))
            assert txt.startswith("("), "unexpected If: %r" % self.code
            cond = CF.first_enclosed(txt)
            self.attr["cond"] = str(CF.ParSplitter(cond)[1])
            self.attr["instr"] = txt.replace(cond, "", 1).strip()
            self.attr["then"] = ""
            self._closed = True

    def _code_text(self):
        """Command line"""
        start, then = self.indent() + "if (", ") %s" % self.attr["then"]
        indent = " " * len(start)
        lines, start, indent, length = self._init_lines(start, indent)
        cond = CF.LogicalSplitter(self.cond)
        allocd = cond.alloc(length - len(then), cont=True)
        lines.append(start + allocd.pop(0))
        lines.extend([indent + grp for grp in allocd])
        last = lines.pop() + then
        if self.attr["then"] != "":
            lines.append(last)
            text = join_lines(lines, end_cr=False)
        else:
            indent = " " * len(last)
            line2, start2, indent, length = self._init_lines(last, indent)
            instr = source(self.attr["instr"], opts={"maxlength": length})
            if not isinstance(instr[0], Untyped):
                allocd = instr.code_text().splitlines()
            else:
                instr = CF.BaseSplitter(self.attr["instr"])
                allocd = instr.alloc(length, cont=True)
            line2.append(start2 + allocd.pop(0))
            line2.extend([indent + grp for grp in allocd])
            lines.extend(line2)
            text = join_lines(lines, end_cr=False)
        return text


class Assign(Statement):
    """An assignment"""

    @classmethod
    def search(cls, first_line, dummy=None):
        """Tell if the 'first_line' matches the block/statement.
        'dummy' (dict) allows more complicated search function."""
        spl = CF.SpaceSplitter(first_line)
        return spl.size() >= 3 and spl[1] == "="

    def _parse(self):
        """Parse the code, store attributes..."""
        spl = CF.SpaceSplitter(self.code)
        assert spl[1] == "=", "unexpected Assign: %r" % self.code
        self.attr["dest"] = str(spl[0])
        self.attr["instr"] = " ".join(spl[2:])

    def _code_text(self):
        """Command line"""
        start = self.indent() + "%(dest)s = " % self.attr
        indent = " " * len(start)
        lines, start, indent, length = self._init_lines(start, indent)
        instr = source(self.attr["instr"], opts={"maxlength": length})
        if not isinstance(instr[0], Untyped):
            allocd = instr.code_text().splitlines()
        else:
            instr = CF.BaseSplitter(self.attr["instr"])
            allocd = instr.alloc(length, cont=True)
        lines.append(start + allocd.pop(0))
        lines.extend([indent + grp for grp in allocd])
        return join_lines(lines, end_cr=False)


class EndInterface(CloseStatement):
    """End of a Interface block"""

    regexp = re.compile(r"^ *end(| +interface(| +\w+)) *$", re.I)


class Interface(Block):
    """A interface statement"""

    # 'assignment(=)' or 'operator(+)' are ignored, kept as 'name'
    regexp = re.compile("^ *interface *(?P<name>|.*) *$", re.I)
    end_block = EndInterface

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected interface: %r" % self.code
        self.attr = mat.groupdict()

    def _code_text(self):
        """Just indent the first line by default"""
        return self.indent() + self.code.lstrip()


class EndSelectCase(CloseStatement):
    """End of a select case block"""

    regexp = re.compile("^ *end(| +select) *$", re.I)


class SelectCase(Block, HasArgs):
    """A select case block"""

    regexp = re.compile("^ *select case *\( *(?P<arg>\w+) *\) *$", re.I)
    end_block = EndSelectCase

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected select case: %r" % self.code
        self.attr = mat.groupdict()

    def _code_text(self):
        """Command line"""
        text = "select case (%s)" % self.args
        return self.indent() + text


_re_case = re.compile("^ *case *((?P<def>default)|\( *(?P<arg>\w+) *\)) *$", re.I)


class EndCase(CloseStatement, HasArgs):
    """A case marks the end of the previous case"""

    regexp = _re_case

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected case: %r" % self.code
        self.attr = mat.groupdict()
        if mat.group("def"):
            self.attr["arg"] = mat.group("def")

    def _code_text(self):
        """Command line"""
        arg = self.args if self.args.strip().lower() == "default" else "(%s)" % self.args
        text = "case %s" % arg
        return self.indent() + text


class Case(Block, HasArgs):
    """A case block"""

    regexp = _re_case
    end_block = EndCase

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected case: %r" % self.code
        self.attr = mat.groupdict()
        if mat.group("def"):
            self.attr["arg"] = mat.group("def")

    def _code_text(self):
        """Command line"""
        arg = self.args if self.args == "default" else "(%s)" % self.args
        text = "case %s" % arg
        return self.indent() + text


class EndModule(CloseStatement):
    """End of a Module block"""

    regexp = re.compile("^ *end(| +module(| +\w+)) *$", re.I)


class Module(Block, HasName):
    """A module statement"""

    regexp = re.compile("^ *module +(?P<name>\w+) *$", re.I)
    end_block = EndModule

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected module: %r" % self.code
        self.attr = mat.groupdict()

    def _code_text(self):
        """Just indent the first line by default"""
        return self.indent() + self.code.lstrip()


class Contains(CloseStatement):
    """A contains statement"""

    regexp = re.compile("^ *contains *$", re.I)


class GlobalPrivate(Statement):
    """A global private statement"""

    regexp = re.compile("^ *private *$", re.I)


class GlobalPublic(Statement):
    """A global public statement"""

    regexp = re.compile("^ *public *$", re.I)


class EndType(CloseStatement):
    """End of a Type block"""

    regexp = re.compile(r"^ *end(| +type(| +\w+)) *$", re.I)


class Type(Block):
    """A module statement"""

    regexp = re.compile(r"^ *type(| +\w+) *$", re.I)
    end_block = EndType

    def _code_text(self):
        """Just indent the first line by default"""
        return self.indent() + self.code.lstrip()


class Use(Statement, HasArgs):
    """A use statement"""

    regexp = re.compile(
        r"^ *use +(?P<mod>\w+)("
        r"|(?P<rename>( *, *\w+ *=> *\w+)+)"
        r"| *, *only *: *(?P<only>(.*)+)"
        r") *$",
        re.I,
    )
    _rename = re.compile(r" *(?P<local>\w+) *=> *(?P<orig>\w+)")

    def _parse(self):
        """Parse the code, store attributes..."""
        mat = self.regexp.search(self.code)
        assert mat, "unexpected use: %r" % self.code
        self.attr = mat.groupdict()
        if self.attr["rename"]:
            rename = {}
            txt = self.attr["rename"]
            for mat in self._rename.finditer(txt):
                rename[mat.group("orig")] = mat.group("local")
            self.attr["rename"] = rename
        if self.attr["only"]:
            symb = {}
            for txt in self.attr["only"].split(","):
                mat = self._rename.search(txt)
                if mat:
                    symb[mat.group("orig")] = mat.group("local")
                else:
                    symb[txt.strip()] = txt.strip()
            self.attr["only"] = symb


_ALL_STMT = [
    EmptyLine,
    Directive,
    Comment,
    Implicit,
    PreprocIfDef,
    PreprocElse,
    PreprocElseIf,
    PreprocEndIf,
    PreprocDefine,
    PreprocInclude,
    Preproc,
    Subroutine,
    Function,
    EndSub,
    Interface,
    EndInterface,
    Module,
    EndModule,
    Type,
    EndType,
    SelectCase,
    EndSelectCase,
    EndCase,
    Case,
    Decl,
    Call,
    Assert,
    Assign,
    Contains,
    Use,
    GlobalPublic,
    GlobalPrivate,
    LoopDo,
    LoopDoWhile,
    EndDo,
    CloseContinue,
    IfThen,
    Else,
    ElseIf,
    EndIf,
    Continue,
]


def statement_factory(first_line, lnum, parent_level, opts, search_args=None):
    """Build a Statement object depending of 'first_line'."""
    for klass in _ALL_STMT:
        if klass.search(first_line, search_args):
            return klass(lnum, parent_level, opts)
    return Untyped(lnum, parent_level, opts)


# convenient functions


def source(text=None, filename=None, init_level=-1, opts=None):
    """Create a Block object parsing 'text' or reading 'filename'."""
    if not text:
        with open(filename, "r") as fobj:
            text = fobj.read()
    src = Block(1, level=init_level, opts=opts)
    src.set_code(text)
    return src


def get_subprogram(obj):
    """Return the subprogram from 'obj'.
    Return a list or a single object."""
    sub = obj.get_type((Subroutine, Function))
    if len(sub) == 1:
        sub = sub[0]
    return sub


@interrupt_decorator
def beautify(fname, backup_ext=None):
    """Beautify a file"""
    logger.info(fname)
    try:
        src = source(filename=fname)
        src.code_text()
        if backup_ext:
            os.rename(fname, fname + backup_ext)
        src.write_code(fname)
    except (AssertionError, Exception):
        print("error reading", fname)
        raise


def insert_block(inblock, idx, newblock):
    """Insert a new block at index idx of 'inblock'"""
    inblock.blocks.insert(idx, newblock)


def remove_block(inblock, idx):
    """Insert a new block at index idx of 'inblock'"""
    del inblock.blocks[idx]
