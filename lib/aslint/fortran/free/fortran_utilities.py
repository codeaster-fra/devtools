# coding=utf-8

"""
This modules provides some helpers functions to Fortran_Code objects
"""

import re
import os.path as osp

from aslint.logger import logger
import aslint.fortran.free.fortran_code as FC


def check_include(src, incname):
    """Tell if 'incname' has already been included"""
    preproc = src.get_type(FC.PreprocInclude)
    for i in preproc:
        if incname in i.code:
            return True
    return False


def check_var(src, varname):
    """Tell if 'varname' is declared in 'src'"""
    decl = src.get_type(FC.Decl)
    for i in decl:
        if varname in i.args:
            return True
    return False


def insert_include(src, incname):
    """insert the #include for incname"""
    if not check_include(src, incname):
        preproc = src.get_type(FC.PreprocInclude)
        if len(preproc) > 0:
            pos, where = src.find_block(preproc[0])
            assert pos >= 0
        else:
            pos, where = src.find_block(src.get_type(FC.Implicit)[0])
            assert pos >= 0
            pos += 1
        lvl = where.level - 1
        newb = FC.source('#include "%s"' % incname, init_level=lvl)[0]
        FC.insert_block(where, pos, newb)


def remove_include(src, incname):
    """remove #include "incname" """
    linc = [i for i in src.get_type(FC.PreprocInclude) if i.name == incname]
    for i in linc:
        pos, where = src.find_block(i)
        FC.remove_block(where, pos)


def sort_include(src):
    """Sort all #include"""
    def _key(path):
        """return a key to sort the include lines"""
        return (path.count('/'), path)
    exclude = ifdef_lines(src)
    linc = [(_key(i.name), i) for i in src.get_type(FC.PreprocInclude)
            if i.lnum not in exclude]
    if not linc:
        return
    pos0, where = src.find_block(linc[0][1])
    assert pos0 > -1, 'no #include in :\n%s' % src.dbg()
    for line, i in linc:
        pos, blck = src.find_block(i)
        FC.remove_block(blck, pos)
    linc.sort()
    sinc = [i[1] for i in linc]
    while sinc:
        FC.insert_block(where, pos0, sinc.pop())


def ifdef_lines(src):
    """return the list of lines between ifdef/endif"""
    lines = []
    sub = src.copy()
    ifdef = sub.get_type(FC.PreprocIfDef)
    while len(ifdef) > 0:
        start = ifdef[0].lnum
        pos, parent = sub.find_block(ifdef[0])
        logger.debug("#ifdef starts at %d, pos %d (in %s)", start, pos, parent)
        for i in range(pos):
            FC.remove_block(parent, 0)
        endif = parent.get_type(FC.PreprocEndIf)
        assert len(endif) > 0
        end = endif[0].lnum
        pos, where = parent.find_block(endif[0])
        logger.debug("#endif closed at %d, pos %d", start, pos)
        for i in range(pos):
            FC.remove_block(where, 0)
        lines.extend(list(range(start, end)))
        ifdef = sub.get_type(FC.PreprocIfDef)
    logger.debug("excluded lines: %s", lines)
    return lines


def eval_arg(value, ctxt={}):
    """Eval an argument: '0' -> 0, '1.d0' -> 1. or `value`."""
    try:
        value = int(value)
    except ValueError:
        try:
            value = float(value)
        except ValueError:
            try:
                value = eval(value, {}, ctxt)
            except (ValueError, SyntaxError, NameError, TypeError):
                logger.debug("eval_arg failed on %r with %r", value, ctxt)
                pass
    return value


def map_eval_args(values, ctxt={}):
    """Loop on values and try to eval each element"""
    vals = []
    for i in values:
        if type(i) is str:
            i = i.strip().replace(' ', '')
        i = eval_arg(i, ctxt)
        vals.append(i)
    return vals


def args_as_dict(args, varnames):
    """Assign positional and keyword arguments.
    All arguments are supposed to be optional."""
    kwargs = dict().fromkeys(varnames)
    assert len(args) <= len(varnames), (args, varnames)
    expr = re.compile('^ *(?P<var>\w+) *= *(?P<val>.*) *')
    kw = False
    loopv = list(varnames)[:len(args)]
    for i, dest in zip(args, loopv):
        if type(i) is str:
            mat = expr.search(i)
            if mat:
                kwargs[mat.group('var')] = eval_arg(mat.group('val'))
                kw = True
                continue
        assert not kw, 'positional arg found after keyword arg: %s' % i
        kwargs[dest] = eval_arg(i)
    return kwargs


def as_str(values):
    """Return a string to represent 'values'"""
    if type(values) not in (list, tuple):
        values = [values]
    return ', '.join([str(i) for i in values])


def get_nb_var(src, typ, args):
    """Extract and evaluate the first two arguments as (nb_value, array)
    'args' is changed (arguments removed) in place."""
    nb = eval_arg(args.pop(0))
    val = eval_arg(args.pop(0))
    new = []
    if nb == 0:
        pass
    elif nb == 1:
        ind1 = ""
        if type(val) is str and is_decl_array(src, val):
            ind1 = "(1)"
        new.append('s%s=%s%s' % (typ, val, ind1))
    else:
        new.extend(['n%s=%s' % (typ, nb), 'val%s=%s' % (typ, val)])
    return new


def is_decl_array(src, varname):
    """Tell if `varname` is declared as an array or not."""
    expr = re.compile('^%s *\(' % re.escape(varname))
    decl = src.get_type(FC.Decl)
    for i in decl:
        for var in i.args:
            if expr.search(var):
                return True
    return False


def get_decl(src, varname):
    """Return the declaration statement of 'varname'"""
    jnam = re.compile(' *\(.*$').sub
    decls = src.get_type(FC.Decl)
    for stmt in decls:
        names = [jnam('', i).strip() for i in stmt.args]
        if varname in names:
            return stmt
    return None


def get_called_sub(code):
    """Return the list of the name of the called subroutines"""
    # first change if/call statements into if/then/call/endif
    chg = code.get_type(FC.IfThen)
    for i in chg:
        if i.attr['then'] != '':
            continue
        pos, where = code.find_block(i)
        assert pos >= 0
        # remove the original if, replaced by a new block of 3 instructions
        FC.remove_block(where, pos)
        newb = "\n".join(["if (%s) then" % i.cond,
                          i.attr['instr'],
                          "endif"])
        FC.insert_block(where, pos, FC.source(newb, init_level=i.level - 1))
    txt = code.code_text()
    src = FC.source(txt)
    calls = src.get_type(FC.Call)
    return [i.name for i in calls]


def statement_following(src, searched, typ):
    """Search in 'src' the statements of type `typ` following `searched`
    Use typ=FC.Block for all statements."""
    seen = False
    unit = []
    for blk in src.loop_blocks():
        new = blk.copy()
        new.blocks = []
        if seen:
            unit.append(new)
        seen = seen or blk is searched
    code = FC.Block(1, 0, src.opts)
    code.blocks.extend(unit)
    sub = code.get_type(typ)
    return sub.copy()


def include_dir(funcname):
    """Tell if 'funcname' include is in 'dirinc'
    NB: only work in 'src'"""
    dirs = ('asterc', 'asterfort', 'blas', 'med', 'mumps')
    found = None
    for ddd in dirs:
        for k in ('', 'fake', 'interfaces'):
            if osp.isfile(osp.join("bibfor", "include", k,
                          ddd, "%s.h" % funcname)):
                return osp.join(k, ddd)
    return found


def eval_parameters(code):
    """Return a dict of the values of the parameters found in code"""
    exp1 = re.compile('parameter.*?\( *(.*) *\)', re.M)
    exp2 = re.compile('parameter.*?:: *(.*) *$', re.M)
    exp3 = re.compile('(\w+) *= *(.+)')
    txt = code.code_text()
    decls = exp1.findall(txt) + exp2.findall(txt)
    coupl = []
    for string in decls:
        sval = string.split(',')
        for i in sval:
            coupl.extend(exp3.findall(i))
    dico = dict(coupl)
    # loop on `coupl` because order may be important
    for key, val in coupl:
        dico[key] = eval_arg(val, dico)
    return dico
