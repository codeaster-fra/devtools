# coding=utf-8

"""
This module defines functions to parse the text code and essentially the classes
used to perform a nested split of a line of code.
"""


import re
from functools import partial

_EMPTY_LINE = '_EMPTY_LINE_'
MARK = '%MARK%'


class BaseSplitter(object):

    """Split a string to iterate on each value.
    The base class does not split. It must be derivated.
    'sep' is the separator to join values."""
    sep = ''

    def __init__(self, arg, prefix=None, suffix=None):
        """Initialization, store the 'arg' string"""
        assert type(arg) is str
        self.prefix = prefix
        self.arg = arg
        self.suffix = suffix
        self.values = None
        self._sub_splitted = False
        self.split()

    def split(self):
        """Build the list '.values' with prefix and suffix."""
        assert self.values is None, "'split' is only once called at startup"
        self.values = []
        if self.prefix:
            self.values.append(self.prefix)
        split = self._split()
        if type(split) not in (list, tuple):
            split = [split, ] if split != '' else []
        self.values.extend(split)
        if self.suffix:
            self.values.append(self.suffix)
        # XXX to remove later
        assert '' not in self.values

    def _split(self):
        """Return the list of values between prefix and suffix"""
        return self.arg

    def __iter__(self):
        """Iterator on couples ('sep', 'value').
        'sep' is the separator to use after 'value'"""
        sep = self.sep
        for i, val in enumerate(self.values):
            if isinstance(val, BaseSplitter):
                for ssep, subv in val:
                    if ssep is None:
                        ssep = sep
                    yield ssep, subv
            else:
                if i + 1 == len(self.values):
                    sep = None
                yield sep, val

    def size(self):
        """Return the number of fields"""
        return len(self.values)

    def join(self):
        """Join the values (same as 'str()')"""
        txt = []
        sep = self.sep
        for nsep, val in self:
            if txt:
                txt.append(sep)
            txt.append(val)
            sep = nsep
        return ''.join(txt)

    def alloc(self, length, maxi=None, cont=True):
        """Allocate values on lines shorter than 'length'."""
        def _add_lines(lines, new, add_endsep):
            """Add 'new' lines to line."""
            new = [line for line in new if line.strip() != '']
            if len(new) == 0:
                return
            if add_endsep:
                new[-1] += endsep
            lines.extend(new)
            return
        end = '&'
        rm_end = partial(re.compile('& *$', re.M).sub, '')

        def _add_cont(line):
            """Add cont char."""
            return rm_end(line) + end
        sep = self.sep
        endsep = sep.rstrip()
        # length - 1 for '&', - 1 for sep
        wklen = length
        if cont:
            wklen = wklen - len(end) - len(endsep)
        lines = []
        curl = []
        i = 0
        kmax = self.size() - 1
        for k, val in enumerate(self.values):
            if len(sep.join(curl)) + len(val) + len(sep) <= wklen \
               and (not maxi or i < maxi):
                curl.append(str(val))
                i += 1
            else:
                _add_lines(lines, [sep.join(curl), ], add_endsep=cont)
                i = 1
                if len(val) <= wklen:
                    curl = [str(val), ]
                else:
                    if not isinstance(val, BaseSplitter):
                        val = BaseSplitter(val)
                    val.sub_split(wklen)
                    subl = val.alloc(length, maxi, cont)
                    _add_lines(lines, subl, add_endsep=(k < kmax and cont))
                    curl = []
        if curl:
            lines.append(sep.join(curl))
        if cont:
            tmp = [line for line in lines if line.strip() != '']
            lines = apply_on_lines(_add_cont, tmp[:-1])
            lines.extend(tmp[-1:])
        return lines

    def __len__(self):
        """Return the size of the joined string"""
        return len(self.join())

    def maxlength(self):
        """Return the size of the longest value"""
        return max([len(val) for val in self.values])

    def __getitem__(self, index):
        """Return the 'index'-th value"""
        return self.values[index]

    def get(self, index, default=None):
        """Return the 'index'-th value or 'default'."""
        if 0 <= index < len(self.values):
            return self.values[index]
        else:
            return default

    def get_str(self, index):
        """Return the 'index'-th value as string"""
        return str(self.values[index])

    def strings(self):
        """Return the values as string"""
        res = [str(val) for val in self.values]
        return res

    def __str__(self):
        """Return as a string, alias to 'join()'"""
        return self.join()

    def set_value(self, index, splitter):
        """Replace a string in '.values' by its Splitter instance."""
        self.values[index] = splitter

    def sub_split(self, length):
        """Sub-split the values of 'obj'. Stop when all fields are shorter than
        'length' characters or the last attempt does not split any field."""
        splitters = [LogicalSplitter, ArgSplitter, ParSplitter, StrSplitter]
        stop = self.maxlength() < length
        # print "#init", self.dbg()
        while not stop:
            cut = False
            lsplit = splitters[:]
            while len(lsplit) > 0 and not cut:
                splitter = lsplit.pop(0)
                for i, val in enumerate(self.values):
                    if isinstance(val, BaseSplitter):
                        val.sub_split(length)
                        continue
                    # prev = spl.size()
                    spl = splitter(val)
                    cut = spl.size() > 1 or splitter is StrSplitter
                    if cut:
                        self.set_value(i, spl)
                        break
            stop = self.maxlength() < length or not cut
        self._sub_splitted = True
        # print "#out ", self

    def dbg(self):
        """Print all the values for debugging"""
        print('DEBUG', self.__class__.__name__, self.size(), len(self))
        for i, arg in enumerate(self):
            sep, val = arg
            print(i, repr(sep), len(val), repr(val))
        print(str(self))


class SpaceSplitter(BaseSplitter):

    """Split a string as ''.split() does but not inside strings"""
    sep = ' '

    def _split(self):
        """Split"""
        arg = replace_enclosed(self.arg, ' ', MARK)
        val = [elt.strip() for elt in arg.split() if elt.strip() != '']
        val = [elt.replace(MARK, ' ') for elt in val]
        return val


class ArgSplitter(BaseSplitter):

    """Split a string as a list of variables without splitting inside
    parenthesis and fortran strings"""
    sep = ', '

    def _split(self):
        """Build the list '.values'."""
        arg = replace_enclosed(self.arg, ',', MARK)
        val = [elt.strip() for elt in arg.split(',') if elt.strip() != '']
        val = [elt.replace(MARK, ',') for elt in val]
        val = apply_on_lines(partial(replace_extra, char=' '), val)
        val = apply_on_lines(partial(replace_extra, char=',', repl=', '), val)
        return val


def args_from_list(list_args):
    """Build an ArgSplitter object from 'list_args'."""
    return ArgSplitter(','.join(list_args))


class ParSplitter(BaseSplitter):

    """Split a string as a call statement or array or simply extract code inside
    parenthesis:
        call sub(xx) --> ['call sub(', 'xxx', ')']
        arr(i:j,k) --> ['arr(', 'i:j, k', ')']
        (code code) --> ['(', 'code code', ')']
    """
    sep = ''

    def _split(self):
        """Build the list '.values'."""
        mat = re.search('^(?P<prefix>[^\(]*\() *'
                        '(?P<inside>.*?) *'
                        '(?P<suffix>\)[^\)]*)$', self.arg)
        if not mat:
            return self.arg
        inside = mat.group('inside')
        id_op = inside.find('(')
        id_cl = inside.find(')')
        nook = id_cl > -1 and id_cl < id_op
        if nook:
            return self.arg
        return mat.group('prefix', 'inside', 'suffix')


class LogicalSplitter(BaseSplitter):

    """Split a string as a logical expression"""
    sep = ' '
    _re1 = re.compile('(\.(?:eq|ne|le|lt|ge|gt|and|or|nor|xor|eqv|neqv)\.)')

    def _split(self):
        """Build the list '.values'."""
        arg = replace_enclosed(self.arg, '.', MARK)
        val = self._re1.split(arg)
        val = [elt.strip() for elt in val if elt.strip() != '']
        val = [elt.replace(MARK, '.') for elt in val]
        return val


class StrSplitter(BaseSplitter):

    """Raw split of string"""
    sep = ''

    def _split(self):
        """Build the list '.values'."""
        return [self.arg, ]

    def sub_split(self, length):
        """Sub-split long string."""
        if self.maxlength() <= length:
            return
        # split long strings only once
        if self._sub_splitted:
            return
        self._sub_splitted = True
        txt = self.arg
        wklen = length - 1
        # + 1 because no & starting the first line
        self.values = [txt[:wklen + 1]]
        txt = txt[wklen + 1:]
        substr = []
        while len(txt) > wklen:
            substr.append(txt[:wklen])
            txt = txt[wklen:]
        if txt:
            substr.append(txt)
        self.values.extend(['&' + txt for txt in substr])


def apply_on_lines(func, lines):
    """Apply 'func' on each line"""
    return [func(line) for line in lines]


def remove_continuation(line):
    """Remove continuation on each lines."""
    rm_end = partial(re.compile('& *$', re.M).sub, '')
    rm_bes = partial(re.compile('^ *&', re.M).sub, '')
    rm_beg = partial(re.compile('^ +', re.M).sub, ' ')
    return rm_beg(rm_bes(rm_end(line)))


def replace_extra(line, char, repl=None, plus=' *'):
    """Replace several 'char' by 'repl' if it is not in a string.
    By default 'repl' = 'char'."""
    assert len(char) == 1
    repl = repl or char
    line = replace_enclosed_in_string(line, char, MARK)
    expr = '(%s)+%s' % (re.escape(char), plus)
    line = re.sub(expr, repl, line)
    line = line.replace(MARK, char)
    return line


def replace_enclosed(txt, sep, repl, start="([{", end="}])", onoff='"\''):
    """Replace 'sep' enclosed between 'start' and 'end' by 'repl'.
    'start' and 'end' are iterable objects."""
    assert len(sep) == 1
    new = []
    dlvl = dict().fromkeys(onoff, 0)
    dlvl['glob'] = 0
    for char in txt:
        if char in start:
            dlvl['glob'] += 1
        elif char in end:
            dlvl['glob'] -= 1
        elif char in onoff:
            dlvl[char] = 1 - dlvl[char]
        elif char == sep:
            char = char if sum(dlvl.values()) == 0 else repl
        new.append(char)
    new = ''.join(new)
    return new


def replace_enclosed_in_string(txt, sep, repl):
    """Replace 'sep' by 'repl' only inside strings."""
    return replace_enclosed(txt, sep, repl, start=[], end=[])

# free.change


def is_continued(previous):
    """Tell if the 'previous' should be continued by another one."""
    return re.search('& *$', previous, flags=re.M)

# cond


def remove_extern(string, start='(', end=')'):
    """Remove most external parenthesis (or other 'start/end')"""
    string = re.sub('^' + re.escape(start), '', string.strip())
    string = re.sub(re.escape(end) + '$', '', string)
    return string


def first_enclosed(txt, start="(", end=")", onoff='"\''):
    """Return the first text enclosed between 'start' and 'end'."""
    new = []
    dlvl = dict().fromkeys(onoff, 0)
    flag = 0
    for char in txt:
        if sum(dlvl.values()) == 0:
            if char in start:
                flag += 1
            elif char in end:
                flag -= 1
                if flag == 0:
                    new.append(char)
                    break
        if char in onoff:
            dlvl[char] = 1 - dlvl[char]
        if flag >= 1:
            new.append(char)
    new = ''.join(new)
    return new
