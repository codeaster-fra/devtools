# coding=utf-8

"""
Change a fortran source file.

- add a line at the beginning of a subroutine before or at the end of the
  declarations.
- suppress empty comments.
"""

import os
import re
from functools import partial

from aslint.logger import logger
from aslint.utils import remove_lines, join_lines
from aslint.fortran.free.code_func import is_continued
import aslint.fortran.free.regexp as RX

L_TYPE = ['implicit', 'integer', 'character', 'real', 'logical', 'complex',
          'common', 'parameter', 'equivalence', 'save']
# 'DATA', # declarations must appear before DATA
IMPLICIT_NONE = re.compile('^ *implicit +none', re.I)
COMMENT_FOR = re.compile('^[!].*?$', re.M)

_empty = '_EMPTY_LINE_'


def _remove_empty_new_lines(txt, end_cr=True):
    """Remove empty added in a previous replacement."""
    return join_lines([li for li in txt.splitlines() if li.strip() != _empty],
                      end_cr=end_cr)


def _remove_empty_continuation(txt, end_cr=True):
    """Remove empty continuation lines.
    Set 'end_cr' to False if 'txt' is only a block."""
    lines = []
    prev = ''
    for i, line in enumerate(txt.splitlines()):
        if i > 0 and is_continued(prev) \
                and re.sub('^ *&', '', line).strip() == '':
            lines[i - 1] = re.sub('& *$', '', lines[i - 1])
        else:
            lines.append(line)
        prev = line
    return join_lines(lines, end_cr=end_cr)


def remove_decls(txt, variables, decls, arrays):
    """Remove declaration in 'txt'."""
    def var_repl(matchobj):
        """Replacement of the captured variable on the line"""
        mgr = matchobj.groupdict()
        if not mgr['type'].endswith(' '):
            mgr['type'] += ' '
        line = [mgr['indent'], mgr['type'], mgr['inter'], ]
        svar = mgr['var']
        # logger.debug('capture var=%r', svar)
        # logger.debug('pattern: %r', matchobj.re.pattern)
        if svar.count(',') > 1:
            line.append(',')
        line.append(mgr['end'])
        return ''.join(line)
    var_template = [', *%(v)s *,', ', *%(v)s *$', ' %(v)s *,', ' %(v)s *$']
    if arrays:
        var_template.extend([', *%(t)s *,', ', *%(t)s *$',
                             ' %(t)s *,', ' %(t)s *$', ])
    var_template = '(' + '|'.join(var_template) + ')'
    for var in variables:
        # ',var,' or ',var$' or 'var,' or ' var$'
        revar = var_template % {'v': var, 't': '%s *%s' % (var, RX.ARRIDX)}
        expr = ''.join([
            '^(?P<indent> *)',
            '(?P<type>%s)' % decls,
            '(?P<inter>.*?)',
            '(?P<var>%s)' % revar,
            '(?P<end>.*?)$',
        ])
        expc = re.compile(expr, re.I | re.M)
        txt = expc.sub(var_repl, txt)
    return txt


def remove_vars(txt, variables):
    """Remove declaration of 'variables' in 'txt'."""
    txt = remove_decls(txt, variables, decls=RX.DECLS_SEP, arrays=True)
    txt = remove_empty_decls(txt)
    txt = _remove_empty_continuation(txt)
    return txt


def remove_params(txt, parameters):
    """Remove declaration of 'parameters' in 'txt'."""
    raise NotImplementedError('todo: free form')


def remove_labels(txt, labels):
    """Remove 'labels' in 'txt'."""
    for lab in labels:
        expc = re.compile("^ *%s +continue$" % lab, re.I | re.M)
        txt = expc.sub(_empty, txt)
        expc = re.compile(
            "^(?P<ident> *)%s(?P<instr> +.*)$" % lab, re.I | re.M)
        txt = expc.sub(r"\g<ident>%s\g<instr>" % (' ' * len(lab)), txt)
    txt = _remove_empty_new_lines(txt)
    return txt


def search_common(txt, common):
    """Tell if one of the 'common' is found in 'txt'."""
    found = False
    for com in common:
        regexp = re.compile('^ +common */ *%s */' % com, re.I | re.M)
        found = regexp.search(txt) is not None
        if found:
            break
    return found


def remove_common(txt, common):
    """Remove 'common' in 'txt'."""
    func = partial(search_common, common=common)
    return remove_lines(txt, func)


def remove_empty_decls(txt):
    """Remove empty declaration."""
    expr = re.compile('^ *%s *$' % RX.DECLS_SEP, re.I | re.M)
    txt = expr.sub(_empty, txt)
    txt = _remove_empty_new_lines(txt)
    return txt


def remove_empty_params(txt):
    """Remove empty PARAMETER statement."""
    expr = re.search('^ +parameter *\([ ,&\+]*\)', txt)
    return remove_lines(txt, expr)


def remove_tabs(txt, tabsize=4):
    """Replace tab characters by 'tabsize' blank characters."""
    if txt.find('\t') > -1:
        txt = txt.expandtabs(tabsize)
    return txt


def add_line_numbers(txt):
    """Add the line number at the beginning of each line."""
    l_txt = txt.splitlines()
    new = []
    i = 0
    for line in l_txt:
        i += 1
        new.append(':%06d:%s' % (i, line))
    return os.linesep.join(new)


def add_statement_line(txt, addline, asdecl=False):
    """Add 'addline' after the declarations.
    If 'asdecl' is True, the line is added just following the other
    declarations, else it is added as the first instructions after the comments
    following the declarations."""
    if txt.find(addline) > -1:
        logger.debug("line already present")
        return txt

    txlin = add_line_numbers(txt)
    l_decl = []
    for typ in L_TYPE:
        lin = re.findall('^:[0-9]{6}: *%s.*$' % typ, txlin, flags=re.I | re.M)
        if len(lin) > 0:
            l_decl.append(lin[-1])      # only keep the last one
    l_decl.sort()
    num = int(l_decl[-1][1:7])
    l_txt = txt.splitlines()
    is_ok = False
    while not is_ok and num < len(l_txt):
        previous = '' if num < 1 else l_txt[num - 1]
        cont = is_continued(previous)
        if asdecl:
            if len(l_txt[num]) == 0 or l_txt[num][0] in ('c', '!', 'C') \
                    or not cont:
                is_ok = True
            else:
                num += 1
            continue
        else:
            if not cont:
                is_ok = True
            else:
                num += 1
    new = l_txt[:num]
    new.append(addline)
    new.extend(l_txt[num:])
    new.append("")
    return os.linesep.join(new)


def add_statement_first(txt, addline):
    """Add `addline` just after SUBROUTINE/FUNCTION and the first comments."""
    if txt.find(addline) > -1:
        logger.debug("line already present")
        return txt
    new = []
    done = False
    prev = ''
    for line in txt.splitlines():
        if not done:
            if RX.SUBFUNC.search(line) \
                or COMMENT_FOR.search(line) or is_continued(prev) \
                or IMPLICIT_NONE.search(line) \
                    or line.strip() == '':
                new.append(line)
                prev = line
                continue
            new.append(addline)
            done = True
        new.append(line)
        prev = line
    new.append("")
    return join_lines(new)


def add_statement(txt, addline, before_decls=True):
    """Add 'addline' in a fortran subroutine at the beginning if 'before_decls'
    is True or just after the declaration else."""
    if before_decls:
        new = add_statement_first(txt, addline)
    else:
        new = add_statement_line(txt, addline)
    return new
