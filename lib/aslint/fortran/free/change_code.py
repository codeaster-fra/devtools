# coding=utf-8

"""
Change a fortran source file using Block objects.
"""

from aslint.logger import logger
from aslint.baseutils import force_list
from aslint.utils import difftxt

from aslint.fortran.utils import sub2filename, read_call_tree
import aslint.fortran.free.fortran_code as FC


def remove_vars(src, subname, lvar):
    """Remove the variables 'lvar' in the subprogram 'subname' in 'src'.
    Return a new Block object."""
    wrk = src.copy()
    sub = [stmt for stmt in wrk.get_type(
        FC.Subroutine) if stmt.name == subname]
    assert len(sub) == 1, 'more than one subroutine %r' % subname
    sub = sub[0]
    lvar = force_list(lvar)
    decls = sub.get_type(FC.Decl)
    # print decls.dbg()
    todo = set(lvar)
    for stmt in decls:
        for var in lvar:
            found = stmt.remove_arg(var)
            if found:
                todo.discard(var)
    if len(todo) > 0:
        logger.warn('variables not removed: %r', tuple(todo))
        assert len(todo) == 0
    logger.debug('remove_vars:\n%s', difftxt(src.code_text(), wrk.code_text()))
    return wrk


def remove_args(src, subname, args, idx=None):
    """Remove the arguments 'args' of the subroutine 'subname'.
    Return a new Block object, add infos about index in 'idx'."""
    wrk = src.copy()
    args = force_list(args)
    sub = [stmt for stmt in wrk.get_type(
        FC.Subroutine) if stmt.name == subname]
    assert len(sub) == 1, 'more than one subroutine %r' % subname
    sub = sub[0]
    init_args = sub.args[:]
    todo = set(args)
    idx = idx if idx is not None else {}
    for arg in args:
        found = sub.remove_arg(arg)
        assert found, '%s: not found %r' % (subname, arg)
        if found:
            idx[arg] = init_args.index(found)
            todo.discard(arg)
    if len(todo) > 0:
        logger.warn('%s: arguments not removed: %r', subname, tuple(todo))
        assert len(todo) == 0
    wrk = remove_vars(wrk, subname, list(idx.keys()))
    logger.debug('remove_args:\n%s', difftxt(src.code_text(), wrk.code_text()))
    idx['total'] = len(init_args)
    return wrk


def remove_call_arg(src, subname, idx, nargs=None):
    """Remove the 'idx'-th argument in the calls of the subprogram 'subname'
    in the 'src' object.
    'nargs' is used to check the number of arguments."""
    wrk = src.copy()
    calls = [stmt for stmt in wrk.get_type(FC.Call) if stmt.name == subname]
    for stmt in calls:
        assert not nargs or len(stmt.args) == nargs, \
            '%d args expected, found %d: %s' % (
                nargs, len(stmt.args), stmt.args)
        stmt.remove_arg(stmt.args[idx])
    logger.debug('remove_call_arg:\n%s',
                 difftxt(src.code_text(), wrk.code_text()))
    return wrk


def remove_dummy_arg(fname, dummy_args, append_to=None):
    """Remove the dummy args in 'fname' and change its callers too.
    NB: all changed files are written."""
    reverse_tree = read_call_tree()[1]
    sub = FC.source(filename=fname)
    subprg = [stmt for stmt in sub.get_type(FC.Subroutine)]
    assert len(subprg) >= 1, 'no subroutine in %r' % fname
    subname = subprg[0].name
    idx = {}
    logger.info('%s: remove args: %s', subname, dummy_args)
    sub = remove_args(sub, subname, dummy_args, idx)
    sub.write_code(fname)
    total = idx['total']
    del idx['total']
    ids = [num for num in list(idx.values())]
    ids.sort(reverse=True)
    # change callers
    for caller in reverse_tree.get(subname, []):
        fname = sub2filename(caller)
        if append_to is not None:
            append_to.append(fname)
        src = FC.source(filename=fname)
        logger.info('%s (calling %s): remove args: %s', caller, subname, ids)
        assert len(ids) > 0
        for i, num in enumerate(ids):
            src = remove_call_arg(src, subname, num, total - i)
        src.write_code(fname)
    return sub
