# coding=utf-8

"""
Regular expressions to read fortran source code in free format.
"""

import re
# TODO move 'generic' regexp from other modules here

DECL_ATTRS = '|'.join(['allocatable', 'pointer', 'automatic', 'private',
                       r'dimension\(.*?\)', r'intent\((in|out|inout)\)',
                       'public', 'external', 'save', 'static', 'intrinsic',
                       'target', 'optional', 'volatile', 'parameter'])

# regular expressions to match declarations
DECLS = "(?P<decltype>(recursive|integer|logical|real|complex|character|" \
        "aster_int|aster_logical|mpi_int|mpi_bool|" \
        "blas_int|hid_t|med_int|med_idt|mumps_int|" \
        "petsc_int|PetscInt|PetscScalar)) *" \
        "(?P<declsize>(|\( *(kind|len) *= *(?P<declkind>[\w\*]+) *\)))" \
        "(|(?P<declattr>( *, *(%s))+))" % DECL_ATTRS
DECLS_TYPE = "(?P<decltype>type *\(\w+ *\))" \
             "(?P<declsize>)(?P<declkind>)" \
             "(|(?P<declattr>( *, *(%s))+))" % DECL_ATTRS
DECLS_SEP = DECLS + " *:: *"

# array index
ARRIDX = "\( *[0-9A-Z_,\-\+\*/]+ *\)"

# subroutine & function declaration
SUBFUNC = re.compile('^ *(subroutine|function)', re.M | re.I)
END_SUBFUNC = re.compile(' *end +(subroutine|function) *$', re.I | re.DOTALL)
END_SUBFUNC2 = re.compile(' *end *$', re.I | re.DOTALL)
SUBFUNC_ARGS = re.compile("^ *(?P<type>(%s)?) *(subroutine|function) +"
                          "(?P<sub>\w+).*?\((?P<args>.*?)\)" % DECLS,
                          re.M | re.I | re.DOTALL)

# comment
COMMENT = re.compile('^[!#].*?$', re.M)

# constant strings
EXPR_STR = re.compile("'(?P<str>.+?)'", re.M)
EXPR_STR2 = re.compile('"(?P<str>.+?)"', re.M)

# spaces
TRAILING_SPACE = re.compile(' +$', re.M)
CONT_SPACE = re.compile('( +&)$', re.M)
FIRST_INDENT = re.compile('^( *)', re.M)
LABEL_INDENT = re.compile('^(?P<bef> *)(?P<label>[0-9]+)(?P<aft> +)', re.M)
