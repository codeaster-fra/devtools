# coding=utf-8

"""
Change a fortran source file.

- add a line at the beginning of a subroutine before or at the end of the
  declarations.
- suppress empty comments.
"""

import os
import re
from functools import partial

from aslint.logger import logger
from aslint.utils import remove_lines, join_lines, re_all_sub
from aslint.fortran.fixed.utils import (
    is_empty_comment, is_continuation, lines_blocks,
)

L_TYPE = ['IMPLICIT', 'INTEGER', 'CHARACTER', 'REAL', 'LOGICAL', 'COMPLEX',
          'COMMON', 'PARAMETER', 'EQUIVALENCE', 'SAVE']
# 'DATA', # declarations must appear before DATA

# regular expressions to match declarations
RE_DECLS = ['INTEGER[\*48 ]*', 'REAL *\* *[48]+', 'COMPLEX[\*816 ]*',
            'CHARACTER *\* *[0-9]+', 'LOGICAL']

_empty = '_EMPTY_LINE_'


def _remove_empty_new_lines(txt, end_cr=True):
    """Remove empty added in a previous replacement."""
    return join_lines([li for li in txt.splitlines() if li.strip() != _empty],
                      end_cr=end_cr)


def _remove_empty_continuation(txt, end_cr=True):
    """Remove empty continuation lines.
    Set 'end_cr' to False if 'txt' is only a block."""
    return join_lines([
        li for li in txt.splitlines()
        if not (is_continuation(li) and li[6:].strip() == '')
    ], end_cr)


def remove_decls(txt, variables, decls, arrays, end=''):
    """Remove declaration in 'txt'."""
    indent = '^(?P<indent>     [ ]+)'
    all_decls = '(?P<type>%s)' % ('|'.join(decls))
    inter = '(?P<inter>.*?)'
    lvars = variables[:]
    if arrays:
        # var + var(5)
        lvars.extend([v + ' *\( *[0-9A-Z_,\-\+\*/]+ *\)' for v in variables])
    end0, end1 = '', ''
    if end:
        end0 = '(?P<end>)'
        end1 = '(?P<end>%s)' % end
    lexp = []
    i = -1
    for var in lvars:
        # separator before/after var, at the end of line or not
        i += 4
        lexp.append('(?P<var>, *%s)(?P<sep>[ ,]+)%s' % (var, end0))
        lexp.append('(?P<sep> +)(?P<var>%s *,)%s' % (var, end0))
        lexp.append('(?P<var>, *%s *)(?P<sep>)%s$' % (var, end0))
        lexp.append('(?P<sep> +)(?P<var>%s *)%s$' % (var, end1))
        logger.debug('%2d %s', i, var)
    # indentation + a declaration + anything else + var
    lexp = [indent + all_decls + inter + expr for expr in lexp]
    lreg = [re.compile(exp, re.I | re.DOTALL) for exp in lexp]
    new = []
    for blck in lines_blocks(txt)[0]:
        for i, regexp in enumerate(lreg):
            mat = regexp.search(blck)
            if mat:
                logger.debug('block matching regexp #%d "%s": %s',
                             i, mat.groups(), blck)
                blck = regexp.sub('\g<indent>\g<type>\g<inter>\g<sep>', blck)
                blck = _remove_empty_continuation(blck, end_cr=False)
                blck = re.sub('\s*,\s*$', '', blck)
                continue
        new.append(blck)
    txt = join_lines(new)
    return txt


def remove_vars(txt, variables):
    """Remove declaration of 'variables' in 'txt'."""
    txt = remove_decls(txt, variables, decls=RE_DECLS, arrays=True)
    txt = remove_empty_decls(txt)
    txt = _remove_empty_continuation(txt)
    return txt


def remove_params(txt, parameters):
    """Remove declaration of 'parameters' in 'txt'."""
    txt = remove_decls(txt, parameters, RE_DECLS, arrays=False)
    param_val = ['%s *= *[0-9\'\"_A-Z]+' % p for p in parameters]
    txt = remove_decls(txt, param_val, decls=[r'PARAMETER *\('],
                       arrays=False, end=r'\)')
    txt = remove_empty_decls(txt)
    txt = _remove_empty_continuation(txt)
    return txt


def remove_labels(txt, labels):
    """Remove 'labels' in 'txt'."""
    lexp = []
    for lab in labels:
        lexp.append('^ *%s +.*$' % lab)
    found, txt = re_all_sub(lexp, _empty, txt, nmax=1, ignore_comment=False)
    if found != len(labels):
        logger.warn("at least one label has not been found "
                    "(or found more than once)")
    txt = _remove_empty_new_lines(txt)
    return txt


def search_common(txt, common):
    """Tell if one of the 'common' is found in 'txt'."""
    found = False
    for com in common:
        regexp = re.compile('^ +COMMON */ *%s */' % com, re.M)
        found = regexp.search(txt) is not None
        if found:
            break
    return found


def remove_common(txt, common):
    """Remove 'common' in 'txt'."""
    func = partial(search_common, common=common)
    return remove_lines(txt, func)


def remove_empty_decls(txt):
    """Remove empty declaration."""
    lexp = []
    for typ in RE_DECLS:
        lexp.append('^ *%s *$' % typ)
    lexp.append('^     [^ ] *$')
    txt = re_all_sub(lexp, _empty, txt, ignore_comment=False)[1]
    txt = _remove_empty_new_lines(txt)
    return txt


def remove_empty_params(txt):
    """Remove empty PARAMETER statement."""
    expr = re.search('^ +PARAMETER *\([ ,&\+]*\)', txt)
    return remove_lines(txt, expr)


def remove_tabs(txt, tabsize=4):
    """Replace tab characters by 'tabsize' blank characters."""
    if txt.find('\t') > -1:
        logger.warn('tabs found')
        txt = txt.expandtabs(tabsize)
    return txt


def add_line_numbers(txt):
    """Add the line number at the beginning of each line."""
    l_txt = txt.splitlines()
    new = []
    i = 0
    for line in l_txt:
        i += 1
        new.append(':%06d:%s' % (i, line))
    return os.linesep.join(new)


def add_statement_line(txt, addline, asdecl=False):
    """Add 'addline' after the declarations.
    If 'asdecl' is True, the line is added just following the other
    declarations, else it is added as the first instructions after the comments
    following the declarations."""
    if txt.find(addline) > -1:
        logger.debug("line already present")
        return txt

    txlin = add_line_numbers(txt)
    l_decl = []
    for typ in L_TYPE:
        lin = re.findall('^:[0-9]{6}: *%s.*$' % typ, txlin, re.MULTILINE)
        if len(lin) > 0:
            l_decl.append(lin[-1])      # ne garder que la dernière
    l_decl.sort()
    num = int(l_decl[-1][1:7])
    # passer les caractères de suite
    l_txt = txt.splitlines()
    is_ok = False
    while not is_ok and num < len(l_txt):
        if asdecl:
            if len(l_txt[num]) == 0 or l_txt[num][0] in ('c', '!', 'C') \
                    or l_txt[num][:6] == ' ' * 6:
                is_ok = True
            else:
                num += 1
            continue
        else:
            if l_txt[num][:6] == ' ' * 6:
                is_ok = True
            else:
                num += 1
    new = l_txt[:num]
    new.append(addline)
    new.extend(l_txt[num:])
    new.append("")
    return os.linesep.join(new)


def add_statement_first(txt, addline):
    """Add `addline` just after SUBROUTINE/FUNCTION and the first comments."""
    if txt.find(addline) > -1:
        logger.debug("line already present")
        return txt
    new = []
    done = False
    for line in txt.splitlines():
        if not done:
            if re.search('(FUNCTION|SUBROUTINE|IMPLICIT|INCLUDE)', line) \
                or re.search('^[cC!]', line) or is_continuation(line) \
                    or line.strip() == '':
                new.append(line)
                continue
            new.append(addline)
            done = True
        new.append(line)
    new.append("")
    return os.linesep.join(new)


def add_statement(txt, addline, before_decls=True):
    """Add 'addline' in a fortran subroutine at the beginning if 'before_decls'
    is True or just after the declaration else."""
    if before_decls:
        new = add_statement_first(txt, addline)
    else:
        new = add_statement_line(txt, addline)
    return new


def suppress_mult_comment(txt):
    """Suppress multiple empty comments"""
    new = []
    prev_cmt = []
    for line in txt.splitlines():
        icmt = is_empty_comment(line)
        if icmt:
            prev_cmt.append(line)
            continue
        elif prev_cmt:
            cmt = [(len(c), c) for c in prev_cmt]
            cmt.sort()
            new.append(cmt[-1][1])
            prev_cmt = []
        new.append(line)
    new.append("")
    return os.linesep.join(new)
