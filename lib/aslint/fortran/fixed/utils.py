# coding=utf-8

"""
Some utilities.
"""

import os
import re

from aslint.utils import all_none

FCHECK = ["gfortran", "-Wall", "-c"]

# empty comments
RE_EMPTY_COMMENT = [re.compile(expr) for expr in [
    '^ *$',
    '^[cC!] *$',
    # '^[cC!] *[\-\+=*#/_$%:,.!§]+$',
]]

# a continuation
RE_CONTINUE = re.compile('^ {5}[^ ]')


def check_line_length(txt, length=72):
    """Return the lines longer than 'length' characters."""
    dline = dict(enumerate(txt.splitlines()))
    lines = [i for i, l in list(dline.items()) if len(l) > length]
    return lines


def is_empty_comment(line):
    """Tell if a line is an empty comment"""
    return not all_none([reg.search(line) for reg in RE_EMPTY_COMMENT])


def is_continuation(line):
    """Tell if the 'line' is a continuation of a previous one."""
    return RE_CONTINUE.search(line) is not None


def lines_blocks(txt):
    """Return a list of blocks of "contiguous" lines and the line number
    of the first line of each block."""
    blocks = []
    lines = txt.splitlines()
    pos = -1
    curb, lenb = [], [0]
    while len(lines) > 0:
        pos += 1
        line = lines.pop(0)
        if is_continuation(line):
            curb.append(line)
        else:
            if curb:
                blocks.append(os.linesep.join(curb))
                lenb.append(pos)
            curb = [line, ]
    if curb:
        blocks.append(os.linesep.join(curb))
    return blocks, lenb


def block_at(blocks, nline):
    """Return the block at the line 'nline'."""
    i = 0
    found = ""
    for bclk in blocks:
        i += len(bclk.splitlines())
        if i > nline:
            found = bclk
            break
    return found
