# coding=utf-8

"""
Generate the fortran interface 'file1.h'.

NB: Intel environment must be sourced before.
"""


import os
import os.path as osp
import re
import shutil
import subprocess
from functools import partial
from glob import glob
from subprocess import PIPE, Popen

from aslint.decorators import interrupt_decorator
from aslint.fortran.free.change import add_statement, remove_vars
from aslint.logger import INFO, logger
from aslint.string_utils import convert
from aslint.utils import apply_func_text, apply_in_sandbox, remove_lines

EXTERN_INTERF = ('asterc', 'blas', 'med', 'mumps')


@interrupt_decorator
def gen_interface(fname, incdir, incdest, tmpdir):
    """Generate the interface of 'fname'.
    incdir: include path
    incdest: destination path to written the interface header
    tmpdir: directory for temporary files."""
    logger.info(fname)
    rootname, ext = osp.splitext(fname)
    bname = osp.basename(rootname)
    fhead = osp.join(incdest, bname + '.h')
    fobj = osp.join(tmpdir, bname + '.o')
    if ext == '.h':
        shutil.copyfile(fname, fhead)
        return 0
    cmd = [
        'ifort', '-gen-interfaces', '-i8', '-r8', '-I' + incdir,
        '-I/opt/aster/public/petsc-3.3/include',
        '-I$HOME/dev/codeaster/src/bibfor/include_mumps-4.10.0_mpi',
        '-I/usr/lib/openmpi/include',
        '-fixed',
        '-O0', '-c', fname, '-o', fobj, '-module', tmpdir,
    ]
    cmd.extend(['-DASTER_HAVE_MUMPS', '-DASTER_HAVE_PETSC',
                '-DASTER_HAVE_INTEL_IFORT',
                '-DASTER_HAVE_MPI', '-DASTER_HAVE_OPENMP'])
    logger.debug(cmd)
    try:
        retcode = subprocess.call(cmd)
    except OSError:
        logger.error('please check that the Intel compiler environment '
                     'is properly set')
        raise
    if retcode != 0:
        logger.error('ifort returns exit code: %s', retcode)
        logger.info('- if the source file is in fixed form, you must provide'
                    ' the "old" bibfor/include with the "-I" option')
        logger.info('- if the source file is in free form, you must provide'
                    ' the "new" bibfor/include with the "-I" option')
        logger.exit()
    assert retcode == 0, 'ifort returns exit code: %s' % retcode
    interf = glob(osp.join(tmpdir, bname + '_*.f90'))
    assert len(interf) == 1, interf
    txt = clean_intel_interface(open(interf[0], 'r').read())
    # assert not osp.exists(fhead), '%s already exists' % fhead
    with open(fhead, 'w') as fdo:
        fdo.write(txt)
    calls = list_call(fobj)
    # write to a file to use it later
    with open(osp.join(incdest, bname + '.calls'), 'w') as fdo:
        fdo.write(os.linesep.join(calls))
    return retcode


def clean_intel_interface(txt):
    """Clean the Intel module created by '-gen-interfaces'."""
    # remove comments, MODULE & END MODULE
    txt = remove_lines(txt, re.compile('^ *!').search)
    txt = remove_lines(txt, re.compile('^ *MODULE').search)
    txt = remove_lines(txt, re.compile('^ *END +MODULE').search)
    # use default INTEGER size
    txt = txt.replace('INTEGER(KIND=8)', 'INTEGER')
    txt = txt.replace('LOGICAL(KIND=8)', 'LOGICAL')
    # FUNCTION without parenthesis
    refunc = re.compile('^(?P<decl> *FUNCTION +[^\(]+?)$', re.M)
    txt = refunc.sub('\g<decl>()', txt)
    return txt


def list_call(objfile):
    """Analyze `objfile` and return the list of called subprograms."""
    cmd = ['nm', objfile]
    prc = Popen(cmd, stdout=PIPE, close_fds=True)
    output = convert(prc.communicate()[0])
    regexp = re.compile('^\s+U\s+([a-zA-Z0-9]+)_\s*$', re.M)
    calls = set(regexp.findall(output))
    return list(calls)


def gen_all(incdir, incdest, numthread, *args):
    """Generate all files"""
    if not osp.isdir(incdest):
        os.makedirs(incdest)
    func_one = partial(gen_interface, incdir=incdir, incdest=incdest)
    apply_in_sandbox(func_one, args, numthread, pass_tmpdir=True)


def remove_unrel_extern(incdir):
    """Remove unrelevant includes in extern (because exist in asterfort too.)"""
    allh = []
    for subdir in ('asterc', 'blas', 'med', 'petsc'):
        allh.extend(glob(osp.join(incdir, subdir, '*.h')))
    for inc in allh:
        basn = osp.basename(inc)
        if osp.exists(osp.join(incdir, 'asterfort', basn)):
            logger.warn('remove %s', inc)
            os.remove(inc)


@interrupt_decorator
def insert_include(fname, incdir, migr=False):
    """Insert INCLUDE for called subprograms in 'args'."""
    rootname = osp.splitext(fname)[0]
    bname = osp.basename(rootname)
    fcall = osp.join(incdir, 'asterfort', bname + '.calls')
    lsub = []
    if osp.exists(fcall):
        with open(fcall, 'r') as fobj:
            lsub = fobj.read().split()
    fmt = '#include "%s"'
    path = []
    for sub in lsub:
        lhf = glob(osp.join(incdir, '*', sub + '.h'))
        if len(lhf) == 1:
            path.append(re.sub(re.escape(incdir) + '/*', '', lhf[0]))
        elif len(lhf) == 0:
            logger.warn("%s: not found: %s", bname, sub)
            if migr:
                path.append(osp.join('asterfort', sub + '.h'))
        else:
            logger.error("%s: more than once: %s", bname, lhf)
    if path:
        path.sort()
        includes = os.linesep.join([fmt % p for p in path])

        def find_external(blck):
            """Tell if the blocks matches 'EXTERNAL sub'"""
            found = blck.find('EXTERNAL')
            if not found:
                return found
            found = False
            for sub in lsub:
                # because of ',' the entire line will be removed!
                expr = re.compile('^ *EXTERNAL[ ,]*%s' % sub, re.M | re.I)
                found = expr.search(blck)
                if found:
                    break
            return found

        def change(txt):
            """Insert the INCLUDES
            + remove duplicate declarations
            + remove external declarations."""
            # to skip quickly already changed files
            if txt.find('asterfort') < 0:
                txt = add_statement(txt, addline=includes)
                txt = remove_vars(txt, lsub)
                txt = remove_lines(txt, find_external)
            return txt
        apply_func_text(change, fname, info_level=INFO)


def gen_fake_interface(orig, dest):
    """Generate the fake interfaces from 'orig' to 'dest'"""
    for inc in glob(osp.join(orig, '*', '*.h')):
        with open(inc, 'r') as fobj:
            txt = fobj.read()
        new = gen_fake_incl(txt)
        fake = inc.replace(orig, dest)
        parent = osp.dirname(fake)
        if not osp.isdir(parent):
            os.makedirs(parent)
        with open(fake, 'w') as fdo:
            fdo.write(new)


def gen_fake_incl(txt):
    """Return the new text for a fake interface, just functions decls"""
    new = 'C transitional empty interface\n'
    mat = re.search('^ *function +(\w+) *\(', txt, flags=re.I | re.M)
    if mat:
        func = mat.group(1)
        typ = re.search('^ *(?P<type>\w+)'
                        '(|\((kind|len)=(?P<size>[0-9]+)\)) *:: * %s' % func,
                        txt, flags=re.I | re.M)
        new += ' ' * 6 + typ.group('type')
        if typ.group('size'):
            new += '*' + typ.group('size')
        new += ' ' + func + '\n'
    return new


if __name__ == '__main__':
    with open('bibfor/include/interfaces/asterfort/nopar2.h', 'r') as fobj:
        txt = fobj.read()
    new = gen_fake_incl(txt)
    print(new)
