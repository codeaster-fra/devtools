#!/usr/bin/env python3
# coding=utf-8

"""
Example #2 to change fortran source files

This example use the Block objects to change all calls to 'getv*' functions.
Before changing these calls, first change if/call into if/then/call/endif
(see example #1).

This script shows how:
- to filter some calls,
- to access to the arguments of a call, to know if a variable is declared
  as an array,
- to define a new statement,
- to remove and insert a statement.

---
This script loops on files given in arguments.
Example: %prog bibfor/utilitai/*.F90
     or: %prog `cat files_to_change`

You just have to modify the 'change_text' function for your needs.

By default, it runs on several threads in parallel. You can set numthread=1
to run in sequential. Use sequential mode to have a detailed traceback in case
of error.
"""

import os.path as osp
import re
from optparse import OptionParser

from aslint.utils import (
    apply_in_sandbox, apply_func_text,
)
from aslint.logger import logger, setlevel
from aslint.baseutils import is_binary
import aslint.fortran.free.fortran_code as FC
# see fortran_utilities for new available utilities
import aslint.fortran.free.fortran_utilities as FUT


def change_text(txt):
    """Call subfunctions to change 'txt'

     call getvr8(motcle, zk16(jpara-1+iacc), iocc, iarg, 0,&
                 r8b, n2)
     =>
     call getvr8(motcle, zk16(jpara-1+iacc), iocc=iocc, mxval=0, nret=n2)

     call getvr8(' ', 'COEF_MULT', inum, iarg, 1,&
                 coesym, n1)
     =>
     call getvr8(' ', 'COEF_MULT', scr=coesym, nret=n1)
    """
    src = FC.source(txt)
    logger.debug(src.dbg())
    sub = FC.get_subprogram(src)
    call = src.get_type(FC.Call)
    # list of calls to change
    chg = [i for i in call if i.name in (
        'getvid', 'getvis', 'getvc8', 'getvr8', 'getvtx')]
    if not chg:
        # no call to "func", return without change
        return txt
    # loop on calls to "func"
    for i in chg:
        func = i.name
        # if it has already been changed, skip this call
        if len(i.args) != 7:
            continue
        # get the position of the call in 'src'
        # (used later to remove this call and insert a new one)
        pos, where = src.find_block(i)
        assert pos >= 0
        # change arguments
        mfact, msimp, iocc, iarg, mxval, tab, nbval = [
            FUT.eval_arg(j) for j in i.args]
        # tests
        hasfact = mfact not in ("' '", '" "') and iocc != 0
        isvect = type(mxval) is not int or mxval > 1
        testnb = type(mxval) is int and mxval < 1
        # new args
        newargs = [mfact, msimp]
        if hasfact:
            newargs.append("iocc=%s" % iocc)
        if isvect or testnb:
            newargs.append("nbval=%s" % mxval)
        if isvect:
            newargs.append("vect=%s" % tab)
        elif not testnb:
            ind1 = ""
            if FUT.is_decl_array(src, tab):
                ind1 = "(1)"
            newargs.append("scal=%s%s" % (tab, ind1))
        # here we can not know if `nbval` was used
        newargs.append("nbret=%s" % nbval)
        # 'grep iarg' to know where iarg is used
        if sub.name in ('cgveli', 'nmdocv', 'nmdopo'):
            newargs.append("isdefault=%s" % iarg)

        FC.remove_block(where, pos)
        newb = FC.source("call %s(%s)" % (func, ",".join(newargs)),
                         init_level=i.level - 1)
        FC.insert_block(where, pos, newb)
        FUT.insert_include(src, 'asterfort/%s.h' % func)

    FUT.sort_include(src)
    txt = src.code_text()
    return txt


def check_ascii_binary(fname):
    """print ascii/binary detection"""
    if not osp.isfile(fname):
        logger.error('not a file: %s', fname)
        return
    typ = is_binary(fname) and 'binary' or 'ascii'
    logger.info('%s: %s', typ, fname)
    if typ == 'ascii':
        apply_func_text(change_text, fname)


def loop_on_files(*args):
    """Apply a function"""
    # set numthread=1 to debug the script
    apply_in_sandbox(check_ascii_binary, args, numthread=None)

if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('files ?')
    loop_on_files(*args)
