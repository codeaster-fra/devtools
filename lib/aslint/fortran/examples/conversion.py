#!/usr/bin/env python3
# coding=utf-8

"""
Main script to run all steps of the conversion.
Some steps can be run in parallel, some others shouldn't for performance reason.

- Generate the interfaces of the fortran routines (3 min)::

    conversion.py --action=interface -j 4

  NB: Intel environment must be sourced before.

  Some array dimensions are incomplete::

    patch -p1 < 1_bad_dimension_in_interfaces.diff

  or with 'interfaces' and 'fake' variants::

    cd bibfor/include/interfaces
    patch -p3 < ../../../1_bad_dimension_in_interfaces.diff

- Insert the include of the header/interface files (7 min)::

    conversion.py --action=insert_include

  Interface required for statement function::

    patch -p1 < 2_missing_interfaces.diff

- Fix bad spacing that avoids conversion to free form::

    conversion.py --action=fix_bad_space

- Convert into free-form::

    conversion.py --action=free_form -j 4

  NB: includes must be changed by hand (convert looses the last lines)

- Check the fortran source code (1 min)::

    conversion.py --action=check,save -j 4

- Report the errors detected by the fortran check::

    conversion.py --action=report

- Fix some errors automatically (use the result of the check)::

    conversion.py --action=fix_errors

"""

import os
import os.path as osp
from optparse import OptionParser
from glob import glob
from functools import partial
import pickle

from aslint.utils import apply_in_parallel, apply_func_text_on_files
from aslint.logger import logger, setlevel, INFO
from aslint.filetype import filter_arguments
from aslint.base_checkers import Report
from aslint.check_files import check_files, read_waf_parameters
from aslint.fortran.migration.gen_interface import (
    EXTERN_INTERF, gen_all, remove_unrel_extern, insert_include,
    gen_fake_interface,
)
from aslint.fortran.migration.fix_code import fix_bad_space
from aslint.fortran.migration.extern_tools import convert_all
from aslint.fortran.migration.responsible import (
    read_csv, change_responsible, change_cmodif,
)

SRCDIR = os.getcwd()
INCDIR = osp.join(SRCDIR, 'bibfor', 'include')
INCDEST = osp.join(INCDIR, 'asterfort')

if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    parser.add_option('-j', '--numthread', action='store', default=1, type=int,
                      help="run in parallel on several threads")
    parser.add_option('-I', '--include', action='store', default=INCDIR,
                      help="directory of include files (default: %s)" % INCDIR)
    parser.add_option('-d', '--incdest', action='store', default=INCDEST,
                      help="directory of include files (default: %r)" % INCDEST)
    act = ''
    parser.add_option('--action', action='store', default=act,
                      help="what do you want ?! (default: %s)" % act)
    opts, args = parser.parse_args()
    todo = set(opts.action.split(','))
    report = None
    # source files
    lferm = glob(osp.join('fermetur', '*.F90'))
    lfor = glob(osp.join('bibfor', '*', '*.F90'))
    linc = glob(osp.join(opts.include, '*.h')) \
        + glob(osp.join(opts.include, '*', '*.h')) \
        + glob(osp.join(opts.include, '*', '*', '*.h'))
    if todo.intersection(['all', 'patch0']):
        logger.info('stage: patch0')
        iret = os.system(';'.join([
            'patch -p1 < patches/0_.hgignore.diff',
            #'patch -p1 < patches/patches/0_waf_free+fake.diff',
        ]))
        assert iret == 0, 'patch failed'
    if todo.intersection(['interface', 'all']):
        if len(args) == 0:
            logger.info('stage: interface')
            j = opts.numthread
            incdir = opts.include
            variant = osp.join(incdir, 'interfaces')
            incdest = osp.join(variant, 'asterfort')
            gen_all(incdir, incdest, j, *lferm)
            gen_all(incdir, incdest, j, *lfor)
            for subdir in EXTERN_INTERF:
                f_h = glob(osp.join('fantome', subdir, '*.f')) \
                    + glob(osp.join('fantome', subdir, '*.h'))
                gen_all(incdir, osp.join(variant, subdir), j, *f_h)
            gen_fake_interface(variant, osp.join(incdir, 'fake'))
        else:
            gen_all(opts.include, opts.incdest, opts.numthread, *args)
        remove_unrel_extern(opts.include)
    if todo.intersection(['all', 'patch1']):
        logger.info('stage: patch1')
        iret = os.system(';'.join([
            'cd bibfor/include/interfaces',
            'patch -p3 < ../../../patches/1_bad_dimension_in_interfaces.diff',
            'cd ../../..']))
        assert iret == 0, 'patch failed'
        # iret = os.system('patch -p1 < patches/1b_logical4_in_fake.diff')
        # assert iret == 0, 'patch failed'
    if todo.intersection(['insert_include', 'all']):
        incdir = opts.include
        if len(args) == 0:
            logger.info('stage: insert_include')
            args = lfor
            incdir = osp.join(incdir, 'interfaces')
        func_one = partial(insert_include, incdir=incdir)
        apply_in_parallel(func_one, args, opts.numthread)
    if todo.intersection(['all', 'patch2']):
        logger.info('stage: patch2')
        iret = os.system('patch -p1 < patches/2_missing_interfaces.diff')
        assert iret == 0, 'patch failed'
    if todo.intersection(['fix_bad_space', 'all']):
        if len(args) == 0:
            logger.info('stage: fix_bad_space')
            args = lfor
        apply_func_text_on_files(fix_bad_space, args, info_level=INFO)
    if todo.intersection(['free_form', 'all']):
        if len(args) == 0:
            logger.info('stage: free_form')
            args = lfor + linc + lferm
        convert_all(args, opts.numthread)
    if todo.intersection(['all', 'patch3']):
        logger.info('stage: patch3')
        iret = os.system(';'.join([
            'patch -p1 < patches/3_change_openmp_directives.diff',
            'patch -p1 < patches/5_function_as_argument.diff',
            'patch -p1 < patches/6_bad_dimension_in_interfaces.diff',
            'patch -p1 < patches/7_continuation_petsc.diff',
            'patch -p1 < patches/8_bad_splittings.diff',
        ]))
        assert iret == 0, 'patch failed'
    if todo.intersection(['resp', 'all']):
        if len(args) == 0:
            logger.info('stage: responsible')
            lfc = glob(osp.join('bibc', '*', '*.c')) + \
                glob(osp.join('bib*', '*', '*.h'))
            lpyt = glob(osp.join('bibpyt', '*', '*.py'))
            lcata = glob(osp.join('catalo', '*', '*.cata'))
            lcapy = glob(osp.join('catapy', '*', '*.capy'))
            ltest = glob(osp.join('astest', '*.com*'))
            args = lfor + linc + lferm + ltest + lpyt + lcata + lcapy
        apply_in_parallel(change_responsible, args, opts.numthread)
    if todo.intersection(['cmodif', 'all']):
        if len(args) == 0:
            logger.info('stage: remove cmodif')
            lfc = glob(osp.join('bibc', '*', '*.c')) + \
                glob(osp.join('bib*', '*', '*.h'))
            lpyt = glob(osp.join('bibpyt', '*', '*.py'))
            lcata = glob(osp.join('catalo', '*', '*.cata'))
            lcapy = glob(osp.join('catapy', '*', '*.capy'))
            ltest = glob(osp.join('astest', '*.com*'))
            args = lfor + linc + lfc + lferm + ltest + lpyt + lcata + lcapy
        apply_in_parallel(change_cmodif, args, opts.numthread)
    if todo.intersection(['check', 'all']):
        if len(args) == 0:
            logger.info('stage: check')
            args = lfor
        # TODO use aslint / check_files to get options from wafc4che
        filtered_args = filter_arguments(args)
        wafc4che = osp.join('build', 'std', 'c4che', 'release_cache.py')
        c4che = read_waf_parameters(wafc4che)
        report = Report()
        check_files(report, filtered_args, c4che)
        logger.info(report.to_text())
        report.report_summary()
        if todo.intersection(['save', 'auto_fix']):
            with open('check_fortran.pick', 'wb') as pick:
                pickle.dump(report, pick)
            logger.info('dict of errors pickled')
    if todo.intersection(['report', 'all']):
        if report is None:
            with open('check_fortran.pick', 'rb') as pick:
                report = pickle.load(pick)
    if todo.intersection(['report', 'all']):
        assert report is not None, 'report required'
        logger.info(report.to_text())
        report.report_summary()
