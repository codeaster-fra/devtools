#!/usr/bin/env python3
# coding=utf-8

"""
Very simple script using the aslint/fortran functions.
"""

from optparse import OptionParser
from functools import partial

from aslint.utils import apply_func_text_on_files
from aslint.logger import logger, setlevel
from aslint.fortran.free.change import add_statement


def simple(*args):
    """Example : add a line at the beginning of the subroutines."""
    # define the 'atomic' function: add_incjev(txt) => txt
    # 'add_incjev' is a function that takes a text and returns the text changed
    add_incjev = partial(add_statement, addline="      INCLUDE 'jeveux.h'")
    # use 'apply_func_text_on_files' to loop on the arguments
    apply_func_text_on_files(add_incjev, args, backup_ext='.orig')


if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('files ?')
    simple(*args)
