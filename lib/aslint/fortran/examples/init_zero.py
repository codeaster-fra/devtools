#!/usr/bin/env python3
# coding=utf-8

"""
Script to force unitialized variables to be initialized to 0.
"""

import os.path as osp
from optparse import OptionParser
from functools import partial

from aslint.config import ASCFG
from aslint.utils import apply_func_text, apply_in_parallel
from aslint.logger import logger, setlevel
from aslint.check_files import read_waf_parameters

from aslint.fortran.check_source import list_uninit
from aslint.fortran.free.change import add_statement
import aslint.fortran.free.fortran_code as FC
import aslint.fortran.free.fortran_utilities as FUT


def clean_source(txt, lvars):
    """Define the successive steps to clean the source text."""
    src = FC.source(txt)
    for varname in lvars:
        decl = FUT.get_decl(src, varname)
        if decl.attr['type'] == 'complex(kind=8)':
            value = "dcmplx(0.d0, 0.d0)"
        elif decl.attr['type'] == 'real(kind=8)':
            value = "0.d0"
        elif decl.attr['type'] == 'integer':
            value = "0"
        else:
            assert False, 'unsupported type: %s' % decl.dbg()
        txt = add_statement(txt, addline="    %s = %s" % (varname, value),
                            before_decls=False)
    return txt


def clean_uninit(fname, incdir, flags):
    """Initialize uninit variables"""
    uninit = list_uninit(fname, incdir, flags)
    logger.debug(uninit)
    # define the 'atomic' function: func(txt) => txt
    func = partial(clean_source, lvars=uninit)
    try:
        apply_func_text(func, fname)
    except AssertionError as exc:
        logger.error("Error with '%s': %s", fname, exc)


if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    parser.add_option('--build', action='store', default=None,
                      metavar='DIR',
                      help="waf build directory containing configuration parameters")
    parser.add_option('--flag', action='append',
                      help="options passed to the compiler")
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('files ?')
    builddir = opts.build or ASCFG.get('waf.builddir')
    flags = opts.flag or []
    wafc4che = osp.join(builddir, 'c4che', 'release_cache.py')
    c4che = read_waf_parameters(wafc4che)
    fclean = partial(clean_uninit,
                     incdir=c4che['INCLUDES'],
                     flags=flags)
    apply_in_parallel(fclean, args, 1)
