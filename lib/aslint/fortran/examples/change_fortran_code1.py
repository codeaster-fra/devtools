#!/usr/bin/env python3
# coding=utf-8

"""
Example #1 to change fortran source files

This example use the Block objects to change:
    if (cond) call u2mes...
into
    if (cond) then
        call ...
    endif

This is because 'call' statements on the same line as 'if' are not identified.
This script should be run before each kind of change of call statements.

---
This script loops on files given in arguments.
Example: %prog bibfor/utilitai/*.F90
     or: %prog `cat files_to_change`

You just have to modify the 'change_text' function for your needs.

By default, it runs on several threads in parallel. You can set numthread=1
to run in sequential. Use sequential mode to have a detailed traceback in case
of error.
"""

import os.path as osp
import re
from optparse import OptionParser

from aslint.utils import (
    apply_in_sandbox, apply_func_text,
)
from aslint.logger import logger, setlevel
from aslint.baseutils import is_binary
import aslint.fortran.free.fortran_code as FC
# see fortran_utilities for new available utilities
import aslint.fortran.free.fortran_utilities as FUT


def change_text(txt):
    """Call subfunctions to change 'txt'
    Change "if (cond) call ..." into
        if (cond) then
            call ...
        endif
    """
    src = FC.source(txt)
    logger.debug(src.dbg())
    sub = FC.get_subprogram(src)
    chg = src.get_type(FC.IfThen)
    # list of statements to change
    if not chg:
        # no statement, return without change
        return txt
    # loop
    for i in chg:
        # check if it can be skipped
        if i.attr['then'] != '' or re.search("^ *call +u2mes", i.attr['instr']) is None:
            continue
        # get the position of the call in 'src'
        # (used later to remove this call and insert a new one)
        pos, where = src.find_block(i)
        assert pos >= 0
        # remove the original if, replaced by a new block of 3 instructions
        FC.remove_block(where, pos)
        newb = "\n".join(["if (%s) then" % i.cond,
                          i.attr['instr'],
                          "endif"])
        FC.insert_block(where, pos, FC.source(newb, init_level=i.level - 1))
    txt = src.code_text()
    return txt


def check_ascii_binary(fname):
    """print ascii/binary detection"""
    if not osp.isfile(fname):
        logger.error('not a file: %s', fname)
        return
    typ = is_binary(fname) and 'binary' or 'ascii'
    logger.info('%s: %s', typ, fname)
    if typ == 'ascii':
        apply_func_text(change_text, fname)


def loop_on_files(*args):
    """Apply a function"""
    # set numthread=1 to debug the script
    apply_in_sandbox(check_ascii_binary, args, numthread=None)

if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('files ?')
    loop_on_files(*args)
