#!/usr/bin/env python3
# coding=utf-8

"""
Fix some errors automatically (use the result of the check).
"""

import os
import os.path as osp
from optparse import OptionParser
import pickle

from aslint.logger import logger, setlevel
from aslint.fortran.check_source import check_fortran_loop
from aslint.fortran.migration.fix_code import fix_code

SRCDIR = os.getcwd()
INCDIR = osp.join(SRCDIR, "bibfor", "include")
INCDEST = osp.join(INCDIR, "asterfort")


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    parser.add_option(
        "-j",
        "--numthread",
        action="store",
        default=1,
        type=int,
        help="run in parallel on several threads",
    )
    parser.add_option(
        "-I",
        "--include",
        action="store",
        default=INCDIR,
        help="directory of include files (default: %s)" % INCDIR,
    )
    parser.add_option(
        "-d",
        "--incdest",
        action="store",
        default=INCDEST,
        help="directory of include files (default: %s)" % INCDEST,
    )
    parser.add_option(
        "--auto", action="store_true", default=False, help="loop automatically on files to fix"
    )
    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error("exactly one argument is required")
    freport = args[0]
    report = None
    while True:
        if not report:
            with open(freport, "rb") as pick:
                report = pickle.load(pick)
        args = fix_code(report, fix=False)
        logger.info("%4d files to fix", len(args))
        if not args:
            break
        logger.info("stage: fix errors")
        args = fix_code(report)
        logger.info("%4d files changed", len(args))
        # loop for 'auto_fix'
        if opts.auto:
            logger.info("stage: check")
            flags = [
                "-DASTER_HAVE_OPENMP",
                "-DASTER_HAVE_MUMPS",
                "-DASTER_HAVE_PETSC",
            ]
            incdir = [opts.include, osp.join(opts.include, "fake")]
            report = check_fortran_loop(args, opts.numthread, flags, incdir, "gfortran")
            logger.error(report.to_text())
            report.report_summary()
        else:
            break
