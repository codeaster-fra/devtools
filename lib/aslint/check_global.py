# coding=utf-8

"""Checkers applied on several types of source files"""

import os
import os.path as osp
import tempfile
from subprocess import PIPE, CalledProcessError, check_call

from .base_checkers import (
    CheckContext,
    CheckList,
    DirnameCat,
    GenericMsg,
    Report,
    TextMsg,
    check_filename,
    checkers_from_context,
)
from .config import ASCFG
from .i18n import _
from .logger import logger
from .messages import CataMessageError, MessagesManager


class ErrorMessages(DirnameCat, GenericMsg):

    """Unused or missing messages
    All error messages must be called in a source file and all messages called
    must exist."""

    id = "C9003"
    fmt = "%(id)s: %(label)s"

    def search(self, srcdir):
        """Check encoding of txt"""
        result = []
        # only in 'src'
        if not (
            osp.isdir(osp.join(srcdir, "bibpyt", "Messages"))
            or osp.isdir(osp.join(srcdir, "code_aster", "Messages"))
        ):
            return result
        try:
            unused, not_found = check_messages(srcdir)
            result.extend([": %s (unused)" % msg for msg in unused])
            result.extend([": %s (missing)" % msg for msg in not_found])
        except ValueError as exc:
            result.append(": check failed: %s" % str(exc))
        return result


CHECK_LIST = checkers_from_context(globals(), TextMsg)


def check_messages(srcdir):
    """Check the errors messages"""
    tmpcache = tempfile.NamedTemporaryFile(prefix="cachedict.").name
    msgman = MessagesManager(repref=srcdir, cache_dict=tmpcache, verbose=False, force=True)
    msgman.read_cata()
    try:
        unused, not_found = msgman.check_cata()
    except CataMessageError as msg:
        logger.error(_("error during check of error messages:\n%s"), str(msg))
        raise ValueError
    finally:
        try:
            os.remove(tmpcache)
        except OSError:
            pass
    return unused, not_found


def check_global(srcdir):
    """Check a directory. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(CHECK_LIST)
    report = Report()
    logger.info(_("add checking of error messages"))
    lmsg = check_filename(srcdir, checklist.on_dirname())
    report.set(srcdir, lmsg)
    return report
