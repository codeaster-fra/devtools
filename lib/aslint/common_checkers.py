# coding=utf-8

"""Checkers common to several type of source files

They may be included together using CHECK_LIST or individually."""

import os
import os.path as osp
import re
import stat
import time
from subprocess import PIPE, CalledProcessError, run

from aslint.base_checkers import (
    CheckContext,
    DiffCat,
    DisableMsg,
    DisableMsgError,
    FileContentCat,
    FilenameCat,
    GenericMsg,
    HiddenMsg,
    TextMsg,
    TextMsgNotFound,
    checkers_from_context,
    search_msg,
)
from aslint.baseutils import is_binary
from aslint.config import ASCFG
from aslint.logger import logger
from aslint.utils import convert_addr, difftxt, have_program

CMT = r"(?:[!#\*%]|/\*|//)"
CURRENT_YEAR = int(time.strftime("%Y"))

SCRIPTS = [
    "^check_.*\.sh",
    "^doc/generate_rst.py",
    "^waf(|\.(engine|main)|_variant)$",
    "configure",
    "\.gitlabci/.*\.sh",
]


def search_case(expr):
    """Convenient wrapper to search 'expr' with relevant re flags."""
    return re.compile(expr, re.M).finditer


class IncorrectPermission(FilenameCat, GenericMsg):

    """Permission of source files must be rw-r--r--
    Use `fix_permission` to fix that."""

    id = "C9005"
    fixme = ASCFG.get("devtools_root") + "/bin/fix_permission"
    fmt = "%(id)s: %(label)s"
    _req = stat.S_IREAD | stat.S_IWRITE
    _bad = stat.S_IEXEC

    def search(self, fname):
        """Check for file mode"""
        result = []
        if not osp.isfile(fname):
            return []
        if set([re.search(expr, fname) for expr in SCRIPTS]) != set([None]):
            return result
        mode = os.stat(fname).st_mode
        if mode & self._req != self._req or mode & self._bad != 0:
            result.append(" not %s" % (oct(mode)[-3:]))
        return result


class AslintDisableWarn(FileContentCat, DisableMsg):

    """Disabled message(s)
    Used to disable a warning that can not be removed in a special case"""

    id = "W8888"
    search = search_msg("^%s *aslint *: *disable=" "(?P<disable>([a-z][0-9]{4} *,? *)+)" % CMT)


class AslintDisableError(FileContentCat, DisableMsgError):

    """Disabled message(s) but not raised
    A disabled message is not raised, please remove it"""

    id = "C8887"
    search = AslintDisableWarn.search


class NewAslintDisable(DiffCat, TextMsg):

    """New disabled message(s)
    If you added or changed one, please check if it can be removed instead
    of disabled."""

    id = "W8889"
    search = search_msg("^%s *aslint *: *disable=" "(?P<main>([a-z][0-9]{4},?)+)" % CMT)


class DeprecatedDisable(FileContentCat, TextMsg):

    """Obsolete format of disabled message
    This is the obsolete format to disable conformance warnings.
    Must be removed."""

    id = "C8884"
    search = search_msg("^%s *TOLE *(?P<main>(cr[sp]_[0-9]+ *)+)" % CMT)


class DeprecatedPersonInCharge(FileContentCat, TextMsg):

    """Obsolete syntax: please use 'person_in_charge: email@domain.com'
    This is the obsolete format to declare the person in charge.
    Must be removed."""

    id = "C8885"
    search = search_msg("^%s *(?P<main>RESPONSABLE +.*) *$" % CMT)


class DeprecatedModif(FileContentCat, TextMsg):

    """Obsolete syntax: please remove this line from the old source manager
    This is the obsolete format to store the modification date.
    Must be removed."""

    id = "C8883"
    search = search_case("^%s +(?P<main>(?:(?:AJOUT *|MODIF +\w+.*)))$" % CMT)


class DeprecatedMarker(FileContentCat, TextMsg):

    """Obsolete syntax: please remove this line from the old source manager
    This is an obsolete marker not used anymore
    Must be removed."""

    id = "C8882"
    search = search_msg("^%s +(?P<main>CONFIGURATION MANAGEMENT.*" "EDF VERSION.*)$" % CMT)


class PersonInCharge(FileContentCat, TextMsg):

    """Person in charge
    The person in charge of this module will be notified if one submit a request
    for integration of these changes."""

    id = "I8801"
    fmt = "%(id)s: %(label)s: %(main)s"
    search = search_msg("^%s *(person_in_charge *:) *" "(?P<main>.*?)(?:| *\*/) *$" % CMT)


class InvalidPersonInCharge(FileContentCat, GenericMsg):

    """Invalid address for the person in charge
    The email address must not contain space and must have a '@'."""

    id = "C8802"
    fmt = "%(id)s: %(label)s"

    def search(self, txt):
        """Check email validity"""
        value = PersonInCharge.search(txt)
        err = []
        for add in [occ.group("main") for occ in value]:
            err.extend([" " + repr(i) for i in convert_addr(add)[1]])
        return err


class InvalidEncoding(FilenameCat, GenericMsg):

    """Invalid encoding, expecting utf-8
    Source files must be encoded in utf-8 (or ascii)."""

    id = "C8501"
    fmt = "%(id)s: %(label)s"

    def search(self, fname):
        """Check for file encoding"""
        result = []
        if not osp.isfile(fname) or fname == "waf.engine" or is_binary(fname):
            return result
        with open(fname, "r") as fobj:
            if not check_encoding(fobj.read(), "utf-8"):
                result.append("")
        return result


def check_encoding(text, encoding):
    """check for supported encoding"""
    valid = False
    try:
        text.encode(encoding)
        valid = True
    except UnicodeDecodeError:
        pass
    return valid


class InvalidEndOfLine(FileContentCat, GenericMsg):
    r"""Invalid end of line, expecting '\n' not '\r\n'"""
    id = "C8502"
    fmt = "%(id)s: %(label)s"

    def search(self, txt):
        """Check for CR"""
        result = []
        if "\r\n" in txt:
            result.append("")
        return result


class OutOfDateCopyright(FileContentCat, GenericMsg):

    """Copyright is out of date, must be updated to the current year
    See `fix_copyright` tool (use --help option for details)."""

    id = "C8503"
    fixme = ASCFG.get("devtools_root") + "/bin/fix_copyright %(filenames)s"
    _re1 = re.compile(r"\s+(copyright.*edf.*code\-aster\.org)(?:| *\*/) *$", flags=re.I | re.M)
    _re2 = re.compile("([0-9]{4})")

    def search(self, txt):
        """Search the text in the comments"""
        txt = self._re1.findall(txt)
        res = []
        for copy in txt:
            years = [int(i) for i in self._re2.findall(copy)] or [0]
            last = max(years)
            if int(last) < CURRENT_YEAR:
                res.append(": replace {0} by {1}".format(last, CURRENT_YEAR))
        return res


CHECK_LIST = checkers_from_context(globals(), (TextMsg, DisableMsg))

# Message with no id are not included in CHECK_LIST


class LineTooLong(TextMsg):

    """Line too long
    Lines must not be too long for readability and convention.
    Maximum line length is 80 columns in python and fortran, 132 for other types
    of source files."""

    # id must be defined in subclasses
    fmt = "%(id)s: %(label)s"
    _maxlength = 0

    def search(self, txt):
        """Return the line numbers"""
        return [(i, line) for i, line in enumerate(txt.splitlines()) if len(line) > self._maxlength]

    def check(self, msglist, txt):
        """Check the strings length"""
        self.match = self.search(txt)
        count = len(self.match)
        if count > 0:
            lines = ", ".join([str(i + 1) for i, line in self.match[:10]])
            longest = max([len(line) for i, line in self.match[:10]])
            if count > 10:
                lines += "..."
            self.store_msg_minimal(
                msglist,
                ": %d lines longer than %d columns (longest:%s, lines: %s)"
                % (count, self._maxlength, longest, lines),
            )
        return count


class LicenseNotFound(GenericMsg, TextMsgNotFound):

    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org"""

    # id must be defined in subclasses
    fixme = ASCFG.get("devtools_root") + "/bin/fix_license %(filenames)s"

    _GPL_ = " ".join(
        [
            "This file is part of code_aster.",
            "code_aster is free software: you can redistribute it and/or modify",
            "it under the terms of the GNU General Public License as published by",
            "the Free Software Foundation, either version 3 of the License, or",
            "(at your option) any later version.",
            "code_aster is distributed in the hope that it will be useful,",
            "but WITHOUT ANY WARRANTY; without even the implied warranty of",
            "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the",
            "GNU General Public License for more details.",
            "You should have received a copy of the GNU General Public License",
            "along with code_aster.  If not, see <http://www.gnu.org/licenses/>.",
        ]
    )
    _REG_COMMENT = re.compile(r"^ *%s(.*?)(?:|\*/) *$" % CMT, re.M)
    _REG_PUNCT = re.compile(r"[,;:!\. \(\)]+", re.M)
    _GPLc_ = _REG_PUNCT.sub(" ", _GPL_)
    _REG_GPL = re.compile(_GPLc_, flags=re.I | re.M)

    def search(self, txt):
        """Search the text in the comments"""
        txt = " ".join(self._REG_COMMENT.findall(txt)).lower()
        # ignore punctation
        txt = self._REG_PUNCT.sub(" ", txt)
        found = self._REG_GPL.findall(txt)
        if not found:
            if logger.debug_enabled():
                fun = lambda string: os.linesep.join(string.split())
                diff = difftxt(fun(self._GPL_), fun(txt))
                logger.info(diff)
                return [", diff: %s" % diff]
            else:
                return [", re-run with '--debug' to see the missing words"]
        return []


class EDFCopyright(TextMsgNotFound):

    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and/or version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2023 EDF R&D www.code-aster.org
    """

    # id must be defined in subclasses
    fmt = "%(id)s: %(label)s"
    search = search_msg("^ *%s *copyright.*[0-9]{2,4}.* +edf.*" r" +www\.code\-aster\.org" % CMT)


class ExternalCopyright(GenericMsg):

    """Another Copyright than EDF
    Only the first one is reported."""

    # id must be defined in subclasses

    def search(self, txt):
        """Search the text in the comments"""
        txt = re.compile(r"\s+copyright(.*)$", flags=re.I | re.M).findall(txt)
        noedf = [line for line in txt if "edf" not in line.lower()][:1]
        return noedf


class EncodingDeclNotFound(TextMsgNotFound):

    """Encoding definition is required in the first or second line
    Conforming to PEP0263, the encoding of the source file must be defined
    by placing a magic comment as first or second line of the file
    (see http://www.python.org/dev/peps/pep-0263/).
    Source files must use utf-8."""

    # id must be defined in subclasses
    fmt = "%(id)s: %(label)s"
    _search_func = search_msg(r"coding[:=]\s*utf-8")

    def search(self, txt):
        """Search the magic comment in the first or second ligne (PEP0263)"""
        return self._search_func(os.linesep.join(txt.splitlines()[:2]))


class InvalidCharacter(GenericMsg):
    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""
    # id must be defined in subclasses
    fmt = "%(id)s: %(label)s"

    def search(self, txt):
        """Check for tabs character"""
        result = []
        if "\t" in txt:
            result.append("")
        return result


class ReformatPy(GenericMsg):
    """Reformat Python source with black."""

    fixme = "black %(filenames)s"
    apply_ctxt = CheckContext(opts=CheckContext.FormatOpt)
    _check = have_program("black")

    def search(self, filename):
        """Check file"""
        err = []
        if not self._check:
            err = [": source format not checked, use a platform where 'black' is available"]
        if not self._check or not osp.isfile(filename):
            return err
        try:
            cmd = ["black", "--check", filename]
            run(cmd, check=True, stderr=PIPE)
        except CalledProcessError as exc:
            ret = exc.returncode
            if ret == 1:
                err.append(": source format does not respect conventions")
            elif ret == 0:
                pass
            else:
                err.append(": black formater failed!")
        return err


class ReformatFort(GenericMsg):
    """Reformat Fortran source with fprettify"""

    fixme = "fprettify %(filenames)s"
    apply_ctxt = CheckContext(opts=CheckContext.FormatOpt)
    _check = have_program("fprettify")

    def search(self, filename):
        """Check file"""
        err = []
        if not self._check:
            err = [": source format not checked, use a platform where 'fprettify' is available"]
        if not self._check or not osp.isfile(filename):
            return err
        try:
            cmd = ["fprettify", "--diff", filename]
            proc = run(cmd, check=True, stdout=PIPE, stderr=PIPE)
            if proc.returncode != 0 or proc.stderr:
                raise CalledProcessError(4, cmd, proc.stdout, proc.stderr)
            if proc.stdout:
                raise CalledProcessError(1, cmd, proc.stdout, proc.stderr)
        except CalledProcessError as exc:
            ret = exc.returncode
            if ret == 1:
                err.append(": source format does not respect conventions")
            elif ret == 0:
                pass
            else:
                err.append(": fprettify formater failed!")
        return err


class ReformatC(GenericMsg):
    """Reformat C/C++ source with clang-format"""

    fixme = "clang-format -i %(filenames)s"
    apply_ctxt = CheckContext(opts=CheckContext.FormatOpt)
    _check = have_program("clang-format")

    def search(self, filename):
        """Check file"""
        err = []
        if not self._check:
            err = [": source format not checked, use a platform where 'clang-format' is available"]
        if not self._check or not osp.isfile(filename):
            return err
        try:
            with open(filename, "rb") as fsrc:
                orig = fsrc.read()
            cmd = ["clang-format", filename]
            proc = run(cmd, check=True, stdout=PIPE, stderr=PIPE)
            if proc.returncode != 0 or proc.stderr:
                raise CalledProcessError(4, cmd, proc.stdout, proc.stderr)
            if proc.stdout != orig:
                raise CalledProcessError(1, cmd, proc.stdout, proc.stderr)
        except CalledProcessError as exc:
            ret = exc.returncode
            if ret == 1:
                err.append(": source format does not respect conventions")
            elif ret == 0:
                pass
            else:
                err.append(": clang-format formater failed!")
        return err
