# coding=utf-8

"""Checking of errors messages"""

import os
import os.path as osp
import re
import pickle
import time
from glob import glob

from aslint.i18n import _
from aslint.logger import logger


class CataMessageError(Exception):

    """Error with a catalog of messages."""

    def __init__(self, fcata, reason):
        self.reason = reason
        self.fcata = fcata

    def __str__(self):
        return "%s: %s, error: %s" % (self.__class__.__name__, self.fcata, self.reason)


class MessageError(Exception):

    """Error on a message."""

    def __init__(self, msgid, reason):
        self.reason = reason
        self.msgid = msgid

    def __str__(self):
        return "%s: %s, error: %s" \
            % (self.__class__.__name__, self.msgid, self.reason)


class MessagesManager(object):

    """Classe pour récupérer des informations sur le catalogue de messages"""

    def __init__(self, repref,
                 fort=("bibfor",),
                 cxx=("bibc", "bibcxx"),
                 pyt="code_aster",
                 cache_dict=None, verbose=True, force=False,
                 ignore_comments=True, debug=False):
        """Initialisations."""
        if type(fort) not in (list, tuple):
            fort = [fort, ]
        self.fort = [osp.join(repref, rep) for rep in fort]
        self.cxx = [osp.join(repref, rep) for rep in cxx]
        self.pyt = osp.join(repref, pyt)
        self.verbose = verbose
        self.debug = debug
        self.force = force
        self.message_dir = 'Messages'
        self.cache_dict = cache_dict or osp.join(
            '/tmp', 'messages_dicts.pick')
        self.ignore_comments = ignore_comments
        logger.debug('Directories :\nREF=%s\nFORTRAN=%s\CXX=%s\nPYTHON=%s',
                     repref, self.fort, self.cxx, self.pyt)
        self.l_cata = self._get_list_cata()

    def read_cata(self):
        """Read all catalogues"""
        self._build_dict()
        self.print_stats()

    def _filename_cata(self, cata):
        """Retourne le nom du fichier associé à un catalogue."""
        name = osp.join(self.pyt, self.message_dir, cata + '.py')
        return name

    def _id2filename(self, msgid):
        """Retourne modelisa5 à partir de MODELISA5_46"""
        mat = re.search('([A-Z]+[0-9]*)_([0-9]+)', msgid)
        if mat is None:
            raise MessageError(msgid, _('invalid message id'))
        cata, num = mat.groups()
        return self._filename_cata(cata.lower()), int(num)

    def _filename2id(self, fcata, num):
        """Retourne MODELISA5_46 à partir de modelisa5"""
        return '%s_%d' % (osp.splitext(osp.basename(fcata))[0].upper(), num)

    def _get_list_cata(self):
        """Retourne la liste des catalogues de messages"""
        excluded = ['__init__.py', 'context_info.py', 'fermetur.py', "Utmess.py"]
        s_cata = set(glob(osp.join(self.pyt, self.message_dir, '*.py')))
        s_cata = set([osp.abspath(f) for f in s_cata
                      if not osp.basename(f) in excluded])
        l_cata = list(s_cata)
        l_cata.sort()
        return l_cata

    def check_cata(self):
        """Vérifie les catalogues"""
        logger.debug('%5d files in %s', len(self.l_cata), self.pyt)
        all_msg = []
        error_occurs = False
        for f in self.l_cata:
            try:
                cata = MsgCata(f)
                cata.check()
                all_msg.extend([self._filename2id(f, i) for i in cata])
                logger.debug(cata)
            except CataMessageError as msg:
                error_occurs = True
                logger.error(msg)
                self.l_cata.remove(f)
        if error_occurs:
            raise CataMessageError(_("global"), _("errors occurred!"))
        all_msg = set(all_msg)

        # messages jamais appelés
        used = set(self.d_msg_call.keys())
        unused = list(all_msg.difference(used))
        unused.sort()

        union = used.union(all_msg)
        not_found = list(used.difference(all_msg))
        not_found.sort()
        if self.verbose:
            logger.info('%6d messages', len(all_msg))
            logger.info(
                '    %6d messages used and found in files', len(used) - len(not_found))
            logger.info('    %6d unused messages', len(unused))
            logger.info('   + %6d messages not found', len(not_found))
        return unused, not_found

    def _get_list_src(self):
        """Retourne la liste des routines fortran, cxx et python.
        """
        lst = self._get_list_fort()
        l_cxx = self._get_list_cxx()
        l_pyt = self._get_list_python()
        logger.debug('%5d fortran subroutines in %s', len(lst), self.fort)
        logger.debug('%5d c++ modules in %s', len(l_cxx), self.cxx)
        logger.debug('%5d python modules in %s', len(l_pyt), self.pyt)
        lst.extend(l_cxx)
        lst.extend(l_pyt)
        return lst

    def _get_list_fort(self):
        """Retourne la liste des fichiers sources fortran"""
        s_f = set()
        for dsrc in self.fort:
            s_f.update(glob(osp.join(dsrc, '**', '*.F90')))
        lst = [osp.abspath(f) for f in s_f]
        lst.sort()
        return lst

    def _get_list_cxx(self):
        """Retourne la liste des fichiers sources C++"""
        s_f = set()
        for dsrc in self.cxx:
            s_f.update(glob(osp.join(dsrc, '**', '*.h')))
            s_f.update(glob(osp.join(dsrc, '**', '*.c')))
            s_f.update(glob(osp.join(dsrc, '**', '*.cxx')))
        lst = [osp.abspath(f) for f in s_f]
        lst.sort()
        return lst

    def _get_list_python(self):
        """Retourne la liste des fichiers python"""
        s_f = glob(osp.join(self.pyt, '**', '*.py'), recursive=True)
        lst = [osp.abspath(f) for f in s_f]
        lst.sort()
        return lst

    def search_message(self, fich):
        """Retourne les messages utilisés dans 'fich'"""
        try:
            with open(fich, 'r') as fobj:
                txt = fobj.read()
        except IOError as msg:
            logger.error(_('Error with file {0}: {1}').format(fich, msg))
            return []
        ext = osp.splitext(fich)[-1]
        txt = clean_source(txt, ext, ignore_comments=self.ignore_comments)

        if ext in ('.F90',):
            expr = (r'CALL\s+(U2MES.|UTEXC[MP]+|UTPRIN|UTMESS|UTMESS_CORE)\s*'
                    r'\(([^,]*?), *[\'\"]+(.*?)[\'\"]+ *[,\)]+')
        elif ext in ('.py',):
            expr = (r'[\s\.:]+(UTMESS|GetText|message_exception)\s*'
                    r'\(([^,]*?), *[\'\"]+(.*?)[\'\"]+ *[,\)]+')
        else:
            expr = (r'[\s\.:]+(UTMESS|CALL_UTMESS|CALL_UTMESS_CORE)\s*'
                    r'\(([^,]*?), *[\'\"]+(.*?)[\'\"]+ *[,\)]+')

        all_msg = re.findall(expr, txt, flags=re.I)
        l_msg = []
        for found in all_msg:
            sub, typ, msg = found
            if msg.startswith('FERMETUR_'):
                pass
            elif re.search('^[A-Z0-9]+_[0-9]+$', msg) is None:
                logger.error(
                    "Invalid message id ('%s') in file %s (%s)", msg, fich, sub)
            else:
                mattyp = re.search(r'[\'\" ]+(.*?)[\+\'\" ]+', typ)
                # variables and numbers (for exception) are allowed
                if mattyp is not None and \
                   mattyp.group(1) not in ('', 'A', 'I', 'E', 'S', 'F', 'M', 'D', 'Z'):
                   # may be '' for example STY//'+'
                    logger.error(
                        "Invalid message type (%s) for message '%s' in file %s",
                        mattyp.group(1), msg, fich)
                else:
                    l_msg.append(msg)
        # verif
        l_res = []
        for msg in l_msg:
            spl = msg.split('_')
            assert len(spl) == 2, 'ERROR invalid : %s' % msg
            msg = '%s_%d' % (spl[0], int(spl[1]))
            l_res.append(msg)

        return l_res

    def _build_dict(self):
        """Construit les dictionnaires :
            fichier source : liste des messsages appelés
            message : liste des fichiers appelant ce message
        """
        # est-ce dans le cache ?
        if not self.force and osp.isfile(self.cache_dict) \
                and os.stat(self.cache_dict).st_size > 0:
            logger.debug('Load dicts from cache (%s)...', self.cache_dict)
            with open(self.cache_dict, 'rb') as pick:
                self.d_msg_used = pickle.load(pick)
                self.d_msg_call = pickle.load(pick)
        else:
            self.d_msg_used = {}
            self.d_msg_call = {}
            l_all = self._get_list_src()
            for _, f in enumerate(l_all):
                key = re.sub('^%s/*', '', f)
                lm = self.search_message(f)
                if len(lm) > 0:
                    self.d_msg_used[key] = lm
                    for msg in lm:
                        self.d_msg_call[msg] = self.d_msg_call.get(
                            msg, []) + [key, ]
            with open(self.cache_dict, 'wb') as pick:
                pickle.dump(self.d_msg_used, pick)
                pickle.dump(self.d_msg_call, pick)

    def print_stats(self):
        """Print statistics"""
        if self.verbose:
            logger.info(
                '%6d messages appelés dans les sources', len(list(self.d_msg_call.keys())))
            logger.info(
                '%6d fichiers sources appellent le catalogue de messages', len(list(self.d_msg_used.keys())))


class MsgCata(object):

    """Classe représentant un catalogue de messages.
    Méthodes attendues :
        - nombre de messages,
        - indice du dernier messages,
        - indice libre,
        - ...
    """

    def __init__(self, fcata):
        """Initialisation
        """
        self.fcata = fcata
        self.cata_msg = {}
        try:
            ctxt = {'_': lambda x: x, }
            with open(fcata) as fobj:
                # Hide import of '_'
                text = fobj.read().replace("from ", "# from ")
                exec(compile(text, fcata, 'exec'), ctxt)
            self.cata_msg = ctxt['cata_msg']
        except Exception as msg:
            logger.error(msg)
            raise CataMessageError(self.fcata, _('unable to import the file'))

    def get_nb_msg(self):
        """Nombre de messages.
        """
        return len(self.cata_msg)

    def get_last_id(self):
        """Indice du dernier message.
        """
        return max(list(self.cata_msg.keys()) or [0, ])

    def get_new_id(self, reuse_hole=True):
        """Indice libre. Si 'end', on prend systématiquement à la fin,
        sinon on cherche aussi dans les trous.
        """
        if not reuse_hole:
            start = self.get_last_id()
        else:
            start = 1
        all = set(range(start, 100))
        free = all.difference(list(self.cata_msg.keys()))
        if len(free) == 0:
            new = -1
        else:
            new = min(free)
        return new

    def __getitem__(self, key):
        """Retourne le contenu du message ou None.
        """
        return self.cata_msg.get(key, None)

    def __setitem__(self, key, msg):
        """Ajoute le message 'msg' à l'indice 'key'.
        """
        if self[key] is not None:
            raise MessageError(key, _('message already exists !'))
        self.cata_msg[key] = msg

    def __repr__(self):
        """Résumé du catalogue.
        """
        return '%3d messages (last=%3d, next=%3d) in %s' % \
            (self.get_nb_msg(), self.get_last_id(),
             self.get_new_id(), self.fcata)

    def __iter__(self):
        """Itération sur les id des messages.
        """
        return iter(self.cata_msg)

    def check(self):
        """Vérifie le texte des messages."""
        def_args = {}
        for i in range(1, 100):
            def_args['i%d' % i] = 99999999
            def_args['r%d' %
                     i] = 1.234e16   # not too big to avoid python issue1742669
            def_args['k%d' % i] = 'xxxxxx'
        def_args['ktout'] = "all strings !"
        error = []
        for num, msg in list(self.cata_msg.items()):
            if type(msg) is dict:
                msg = msg['message']
            try:
                txt = msg % def_args
            except:
                error.append('message #%s invalid : %s' % (num, msg))
        if len(error) > 0:
            raise CataMessageError(self.fcata, os.linesep.join(error))


def clean_source(content, ext, ignore_comments):
    """Nettoie un fichier source"""
    if ignore_comments:
        assert ext in (".F90", ".py", ".h", ".c", ".cxx"), 'unknown type : %s' % str(ext)
        if ext in (".F90",):
            expr = "^ *[!#].*$"
        elif ext in (".py",):
            expr = "^ *#.*$"
        elif ext in (".h", ".c", ".cxx"):
            expr = r"^ */\*|//.*$"
        reg_ign = re.compile(expr, re.MULTILINE)
        content = reg_ign.sub("", content).expandtabs()
    return content
