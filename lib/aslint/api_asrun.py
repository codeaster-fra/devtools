# coding: utf-8

"""Interfaces to call asrun functions"""

import sys
import os
import os.path as osp
from functools import partial
import tempfile
from functools import wraps
import subprocess as SP

from aslint.i18n import _
from aslint.logger import logger
from aslint.config import ASCFG


def check_asrun(raise_on_error=True):
    """Check import of asrun"""
    isok = getattr(check_asrun, "_isok", None)
    if isok is None:
        isok = True
        check_asrun_bin()
        asrun_lib = ASCFG.get("asrun.lib")
        if not asrun_lib in sys.path:
            sys.path.append(asrun_lib)
        try:
            if sys.version_info > (3,):
                from asrun import PY3_ONLY
            import asrun.run

            assert hasattr(asrun.run, "AsRunFactory")
        except (ImportError, AssertionError, SyntaxError):
            logger.warn(
                _(
                    "please check the parameter 'asrun-bin' is correct in "
                    "your ~/.gitconfig configuration file, if it is, you may "
                    "have to set manually 'asrun-lib'"
                )
            )
            isok = False
        check_asrun._isok = isok
    if raise_on_error:
        assert isok, _("'asrun' is required")
    return isok


def check_asrun_bin():
    """Check availability of as_run script"""
    try:
        SP.check_call([ASCFG.get("asrun.bin"), "--version"], stdout=SP.PIPE)
    except (OSError, SP.CalledProcessError):
        logger.error(
            _(
                "'as_run' script not found in your $PATH; sourcing an "
                "environment may be required. You can also "
                "define 'asrun-bin' in your ~/.gitconfig configuration file "
                "to select a specific version."
            )
        )
        raise


def need_asrun(func):
    """decorator for functions that require asrun package"""

    @wraps(func)
    def wrapper(*args, **kwds):
        """wrapper"""
        check_asrun()
        return func(*args, **kwds)

    return wrapper


@need_asrun
def get_asrun_config(version_path):
    """Return the :py:class:`AsterConfig` object for a version.

    Arguments:
        version_path (str): Path to the version.

    Returns:
        *AsterConfig*: asrun object.
    """
    from asrun.config import AsterConfig

    config = AsterConfig(osp.join(version_path, "config.txt"))
    return config
