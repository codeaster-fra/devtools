# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-04 11:18+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../aslint/api_asrun.py:33
msgid ""
"please check the parameter 'asrun.bin' is correct in your ~/.hgrc "
"configuration file, if it is, you may have to set manually 'asrun.lib'"
msgstr ""

#: ../../aslint/api_asrun.py:39
msgid "'asrun' is required"
msgstr ""

#: ../../aslint/api_asrun.py:48
msgid ""
"'as_run' script not found in your $PATH; sourcing an environment may be "
"required. You can also define 'asrun.bin' in your ~/.hgrc configuration file "
"to select a specific version."
msgstr ""

#: ../../aslint/api_asrun.py:80
msgid "error during connection to REX database"
msgstr ""

#: ../../aslint/api_asrun.py:91
msgid ""
"perhaps MySQLdb is missing or the environment is not properly set, try:\n"
msgstr ""

#: ../../aslint/api_asrun.py:105
msgid "asrun 1.13.1 or newer is required"
msgstr ""

#: ../../aslint/check_inst.py:31 ../../hgaster/ascommands.py:836
msgid "waf parameters are required"
msgstr ""

#: ../../aslint/check_inst.py:33
#, python-format
msgid "checking installation in %s"
msgstr ""

#: ../../aslint/check_inst.py:45
#, python-brace-format
msgid "not found: {0}"
msgstr ""

#: ../../aslint/check_inst.py:55
#, python-brace-format
msgid "at least one of {0} is expected"
msgstr ""

#: ../../aslint/check_inst.py:63
msgid "invalid installation"
msgstr ""

#: ../../aslint/check_inst.py:65
msgid "(DRY RUN) invalid installation"
msgstr ""

#: ../../aslint/check_inst.py:67
msgid "installation ok"
msgstr ""

#: ../../aslint/check_inst.py:82 ../../aslint/check_files.py:152
#, python-format
msgid "(DRY RUN) %s"
msgstr ""

#: ../../aslint/fortran/check_source.py:61
#, python-format
msgid "cannot check %r"
msgstr ""

#: ../../aslint/fortran/check_source.py:85
msgid "add checkings of the source directory"
msgstr ""

#: ../../aslint/fortran/check_source.py:90
msgid "'bibfor' not found in path, duplicate filenames not checked"
msgstr ""

#: ../../aslint/fortran/check_source.py:132
msgid "preparing module files"
msgstr ""

#: ../../aslint/fortran/check_source.py:179
#, python-brace-format
msgid "external module: {0}"
msgstr ""

#: ../../aslint/fortran/migration/extern_tools.py:43
msgid "check that the source file is in fixed-form"
msgstr ""

#: ../../aslint/utils.py:112
#, python-brace-format
msgid "removing '{0}'..."
msgstr ""

#: ../../aslint/utils.py:122
#, python-format
msgid "can not remove: '%s'"
msgstr ""

#: ../../aslint/utils.py:123
#, python-format
msgid ""
"Error:\n"
"%s"
msgstr ""

#: ../../aslint/utils.py:152
#, python-format
msgid "Enter in directory %s..."
msgstr ""

#: ../../aslint/utils.py:221
msgid ""
"Problem detected with your Python interpreter (see http://bugs.python.org/"
"issue3770): only one thread will be used"
msgstr ""

#: ../../aslint/utils.py:256
#, python-format
msgid "invalid email address: '%s'"
msgstr ""

#: ../../aslint/utils.py:289 ../../aslint/utils.py:290
#, python-format
msgid ""
"unexpected diff format:\n"
"%s"
msgstr ""

#: ../../aslint/utils.py:292
#, python-format
msgid ""
"unexpected split of diff:\n"
"%s"
msgstr ""

#: ../../aslint/messages.py:84
msgid "invalid message id"
msgstr ""

#: ../../aslint/messages.py:117
msgid "global"
msgstr ""

#: ../../aslint/messages.py:117
msgid "errors occurred!"
msgstr ""

#: ../../aslint/messages.py:180
#, python-brace-format
msgid "Error with file {0}: {1}"
msgstr ""

#: ../../aslint/messages.py:281
msgid "unable to import the file"
msgstr ""

#: ../../aslint/messages.py:318
msgid "message already exists !"
msgstr ""

#: ../../aslint/check_global.py:90
#, python-format
msgid ""
"error during check of error messages:\n"
"%s"
msgstr ""

#: ../../aslint/check_global.py:105
msgid "add checking of error messages"
msgstr ""

#: ../../aslint/check_files.py:49
msgid "checking the changed files"
msgstr ""

#: ../../aslint/check_files.py:51
msgid ""
"Some checkings can not pass with a parallel build. Try 'aslint --build=...' "
"option or use 'waf.builddir' in your ~/.hgrc file."
msgstr ""

#: ../../aslint/check_files.py:63
#, python-brace-format
msgid "checking {0:4d} files of type '{1}'"
msgstr ""

#: ../../aslint/check_files.py:66
msgid "(DRY RUN) list of files:"
msgstr ""

#: ../../aslint/check_files.py:75
msgid "waf parameters are required, run 'waf configure' to fill the parameters"
msgstr ""

#: ../../aslint/check_files.py:105
#, python-format
msgid "this type is not yet checked: '%s'"
msgstr ""

#: ../../aslint/check_files.py:120
#, python-brace-format
msgid "checking diff of {0:4d} files of type '{1}'"
msgstr ""

#: ../../aslint/check_files.py:132
#, python-brace-format
msgid "run the waf command: {0} {1} {2}"
msgstr ""

#: ../../aslint/check_files.py:154
#, python-format
msgid "waf failed: %s"
msgstr ""

#: ../../aslint/check_files.py:158
msgid "waf ended successfully"
msgstr ""

#: ../../aslint/check_files.py:160
#, python-format
msgid "waf exited with code %d"
msgstr ""

#: ../../aslint/check_files.py:233
msgid "running the testcases without those with 'verif_interact' tag"
msgstr ""

#: ../../aslint/check_files.py:241
msgid "running the testcases with 'verif_interact' tag"
msgstr ""

#: ../../aslint/check_files.py:253
msgid "final results of all the testcases"
msgstr ""

#: ../../aslint/check_files.py:285
msgid "no testcases to execute"
msgstr ""

#: ../../aslint/check_files.py:288
#, python-format
msgid ""
"calling as_run with the export file:\n"
"%s"
msgstr ""

#: ../../aslint/check_files.py:293
#, python-format
msgid "directory of the results of the testcases: %s"
msgstr ""

#: ../../aslint/check_files.py:296
#, python-format
msgid "(DRY RUN) execute: %s"
msgstr ""

#: ../../aslint/check_files.py:297
msgid "(DRY RUN) testcases not run"
msgstr ""

#: ../../aslint/check_files.py:299
#, python-format
msgid "waiting for the end of the %d testcases..."
msgstr ""

#: ../../aslint/check_files.py:301
#, python-format
msgid "output is buffered in %s"
msgstr ""

#: ../../aslint/check_files.py:312 ../../hgaster/maintenance.py:351
#, python-format
msgid "no such file: %s"
msgstr ""

#: ../../aslint/check_files.py:315
msgid "results of the testcases run"
msgstr ""

#: ../../aslint/check_files.py:320 ../../aslint/check_files.py:366
#, python-format
msgid "%6d testcases failed"
msgstr ""

#: ../../aslint/check_files.py:322
#, python-format
msgid "can not run testcases: %s"
msgstr ""

#: ../../aslint/check_files.py:326
msgid "working directory cleaned"
msgstr ""

#: ../../aslint/check_files.py:328
msgid "testcases run successfully"
msgstr ""

#: ../../aslint/check_files.py:330
#, python-format
msgid "as_run exited with code %s"
msgstr ""

#: ../../aslint/check_files.py:331
#, python-format
msgid ""
"detailed output and results of the testcases execution can be found in: %s"
msgstr ""

#: ../../aslint/check_files.py:346
#, python-format
msgid "giving a second chance to %d testcases"
msgstr ""

#: ../../aslint/check_files.py:360
msgid "results of the testcases run after the second run"
msgstr ""

#: ../../aslint/check_files.py:384
#, python-format
msgid "export parameters: %s"
msgstr ""

#: ../../aslint/check_files.py:395
msgid "build the testcases dependencies"
msgstr ""

#: ../../aslint/check_files.py:404
#, python-format
msgid "the file seems not used by any testcase: %s"
msgstr ""

#: ../../aslint/check_files.py:415
msgid "can not build the list of testcases"
msgstr ""

#: ../../aslint/check_files.py:427 ../../hgaster/maintenance.py:358
msgid "build the list of testcases"
msgstr ""

#: ../../aslint/check_files.py:443
#, python-brace-format
msgid "filters used: {0}"
msgstr ""

#: ../../aslint/check_files.py:486
#, python-format
msgid "unexpected filenames for a testcase: %s"
msgstr ""

#: ../../aslint/check_files.py:547
msgid "No testcases directory found!"
msgstr ""

#: ../../aslint/check_files.py:588
#, python-brace-format
msgid "Changed export files written into {0}"
msgstr ""

#: ../../aslint/test_result.py:187
msgid "can not create file {}"
msgstr ""

#: ../../aslint/test_result.py:212
#, python-format
msgid "The %d longest testcases are:"
msgstr ""

#: ../../aslint/test_result.py:233
msgid "{:4d} tests"
msgstr ""

#: ../../aslint/test_result.py:234
msgid "{:4d} errors"
msgstr ""

#: ../../aslint/base_checkers.py:680
msgid "report summary:"
msgstr ""

#: ../../aslint/base_checkers.py:697
#, python-brace-format
msgid "summary: {0:d} errors, {1:d} warnings in {2:d} files/directories"
msgstr ""

#: ../../aslint/base_checkers.py:727
msgid ""
"following commands should help you to fix some errors (try to copy+paste "
"these commands in your terminal):"
msgstr ""

#: ../../aslint/base_checkers.py:736
#, python-brace-format
msgid "# for {0}:"
msgstr ""

#: ../../aslint/base_checkers.py:739
msgid "end of commands"
msgstr ""

#: ../../aslint/base_checkers.py:757
msgid "for integration"
msgstr ""

#: ../../aslint/base_checkers.py:761
#, python-format
msgid "in repository '%s'"
msgstr ""

#: ../../aslint/base_checkers.py:765
#, python-format
msgid "for '%s'"
msgstr ""

#: ../../aslint/base_checkers.py:767
msgid "audit report"
msgstr ""

#: ../../aslint/test/check_test.py:34
msgid "add checking of testcases filenames"
msgstr ""

#: ../../project/responsability.py:93
#, python-brace-format
msgid "fix {0} by {2}, found in {1}"
msgstr ""

#: ../../project/responsability.py:105
#, python-brace-format
msgid "unknown address in rex: {0} found in {1}"
msgstr ""

#: ../../project/responsability.py:178
#, python-brace-format
msgid "use similar username: {0} (doc) vs {1} (rex)"
msgstr ""

#: ../../project/responsability.py:183
#, python-brace-format
msgid "unknown username in rex: {0} for {1} ({2})"
msgstr ""

#: ../../project/responsability.py:192
#, python-brace-format
msgid "source and doc mismatch: {0} ({1}, {2}): {3} vs {4}"
msgstr ""

#: ../../project/responsability.py:283
#, python-format
msgid ""
"connection failed (url: %(url)s):\n"
"%(exc)s"
msgstr ""

#: ../../project/responsability.py:285
msgid "interrupted because of previous errors"
msgstr ""

#: ../../api_roundup/histor.py:410
#, python-brace-format
msgid "Unknown histor format : {0}"
msgstr ""

#: ../../api_roundup/rex.py:146
#, python-brace-format
msgid "the status of issue #{0}, solved by {1}, is {2}, not {3}"
msgstr ""

#: ../../api_roundup/rex.py:149
#, python-brace-format
msgid "the status of issue #{0}, solved by {1}, is {2}"
msgstr ""

#: ../../api_roundup/rex.py:203
#, python-brace-format
msgid "{0} should not be solved in V{1}"
msgstr ""

#: ../../api_roundup/rex.py:220
#, python-brace-format
msgid "closing {0}"
msgstr ""

#: ../../api_roundup/rex.py:222
#, python-brace-format
msgid "{0} must be solved in V{1}"
msgstr ""

#: ../../api_roundup/rex.py:244
#, python-brace-format
msgid "{0}: status changed"
msgstr ""

#: ../../api_roundup/rex.py:246
#, python-brace-format
msgid "{2}: expecting status '{0}', not '{1}': unchanged"
msgstr ""

#: ../../api_roundup/rex.py:279
msgid "message {}:"
msgstr ""

#: ../../api_roundup/rex.py:365
msgid "exporting data as csv..."
msgstr ""

#: ../../api_roundup/rex.py:368
msgid "{:10} bytes downloaded"
msgstr ""

#: ../../api_roundup/stats.py:173
#, python-brace-format
msgid "Ignore line #{0}: {1}"
msgstr ""

#: ../../api_roundup/stats.py:181
#, python-brace-format
msgid ""
"Error line #{0}:\n"
" keys: {1}\n"
" values: {2}"
msgstr ""

#: ../../api_roundup/stats.py:183
#, python-brace-format
msgid "{0:4} lines read"
msgstr ""

#: ../../api_roundup/stats.py:203
#, python-brace-format
msgid "{1:6} users read from {0}"
msgstr ""

#: ../../api_roundup/stats.py:210
#, python-brace-format
msgid "reading user {0}..."
msgstr ""

#: ../../api_roundup/stats.py:234
#, python-brace-format
msgid "data written into {0}"
msgstr ""

#: ../../api_roundup/stats.py:240
#, python-brace-format
msgid "{1:6} issues read from {0}"
msgstr ""

#: ../../api_bitbucket/__init__.py:60
#, python-format
msgid ""
"connection to bitbucket failed (url: %(url)s):\n"
"%(exc)s"
msgstr ""

#: ../../api_bitbucket/__init__.py:104
#, python-format
msgid "getting Bitbucket issue #%s"
msgstr ""

#: ../../hgaster/ext_utils.py:88
#, python-format
msgid "checking for updates for repository '%s'"
msgstr ""

#: ../../hgaster/ext_utils.py:139
#, python-format
msgid "%d files added"
msgstr ""

#: ../../hgaster/ext_utils.py:140
msgid "added files:"
msgstr ""

#: ../../hgaster/ext_utils.py:143
#, python-format
msgid "%d files changed"
msgstr ""

#: ../../hgaster/ext_utils.py:144
msgid "changed files:"
msgstr ""

#: ../../hgaster/ext_utils.py:147
#, python-format
msgid "%d files removed"
msgstr ""

#: ../../hgaster/ext_utils.py:148
msgid "removed files:"
msgstr ""

#: ../../hgaster/ext_utils.py:151
#, python-format
msgid "%d files missing"
msgstr ""

#: ../../hgaster/ext_utils.py:152
msgid "missing files:"
msgstr ""

#: ../../hgaster/ext_utils.py:155
#, python-format
msgid "%d files not tracked"
msgstr ""

#: ../../hgaster/ext_utils.py:156
msgid "unknown files:"
msgstr ""

#: ../../hgaster/ext_utils.py:170
#, python-format
msgid "'hg diff' exists with code %s"
msgstr ""

#: ../../hgaster/ext_utils.py:317
#, python-format
msgid "mail recipients: %r"
msgstr ""

#: ../../hgaster/ext_utils.py:331
#, python-format
msgid ""
"no destination address, %s is neither a valid address nor defined in the "
"[aster] section"
msgstr ""

#: ../../hgaster/ext_utils.py:338
#, python-format
msgid "sending email to %s"
msgstr ""

#: ../../hgaster/ext_utils.py:340
msgid "(DRY RUN) not sent"
msgstr ""

#: ../../hgaster/ext_utils.py:365
#, python-format
msgid "sent from %s"
msgstr ""

#: ../../hgaster/ext_utils.py:374
#, python-format
msgid ""
"can not send the email:\n"
"Exception: %s"
msgstr ""

#: ../../hgaster/ext_utils.py:375
msgid "this happens on some machines."
msgstr ""

#: ../../hgaster/ext_utils.py:376
msgid "trying using the shell command..."
msgstr ""

#: ../../hgaster/ext_utils.py:393
msgid "mail sent using the shell command"
msgstr ""

#: ../../hgaster/ext_utils.py:396
#, python-format
msgid ""
"can not send the email using mailutils:\n"
"Exception: %s"
msgstr ""

#: ../../hgaster/ext_utils.py:419
msgid "invalid author field: {}"
msgstr ""

#: ../../hgaster/request_queue.py:114
msgid "this is not the right repository"
msgstr ""

#: ../../hgaster/request_queue.py:146
#, python-format
msgid "invalid line: '%s'"
msgstr ""

#: ../../hgaster/request_queue.py:198
#, python-format
msgid "try #%d..."
msgstr ""

#: ../../hgaster/request_queue.py:210
#, python-brace-format
msgid ""
"commit in the pull-request repository failed with message:\n"
"{0}: {1}"
msgstr ""

#: ../../hgaster/request_queue.py:217
#, python-brace-format
msgid "failed after {0:d} attempts: {1}"
msgstr ""

#: ../../hgaster/request_queue.py:219
#, python-brace-format
msgid "failed: {0}"
msgstr ""

#: ../../hgaster/request_queue.py:235
#, python-format
msgid "are there new changesets: %s"
msgstr ""

#: ../../hgaster/request_queue.py:310
#, python-brace-format
msgid "can not add request: {0}"
msgstr ""

#: ../../hgaster/request_queue.py:320
msgid "status {0!r} is expected"
msgstr ""

#: ../../hgaster/request_queue.py:340
#, python-brace-format
msgid "submit request for {0} ({1}/{2}) {3}"
msgstr ""

#: ../../hgaster/request_queue.py:344
#, python-brace-format
msgid "request submitted for {0} in '{1}', branch {2}"
msgstr ""

#: ../../hgaster/request_queue.py:353
#, python-format
msgid "integration already started for %s"
msgstr ""

#: ../../hgaster/request_queue.py:357
#, python-brace-format
msgid "start integration for {0} ({1}/{2}) {3}"
msgstr ""

#: ../../hgaster/request_queue.py:360
#, python-brace-format
msgid "integration started for {0} in '{1}', branch {2}"
msgstr ""

#: ../../hgaster/request_queue.py:369
#, python-format
msgid "integration already stopped for %s"
msgstr ""

#: ../../hgaster/request_queue.py:372
#, python-brace-format
msgid "stop integration for {0} ({1}/{2}) {3}"
msgstr ""

#: ../../hgaster/request_queue.py:376
#, python-brace-format
msgid "integration stopped for {0} in '{1}', branch {2}"
msgstr ""

#: ../../hgaster/request_queue.py:385
#, python-brace-format
msgid "cancel request for {0} ({1}/{2}) {3}"
msgstr ""

#: ../../hgaster/request_queue.py:390
#, python-brace-format
msgid "request cancelled for {0} ({1}/{2}) {3}"
msgstr ""

#: ../../hgaster/request_queue.py:406
#, python-format
msgid "as %s"
msgstr ""

#: ../../hgaster/request_queue.py:408
#, python-format
msgid "of %s"
msgstr ""

#: ../../hgaster/request_queue.py:412 ../../hgaster/request_queue.py:416
#, python-brace-format
msgid "mark revision to test for {0} ({1}/{2}) {3}"
msgstr ""

#: ../../hgaster/request_queue.py:425 ../../hgaster/request_queue.py:429
#, python-brace-format
msgid "revision integrated for {0} ({1}/{2}) {3}"
msgstr ""

#: ../../hgaster/request_queue.py:438 ../../hgaster/request_queue.py:443
#, python-brace-format
msgid "revision refused for {0} ({1}/{2}) {3}"
msgstr ""

#: ../../hgaster/bisect_engine.py:32
msgid "run configure at the revision"
msgstr ""

#: ../../hgaster/bisect_engine.py:47
msgid "build at the revision"
msgstr ""

#: ../../hgaster/bisect_engine.py:76
msgid "applying the patch..."
msgstr ""

#: ../../hgaster/bisect_engine.py:81
msgid "run the testcase"
msgstr ""

#: ../../hgaster/bisect_engine.py:91
#, python-format
msgid "testcase output: %s"
msgstr ""

#: ../../hgaster/bisect_engine.py:94
msgid "calling hook..."
msgstr ""

#: ../../hgaster/bisect_engine.py:99
msgid "patch reverted."
msgstr ""

#: ../../hgaster/bisect_engine.py:118
msgid ""
"`hg bisect` command failed. Usually this means that there are uncommitted "
"changes. If you have to patch the source code, consider the `--patch` option."
msgstr ""

#: ../../hgaster/bisect_engine.py:129
#, python-brace-format
msgid "this revision is {0}"
msgstr ""

#: ../../hgaster/bisect_engine.py:151
#, python-brace-format
msgid "run at revision {0} ({1:d}/{2:d})"
msgstr ""

#: ../../hgaster/bisect_engine.py:156
#, python-format
msgid "testcase ended %s"
msgstr ""

#: ../../hgaster/bisect_engine.py:157
msgid "successfully"
msgstr ""

#: ../../hgaster/bisect_engine.py:157
msgid "with errors"
msgstr ""

#: ../../hgaster/bisect_engine.py:163
msgid ""
"you should remove the temporary directories if you do not need them: rm -rf /"
"tmp/runtest_*"
msgstr ""

#: ../../hgaster/ascommands.py:44
#, python-format
msgid ""
"\n"
"The developer (username=%(dvp)s, address=%(address)s) submitted a\n"
"request for integration of the revision %(wrkrev)s in the Code_Aster "
"repository.\n"
"\n"
"You are registered as the person in charge of the following files.\n"
"\n"
"If you don't agree with these changes, you must warn the integrators and "
"the\n"
"developer not to integrate them as soon as possible.\n"
"\n"
"URL of the submitted changeset: %(url)s/rev/%(wrkrev)s\n"
"\n"
"Note that its ancestors are also part of the changes.\n"
"\n"
"If you want to rebuild this entire diff file in your local repository, you "
"have\n"
"to pull this revision and type:\n"
"\n"
"    hg pull --rev %(wrkrev)s %(url)s\n"
"    hg diff --rev %(refrev)s --rev %(wrkrev)s\n"
msgstr ""

#: ../../hgaster/ascommands.py:64
#, python-format
msgid ""
"\n"
"Important messages of the submission:\n"
"\n"
"%(msg)s\n"
"\n"
"Full log content was written to: %(logf)s\n"
msgstr ""

#: ../../hgaster/ascommands.py:94 ../../hgaster/maintenance.py:48
msgid "command interrupted because of errors"
msgstr ""

#: ../../hgaster/ascommands.py:95
msgid "do you want to continue (y/n)? "
msgstr ""

#: ../../hgaster/ascommands.py:96
msgid "command cancelled"
msgstr ""

#: ../../hgaster/ascommands.py:145
msgid "skip confirmation"
msgstr ""

#: ../../hgaster/ascommands.py:165
#, python-brace-format
msgid "not in a Code_Aster repository: {0}"
msgstr ""

#: ../../hgaster/ascommands.py:169
msgid "'--rev' must only be used with '--local'"
msgstr ""

#: ../../hgaster/ascommands.py:176
#, python-brace-format
msgid "integration is not allowed in the branch '{0}'"
msgstr ""

#: ../../hgaster/ascommands.py:179
msgid ""
"without remote connection (--local enabled), you must provide the last "
"ancestor revision already integrated using --rev"
msgstr ""

#: ../../hgaster/ascommands.py:185
msgid "The submitted revision must be a descendant of the reference head."
msgstr ""

#: ../../hgaster/ascommands.py:188
msgid "You are submitting the reference revision!"
msgstr ""

#: ../../hgaster/ascommands.py:194
#, python-brace-format
msgid "source repository not found: '{0}'"
msgstr ""

#: ../../hgaster/ascommands.py:221
msgid "the submitted revision has children"
msgstr ""

#: ../../hgaster/ascommands.py:227
msgid "there are uncommitted changes. Please commit or revert first."
msgstr ""

#: ../../hgaster/ascommands.py:230
msgid "uncommitted changes, skipped in TEST mode"
msgstr ""

#: ../../hgaster/ascommands.py:232
msgid "there are untracked files"
msgstr ""

#: ../../hgaster/ascommands.py:244 ../../hgaster/ascommands.py:472
msgid "skipping check for updates"
msgstr ""

#: ../../hgaster/ascommands.py:256
#, python-brace-format
msgid ""
"run 'hg update {0}' to update the working directory to the revision to "
"integrate"
msgstr ""

#: ../../hgaster/ascommands.py:258
#, python-brace-format
msgid "getting changes between revisions '{0}' and '{1}'"
msgstr ""

#: ../../hgaster/ascommands.py:262
#, python-brace-format
msgid "'{0}' must be a descendant of '{1}'"
msgstr ""

#: ../../hgaster/ascommands.py:267
msgid ""
"there are missing files (deleted by non-hg command, but still tracked, check "
"with 'hg status')"
msgstr ""

#: ../../hgaster/ascommands.py:283
#, python-brace-format
msgid "checking descriptions between revisions '{0}' and '{1}'"
msgstr ""

#: ../../hgaster/ascommands.py:287
msgid "there is no revision to submit"
msgstr ""

#: ../../hgaster/ascommands.py:296
msgid "report limited to the added files"
msgstr ""

#: ../../hgaster/ascommands.py:324
#, python-brace-format
msgid "getting solved issues between '{0}' and '{1}'"
msgstr ""

#: ../../hgaster/ascommands.py:328
msgid "status of issues can not be checked in --local mode"
msgstr ""

#: ../../hgaster/ascommands.py:341
#, python-format
msgid "ensure that the issue #%d is solved on bitbucket"
msgstr ""

#: ../../hgaster/ascommands.py:354
msgid "check that expected documents have been committed"
msgstr ""

#: ../../hgaster/ascommands.py:359
#, python-brace-format
msgid "no changes needed for #{0}"
msgstr ""

#: ../../hgaster/ascommands.py:366
#, python-brace-format
msgid "issue #{0} needs changes in {1}: {2}"
msgstr ""

#: ../../hgaster/ascommands.py:368
msgid "found"
msgstr ""

#: ../../hgaster/ascommands.py:371
msgid "issue number not found in document history"
msgstr ""

#: ../../hgaster/ascommands.py:374
msgid ""
"\n"
"You have to mention the issue id with '[#nnnn]' when you are committing a "
"new revision of a document. If you mispelled the issue number just add a "
"minor change (add a line feed for example). Note that you have to wait at "
"least 10 minutes for the last changes are synced.\n"
"Please do not continue without contacting the administrators."
msgstr ""

#: ../../hgaster/ascommands.py:395
#, python-format
msgid "pushing the revision %s to the remote server"
msgstr ""

#: ../../hgaster/ascommands.py:401 ../../hgaster/maintenance.py:269
msgid "push ended successfully"
msgstr ""

#: ../../hgaster/ascommands.py:403
#, python-format
msgid "hg push exits with code %s"
msgstr ""

#: ../../hgaster/ascommands.py:404 ../../hgaster/maintenance.py:271
msgid "no revision pushed, maybe already there"
msgstr ""

#: ../../hgaster/ascommands.py:405
msgid "Please force the push by using:"
msgstr ""

#: ../../hgaster/ascommands.py:408
msgid "the revision is not push to the remote server"
msgstr ""

#: ../../hgaster/ascommands.py:416
#, python-format
msgid ""
"incorrect configuration:\n"
"%s"
msgstr ""

#: ../../hgaster/ascommands.py:418
msgid "running is TEST mode"
msgstr ""

#: ../../hgaster/ascommands.py:427
msgid "can not find the last integrated revision"
msgstr ""

#: ../../hgaster/ascommands.py:458
msgid "you can execute:"
msgstr ""

#: ../../hgaster/ascommands.py:463
#, python-brace-format
msgid "'{0}' must be updated to the last revision of branch '{1}'"
msgstr ""

#: ../../hgaster/ascommands.py:491
msgid ""
"warnings have been emitted during check, try to fix most of them before "
"continuing"
msgstr ""

#: ../../hgaster/ascommands.py:495
msgid "Errors have been emitted during check, you must fix them."
msgstr ""

#: ../../hgaster/ascommands.py:504
msgid "checking the diff of files"
msgstr ""

#: ../../hgaster/ascommands.py:516
#, python-brace-format
msgid "extracting the changes of {0} files ('.' represents {1} files)"
msgstr ""

#: ../../hgaster/ascommands.py:519
#, python-brace-format
msgid "extracting the changes of {0} files "
msgstr ""

#: ../../hgaster/ascommands.py:541
msgid "building the binaries using waf"
msgstr ""

#: ../../hgaster/ascommands.py:544
msgid ""
"a sequential version is necessary; use 'waf.builddir' to select another "
"build."
msgstr ""

#: ../../hgaster/ascommands.py:551
msgid ""
"source and installation directories seem to be on different filesystems."
msgstr ""

#: ../../hgaster/ascommands.py:568
#, python-brace-format
msgid ""
"no such file: '{0}', please run '{1} configure'. If you are using a specific "
"version you can adjust the 'waf.builddir' parameter in the [aster] section "
"of your ~/.hgrc file."
msgstr ""

#: ../../hgaster/ascommands.py:591
msgid "submission interrupted because of errors"
msgstr ""

#: ../../hgaster/ascommands.py:592
msgid "submission cancelled"
msgstr ""

#: ../../hgaster/ascommands.py:603 ../../hgaster/ascommands.py:1085
msgid "no argument expected"
msgstr ""

#: ../../hgaster/ascommands.py:609
msgid ""
"no request for integration will be submitted, only check the source files"
msgstr ""

#: ../../hgaster/ascommands.py:613
msgid ""
"you must fill the 'username' parameter in the [ui] section of your ~/.hgrc "
"file"
msgstr ""

#: ../../hgaster/ascommands.py:620
#, python-format
msgid "you have already submitted a request for integration of the revision %s"
msgstr ""

#: ../../hgaster/ascommands.py:623
msgid ""
"the integration is started, you must contact the administrators to cancel it"
msgstr ""

#: ../../hgaster/ascommands.py:626
msgid ""
"if you continue, the previously submitted revisions won't be integrated."
msgstr ""

#: ../../hgaster/ascommands.py:633
msgid "(DRY RUN) cancelling the submission"
msgstr ""

#: ../../hgaster/ascommands.py:636
#, python-format
msgid "this revision has already been submitted by another developer (%s)"
msgstr ""

#: ../../hgaster/ascommands.py:651
msgid "PLEASE FORWARD THIS MESSAGE TO AN ADMINISTRATOR"
msgstr ""

#: ../../hgaster/ascommands.py:654
#, python-format
msgid "(DRY RUN) submit request for %s"
msgstr ""

#: ../../hgaster/ascommands.py:657
msgid "your request for integration has not been submitted"
msgstr ""

#: ../../hgaster/ascommands.py:665 ../../hgaster/ascommands.py:977
msgid "do not send notification to persons in charge"
msgstr ""

#: ../../hgaster/ascommands.py:672
msgid "no person in charge of the changed modules"
msgstr ""

#: ../../hgaster/ascommands.py:674
#, python-format
msgid "%d source files have a person in charge"
msgstr ""

#: ../../hgaster/ascommands.py:701
msgid "The entire diff is too long. It has been truncated."
msgstr ""

#: ../../hgaster/ascommands.py:704
#, python-brace-format
msgid "invalid address '{0}' in files: {1}"
msgstr ""

#: ../../hgaster/ascommands.py:708
#, python-brace-format
msgid "no mail sent for the files which you are the person in charge: {0}"
msgstr ""

#: ../../hgaster/ascommands.py:717
msgid "List of changed files:"
msgstr ""

#: ../../hgaster/ascommands.py:739
#, python-brace-format
msgid "Executing: {0}"
msgstr ""

#: ../../hgaster/ascommands.py:743
#, python-brace-format
msgid "'{0}' ended successfully"
msgstr ""

#: ../../hgaster/ascommands.py:745
#, python-brace-format
msgid ""
"'{0}' failed.\n"
"Command line: {1}"
msgstr ""

#: ../../hgaster/ascommands.py:753
msgid "checking for automatic merge"
msgstr ""

#: ../../hgaster/ascommands.py:759
msgid "checking for source files"
msgstr ""

#: ../../hgaster/ascommands.py:778
msgid "only check files has been done"
msgstr ""

#: ../../hgaster/ascommands.py:781
msgid "the integration may be rejected because of previous alarms"
msgstr ""

#: ../../hgaster/ascommands.py:784
msgid "the integration should be accepted"
msgstr ""

#: ../../hgaster/ascommands.py:787
#, python-brace-format
msgid "[submit@{{host}}] request for integration submitted ({0}/{1}) {2}"
msgstr ""

#: ../../hgaster/ascommands.py:801 ../../hgaster/ascommands.py:873
#, python-brace-format
msgid "asking integration in branch '{0}' of the repository '{1}'"
msgstr ""

#: ../../hgaster/ascommands.py:803 ../../hgaster/ascommands.py:875
#, python-format
msgid "reference revision is %s"
msgstr ""

#: ../../hgaster/ascommands.py:862
msgid ""
"Your request should be refused, but if you think that it will be accepted "
"anyway, you can continue."
msgstr ""

#: ../../hgaster/ascommands.py:877
msgid ""
"please check that the current working directory of 'src' is consistent with "
"this repository because it will be used to run testcases"
msgstr ""

#: ../../hgaster/ascommands.py:934
msgid "check build of html documentation"
msgstr ""

#: ../../hgaster/ascommands.py:937
msgid "running testcases"
msgstr ""

#: ../../hgaster/ascommands.py:988 ../../hgaster/ascommands.py:1037
msgid "check coverage"
msgstr ""

#: ../../hgaster/ascommands.py:1068
msgid "integration process interrupted because of errors"
msgstr ""

#: ../../hgaster/ascommands.py:1069
msgid "integration cancelled"
msgstr ""

#: ../../hgaster/ascommands.py:1082
msgid "exactly one argument is expected"
msgstr ""

#: ../../hgaster/ascommands.py:1089
#, python-format
msgid "unsupported action: '%s' (one of --status, --check... is required)"
msgstr ""

#: ../../hgaster/ascommands.py:1106 ../../hgaster/ascommands.py:1234
#, python-format
msgid "no request for this revision '%s'"
msgstr ""

#: ../../hgaster/ascommands.py:1114
#, python-format
msgid "message written into: %s"
msgstr ""

#: ../../hgaster/ascommands.py:1139
#, python-format
msgid "pulling the revision %s from the remote server"
msgstr ""

#: ../../hgaster/ascommands.py:1142
msgid "pull ended successfully"
msgstr ""

#: ../../hgaster/ascommands.py:1144
#, python-format
msgid "hg pull exits with code %s"
msgstr ""

#: ../../hgaster/ascommands.py:1145
msgid "You must pull the revision manually."
msgstr ""

#: ../../hgaster/ascommands.py:1147
msgid "next steps: hg rebase ..., hg up ..., pull_request --check"
msgstr ""

#: ../../hgaster/ascommands.py:1153
#, python-format
msgid "starting request for %s..."
msgstr ""

#: ../../hgaster/ascommands.py:1174
msgid "next step: pull_request --test or --testall ..."
msgstr ""

#: ../../hgaster/ascommands.py:1200
#, python-format
msgid "closing request for %s..."
msgstr ""

#: ../../hgaster/ascommands.py:1213
#, python-brace-format
msgid "the revision {0} will become the new reference head of {1}/{2}"
msgstr ""

#: ../../hgaster/ascommands.py:1218
msgid "next step: run testcases"
msgstr ""

#: ../../hgaster/ascommands.py:1224
#, python-format
msgid "mark %s as TEST..."
msgstr ""

#: ../../hgaster/ascommands.py:1229
msgid "status of the requests for integration:"
msgstr ""

#: ../../hgaster/ascommands.py:1241
msgid "no request"
msgstr ""

#: ../../hgaster/ascommands.py:1254
#, python-format
msgid "checking for updates for the repository '%s'"
msgstr ""

#: ../../hgaster/ascommands.py:1259
msgid ""
"there are available updates, please pull & update in this repository before "
"asking for the integration"
msgstr ""

#: ../../hgaster/ascommands.py:1261
#, python-format
msgid "you should run: %s"
msgstr ""

#: ../../hgaster/ascommands.py:1265
msgid ""
"then you must update your development onto the last 'reference' revision by "
"using rebase or merge"
msgstr ""

#: ../../hgaster/ascommands.py:1268
#, python-brace-format
msgid "can not check for updates for the repository '{0}' (options: {1})"
msgstr ""

#: ../../hgaster/maintenance.py:36
#, python-format
msgid ""
"\n"
"Important messages:\n"
"\n"
"%(msg)s\n"
"\n"
"Full log content was written to: %(logf)s\n"
msgstr ""

#: ../../hgaster/maintenance.py:225
msgid "only administrators are authorized to perform this operation"
msgstr ""

#: ../../hgaster/maintenance.py:232
#, python-brace-format
msgid "update repository '{0}' to '{1}'"
msgstr ""

#: ../../hgaster/maintenance.py:235 ../../hgaster/maintenance.py:260
#: ../../hgaster/maintenance.py:473
#, python-format
msgid "not a repository: %s"
msgstr ""

#: ../../hgaster/maintenance.py:257
#, python-brace-format
msgid "push the revision '{0}' of the repository '{1}'"
msgstr ""

#: ../../hgaster/maintenance.py:292
#, python-format
msgid "can not get the reference revision of '%s'"
msgstr ""

#: ../../hgaster/maintenance.py:303
msgid "set revisions to test"
msgstr ""

#: ../../hgaster/maintenance.py:325
#, python-brace-format
msgid ""
"this version must be configured first, not found: {0}\n"
"Maybe you need to use --root/--builddir options.\n"
msgstr ""

#: ../../hgaster/maintenance.py:333
#, python-format
msgid "version directory is '%s'"
msgstr ""

#: ../../hgaster/maintenance.py:349
#, python-format
msgid "list of testcases is read from the file '%s'"
msgstr ""

#: ../../hgaster/maintenance.py:355
msgid "'src' is deprecated, please use 'verification' instead."
msgstr ""

#: ../../hgaster/maintenance.py:403
#, python-brace-format
msgid ""
"Exclude europlexus testcases:\n"
"{0:6d} testcases in the list"
msgstr ""

#: ../../hgaster/maintenance.py:447
#, python-brace-format
msgid "{0} will be removed."
msgstr ""

#: ../../hgaster/maintenance.py:448
msgid "do you want to continue (y/n) ?"
msgstr ""

#: ../../hgaster/maintenance.py:450
msgid "interrupt by user"
msgstr ""

#: ../../hgaster/maintenance.py:455
#, python-format
msgid "current revision: %s"
msgstr ""

#: ../../hgaster/maintenance.py:477
#, python-format
msgid "add/remove files in %s"
msgstr ""

#: ../../hgaster/maintenance.py:479
#, python-format
msgid "failure during add/remove files in %s"
msgstr ""

#: ../../hgaster/maintenance.py:488
msgid "new results committed successfully"
msgstr ""

#: ../../hgaster/maintenance.py:490
#, python-format
msgid "failure during committing files in %s"
msgstr ""

#: ../../hgaster/maintenance.py:498
msgid "update repositories"
msgstr ""

#: ../../hgaster/maintenance.py:510
msgid "update remote repositories"
msgstr ""

#: ../../hgaster/maintenance.py:534
msgid "--options=--prefix=XXX is required to install the testcases"
msgstr ""

#: ../../hgaster/maintenance.py:551
msgid "remove the request for integration from the queue"
msgstr ""

#: ../../hgaster/maintenance.py:600
#, python-brace-format
msgid "[{host}] update interrupted because of errors"
msgstr ""

#: ../../hgaster/maintenance.py:602
#, python-brace-format
msgid "[{host}] {date} installation ended"
msgstr ""

#: ../../hgaster/maintenance.py:624
#, python-format
msgid "last installed revisions: %s"
msgstr ""

#: ../../hgaster/maintenance.py:627
msgid "these revisions have already been installed, exit"
msgstr ""

#: ../../hgaster/maintenance.py:640
#, python-brace-format
msgid "write installed revisions in {0}: {1}"
msgstr ""

#: ../../hgaster/maintenance.py:651
#, python-format
msgid ""
"can not install dictionnaries\n"
"Message:\n"
"%s"
msgstr ""

#: ../../hgaster/maintenance.py:667
msgid "update devtools repository"
msgstr ""

#: ../../hgaster/maintenance.py:684
#, python-brace-format
msgid "[{host}] validation interrupted because of errors"
msgstr ""

#: ../../hgaster/maintenance.py:686
#, python-brace-format
msgid "[{host}] {date} validation of revisions ended"
msgstr ""

#: ../../hgaster/maintenance.py:709
#, python-format
msgid "%4d ancestor revisions will be tested in case of failure"
msgstr ""

#: ../../hgaster/maintenance.py:777
msgid "testcases failed"
msgstr ""

#: ../../hgaster/maintenance.py:795
msgid "run validation testcases in syntax mode only"
msgstr ""

#: ../../hgaster/maintenance.py:823
#, python-brace-format
msgid "[{host}] test run interrupted because of errors"
msgstr ""

#: ../../hgaster/maintenance.py:825
#, python-brace-format
msgid "[{host}] {date} testcases ended"
msgstr ""

#: ../../hgaster/maintenance.py:901
msgid "'--mpi' option is deprecated, MPI version is automatically detected."
msgstr ""

#: ../../hgaster/maintenance.py:912
msgid "--resutest option is required"
msgstr ""

#: ../../hgaster/maintenance.py:947
msgid "can not get reference head, use current revision"
msgstr ""
