# coding=utf-8

"""Checkers for datg files"""

from aslint.base_checkers import (
    TextMsg, TextMsgNotFound, FilenameCat, FileContentCat,
    checkers_from_context, search_msg,
)
import aslint.common_checkers as COMM


class DatgFilename(FilenameCat, TextMsgNotFound):

    """Unexpected filename
    Only this suffix are allowed: '.datg'"""
    id = "W5001"
    fmt = "%(id)s: %(label)s"
    search = search_msg("(\.datg)$")


class LicenseNotFound(FileContentCat, COMM.LicenseNotFound):

    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org"""
    id = "C5007"


class EDFCopyright(FileContentCat, COMM.EDFCopyright):

    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and/or version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2013 EDF R&D www.code-aster.org
    """
    id = "C5008"


class ExternalCopyright(FileContentCat, COMM.ExternalCopyright):

    """Another Copyright than EDF"""
    id = "W5009"

CHECK_LIST = checkers_from_context(globals(), TextMsg)
