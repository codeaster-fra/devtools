# coding=utf-8

"""Checkers for C source files"""

import aslint.common_checkers as COMM
from aslint.base_checkers import (
    CheckContext,
    FileContentCat,
    FilenameCat,
    TextMsg,
    checkers_from_context,
    search_msg,
)


class LineTooLong(FileContentCat, COMM.LineTooLong):

    """Line too long
    Lines must not be too long for readability and convention.
    Maximum line length is 100 columns in C."""

    id = "C3001"
    _maxlength = 100


class LicenseNotFound(FileContentCat, COMM.LicenseNotFound):

    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org"""

    id = "C3002"


class EDFCopyright(FileContentCat, COMM.EDFCopyright):

    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and/or version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2023 EDF R&D www.code-aster.org
    """

    id = "C3003"


class ExternalCopyright(FileContentCat, COMM.ExternalCopyright):

    """Another Copyright than EDF"""

    id = "W3004"


class InvalidCharacter(FileContentCat, COMM.InvalidCharacter):
    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""
    id = "C3005"


class UniqueIncludePython(FileContentCat, TextMsg):

    """Include "astercxx.h" or "aster.h" instead of "Python.h" directly
    Include "astercxx.h" or "aster.h" instead of directly including "Python.h"
    to avoid unconsistent configuration.
    """

    id = "C3401"
    search = search_msg("^ *(?P<main># *include +['\"]Python\.h['\"])")


class ReformatSource(FilenameCat, COMM.ReformatC):
    """Reformat C/C++ source"""

    id = "C3522"


CHECK_LIST = checkers_from_context(globals(), TextMsg)
