# coding=utf-8
"""Checkers for CXX source files"""

import os.path as osp
import re

import aslint.common_checkers as COMM
from aslint.base_checkers import (FileContentCat, FilenameCat, GenericMsg,
                                  TextMsg, checkers_from_context, search_msg)


class MissingConstructor(FilenameCat, GenericMsg):
    """Missing constructor
    Each class should define a constructor used by users and a constructor
    required at restart that accepts the Jeveux name as argument.
    This checker expects at least two constructors per class. It may be
    unrelevant in some cases.
    """
    id = "C3006"
    fmt = "%(id)s: %(label)s"

    def search(self, filename):
        """Check that at least two constructors exist for each class."""
        error = []
        if not filename.endswith("Interface.cxx") or not osp.isfile(filename):
            return error

        with open(filename, "r") as fobj:
            txt = fobj.read()

        expr1 = re.compile('^ *(|py::)class_', re.M)
        expr2 = re.compile('initFactoryPtr', re.M)
        nbclass = len(expr1.findall(txt))
        nbinit = len(expr2.findall(txt))
        if nbinit < nbclass * 2:
            error.append(": {0} ({1} constructors missing)".format(
                filename, nbclass * 2 - nbinit))
        return error


class UsingNamespace(FileContentCat, TextMsg):
    """Unexpected 'using namespace'
    "using namespace" is considered as a bad practice because it may be
    ambiguous.
    Prefer use for example "namespace py = boost::python;" to create an alias.
    """
    id = 'C3007'
    fmt = "%(id)s: %(label)s: %(main)s"
    search = search_msg("^ *(?P<main>using namespace .*;)")

RE_ASTERCXX = re.compile("#define ASTERCXX_H_")
def _except_astercxx_h(expr):
    """Adapt 'search_msg' by skipping 'astercxx.h'"""
    def _search(self, txt):
        if RE_ASTERCXX.search(txt):
            return []
        return search_msg(expr, ignore_case=False)(txt)
    return _search

class VectorInt(FileContentCat, TextMsg):
    """Use 'VectorInt'
    Please use the typdefs already defined in 'astercxx.h'.
    """
    id = 'C3008'
    fmt = "%(id)s: %(label)s: %(main)s"
    search = _except_astercxx_h("(?P<main>std::vector *< *(ASTERINTEGER4|int) *>)")


class VectorLong(FileContentCat, TextMsg):
    """Use 'VectorLong'
    Please use the typdefs already defined in 'astercxx.h'.
    """
    id = 'C3009'
    fmt = "%(id)s: %(label)s: %(main)s"
    search = _except_astercxx_h("(?P<main>std::vector *< *(ASTERINTEGER|long) *>)")


class VectorReal(FileContentCat, TextMsg):
    """Use 'VectorReal'
    Please use the typdefs already defined in 'astercxx.h'.
    """
    id = 'C3010'
    fmt = "%(id)s: %(label)s: %(main)s"
    search = _except_astercxx_h("(?P<main>std::vector *< *(ASTERDOUBLE|double) *>)")


class VectorComplex(FileContentCat, TextMsg):
    """Use 'VectorComplex'
    Please use the typdefs already defined in 'astercxx.h'.
    """
    id = 'C3011'
    fmt = "%(id)s: %(label)s: %(main)s"
    search = _except_astercxx_h("(?P<main>std::vector *< *ASTERCOMPLEX *>)")


class VectorString(FileContentCat, TextMsg):
    """Use 'VectorString'
    Please use the typdefs already defined in 'astercxx.h'.
    """
    id = 'C3012'
    fmt = "%(id)s: %(label)s: %(main)s"
    search = _except_astercxx_h("(?P<main>std::vector *< *std::string *>)")


class ASTERCOMPLEX(FileContentCat, TextMsg):
    """Use 'ASTERCOMPLEX'
    Please use the typdefs already defined in 'astercxx.h'.
    """
    id = 'C3013'
    fmt = "%(id)s: %(label)s: %(main)s"
    search = _except_astercxx_h("(?P<main>std::complex *< *double *>)")


CHECK_LIST = checkers_from_context(globals(), TextMsg)
