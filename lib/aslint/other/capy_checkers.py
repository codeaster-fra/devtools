# coding=utf-8

"""Checkers for catalo files"""

import aslint.common_checkers as COMM
from aslint.base_checkers import (
    FileContentCat,
    FilenameCat,
    GenericMsg,
    TextMsg,
    checkers_from_context,
)
from aslint.common_checkers import check_encoding
from aslint.config import ASCFG


class LicenseNotFound(FileContentCat, COMM.LicenseNotFound):

    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org
    See `fix_license` tool (use --help option for details)."""

    id = "C6005"
    fixme = ASCFG.get("devtools_root") + "/bin/fix_license %(filenames)s"


class EDFCopyright(FileContentCat, COMM.EDFCopyright):

    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and/or version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2013 EDF R&D www.code-aster.org
    """

    id = "C6006"


class ExternalCopyright(FileContentCat, COMM.ExternalCopyright):

    """Another Copyright than EDF"""

    id = "W6010"


class InvalidCharacter(FileContentCat, COMM.InvalidCharacter):

    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""
    id = "C6007"


class InvalidCharacter(FileContentCat, COMM.InvalidCharacter):

    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""
    id = "C6008"


class ReformatSource(FilenameCat, COMM.ReformatPy):
    """Reformat Python source"""

    id = "C6522"


CHECK_LIST = checkers_from_context(globals(), TextMsg)
