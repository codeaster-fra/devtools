# coding=utf-8

"""Checkers for material data files"""

from aslint.base_checkers import (
    TextMsg, TextMsgNotFound, FilenameCat, FileContentCat,
    checkers_from_context, search_msg,
)
import aslint.common_checkers as COMM


class MaterFilename(FilenameCat, TextMsgNotFound):

    """Unexpected filename
    Only this suffix is allowed: '.mater'"""
    id = "W5002"
    fmt = "%(id)s: %(label)s"
    search = search_msg("(\.NOMI)$")


class EncodingDeclNotFound(FileContentCat, COMM.EncodingDeclNotFound):

    """Encoding definition is required in the first or second line
    Conforming to PEP0263, the encoding of the source file must be defined
    by placing a magic comment as first or second line of the file
    (see http://www.python.org/dev/peps/pep-0263/)."""
    id = "C5003"


class LicenseNotFound(FileContentCat, COMM.LicenseNotFound):

    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org"""
    id = "C5004"


class EDFCopyright(FileContentCat, COMM.EDFCopyright):

    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and/or version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2013 EDF R&D www.code-aster.org
    """
    id = "C5005"


class ExternalCopyright(FileContentCat, COMM.ExternalCopyright):

    """Another Copyright than EDF"""
    id = "W5006"


class InvalidCharacter(FileContentCat, COMM.InvalidCharacter):

    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""
    id = "C5010"

CHECK_LIST = checkers_from_context(globals(), TextMsg)
