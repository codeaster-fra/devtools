# coding=utf-8

"""Checkers for untyped files"""

from aslint.base_checkers import (
    TextMsg, TextMsgNotFound, checkers_from_context, search_msg, FilenameCat
)


class DoNotChange(FilenameCat, TextMsgNotFound):

    """Change not authorized on this file
    Some files can not be changed by developers, contact an administrator if
    you must change one."""
    id = "W9001"
    fmt = "%(id)s: %(label)s"
    search = search_msg("^(\.hgtags)$")

CHECK_LIST = checkers_from_context(globals(), TextMsg)
