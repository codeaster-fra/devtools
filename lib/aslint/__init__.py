# coding=utf-8

"""This package gives the functions to check code_aster source files.

Why the name 'aslint' ?
    It is a short name for Aster + Lint.
    See http://en.wikipedia.org/wiki/Lint_(software) or
    http://en.wikipedia.org/wiki/Pylint
"""
