# coding=utf-8

"""Configuration of the aslint package

It uses the user's configuration file of Git ``~/.gitconfig``.
If it does not exist, the default configuration will be used.

``git config`` does not support dots in keys, use a dash instead.
Example: use ``check-commit`` instead of ``check.commit``.
"""

import os
import os.path as osp
import re
import shutil
import stat
import tempfile
from functools import partial
from getpass import getuser
from subprocess import PIPE, Popen

from repository_api import UI

from aslint.baseutils import get_absolute_dirname, on_calibre
from aslint.string_utils import convert

CODEASTER_ADDR = "code_aster <code-aster@edf.fr>"
ADMIN_ADDR = "retd-codeaster-integr@groups.edf.fr"
TEST_ADDR = "retd-codeaster-integr@groups.edf.fr"
ASTEREDA = "retd-codeaster-info@groups.edf.fr"
ASTERDEV = "retd-codeaster-info@groups.edf.fr"
QUEUE_URI = "https://%(username)s:%(password)s@aster.retd.edf.fr/scm/hg/codeaster_push/requestqueue"
QUEUE_REPOID = "53b8abdf61901695c8681c31e614939c4375d044"
DOCASTER_URI = "https://gitlab.pleiade.edf.fr/api/v4/projects/{project}/repository"

# these environment variables may be set by `env.sh`
FORTRAN_COMPILER = os.environ.get("ASLINT_FORTRAN_COMPILER", "gfortran")

UNCACHED = object()  # for value to cache only if needed


class AsterCfgSection(object):  # pragma pylint: disable=R0902
    """Store the values of the [aster] section and give access to other
    configuration parameters.
    """

    __slots__ = (
        "_ui",
        "_read",
        "_readbool",
        "translation",
        "username",
        "notify_mailhost",
        "notify_port",
        "notify_user",
        "notify_admins",
        "notify_codeaster",
        "notify_eda",
        "notify_devel",
        "notify_resutest",
        "notify_method",
        "waf_bin",
        "waf_env",
        "waf_builddir",
        "waf_prefix",
        "waf_c4che",
        "aslint_root",
        "devtools_root",
        "devtools_cache",
        "admin_queue_uri",
        "admin_queue_repoid",
        "admin_queue_username",
        "admin_queue_password",
        "admin__test",
        "admin_push_uri",
        "admin_pull_uri",
        "admin_intg_uri",
        "admin_ci_uri",
        "hg_bin",
        "asrun_bin",
        "asrun_lib",
        "fortran_form",  # transitional option to choose fixed or free form
        "fortran_compiler",
        "fortran_fcflags",
        "fortran_moddest",
        "fortran_intsize",
        "fortran_realsize",
        "test_nbmaxnook",
        "test_facmtps",
        "rex_url",
        "rex_write_user",
        "rex_write_password",
        "branches",
        "doc_mirror",
        "check_commit",
        "check_submit",
        "check_precommit",
        "check_prepush",
        "docaster_uri",
        "docaster_cafile",
        "clang_format",
    )

    def __init__(self):
        """Initialization"""
        uio = UI.ui()
        _read = partial(uio.config, "aster")
        _readbool = partial(uio.configbool, "aster")

        self._ui = uio
        self._read = _read
        self._readbool = _readbool
        self.translation = _readbool("translation", True)
        self.username = UNCACHED
        self.hg_bin = _read("hg.bin", "hg")
        self.asrun_bin = _read("asrun.bin", "as_run")
        self.asrun_lib = _read("asrun.lib", UNCACHED)
        # one of curl, smtplib, mailutils
        self.notify_method = _read("notify.method", "curl")
        self.notify_mailhost = _read("notify.mailhost", "mailhost.der.edf.fr")
        self.notify_port = _read("notify.port")
        self.notify_user = UNCACHED
        self.notify_admins = ADMIN_ADDR
        self.notify_resutest = TEST_ADDR
        self.notify_codeaster = CODEASTER_ADDR
        self.notify_eda = ASTEREDA
        self.notify_devel = ASTERDEV
        self.waf_bin = _read("waf.bin", "waf")
        self.waf_env = _read("waf.env")
        self.waf_prefix = UNCACHED
        self.waf_builddir = _read("waf.builddir", UNCACHED)
        self.waf_c4che = UNCACHED
        self.check_commit = _read("check-commit", "warn")
        self.check_submit = _read("check-submit", "warn")
        self.check_precommit = _read("check-precommit", "undef")
        self.check_prepush = _read("check-prepush", "undef")
        # installation root of devtools
        self.aslint_root = get_absolute_dirname(__file__)
        self.devtools_root = get_absolute_dirname(osp.join(self.aslint_root, os.pardir))
        cachedir = osp.join(os.environ.get("HOME", "/"), ".cache", "devtools")
        if not os.access(cachedir, os.W_OK):
            try:
                os.makedirs(cachedir)
            except OSError:
                cachedir = osp.join("/tmp", ".cache_devtools_" + getuser())
                try:
                    os.makedirs(cachedir)
                except OSError:
                    cachedir = "/tmp"
        self.devtools_cache = cachedir
        self.docaster_cafile = osp.join(
            self.devtools_root, "share", "ca-certificates", "ac_aster_pam.crt"
        )
        # should not defined by user, just for test, not documented
        self.admin_queue_username = _read("admin.queue_username", "asterdev")
        self.admin_queue_password = _read("admin.queue_password", "asterdev")
        self.admin_queue_uri = _read(
            "admin.queue_uri",
            QUEUE_URI
            % {"username": self.admin_queue_username, "password": self.admin_queue_password},
        )
        # repository where the developers push to
        self.admin_push_uri = "codeaster_push"
        # repository where the developers push to for continuous integration
        self.admin_ci_uri = "codeaster_push"
        # repository where the developers pull from
        self.admin_pull_uri = "codeaster"
        # repository where the integrators push to and pull from
        self.admin_intg_uri = "codeaster"
        # can not be changed in ~/.gitconfig
        self.admin_queue_repoid = QUEUE_REPOID
        self.admin__test = _readbool("admin._test")
        if self.admin__test:
            self.admin_push_uri = "test"
            self.admin_ci_uri = "test"
            self.admin_pull_uri = "test"
            self.admin_intg_uri = "test"
        self.fortran_form = _read("fortran.form", "free")
        self.fortran_compiler = _read("fortran.compiler", FORTRAN_COMPILER)
        self.fortran_fcflags = _read("fortran.fcflags", "")
        assert self.fortran_form in ("free", "fixed"), self.fortran_form
        self.fortran_moddest = _read("fortran.moddest", UNCACHED)
        self.fortran_intsize = _read("fortran.intsize", UNCACHED)
        self.fortran_realsize = _read("fortran.realsize", UNCACHED)
        self.test_nbmaxnook = _read("test.nbmaxnook", 5)
        self.test_facmtps = _read("test.facmtps", 3.0)
        self.rex_url = "https://aster.retd.edf.fr/rex"
        self.rex_write_user = _read("rex.write_user")
        self.rex_write_password = UNCACHED
        self.doc_mirror = "aster-services.der.edf.fr/doc"
        self.branches = ("main", "v16")
        self.docaster_uri = _read("docaster_uri", DOCASTER_URI)
        # tools
        self.clang_format = _read("clang_format", UNCACHED)

    @staticmethod
    def _attrname(name):
        """Convert a option name to an attribute name"""
        return name.replace(".", "_")

    def get(self, name, default=None):
        """Return a value of a parameter"""
        name = self._attrname(name)
        value = getattr(self, name, default)
        if value is UNCACHED:
            if name == "asrun_lib":
                proc = Popen([self.asrun_bin, "--nodebug_stderr", "--showme", "lib"], stdout=PIPE)
                value = self.asrun_lib = convert(proc.communicate()[0]).strip()
            elif name in ("username", "notify_user"):
                hguser = self._ui.username()
                expr = re.compile("(?P<username>.+?)<(?P<notify_user>.+?)>")
                mat = expr.search(hguser)
                assert mat, "name and email must be defined in ~/.gitconfig in the section [user]"
                if name == "username":
                    self.username = mat.group("username").strip()
                if name == "notify_user":
                    add = mat.group("notify_user").split(",")
                    self.notify_user = add and add[0] or ""
                value = self.get(name, default)
            elif name == "waf_builddir":
                waf = self.get("waf.bin")
                if not osp.exists(waf):
                    waf = osp.join("..", "src", waf)
                suffix = ""
                if osp.islink(waf):
                    waf = osp.basename(os.readlink(waf))
                    suffix = re.sub("^waf_", "", waf)
                    if suffix:
                        suffix = "/" + suffix
                    if not osp.exists("build" + suffix):
                        for test in ("mpi", "std", "mpidebug"):
                            if osp.exists("build" + "/" + test):
                                suffix = "/" + test
                                break
                self.waf_builddir = "build" + suffix
                value = self.waf_builddir
            elif name == "waf_c4che":
                builddir = self.get("waf.builddir")
                wafc4che = osp.join(self.get("waf.builddir"), "c4che", "release_cache.py")
                value = {}
                if osp.isfile(wafc4che):
                    from aslint.check_files import read_waf_parameters

                    self.waf_c4che = read_waf_parameters(wafc4che)
                    value = self.waf_c4che
            elif name == "fortran_moddest":
                value = self.fortran_moddest = tempfile.mkdtemp()
            elif name in ("fortran_intsize", "fortran_realsize", "waf_prefix"):
                c4che = self.get("waf.c4che")
                if c4che is UNCACHED:
                    c4che = {}
                if name == "fortran_intsize":
                    value = self.fortran_intsize = c4che.get("ASTER_INT_SIZE", 8)
                elif name == "fortran_realsize":
                    value = self.fortran_realsize = c4che.get("ASTER_REAL8_SIZE", 8)
                elif name == "waf_prefix":
                    value = c4che.get("PREFIX", "")
            elif name == "rex_write_password":
                gitrc = osp.join(os.environ.get("HOME", ""), ".gitconfig")
                mode = os.stat(gitrc).st_mode
                assert not mode & stat.S_IRGRP and not mode & stat.S_IROTH, (
                    "Your ~/.gitconfig file must not be readable by others, "
                    "run 'chmod 600 ~/.gitconfig'"
                )
                self.rex_write_password = self._read("rex.write_password", "")
                value = self.rex_write_password
            elif name == "clang_format":
                value = "clang-format"
                if on_calibre():
                    value = "clang-format-3.5"

        return value

    def set(self, name, value):
        """Set the value of a parameter.
        Only allowed for version related parameters.

        Arguments:
            name (str): parameter access key.
            value (misc): parameter value.
        """
        name = self._attrname(name)
        if name in ("waf_builddir", "waf_c4che"):
            setattr(self, name, value)

    def check_init(self):
        """Force initialization of required parameters"""
        username = self.get("username")
        assert (
            username is not None
        ), "invalid username, check the [user] section of your ~/.gitconfig file"
        email = self.get("notify.user")
        assert (
            email is not None
        ), "invalid email address, check the [user] section of your ~/.gitconfig file"

    def clean(self, name):
        """Remove a temporary directory and reset value as uncached
        'fortran.moddest' for example."""
        name = self._attrname(name)
        directory = self.get(name)
        try:
            if type(directory) is str:
                shutil.rmtree(directory)
        except (OSError, IOError):
            pass
        setattr(self, name, UNCACHED)


class ToolsCfgSection(AsterCfgSection):
    """Variant object for devtools repository."""

    def __init__(self):
        super(ToolsCfgSection, self).__init__()
        # repository where the developers push to
        self.admin_push_uri = None
        # repository where the developers push to for continuous integration
        self.admin_ci_uri = "jenkins"
        # repository where the integrators push to and pull from
        self.admin_intg_uri = None


class SMecaCfgSection(AsterCfgSection):
    """Variant object for salome_meca repositories."""

    def __init__(self):
        super(SMecaCfgSection, self).__init__()
        self.notify_admins = None
        # repository where the developers push to
        self.admin_push_uri = "smeca_push"
        # repository where the developers push to for continuous integration
        self.admin_ci_uri = "jenkins"
        # repository where the developers pull from
        self.admin_pull_uri = "smeca"
        # repository where the integrators push to and pull from
        self.admin_intg_uri = None


# singleton instances
ASCFG = AsterCfgSection()
TOOLCFG = ToolsCfgSection()
SMCFG = SMecaCfgSection()
