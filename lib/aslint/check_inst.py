# coding=utf-8

"""Function to check the installation"""

import os
import os.path as osp
from glob import glob
import tempfile
import shutil
from subprocess import Popen, PIPE, STDOUT

from aslint.logger import logger, ErrorCode
from aslint.i18n import _


def check_install(c4che, dry_run=False):
    """Check installation"""
    legacy = not (bool(c4che.get('INCLUDES_BIBCXX') or
                  c4che.get('INCLUDES_BOOST')))
    try:
        libdir = c4che['ASTERLIBDIR']
        if legacy:
            binary = osp.join(c4che['BINDIR'], c4che['ASTERBINOPT'])
        else:
            binary = osp.join(libdir, "libaster.so")
        python = c4che['PYTHON'][0]
        prefix = c4che['PREFIX']
        datadir = c4che['ASTERDATADIR']
    except (TypeError, KeyError):
        logger.error(_("waf parameters are required"))
        return logger.errcode
    logger.title(_("checking installation in %s"), prefix)
    errcode = ErrorCode()
    expected = []
    expected.append(binary)
    expected.extend([osp.join(datadir, path)
                     for path in ('config.txt', 'profile.sh')])
    expected.append(osp.join(libdir, "code_aster"))

    for path in expected:
        if len(glob(path)) != 1:
            msg = _('not found: {0}').format(path)
            if not dry_run:
                logger.error(msg)
            else:
                logger.info(msg)
    cata = [osp.join(libdir, path)
            for path in (osp.join('code_aster', 'Cata'), 'Cata')]
    lcata = [len(glob(path)) for path in cata]
    if legacy and c4che.get('do_i18n_qtlinguist'):
        # currently Cata still exists in legacy even if code_aster/Cata exists (i18n files)
        nbdir = 2
    else:
        nbdir = 1
    if sum(lcata) != nbdir:
        msg = _('expecting: {1} {2}, not {0}').format(
            tuple(cata),
            nbdir,
            _("directory") if nbdir < 2 else _("directories")
        )
        if not dry_run:
            logger.error(msg)
        else:
            logger.info(msg)
    errcode.add(check_import_cata(python, libdir,
                                  env=osp.join(datadir, 'profile.sh'),
                                  legacy=legacy,
                                  dry_run=dry_run))
    if errcode.is_error():
        if not dry_run:
            logger.error(_("invalid installation"))
        else:
            logger.info(_("(DRY RUN) invalid installation"))
    else:
        logger.info(_("installation ok"))
    return errcode


def check_import_cata(python, libdir, env=None, legacy=False, dry_run=False):
    """Check the cata.py can be imported with only the minimal number
    of required packages"""
    errcode = ErrorCode()

    def _exec(cmd):
        """Execute an external command"""
        if not dry_run:
            cmd_env = ". {env} ; {cmd}".format(cmd=" ".join([repr(i) for i in cmd]), env=env)
            cmd = ["bash", "-c", cmd_env]
            proc = Popen(cmd, stdout=PIPE, stderr=STDOUT,
                         shell=False, universal_newlines=True)
            out = proc.communicate()[0]
            retcode = proc.returncode
            if retcode != 0:
                logger.warn("command:" + cmd_env)
                logger.warn(out)
            errcode.from_status(retcode)
        else:
            logger.info(_("(DRY RUN) %s"), cmd)
    dtmp = tempfile.mkdtemp()
    prev = os.getcwd()
    os.chdir(dtmp)
    try:
        if legacy and not dry_run:
            # only needed for legacy versions
            with open("aster.py", "w") as fobj:
                fobj.write(os.linesep.join([
                    "class error(Exception):",
                    "   pass",
                    "class FatalError(Exception):",
                    "   pass",
                    "def affiche(unit, txt):",
                    "   print(txt)",
                    "def onFatalError():",
                    "   return 'ABORT'",
                ]))
            with open("_aster_core.py", "w") as fobj:
                symbols = ("matfpe", "get_mem_stat", "set_mem_stat",
                           "MPI_CommRankSize", "MPI_Warn", "MPI_Barrier",
                           "MPI_Bcast", "MPI_GatherStr", "_USE_MPI",
                           "_USE_OPENMP", "_USE_64_BITS", "_POSIX", "_NO_EXPIR",
                           "ASTER_INT_SIZE")
                lines = [symb + " = 1" for symb in symbols]
                fobj.write(os.linesep.join(lines))

        _exec([python, '-c', "import code_aster.Cata"])
        _exec([python, '-c', "from code_aster.Cata import Language"])
        _exec([python, '-c', "from code_aster.Cata import Commands"])
    finally:
        os.chdir(prev)
        shutil.rmtree(dtmp)
    return errcode
