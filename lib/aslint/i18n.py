# coding=utf-8

"""Internationalization support for aslint (and for the hgaster extension too).

Translation is enabled by default (if the related .mo file is found).
To disable it, add in your hg configuration (~/.gitconfig ou .git/config):
[aster]
translation = false
"""

import os
import os.path as osp
import gettext

from aslint.baseutils import get_absolute_dirname, get_encoding
from aslint.config import ASCFG

TRANSLATE = (ASCFG.get('translation') or
             os.getenv('ASLINT_WITHIN_TESTS', '0') == '1')

_locdir = osp.normpath(osp.join(get_absolute_dirname(__file__),
                       os.pardir, os.pardir, 'share', 'locale'))
t = gettext.translation('aslint', localedir=_locdir, fallback=True)

encoding = get_encoding()

# very closed to mercurial/i18n.py


def func_(message):
    """Translate message.

    The message is looked up in the catalog to get a Unicode string,
    which is encoded in the local encoding before being returned.

    Important: message is restricted to characters in the encoding
    given by sys.getdefaultencoding() which is most likely 'ascii'.
    """
    # If message is None, t.ugettext will return u'None' as the
    # translation whereas our callers expect us to return None.
    if message is None:
        return message

    paragraphs = message.split('\n\n')
    # Be careful not to translate the empty string -- it holds the
    # meta data of the .po file.
    return '\n\n'.join([p and t.gettext(p) or '' for p in paragraphs])

if TRANSLATE:
    _ = func_
else:
    _ = lambda message: message
