# coding=utf-8

"""Utilities to access to some external tools."""


import os
from subprocess import PIPE, Popen

from aslint.config import ASCFG
from aslint.decorators import interrupt_decorator
from aslint.logger import logger
from aslint.string_utils import convert


@interrupt_decorator
def clang_format(fname, backup_ext=None):
    """Reformat a file using `clang-format`.

    Arguments:
        filename (str): Path to the source file.
        backup_ext (str, optional): Extension used to save the original file.
    """
    logger.info(fname)
    try:
        cmd = [ASCFG.get('clang_format'), '-style=file', fname]
        logger.debug("Running {0}".format(cmd))
        out = convert(Popen(cmd, stdout=PIPE).communicate()[0])
        if backup_ext:
            os.rename(fname, fname + backup_ext)
        with open(fname, "w") as fsrc:
            fsrc.write(out)
    except Exception:
        print('error reading', fname)
        raise
