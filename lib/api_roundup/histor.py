# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

"""
Define classes representing an histor file from issues objects.
"""


import os
import re
from xml.sax import saxutils

from aslint.i18n import _
from aslint.utils import indent
from aslint.string_utils import convert
from .utils import format_date, get_value, get_realname


NumberTypes = (float, int)
EnumTypes = (list, tuple)

french_date_fmt = "%d/%m/%Y"


class HistorError(Exception):
    """Exception for this module"""


class HISTOR(object):
    """Class representing a "standard" histor i.e. as a text file."""

    CR = os.linesep
    format = "text"
    header = ""
    footer = ""
    separ = "=" * 80
    seprep = CR * 2
    prefix = "  | "
    mask_summary = "{id:>6s} {Sassignedto:<24s} {Stitle}" + CR
    mask = (
        CR
        + """
{separ}
                RESTITUTION FICHE {id} DU {creation}
{separ}
- AUTEUR : {assignedto}
- TYPE : {type} concernant {produit} (VERSION {version})
- TITRE : {title}
- FONCTIONNALITE :
{messages}

- RESU_FAUX_VERSION_EXPLOITATION : {fauxVexpl}
- RESU_FAUX_VERSION_DEVELOPPEMENT : {fauxVdev}
- RESTITUTION_VERSION_EXPLOITATION : {corrVexpl}
- RESTITUTION_VERSION_DEVELOPPEMENT : {corrVdev}
- IMPACT_DOCUMENTAIRE : {impactDoc}
- VALIDATION : {validation}
{fix_in}{nbJours}"""
    )
    mask_nbjours = "- NB_JOURS_TRAV  : {nbJours}" + CR
    mask_tma = "- TMA : {tma}" + CR
    mask_restit = "- DEJA RESTITUE DANS : {versCorr}" + CR
    mask_user_begin = separ + CR + "--- AUTEUR {realname:<20s}" + CR * 2
    mask_user_end = separ + CR

    def __init__(self, server, **kargs):
        """Initialize histor object."""
        self._serv = server
        self.l_summ = []
        self.l_bloc = []

    def clean_dict(self, dico):
        """Keep only elements of dict of type string and number,
        and escape all HTML characters."""
        newd = {}
        for k, v in list(dico.items()):
            if k == "CR" or type(v) in NumberTypes:
                newd[k] = v
            elif type(v) is str:
                newd[k] = convert(v)
            elif v is None:
                newd[k] = ""
        return newd

    def DictIssue(self, issueid, l_rep=None):
        """Represent an issue as a dict object.
        l_rep : list of the content of messages to include."""
        # 0. store issue infos
        issue = "issue" + str(issueid)
        dico = self._serv.display(
            issue,
            "id",
            "creation",
            "assignedto",
            "type",
            "produit",
            "version",
            "title",
            "messages",
            "fauxVexpl",
            "corrVdev",
            "impactDoc",
            "validation",
            "nbJours",
            "corrVexpl",
            "corrVdev",
            "intervenant",
            "verCorrVexpl",
            "verCorrVdev",
            "fauxVdev",
        )

        dico["separ"] = self.separ
        if dico["creation"] is not None:
            dico["creation"] = format_date(dico["creation"])
        if dico["assignedto"] is not None:
            userid = dico["assignedto"]
            dico["assignedto"] = get_realname(self._serv, userid)
        if dico["type"] is not None:
            dico["type"] = get_value(self._serv, "type", dico["type"], "name")
        if dico["produit"] is not None:
            dico["produit"] = get_value(self._serv, "produit", dico["produit"], "name")
        if dico["version"] is not None:
            dico["version"] = get_value(self._serv, "version", dico["version"], "name")

        # 1a. list of messages or last message
        if l_rep is not None and len(l_rep) > 0:
            if not type(l_rep) in EnumTypes:
                l_rep = [l_rep]
            l_rep = [indent(convert(s), self.prefix) for s in l_rep]
            dico["messages"] = self.seprep.join(l_rep)
        else:
            d_msg = dico["messages"]
            msg_id = max(d_msg)
            msgid = "msg" + msg_id
            msg = self._serv.display(msgid)
            dico["messages"] = msg["summary"] + "..."

        # 1b. text fields
        for k in ("title", "validation"):
            dico[k] = convert(dico[k])

        # 2. champs OUI/NON
        for key in ("corrVdev", "corrVexpl"):
            if dico[key] not in (None, 0):
                dico[key] = "OUI"
            else:
                dico[key] = "NON"
        for key in ("fauxVdev", "fauxVexpl"):
            if dico[key] not in (None, 0):
                dico[key] = "OUI   DEPUIS : {0}".format(dico[key])
            else:
                dico[key] = "NON"

        # 3. "pour le compte de" field
        if dico["intervenant"] is not None:
            tma_name = get_value(self._serv, "intervenant", dico["intervenant"], "name")
            dico["tma"] = self.mask_tma.format(tma=tma_name)
        else:
            dico["tma"] = None

        # 4. hide 'nbJours' field if there is an 'intervenant'
        if dico["intervenant"] is None:
            dico["nbJours"] = self.mask_nbjours.format(nbJours=dico["nbJours"])
        else:
            dico["nbJours"] = ""

        # 5. fixed in...
        val = ""
        if dico["verCorrVexpl"] is not None:
            val += dico["verCorrVexpl"]
        if dico["verCorrVdev"] is not None:
            if val:
                val += ", "
            val += dico["verCorrVdev"]
        if val:
            dico["fix_in"] = self.mask_restit.format(versCorr=val)
        else:
            dico["fix_in"] = ""

        # 6. keep only strings
        dico = self.clean_dict(dico)

        return dico

    def summary_line(self, issue_dict):
        """Create a summary line for the issue."""
        infos = dict(issue_dict)
        infos["Stitle"] = infos["title"]
        if len(infos["Stitle"]) > 80:
            infos["Stitle"] = infos["Stitle"][: 80 - 3] + "..."
        infos["Sassignedto"] = infos["assignedto"] or "?"
        if len(infos["Sassignedto"]) > 24:
            infos["Sassignedto"] = infos["Sassignedto"][:24]
        return self.mask_summary.format(**infos)

    def AddIssue(self, issue, l_rep=None):
        """Add a bloc for an issue."""
        issue_dict = self.DictIssue(issue, l_rep)
        self.l_summ.append(self.summary_line(issue_dict))
        self.l_bloc.append(self.mask.format(**issue_dict))

    def DictUser(self, user):
        """Represent an user as a dict object."""
        # required fields for formatting
        dico = self._serv.display(user, "loginaster", "realname")
        dico["CR"] = self.CR
        return dico

    def BeginUser(self, user):
        """Start a bloc to identify the user who answers following issues."""
        self.l_bloc.append(self.mask_user_begin.format(**self.DictUser(user)))

    def EndUser(self, user):
        """End the bloc."""
        self.l_bloc.append(self.mask_user_end.format(**self.DictUser(user)))

    def __repr__(self):
        """Modify representation."""
        return self._getrepr()

    def _getrepr(self):
        """Representation of the histor object."""
        txt = [self.header]
        if self.l_summ:
            sep_line = self.summary_line({"id": "-" * 6, "assignedto": "-" * 24, "title": "-" * 80})
            txt.append(sep_line)
            txt.append(self.summary_line(dict(id="FICHE ", assignedto="AUTEUR", title="TITRE")))
            txt.append(sep_line)
            txt.extend(self.l_summ)
            txt.append(sep_line)
        txt.extend(self.l_bloc)
        txt.append(self.footer)
        return "".join([convert(s) for s in txt])


class HISTOR_HTML(HISTOR):
    """Class representing a histor in HTML format."""

    CR = "<br />"
    format = "html"
    header = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>histor - code_aster</title>
</head>
    <style type="text/css">
        body {
            font-family: monospace;
            text-align: justify;
        }
        .fiche {
            font-style: normal;
            line-height: 1.5em ;
        }
        .par {
            margin-left: 50px ;
            line-height: 1.3em ;
        }
        .enteteuser {
            font-weight: bold;
            text-align: center;
        }
    </style>
<body>
"""
    footer = """</body>
</html>
"""
    separ = "<hr />"
    prefix = "    "
    mask = (
        separ
        + """

<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
<div class="fiche">
RESTITUTION FICHE <a href="{url_issue}">{id}</a> DU {creation}{CR}
AUTEUR : {assignedto}{CR}
TYPE {type}{CR}
PRODUIT {produit} (VERSION {version}){CR}
{tma}{CR}TITRE
<div class="par">
    {title}
</div>
FONCTIONNALITE
<div class="par">
    {messages}
</div>
RESU_FAUX_VERSION_EXPLOITATION&nbsp;:  {fauxVexpl}{CR}
RESU_FAUX_VERSION_DEVELOPPEMENT&nbsp;:  {fauxVdev}{CR}
RESTITUTION_VERSION_EXPLOITATION&nbsp;:  {corrVexpl}{CR}
RESTITUTION_VERSION_DEVELOPPEMENT&nbsp;:  {corrVdev}{CR}
IMPACT_DOCUMENTAIRE : {impactDoc}{CR}
VALIDATION
<div class="par">
    {validation}
</div>
{fix_in}{CR}{nbJours}{CR}</div>
"""
    )
    mask_user_begin = """
<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
{CR}
<p class="enteteuser">
    AUTEUR {loginaster} {realname} &nbsp;&nbsp;&nbsp;&nbsp;
</p>
"""
    mask_nbjours = "NB_JOURS_TRAV  : {nbJours}"
    mask_tma = "TMA : {tma}"
    mask_restit = "DEJA RESTITUE DANS : {versCorr}"
    mask_user_end = separ + CR

    def __init__(self, server, **kargs):
        """Initialize histor object."""
        super(HISTOR_HTML, self).__init__(server, **kargs)
        # specific arguments
        self.server_url = kargs.get("url", None)
        self.fmt_url_issue = "{0}/issue{{issue}}".format(saxutils.escape(self.server_url))

    def clean_dict(self, dico):
        """Keep only elements of dict of type string and number,
        and escape all HTML characters."""
        newd = {}
        for k, v in list(dico.items()):
            if k == "CR" or type(v) in NumberTypes:
                newd[k] = v
            elif type(v) is str:
                newd[k] = saxutils.escape(convert(v))
            elif v is None:
                newd[k] = ""
            if k == "messages":
                newd[k] = self.format_messages(newd[k])
        return newd

    def DictIssue(self, issue, l_rep=None):
        """Represent an issue as a dict object."""
        dico = HISTOR.DictIssue(self, issue, l_rep)

        # specific fields
        dico["url_issue"] = self.fmt_url_issue.format(issue=issue)

        # multi-lines fields
        for k in ("messages",):
            dico[k] = dico[k].replace(os.linesep, self.CR)

        dico["CR"] = self.CR

        return dico

    def format_messages(self, orig):
        """Format magic fields (example 'issue1234' as hyperlink)"""
        magic = re.compile("(?P<class>issue) *(?P<id>[0-9]{2}[0-9]+)", re.M | re.I)
        text = magic.sub(
            r'<a href="{0}/\g<class>\g<id>">\g<class>\g<id></a>'.format(
                saxutils.escape(self.server_url)
            ),
            orig,
        )
        return text

    def AddIssue(self, issue, l_rep=None):
        """Add a bloc for an issue."""
        issue_dict = self.DictIssue(issue, l_rep)
        self.l_bloc.append(self.mask.format(**issue_dict))


class HISTOR_FSQ(HISTOR):
    """Class representing issues for quality purpose."""

    CR = os.linesep
    format = "fsq"
    separ = CR
    prefix = "    "
    mask = (
        separ
        + """Fiche {id} : {title}

{messages}

Risque de résultats faux depuis la version : {fauxVexpl} {fauxVdev}, corrigé en {corrVexpl} {corrVdev}

"""
    )
    mask_user_begin = ""
    mask_user_end = ""

    def DictIssue(self, issue, l_rep=None):
        """Represent an issue as a dict object."""
        dico = HISTOR.DictIssue(self, issue, l_rep)

        # clean response
        for k in ("messages",):
            dico[k] = re.sub(" +", " ", dico[k])
            dico[k] = dico[k].strip()

        return dico

    def AddIssue(self, issue, l_rep=None):
        """Add a bloc for an issue."""
        issue_dict = self.DictIssue(issue, l_rep)
        self.l_bloc.append(self.mask.format(**issue_dict))


def InitHistor(server, format="text", **kargs):
    """Return a histor object according to `format`.
    server: ServerProxy object to connect the XMLRPC server
    """
    if format == "text":
        return HISTOR(server, **kargs)
    elif format == "html":
        return HISTOR_HTML(server, **kargs)
    elif format == "fsq":
        return HISTOR_FSQ(server, **kargs)
    else:
        raise HistorError(_("Unknown histor format : {0}").format(format))
