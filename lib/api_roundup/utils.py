# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2024 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

"""
Convenient functions to manipulate values returned by the Roundup XMLRPC server.
"""

import re
from datetime import datetime

from aslint.i18n import _
from aslint.utils import convert


def get_value(server, klass, idx, field):
    """Return the value of a field in a REX entity.

    Arguments:
        server (*ServerProxy*): Server to connect.
        klass (str): Class of the requested object.
        idx (int): Identifier of the object.
        field (str): Field to return.

    Returns:
        *misc*: Value of the field in the identified object.
    """
    return server.display(klass + str(idx), field)[field]


def get_username(server, idx):
    """Return the username.

    Arguments:
        server (*ServerProxy*): Server to connect.
        idx (int): Identifier of the user.

    Returns:
        str: Username with the given id.
    """
    return get_value(server, "user", idx, "username")


def get_realname(server, idx):
    """Return the realname of a user.

    Arguments:
        server (*ServerProxy*): Server to connect.
        idx (int): Identifier of the user.

    Returns:
        str: Realname of the user with the given id.
    """
    return get_value(server, "user", idx, "realname")


def get_productname(server, idx):
    """Return the name of a product.

    Arguments:
        server (*ServerProxy*): Server to connect.
        idx (int): Identifier of the product.

    Returns:
        str: Name of the product.
    """
    return get_value(server, "produit", idx, "name")


def format_date(date):
    """Return the date in a common format"""
    return datetime.strptime(date, "<Date %Y-%m-%d.%H:%M:%S.%f>").strftime("%d/%m/%Y")


def read_issue_list(fname):
    """Read the issues numbers from 'fname'
    One number at each beginning of lines."""
    with open(fname, "r") as fobj:
        content = fobj.read()
    expr = re.compile("^ *([0-9]{4}[0-9]?)", re.MULTILINE)
    issues = [int(i) for i in expr.findall(content)]
    return issues


def read_issue_changelog(fname):
    """Read the issues numbers from the changelog 'fname'"""
    with open(fname, "rb") as fobj:
        hist_content = convert(fobj.read())
    expr = re.compile("RESTITUTION FICHE +([0-9]+)", re.MULTILINE)
    issues = [int(i) for i in expr.findall(hist_content)]
    return issues
