# coding=utf-8

"""
This package gives the functions to interact with Docaster.
"""

# https://gitlab.pleiade.edf.fr/api/v4/projects/7731/repository/commits
#   ?path=source/manuals/man_u/u4/u4.41.01

import json
import os
import re
import ssl
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.request import Request, urlopen

from aslint.config import ASCFG
from aslint.logger import logger
from aslint.string_utils import convert

RE_KEY = re.compile(
    "(?P<key>(?P<chap>(?P<man>s?[arduvm]){1}[0-9]+)" r"\.[0-9a-zA-Z]+\.[0-9]+)$", re.I
)
RE_TXT = re.compile("<.*?>", re.M)


def changelog_doc(key, folder=None, limit=50):
    """Return the changelogs for a document.

    Arguments:
        key (str): *keyword* of the document (in Docaster terminology, for
            example "u4.32.04").
        folder (str): if specified, only request the given folder (one of: manuals
            or intranet).
        limit (int): Maximum number of changesets read (limited to 100 by API).

    Returns:
        list[str]: Descriptions of the changesets.
    """
    assert os.environ.get("GITLAB_TOKEN"), "GITLAB_TOKEN with read permission is required"
    project_id = {"manuals": 7731, "intranet": 7732}
    folders = [folder] if folder else ("manuals", "intranet")
    keyw = RE_KEY.search(key.lower())
    args = keyw.groupdict()

    for folder in folders:
        project = project_id[folder]
        branches = [br["name"] for br in api_request(project, query=f"branches?per_page={limit}")]
        args["folder"] = f"source/{folder}/" if folder == "manuals" else ""
        values = {"path": "{folder}man_{man[0]}/{chap}/{key}".format(**args)}
        descr = []
        for branch in branches:
            values["ref_name"] = branch
            query = "commits?" + urlencode(values)
            results = api_request(project, query)
            descr.extend([commit["title"] for commit in results])
        if descr:
            break
    return descr


def api_request(project, query):
    """Request GitLab API Resources.

    Arguments:
        url (str): Url of the project API.
        query (str): Query with its encoded args.
    """
    sctxt = ssl.create_default_context()
    # sctxt.load_verify_locations(ASCFG.get("docaster_cafile"))
    sctxt.check_hostname = False
    # do not check certificate - only used on private network
    sctxt.verify_mode = ssl.CERT_NONE

    url = ASCFG.get("docaster_uri").format(project=project) + "/" + query
    logger.debug(f"url: {url}")
    req = Request(url, data=None)
    req.add_header("PRIVATE-TOKEN", os.environ["GITLAB_TOKEN"])
    try:
        resp = urlopen(req, context=sctxt)
    except HTTPError as exc:
        raise ValueError(str(exc))
    content = convert(resp.read())
    results = json.loads(content)
    logger.debug(f"{len(results)} records returned")
    return results
