#!/usr/bin/env python3
# coding=utf-8

"""
Décodage d'un fichier obtenu avec IMPR_JEVEUX(ENTITE="MEMOIRE", IMPRESSION=_F(UNITE=nn))
"""

import argparse
import re


class Obj:
    name = size = attr = None

    def __init__(self, name, size, attr=""):
        self.name = name
        self.size = size
        self.attr = attr

    def copy(self):
        return Obj(self.name, self.size, self.attr)

    def __repr__(self):
        return f"{self.name:32s} {self.size:16d}"


class Mem:
    filename = data = None

    def __init__(self, filename=None):
        self.filename = filename
        self.data = {}
        if filename:
            self.decode()

    @staticmethod
    def read(filename):
        """Factory that returns the relevant object to read the given file."""
        with open(filename) as fobj:
            memory = "OBJETS ALLOUES DYNAMIQUEMENT" in fobj.read()
        klas = Mem
        if not memory:
            klas = MemDisque
        return klas(filename)

    def decode(self):
        """Decode the content of jeimpm output (ENTITE="MEMOIRE")."""
        re0 = re.compile("^\|[GV]\|", re.M)
        with open(self.filename) as fobj:
            lines = [line.strip() for line in fobj.readlines() if re0.search(line)]

        for line in lines:
            if not line.strip():
                continue
            spl = line.split("|")
            assert len(spl) == 10, spl
            obj = Obj(spl[9].strip(), int(spl[7]), spl[8].strip())
            self.data[obj.name] = obj

    def __len__(self):
        return len(self.data)

    def get_names(self):
        """Return the list of objects names."""
        return sorted(self.data.keys())

    def sizeof(self, names=None):
        """Return the size of the objects passed or all."""
        names = names or self.get_names()
        return sum([self.data[name].size for name in names])

    def sizeofMB(self, names=None):
        """Return the size of the objects passed or all in MB."""
        return self.sizeof(names) * 8 / 1024 / 1024

    def extend(self, other):
        """Extend by union with another."""
        for name, obj in other.data.items():
            obj = obj.copy()
            if name in self.data:
                current = self.data[name]
                if "__GLOBALE" not in name and current.size != obj.size and "A" not in current.attr:
                    # no yet rewritten on disk
                    print(f" mem & disque: {name}: {current.attr}: {current.size} vs {obj.size}")
                obj.size = max(current.size, obj.size)
                obj.attr = current.attr or obj.attr
            self.data[name] = obj

    def difference(self, other):
        """Return the difference with another.

        (i.e. all objects that are in this *Mem* but not in the other.)
        """
        diff = Mem()
        new = set(self.get_names()).difference(other.get_names())
        for name in new:
            diff.data[name] = self.data[name]
        return diff

    def changed(self, other):
        """Return the change of size of the objects common with another."""
        diff = Mem()
        common = set(self.get_names()).intersection(other.get_names())
        for name in common:
            diff.data[name] = self.data[name].copy()
            diff.data[name].size -= other.data[name].size
        return diff

    def root_names(self):
        """Return the root objects names."""
        return sorted(set([self._root(name) for name in self.get_names()]))

    @staticmethod
    def _root(name):
        return name.split(".")[0].strip()

    def group(self):
        """Group objects by root name."""
        grp = Mem()
        data = grp.data
        for obj in self.data.values():
            name = self._root(obj.name)
            data.setdefault(name, Obj(name, 0, obj.attr))
            data[name].size += obj.size
        return grp

    def __repr__(self):
        nbroot = len(self.root_names())
        return (
            f"{nbroot:4d} objets ({len(self)} au total) "
            f"pour {self.sizeof() * 8 / 1024 / 1024:.3f} Mo"
        )

    def show(self, sortby="name", groupby=None):
        """Print content"""
        assert sortby in ("name", "size")
        assert groupby in (None, "root")

        def fkey(item):
            return item[0] if sortby == "name" else item[1].size

        items = sorted(list(self.data.items()), key=fkey)
        for _, obj in items:
            print(obj)


# 0         1         2         3         4         5         6         7         8
# 01234567890123456789012345678901234567890123456789012345678901234567890123456789
#     1123 00000033.NUME000000.DELG          -V-I-  8 179070   1128        0     1


class MemDisque(Mem):

    def decode(self):
        """Decode the content of jeimpm output (ENTITE="DISQUE")."""
        re0 = re.compile("^ +[0-9]+", re.M)
        with open(self.filename) as fobj:
            lines = [line for line in fobj.readlines() if re0.search(line)]

        for line in lines:
            assert line[43] == line[45] == line[47] == "-", line[43:47]
            name = line[9:43].strip()
            size = line[51:58].strip()
            obj = Obj(name, int(size))
            self.data[obj.name] = obj


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", nargs="*")
    args = parser.parse_args()
    mem = [Mem.read(fn) for fn in args.filename]
