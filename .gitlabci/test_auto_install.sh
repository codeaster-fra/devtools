#!/bin/bash

export GIT_SSL_NO_VERIFY=true

# otherwise install_env detects it is running in devtools
mv .git .__git__

DEST=$(pwd)/dev-codeaster
printf "John CI\nci@runner.com\njc\n" | \
    ./bin/install_env --clone --reset --branch=main --no-build ${DEST}

is_dir() {
    printf "checking directory ${1}: "
    if [ -d ${1} ]; then
        echo "exists"
    else
        echo "no found"
        iret=$(( $iret + 1 ))
    fi
}

is_file() {
    printf "checking file ${1}: "
    if [ -f ${1} ]; then
        echo "exists"
    else
        echo "no found"
        iret=$(( $iret + 1 ))
    fi
}

iret=0

is_dir ${DEST}
is_dir ${DEST}/devtools
is_dir ${DEST}/src

is_file ${HOME}/.gitconfig

exit ${iret}
