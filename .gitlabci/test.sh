#!/bin/bash

jobs=${NPROC_MAX}
args=( "-V" "--jobs=${jobs}" "$@" )

# unittests needs a complete clone
git fetch --unshallow

echo "+ downloading src..."
SRC_URL=${ROOT_URL}/src.git
git clone ${SRC_URL} src
# creates main local branches
(cd src ; git checkout v16 ; git checkout main)

# path to the src clone
export CODEASTER_SRC=$(pwd)/src
# configure src
(
    cd $CODEASTER_SRC ;
    git tag -d stable || true  # waiting fix in '_gettag()'
    ./configure
)

cd share/test
. env_test.sh

printf "\nrunning testcases #1... - $(date)\n"
ctest "${args[@]}"
iret=$?

if [ ${iret} -ne 0 ]; then
    printf "\nrunning testcases #2 (rerun-failed)... - $(date)\n"
    ctest "${args[@]}" --rerun-failed
    iret=$?
fi

exit ${iret}
